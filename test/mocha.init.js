const chai = require('chai');

// https://github.com/domenic/chai-as-promised
const chaiAsPromised = require('chai-as-promised');

// https://github.com/domenic/sinon-chai
const sinonChai = require('sinon-chai');

const chaiSubset = require('chai-subset');

const chaiDateTime = require('chai-datetime');

chai.should();
// @NOTE: Need to include subset before promised as it ports all known assertions / expectations at point of inclusion.
chai.use(chaiSubset);
chai.use(sinonChai);
chai.use(chaiDateTime);
chai.use(chaiAsPromised);

// Make sure to expose expect as a global.
global.expect = chai.expect;
