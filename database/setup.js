const { exec } = require("child_process");

module.exports = async function() {
    return new Promise((resolve) => {
        const child = exec("docker exec -i database mysql -u fvd -pFVD123 fvd_test < database/init.sql");
        child.stdout.on('data', function(data) {
            console.log('stdout: ' + data);
        });
        child.stderr.on('data', function(data) {
            console.log('stderr: ' + data);
        });
        child.on('close', function(code) {
            console.log(code);
            resolve();
        });
    });
};
