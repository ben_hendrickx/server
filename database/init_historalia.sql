-- MySQL dump 10.13  Distrib 8.0.16, for osx10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: historalia
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_role_rights`
--

DROP TABLE IF EXISTS `access_role_rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `access_role_rights` (
  `accessRoleId` smallint unsigned NOT NULL,
  `rightId` smallint unsigned NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `create` tinyint(1) NOT NULL DEFAULT '0',
  `update` tinyint(1) NOT NULL DEFAULT '0',
  `delete` tinyint(1) NOT NULL DEFAULT '0',
  KEY `accessRoleId` (`accessRoleId`),
  KEY `rightId` (`rightId`),
  CONSTRAINT `access_role_rights_ibfk_1` FOREIGN KEY (`accessRoleId`) REFERENCES `access_roles` (`id`),
  CONSTRAINT `access_role_rights_ibfk_2` FOREIGN KEY (`rightId`) REFERENCES `rights` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_roles`
--

LOCK TABLES `access_role_rights` WRITE;
/*!40000 ALTER TABLE `access_role_rights` DISABLE KEYS */;
INSERT INTO `access_role_rights` VALUES (1,20,1,1,1,1);
/*!40000 ALTER TABLE `access_role_rights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `access_role_sub_rights`
--

DROP TABLE IF EXISTS `access_role_sub_rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `access_role_sub_rights` (
  `accessRoleId` smallint unsigned NOT NULL,
  `subRightId` smallint unsigned NOT NULL,
  `access` tinyint(1) NOT NULL DEFAULT '0',
  KEY `accessRoleId` (`accessRoleId`),
  KEY `subRightId` (`subRightId`),
  CONSTRAINT `access_role_sub_rights_ibfk_1` FOREIGN KEY (`accessRoleId`) REFERENCES `access_roles` (`id`),
  CONSTRAINT `access_role_sub_rights_ibfk_2` FOREIGN KEY (`subRightId`) REFERENCES `sub_rights` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `access_roles`
--

DROP TABLE IF EXISTS `access_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `access_roles` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_roles`
--

LOCK TABLES `access_roles` WRITE;
/*!40000 ALTER TABLE `access_roles` DISABLE KEYS */;
INSERT INTO `access_roles` VALUES (1,'Administrator', 'Beheerder van de applicatie');
/*!40000 ALTER TABLE `access_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_settings`
--

DROP TABLE IF EXISTS `app_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `app_settings` (
  `key` varchar(255) DEFAULT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `article_clothing_sizes`
--

DROP TABLE IF EXISTS `article_clothing_sizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `article_clothing_sizes` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `size` varchar(25) NOT NULL,
  `deletedDt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `articles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `stock` smallint unsigned NOT NULL DEFAULT '1',
  `clothingSizeId` smallint unsigned DEFAULT NULL,
  `categoryId` int unsigned NOT NULL,
  `patternId` int unsigned DEFAULT NULL,
  `fabricId` int unsigned DEFAULT NULL,
  `colorId` int unsigned DEFAULT NULL,
  `rackId` int unsigned DEFAULT NULL,
  `status` smallint unsigned DEFAULT NULL,
  `tags` text DEFAULT NULL,
  `washing` smallint unsigned DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clothingSizeId` (`clothingSizeId`),
  KEY `categoryId` (`categoryId`),
  KEY `patternId` (`patternId`),
  KEY `fabricId` (`fabricId`),
  KEY `colorId` (`colorId`),
  CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`clothingSizeId`) REFERENCES `clothing_sizes` (`id`),
  CONSTRAINT `articles_ibfk_2` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`id`),
  CONSTRAINT `articles_ibfk_3` FOREIGN KEY (`patternId`) REFERENCES `patterns` (`id`),
  CONSTRAINT `articles_ibfk_4` FOREIGN KEY (`fabricId`) REFERENCES `fabrics` (`id`),
  CONSTRAINT `articles_ibfk_5` FOREIGN KEY (`colorId`) REFERENCES `colors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `casts`
--

DROP TABLE IF EXISTS `casts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `casts` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `categories` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `category_prices`
--

DROP TABLE IF EXISTS `category_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `category_prices` (
  `categoryId` int unsigned NOT NULL AUTO_INCREMENT,
  `internalPrice` float NOT NULL,
  `externalPrice` float NOT NULL,
  `replacementPrice` float NOT NULL,
  `startDt` datetime DEFAULT (now()),
  `endDt` datetime DEFAULT NULL,
  KEY `categoryId` (`categoryId`),
  CONSTRAINT `category_prices_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clothing_sizes`
--

DROP TABLE IF EXISTS `clothing_sizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `clothing_sizes` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `size` smallint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clothing_sizes`
--

LOCK TABLES `clothing_sizes` WRITE;
/*!40000 ALTER TABLE `clothing_sizes` DISABLE KEYS */;
INSERT INTO `clothing_sizes` VALUES (1,34),(2,36),(3,38),(4,40),(5,42),(6,44),(7,46),(8,48),(9,50),(10,52),(11,54),(12,56),(13,58),(14,60),(15,62),(16,64),(17,66),(18,122),(19,128),(20,134),(21,140),(22,146),(23,152),(24,158),(25,164),(26,170),(27,176);
/*!40000 ALTER TABLE `clothing_sizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `colors` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `deletedDt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `countries` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Afghanistan'),(2,'Ålandeilanden'),(3,'Albanië'),(4,'Algerije'),(5,'Amerikaans Samoa'),(6,'Andorra'),(7,'Angola'),(8,'Anguilla'),(9,'Antarctica'),(10,'Antigua en Barbuda'),(11,'Argentinië'),(12,'Armenië'),(13,'Aruba'),(14,'Australië'),(15,'Oostenrijk'),(16,'Azerbeidzjan'),(17,'Bahama’s'),(18,'Bahrein'),(19,'Bangladesh'),(20,'Barbados'),(21,'Wit-Rusland'),(22,'België'),(23,'Belize'),(24,'Benin'),(25,'Bermuda'),(26,'Bhutan'),(27,'Bolivia'),(28,'Bonaire, Sint Eustatius and Saba'),(29,'Bosnië en Herzegovina'),(30,'Botswana'),(31,'Bouveteiland'),(32,'Brazilië'),(33,'Britse Gebieden in de Indische Oceaan'),(34,'Kleine afgelegen eilanden van de Verenigde Staten'),(35,'Britse Maagdeneilanden'),(36,'Verenigde Staten Maagdeneilanden'),(37,'Brunei'),(38,'Bulgarije'),(39,'Burkina Faso'),(40,'Burundi'),(41,'Cambodja'),(42,'Kameroen'),(43,'Canada'),(44,'Kaapverdië'),(45,'Caymaneilanden'),(46,'Centraal-Afrikaanse Republiek'),(47,'Tsjaad'),(48,'Chili'),(49,'China'),(50,'Christmaseiland'),(51,'Cocoseilanden'),(52,'Colombia'),(53,'Comoren'),(54,'Congo [Republiek]'),(55,'Congo [DRC]'),(56,'Cookeilanden'),(57,'Costa Rica'),(58,'Kroatië'),(59,'Cuba'),(60,'Curaçao'),(61,'Cyprus'),(62,'Tsjechië'),(63,'Denemarken'),(64,'Djibouti'),(65,'Dominica'),(66,'Dominicaanse Republiek'),(67,'Ecuador'),(68,'Egypte'),(69,'El Salvador'),(70,'Equatoriaal-Guinea'),(71,'Eritrea'),(72,'Estland'),(73,'Ethiopië'),(74,'Falklandeilanden [Islas Malvinas]'),(75,'Faeröer'),(76,'Fiji'),(77,'Finland'),(78,'Frankrijk'),(79,'Frans-Guyana'),(80,'Frans-Polynesië'),(81,'Franse Gebieden in de zuidelijke Indische Oceaan'),(82,'Gabon'),(83,'Gambia'),(84,'Georgië'),(85,'Duitsland'),(86,'Ghana'),(87,'Gibraltar'),(88,'Griekenland'),(89,'Groenland'),(90,'Grenada'),(91,'Guadeloupe'),(92,'Guam'),(93,'Guatemala'),(94,'Guernsey'),(95,'Guinee'),(96,'Guinee-Bissau'),(97,'Guyana'),(98,'Haïti'),(99,'Heard- en McDonaldeilanden'),(100,'Heilige Stoel'),(101,'Honduras'),(102,'Hongkong'),(103,'Hongarije'),(104,'IJsland'),(105,'India'),(106,'Indonesië'),(107,'Ivoorkust'),(108,'Iran'),(109,'Irak'),(110,'Ierland'),(111,'Isle of Man'),(112,'Israël'),(113,'Italië'),(114,'Jamaica'),(115,'Japan'),(116,'Jersey'),(117,'Jordanië'),(118,'Kazachstan'),(119,'Kenia'),(120,'Kiribati'),(121,'Koeweit'),(122,'Kirgizië'),(123,'Laos'),(124,'Letland'),(125,'Libanon'),(126,'Lesotho'),(127,'Liberia'),(128,'Libië'),(129,'Liechtenstein'),(130,'Litouwen'),(131,'Luxemburg'),(132,'Macao'),(133,'Macedonië'),(134,'Madagaskar'),(135,'Malawi'),(136,'Maleisië'),(137,'Maldiven'),(138,'Mali'),(139,'Malta'),(140,'Marshalleilanden'),(141,'Martinique'),(142,'Mauritanië'),(143,'Mauritius'),(144,'Mayotte'),(145,'Mexico'),(146,'Micronesië'),(147,'Moldavië'),(148,'Monaco'),(149,'Mongolië'),(150,'Montenegro'),(151,'Montserrat'),(152,'Marokko'),(153,'Mozambique'),(154,'Myanmar'),(155,'Namibië'),(156,'Nauru'),(157,'Nepal'),(158,'Nederland'),(159,'Nieuw-Caledonië'),(160,'Nieuw-Zeeland'),(161,'Nicaragua'),(162,'Niger'),(163,'Nigeria'),(164,'Niue'),(165,'Norfolkeiland'),(166,'Noord-Korea'),(167,'Noordelijke Marianeneilanden'),(168,'Noorwegen'),(169,'Oman'),(170,'Pakistan'),(171,'Palau'),(172,'Palestijnse gebieden'),(173,'Panama'),(174,'Papoea-Nieuw-Guinea'),(175,'Paraguay'),(176,'Peru'),(177,'Filipijnen'),(178,'Pitcairneilanden'),(179,'Polen'),(180,'Portugal'),(181,'Puerto Rico'),(182,'Qatar'),(183,'Republic of Kosovo'),(184,'Réunion'),(185,'Roemenië'),(186,'Rusland'),(187,'Rwanda'),(188,'Saint Barthélemy'),(189,'Sint-Helena'),(190,'Saint Kitts en Nevis'),(191,'Saint Lucia'),(192,'Saint-Martin'),(193,'Saint Pierre en Miquelon'),(194,'Saint Vincent en de Grenadines'),(195,'Samoa'),(196,'San Marino'),(197,'Sao Tomé en Principe'),(198,'Saoedi-Arabië'),(199,'Senegal'),(200,'Servië'),(201,'Seychellen'),(202,'Sierra Leone'),(203,'Singapore'),(204,'Sint Maarten'),(205,'Slowakije'),(206,'Slovenië'),(207,'Salomonseilanden'),(208,'Somalië'),(209,'Zuid-Afrika'),(210,'Zuid-Georgia en Zuidelijke Sandwicheilanden'),(211,'Zuid-Korea'),(212,'Zuid-Soedan'),(213,'Spanje'),(214,'Sri Lanka'),(215,'Soedan'),(216,'Suriname'),(217,'Svalbard en Jan Mayen'),(218,'Swaziland'),(219,'Zweden'),(220,'Zwitserland'),(221,'Syrië'),(222,'Taiwan'),(223,'Tadzjikistan'),(224,'Tanzania'),(225,'Thailand'),(226,'Oost-Timor'),(227,'Togo'),(228,'Tokelau'),(229,'Tonga'),(230,'Trinidad en Tobago'),(231,'Tunesië'),(232,'Turkije'),(233,'Turkmenistan'),(234,'Turks- en Caicoseilanden'),(235,'Tuvalu'),(236,'Oeganda'),(237,'Oekraïne'),(238,'Verenigde Arabische Emiraten'),(239,'Verenigd Koninkrijk'),(240,'Verenigde Staten'),(241,'Uruguay'),(242,'Oezbekistan'),(243,'Vanuatu'),(244,'Venezuela'),(245,'Vietnam'),(246,'Wallis en Futuna'),(247,'Westelijke Sahara'),(248,'Jemen'),(249,'Zambia'),(250,'Zimbabwe');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fabrics`
--

DROP TABLE IF EXISTS `fabrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `fabrics` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `deletedDt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `group_of_scenes`
--

DROP TABLE IF EXISTS `group_of_scenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `group_of_scenes` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `projectId` smallint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projectId` (`projectId`),
  CONSTRAINT `group_of_scenes_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `group_users`
--

DROP TABLE IF EXISTS `group_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `group_users` (
  `groupId` smallint unsigned NOT NULL,
  `userId` smallint unsigned NOT NULL,
  `castId` smallint unsigned NOT NULL,
  `creationDt` datetime DEFAULT (now()),
  KEY `groupId` (`groupId`),
  KEY `userId` (`userId`),
  KEY `castId` (`castId`),
  CONSTRAINT `group_users_ibfk_1` FOREIGN KEY (`groupId`) REFERENCES `groupes` (`id`),
  CONSTRAINT `group_users_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `group_users_ibfk_3` FOREIGN KEY (`castId`) REFERENCES `casts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `groupes`
--

DROP TABLE IF EXISTS `groupes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `groupes` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `projectId` smallint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projectId` (`projectId`),
  CONSTRAINT `groupes_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `locations` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `streetName` varchar(100) NOT NULL,
  `houseNumber` varchar(10) NOT NULL,
  `cityName` varchar(50) NOT NULL,
  `zipCode` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logins`
--

DROP TABLE IF EXISTS `logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `logins` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `pwd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoggedIn` datetime DEFAULT NULL,
  `subscribed` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1233 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logins`
--

LOCK TABLES `logins` WRITE;
/*!40000 ALTER TABLE `logins` DISABLE KEYS */;
INSERT INTO `logins` VALUES (1,'info@historalia.be','b85ceb7887d28373fecfec6d10dca852d54e885742704c3994b2d01e9ec01729643353a6e1aa01a02ccb3ad4f10618679eb706db5dc5674c33315aa12183971f',1,'2021-07-30 12:34:34',1);
/*!40000 ALTER TABLE `logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_opened`
--

DROP TABLE IF EXISTS `mail_opened`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mail_opened` (
  `mail` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `opened` tinyint(1) NOT NULL DEFAULT '0',
  `creationDt` datetime DEFAULT (now()),
  `openedDt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification_read`
--

DROP TABLE IF EXISTS `notification_read`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notification_read` (
  `notificationId` int unsigned NOT NULL,
  `userId` smallint unsigned NOT NULL,
  KEY `notificationId` (`notificationId`),
  KEY `userId` (`userId`),
  CONSTRAINT `notification_read_ibfk_1` FOREIGN KEY (`notificationId`) REFERENCES `notifications` (`id`),
  CONSTRAINT `notification_read_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification_removed`
--

DROP TABLE IF EXISTS `notification_removed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notification_removed` (
  `notificationId` int unsigned NOT NULL,
  `userId` smallint unsigned NOT NULL,
  KEY `notificationId` (`notificationId`),
  KEY `userId` (`userId`),
  CONSTRAINT `notification_removed_ibfk_1` FOREIGN KEY (`notificationId`) REFERENCES `notifications` (`id`),
  CONSTRAINT `notification_removed_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notifications` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `label` text NOT NULL,
  `notification` text NOT NULL,
  `creationDt` datetime DEFAULT (now()),
  `userId` smallint unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=481 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parts_of_town`
--

DROP TABLE IF EXISTS `parts_of_town`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `parts_of_town` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parts_of_town`
--

LOCK TABLES `parts_of_town` WRITE;
/*!40000 ALTER TABLE `parts_of_town` DISABLE KEYS */;
INSERT INTO `parts_of_town` VALUES (1,'Bel'),(2,'Elsum'),(3,'Holven'),(4,'Larum'),(5,'Oosterlo'),(6,'Punt'),(7,'Sint-Amands'),(8,'Sint-Dimpna'),(9,'Stelen'),(10,'Ten Aard'),(11,'Winkelomheide'),(12,'Zammel');
/*!40000 ALTER TABLE `parts_of_town` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_change_tokens`
--

DROP TABLE IF EXISTS `password_change_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `password_change_tokens` (
  `email` varchar(100) NOT NULL,
  `token` varchar(50) NOT NULL,
  `creationDt` datetime DEFAULT (now()),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `patterns`
--

DROP TABLE IF EXISTS `patterns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `patterns` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `deletedDt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_access_roles`
--

DROP TABLE IF EXISTS `project_access_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `project_access_roles` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `projectId` smallint unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `accessRoleId` smallint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projectId` (`projectId`),
  KEY `accessRoleId` (`accessRoleId`),
  CONSTRAINT `project_access_roles_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`),
  CONSTRAINT `project_access_roles_ibfk_2` FOREIGN KEY (`accessRoleId`) REFERENCES `access_roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_again`
--

DROP TABLE IF EXISTS `project_again`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `project_again` (
  `email` varchar(255) NOT NULL,
  `creationDt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `answer` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_role_users`
--

DROP TABLE IF EXISTS `project_role_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `project_role_users` (
  `project_role_id` smallint unsigned NOT NULL,
  `userId` smallint unsigned NOT NULL,
  `castId` smallint unsigned NOT NULL,
  `creationDt` datetime DEFAULT (now()),
  KEY `project_role_id` (`project_role_id`),
  KEY `userId` (`userId`),
  KEY `castId` (`castId`),
  CONSTRAINT `project_role_users_ibfk_1` FOREIGN KEY (`project_role_id`) REFERENCES `project_roles` (`id`),
  CONSTRAINT `project_role_users_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `project_role_users_ibfk_3` FOREIGN KEY (`castId`) REFERENCES `casts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_roles`
--

DROP TABLE IF EXISTS `project_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `project_roles` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `speaking` tinyint(1) NOT NULL DEFAULT '0',
  `projectId` smallint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projectId` (`projectId`),
  CONSTRAINT `project_roles_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_user_access_roles`
--

DROP TABLE IF EXISTS `project_user_access_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `project_user_access_roles` (
  `projectAccessRoleId` smallint unsigned NOT NULL,
  `userId` smallint unsigned NOT NULL,
  KEY `projectAccessRoleId` (`projectAccessRoleId`),
  KEY `userId` (`userId`),
  CONSTRAINT `project_user_access_roles_ibfk_1` FOREIGN KEY (`projectAccessRoleId`) REFERENCES `project_access_roles` (`id`),
  CONSTRAINT `project_user_access_roles_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_user_articles`
--

DROP TABLE IF EXISTS `project_user_articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `project_user_articles` (
  `uuid` varchar(50) NOT NULL,
  `projectUserUuid` varchar(50) NOT NULL,
  `articleId` int unsigned DEFAULT NULL,
  `status` smallint unsigned DEFAULT NULL,
  `remarks` text,
  PRIMARY KEY (`uuid`),
  KEY `projectUserUuid` (`projectUserUuid`),
  KEY `articleId` (`articleId`),
  CONSTRAINT `project_user_articles_ibfk_1` FOREIGN KEY (`projectUserUuid`) REFERENCES `project_users` (`uuid`),
  CONSTRAINT `project_user_articles_ibfk_2` FOREIGN KEY (`articleId`) REFERENCES `articles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_user_registrations`
--

DROP TABLE IF EXISTS `project_user_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `project_user_registrations` (
  `projectId` smallint unsigned NOT NULL,
  `userId` smallint unsigned NOT NULL,
  `registrationId` smallint unsigned NOT NULL,
  KEY `projectId` (`projectId`),
  KEY `userId` (`userId`),
  KEY `registrationId` (`registrationId`),
  CONSTRAINT `project_user_registrations_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`),
  CONSTRAINT `project_user_registrations_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `project_user_registrations_ibfk_3` FOREIGN KEY (`registrationId`) REFERENCES `registrations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_user_rights`
--

DROP TABLE IF EXISTS `project_user_rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `project_user_rights` (
  `projectId` smallint unsigned NOT NULL,
  `userId` smallint unsigned NOT NULL,
  `rightId` smallint unsigned NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `create` tinyint(1) NOT NULL DEFAULT '0',
  `update` tinyint(1) NOT NULL DEFAULT '0',
  `delete` tinyint(1) NOT NULL DEFAULT '0',
  KEY `projectId` (`projectId`),
  KEY `userId` (`userId`),
  KEY `rightId` (`rightId`),
  CONSTRAINT `project_user_rights_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`),
  CONSTRAINT `project_user_rights_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `project_user_rights_ibfk_3` FOREIGN KEY (`rightId`) REFERENCES `rights` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_user_sub_rights`
--

DROP TABLE IF EXISTS `project_user_sub_rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `project_user_sub_rights` (
  `projectId` smallint unsigned NOT NULL,
  `userId` smallint unsigned NOT NULL,
  `subRightId` smallint unsigned NOT NULL,
  `access` tinyint(1) NOT NULL DEFAULT '0',
  KEY `projectId` (`projectId`),
  KEY `userId` (`userId`),
  KEY `subRightId` (`subRightId`),
  CONSTRAINT `project_user_sub_rights_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`),
  CONSTRAINT `project_user_sub_rights_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `project_user_sub_rights_ibfk_3` FOREIGN KEY (`subRightId`) REFERENCES `sub_rights` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_users`
--

DROP TABLE IF EXISTS `project_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `project_users` (
  `uuid` varchar(50) NOT NULL,
  `projectId` smallint unsigned NOT NULL,
  `sceneId` smallint unsigned NOT NULL,
  `roleId` smallint unsigned DEFAULT NULL,
  `groupId` smallint unsigned DEFAULT NULL,
  `castId` smallint unsigned NOT NULL,
  `userId` smallint unsigned NOT NULL,
  `updatedDt` datetime DEFAULT (now()),
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uuid`),
  KEY `projectId` (`projectId`),
  KEY `sceneId` (`sceneId`),
  KEY `roleId` (`roleId`),
  KEY `groupId` (`groupId`),
  KEY `castId` (`castId`),
  KEY `userId` (`userId`),
  CONSTRAINT `project_users_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`),
  CONSTRAINT `project_users_ibfk_2` FOREIGN KEY (`sceneId`) REFERENCES `scenes` (`id`),
  CONSTRAINT `project_users_ibfk_3` FOREIGN KEY (`roleId`) REFERENCES `project_roles` (`id`),
  CONSTRAINT `project_users_ibfk_4` FOREIGN KEY (`groupId`) REFERENCES `groupes` (`id`),
  CONSTRAINT `project_users_ibfk_5` FOREIGN KEY (`castId`) REFERENCES `casts` (`id`),
  CONSTRAINT `project_users_ibfk_6` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `projects` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `auditionHint` varchar(255) DEFAULT NULL,
  `openFrom` datetime DEFAULT NULL,
  `closedFrom` datetime DEFAULT NULL,
  `creationDt` datetime DEFAULT (now()),
  `extended` tinyint(1) NOT NULL DEFAULT '0',
  `auditions_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `rolesOpen` datetime DEFAULT NULL,
  `closedOffStage` datetime DEFAULT NULL,
  `rolesClosed` datetime DEFAULT NULL,
  `editRegistrationClosed` datetime DEFAULT NULL,
  `deleteRegistrationClosed` datetime DEFAULT NULL,
  `projectClosed` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `racks`
--

DROP TABLE IF EXISTS `racks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `racks` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `deletedDt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `registrations`
--

DROP TABLE IF EXISTS `registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `registrations` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `auditioning` tinyint(1) NOT NULL,
  `talents` text,
  `remarks` text,
  `creationDt` datetime DEFAULT (now()),
  `part_of_town` smallint unsigned DEFAULT NULL,
  `owner_remarks` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2095 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `registrations_roles`
--

DROP TABLE IF EXISTS `registrations_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `registrations_roles` (
  `registrationId` smallint unsigned NOT NULL,
  `roleId` smallint unsigned NOT NULL,
  KEY `registrationId` (`registrationId`),
  KEY `roleId` (`roleId`),
  CONSTRAINT `registrations_roles_ibfk_1` FOREIGN KEY (`registrationId`) REFERENCES `registrations` (`id`),
  CONSTRAINT `registrations_roles_ibfk_2` FOREIGN KEY (`roleId`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rental_items`
--

DROP TABLE IF EXISTS `rental_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `rental_items` (
  `rentalId` int unsigned NOT NULL,
  `artId` int unsigned NOT NULL,
  `amount` int unsigned NOT NULL DEFAULT '1',
  `remarks` text,
  KEY `rentalId` (`rentalId`),
  KEY `artId` (`artId`),
  CONSTRAINT `rental_items_ibfk_1` FOREIGN KEY (`rentalId`) REFERENCES `rentals` (`id`),
  CONSTRAINT `rental_items_ibfk_2` FOREIGN KEY (`artId`) REFERENCES `articles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rentals`
--

DROP TABLE IF EXISTS `rentals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `rentals` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `type` smallint unsigned NOT NULL,
  `fromDt` datetime DEFAULT (now()),
  `creationDt` datetime DEFAULT (now()),
  `companyName` varchar(255) DEFAULT NULL,
  `streetName` varchar(100) DEFAULT NULL,
  `houseNumber` varchar(10) DEFAULT NULL,
  `bus` varchar(10) DEFAULT NULL,
  `cityName` varchar(50) DEFAULT NULL,
  `zipCode` varchar(10) DEFAULT NULL,
  `cellPhone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `bankAccount` varchar(50) DEFAULT NULL,
  `uniqueNumber` varchar(50) DEFAULT NULL,
  `isFor` varchar(50) DEFAULT NULL,
  `periods` smallint unsigned NOT NULL,
  `internal` smallint unsigned NOT NULL DEFAULT 0,
  `toDt` date NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` float NOT NULL,
  `replacementPrice` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `repetition_user_presence`
--

DROP TABLE IF EXISTS `repetition_user_presence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `repetition_user_presence` (
  `repetitionId` smallint unsigned NOT NULL,
  `userId` smallint unsigned DEFAULT NULL,
  `markedPresent` tinyint(1) NOT NULL DEFAULT '1',
  `present` tinyint(1) NOT NULL DEFAULT '0',
  KEY `repetitionId` (`repetitionId`),
  KEY `userId` (`userId`),
  CONSTRAINT `repetition_user_presence_ibfk_1` FOREIGN KEY (`repetitionId`) REFERENCES `repetitions` (`id`),
  CONSTRAINT `repetition_user_presence_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `repetition_users`
--

DROP TABLE IF EXISTS `repetition_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `repetition_users` (
  `repetitionId` smallint unsigned NOT NULL,
  `sceneId` smallint unsigned DEFAULT NULL,
  `groupId` smallint unsigned DEFAULT NULL,
  `roleId` smallint unsigned DEFAULT NULL,
  `userId` smallint unsigned DEFAULT NULL,
  `castId` smallint unsigned DEFAULT NULL,
  KEY `repetitionId` (`repetitionId`),
  KEY `sceneId` (`sceneId`),
  KEY `groupId` (`groupId`),
  KEY `roleId` (`roleId`),
  KEY `userId` (`userId`),
  KEY `castId` (`castId`),
  CONSTRAINT `repetition_users_ibfk_1` FOREIGN KEY (`repetitionId`) REFERENCES `repetitions` (`id`),
  CONSTRAINT `repetition_users_ibfk_2` FOREIGN KEY (`sceneId`) REFERENCES `scenes` (`id`),
  CONSTRAINT `repetition_users_ibfk_3` FOREIGN KEY (`groupId`) REFERENCES `groupes` (`id`),
  CONSTRAINT `repetition_users_ibfk_4` FOREIGN KEY (`roleId`) REFERENCES `project_roles` (`id`),
  CONSTRAINT `repetition_users_ibfk_5` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `repetition_users_ibfk_6` FOREIGN KEY (`castId`) REFERENCES `casts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `repetitions`
--

DROP TABLE IF EXISTS `repetitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `repetitions` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(10) NOT NULL,
  `uuid` varchar(255),
  `name` varchar(70) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `startDt` datetime NOT NULL,
  `endDt` datetime NOT NULL,
  `locationId` smallint unsigned DEFAULT NULL,
  `typeId` smallint unsigned DEFAULT NULL,
  `register` tinyint(1) NOT NULL DEFAULT '0',
  `open` tinyint(1) NOT NULL DEFAULT '0',
  `projectId` smallint unsigned NOT NULL,
  `allChecked` tinyint(1) NOT NULL DEFAULT '0',
  `castChecked` tinyint(1) NOT NULL DEFAULT '0',
  `notifications` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `locationId` (`locationId`),
  KEY `typeId` (`typeId`),
  KEY `projectId` (`projectId`),
  CONSTRAINT `repetitions_ibfk_1` FOREIGN KEY (`locationId`) REFERENCES `locations` (`id`),
  CONSTRAINT `repetitions_ibfk_2` FOREIGN KEY (`typeId`) REFERENCES `types` (`id`),
  CONSTRAINT `repetitions_ibfk_3` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=422 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rights`
--

DROP TABLE IF EXISTS `rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `rights` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rights`
--

LOCK TABLES `rights` WRITE;
/*!40000 ALTER TABLE `rights` DISABLE KEYS */;
INSERT INTO `rights` VALUES (1,'projects','Projecten'),(2,'registrations','Inschrijvingen binnen projecten'),(3,'users','Medewerkers'),(4,'casts','Casts'),(5,'locations','Locaties'),(6,'repetitiontypes','Repetitie-types'),(7,'scenes','Scnes'),(8,'roles','Rollen'),(9,'groups','Groepen'),(10,'repetitions','Repetitions'),(11,'articles','Artikelen'),(12,'clothing','Kledingstukken'),(13,'fabrics','Stoffen'),(14,'patterns','Motieven'),(15,'colors','Kleuren'),(16,'sizes','Kledingmaten'),(17,'racks','Rekken'),(18,'categories','Retributie-categorien'),(19,'orders','Bestellingen'),(20,'rights','Toegangsrollen'),(21,'threads','Gesprekken'),(22,'thread_actions','Gespreksonderwerpen'),(23,'settings','Instellingen'),(24,'calendar','Kalender');
/*!40000 ALTER TABLE `rights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `roles` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(30) NOT NULL,
  `onStage` tinyint(1) NOT NULL,
  `for_extended` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Acteren',1,0),(2,'Figureren',1,0),(3,'Zingen',1,0),(4,'Dansen',1,0),(5,'Kledijteam',0,0),(6,'Toneelmeester',0,0),(7,'Aankleedmoeder',0,0),(8,'Decorploeg',0,0),(9,'Attributenteam',0,0),(10,'Steward',0,0),(11,'Begeleider',0,0),(12,'Grime',0,0),(13,'Regie-assistent',0,0),(14,'Kookteam',0,0),(15,'Musiceren',1,1),(16,'Catering',0,1),(17,'Seingever',0,1),(18,'Verkoper programmaboekje',0,1);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scene_groups`
--

DROP TABLE IF EXISTS `scene_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `scene_groups` (
  `sceneId` smallint unsigned NOT NULL,
  `groupId` smallint unsigned NOT NULL,
  KEY `sceneId` (`sceneId`),
  KEY `groupId` (`groupId`),
  CONSTRAINT `scene_groups_ibfk_1` FOREIGN KEY (`sceneId`) REFERENCES `scenes` (`id`),
  CONSTRAINT `scene_groups_ibfk_2` FOREIGN KEY (`groupId`) REFERENCES `groupes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scene_roles`
--

DROP TABLE IF EXISTS `scene_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `scene_roles` (
  `sceneId` smallint unsigned NOT NULL,
  `project_role_id` smallint unsigned NOT NULL,
  KEY `sceneId` (`sceneId`),
  KEY `project_role_id` (`project_role_id`),
  CONSTRAINT `scene_roles_ibfk_1` FOREIGN KEY (`sceneId`) REFERENCES `scenes` (`id`),
  CONSTRAINT `scene_roles_ibfk_2` FOREIGN KEY (`project_role_id`) REFERENCES `project_roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scenes`
--

DROP TABLE IF EXISTS `scenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `scenes` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `projectId` smallint unsigned NOT NULL,
  `groupId` smallint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projectId` (`projectId`),
  CONSTRAINT `scenes_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shirt_sizes`
--

DROP TABLE IF EXISTS `shirt_sizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `shirt_sizes` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shirt_sizes`
--

LOCK TABLES `shirt_sizes` WRITE;
/*!40000 ALTER TABLE `shirt_sizes` DISABLE KEYS */;
INSERT INTO `shirt_sizes` VALUES (1,'122'),(2,'128'),(3,'134'),(4,'140'),(5,'146'),(6,'152'),(7,'158'),(8,'164'),(9,'170'),(10,'176'),(11,'XS'),(12,'S'),(13,'M'),(14,'L'),(15,'XL'),(16,'XXL'),(17,'XXXL');
/*!40000 ALTER TABLE `shirt_sizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_rights`
--

DROP TABLE IF EXISTS `sub_rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sub_rights` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `rightId` smallint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rightId` (`rightId`),
  CONSTRAINT `sub_rights_ibfk_1` FOREIGN KEY (`rightId`) REFERENCES `rights` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_rights`
--

LOCK TABLES `sub_rights` WRITE;
/*!40000 ALTER TABLE `sub_rights` DISABLE KEYS */;
INSERT INTO `sub_rights` VALUES (1,'projects_export','Exporteer projecten',1),(2,'rights','Toegangsrollen',1),(3,'badges','Badges aanmaken',1),(4,'registrations_export','Exporteer inschrijvingen',2),(5,'general','Algemene gegevens',2),(6,'preferred','Voorkeurs-gegevens',2),(7,'other','Overige gegevens',2),(8,'picture','Profielfoto',3),(9,'personal','Persoonlijke gegevens',3),(10,'contact','Contactgegevens',3),(11,'address','Adresgegevens',3),(12,'other','Overige gegevens',3),(13,'clothing','Kledijgegevens',3),(14,'remarks','Opmerkingen',3),(15,'registrations','Inschrijvingen',3),(16,'roles','Rollen',3),(17,'groups','Groepen',3),(18,'scenes','Scnes',3),(19,'repetitions','Repetities',3),(20,'rights','Toegangsrollen',3),(21,'export','Exporteren',7),(22,'addusers','Beheer rollen/groepen',7),(23,'export','Exporteren',8),(24,'addusers','Medewerkers beheren',8),(25,'export','Exporteren',9),(26,'addusers','Medewerkers beheren',9),(27,'export','Exporteren',10),(28,'openclose','Openen/sluiten',10),(29,'pin','Pinscherm',10),(30,'present','Aanwezigen beheren',10),(31,'export','Exporteren',11),(32,'receive','Ontvangen',11),(33,'export','Exporteren',12),(34,'export','Exporteren',13),(35,'export','Exporteren',14),(36,'export','Exporteren',15),(37,'export','Exporteren',16),(38,'export','Exporteren',17),(39,'export','Exporteren',18),(40,'export','Exporteren',19),(41,'copy','Kopiëer inschrijvingen',2),(42,'copy','Kopier inschrijvingen',2),(43,'transfer','Overzetten naar kledij-beheer',1),(44,'transfer','Overzetten naar kledij-beheer',1),(45,'export_users','Exporteren',3);
/*!40000 ALTER TABLE `sub_rights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thread_action_users`
--

DROP TABLE IF EXISTS `thread_action_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `thread_action_users` (
  `threadActionId` int unsigned NOT NULL,
  `userId` smallint unsigned NOT NULL,
  `isResponsible` tinyint(1) NOT NULL DEFAULT '0',
  KEY `userId` (`userId`),
  KEY `threadActionId` (`threadActionId`),
  CONSTRAINT `thread_action_users_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `thread_action_users_ibfk_2` FOREIGN KEY (`threadActionId`) REFERENCES `thread_actions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `thread_actions`
--

DROP TABLE IF EXISTS `thread_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `thread_actions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `defaultMessage` text,
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `thread_messages`
--

DROP TABLE IF EXISTS `thread_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `thread_messages` (
  `id` varchar(255) NOT NULL,
  `userId` smallint unsigned NOT NULL,
  `threadId` int unsigned NOT NULL,
  `message` text,
  `replyOn` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `createdDt` datetime DEFAULT (now()),
  KEY `userId` (`userId`),
  KEY `threadId` (`threadId`),
  CONSTRAINT `thread_messages_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `thread_messages_ibfk_2` FOREIGN KEY (`threadId`) REFERENCES `threads` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `thread_users`
--

DROP TABLE IF EXISTS `thread_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `thread_users` (
  `userId` smallint unsigned NOT NULL,
  `isResponsible` tinyint(1) NOT NULL DEFAULT '0',
  `threadId` int unsigned NOT NULL,
  `archive` tinyint(1) NOT NULL DEFAULT '0',
  `lastRead` datetime DEFAULT NULL,
  KEY `userId` (`userId`),
  KEY `threadId` (`threadId`),
  CONSTRAINT `thread_users_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `thread_users_ibfk_2` FOREIGN KEY (`threadId`) REFERENCES `threads` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `threads`
--

DROP TABLE IF EXISTS `threads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `threads` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `archive` tinyint(1) NOT NULL DEFAULT '0',
  `projectId` smallint unsigned DEFAULT NULL,
  `actionId` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projectId` (`projectId`),
  KEY `actionId` (`actionId`),
  CONSTRAINT `threads_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`),
  CONSTRAINT `threads_ibfk_2` FOREIGN KEY (`actionId`) REFERENCES `thread_actions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `types` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_access_roles`
--

DROP TABLE IF EXISTS `user_access_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_access_roles` (
  `accessRoleId` smallint unsigned NOT NULL,
  `userId` smallint unsigned NOT NULL,
  KEY `accessRoleId` (`accessRoleId`),
  KEY `userId` (`userId`),
  CONSTRAINT `user_access_roles_ibfk_1` FOREIGN KEY (`accessRoleId`) REFERENCES `access_roles` (`id`),
  CONSTRAINT `user_access_roles_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_access_roles`
--

LOCK TABLES `user_access_roles` WRITE;
/*!40000 ALTER TABLE `user_access_roles` DISABLE KEYS */;
INSERT INTO `user_access_roles` VALUES (1,1);
/*!40000 ALTER TABLE `user_access_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_app_settings`
--

DROP TABLE IF EXISTS `user_app_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_app_settings` (
  `userId` smallint unsigned NOT NULL,
  `level` smallint unsigned NOT NULL,
  `pin` mediumint unsigned NOT NULL,
  `privacy` tinyint(1) NOT NULL,
  `pictures` tinyint(1) NOT NULL,
  KEY `userId` (`userId`),
  CONSTRAINT `user_app_settings_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_app_settings`
--

LOCK TABLES `user_app_settings` WRITE;
/*!40000 ALTER TABLE `user_app_settings` DISABLE KEYS */;
INSERT INTO `user_app_settings` VALUES (1,1,12345,1,1);
/*!40000 ALTER TABLE `user_app_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_rights`
--

DROP TABLE IF EXISTS `user_rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_rights` (
  `userId` smallint unsigned NOT NULL,
  `rightId` smallint unsigned NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `create` tinyint(1) NOT NULL DEFAULT '0',
  `update` tinyint(1) NOT NULL DEFAULT '0',
  `delete` tinyint(1) NOT NULL DEFAULT '0',
  KEY `userId` (`userId`),
  KEY `rightId` (`rightId`),
  CONSTRAINT `user_rights_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `user_rights_ibfk_2` FOREIGN KEY (`rightId`) REFERENCES `rights` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_sizes`
--

DROP TABLE IF EXISTS `user_sizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_sizes` (
  `userId` smallint unsigned NOT NULL,
  `shirtSize` smallint DEFAULT NULL,
  `height` smallint unsigned DEFAULT NULL,
  `chest` smallint unsigned DEFAULT NULL,
  `waist` smallint unsigned DEFAULT NULL,
  `hipSize` smallint unsigned DEFAULT NULL,
  `headSize` smallint unsigned DEFAULT NULL,
  `clothingSize` smallint unsigned DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  KEY `userId` (`userId`),
  CONSTRAINT `user_sizes_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_sizes`
--

LOCK TABLES `user_sizes` WRITE;
/*!40000 ALTER TABLE `user_sizes` DISABLE KEYS */;
INSERT INTO `user_sizes` VALUES (1,13,180,54,54,54,54,152,NULL);
/*!40000 ALTER TABLE `user_sizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_sub_rights`
--

DROP TABLE IF EXISTS `user_sub_rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_sub_rights` (
  `userId` smallint unsigned NOT NULL,
  `subRightId` smallint unsigned NOT NULL,
  `access` tinyint(1) NOT NULL DEFAULT '0',
  KEY `userId` (`userId`),
  KEY `subRightId` (`subRightId`),
  CONSTRAINT `user_sub_rights_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `user_sub_rights_ibfk_2` FOREIGN KEY (`subRightId`) REFERENCES `sub_rights` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(30) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `date_of_birth` date NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `origin` varchar(20) NOT NULL,
  `sex` smallint unsigned NOT NULL,
  `streetName` varchar(100) NOT NULL,
  `houseNumber` varchar(10) NOT NULL,
  `cityName` varchar(50) NOT NULL,
  `zipCode` varchar(10) NOT NULL,
  `phoneNumber` varchar(20) DEFAULT NULL,
  `cellPhone` varchar(20) DEFAULT NULL,
  `secondaryEmail` varchar(100) DEFAULT NULL,
  `creationDt` datetime DEFAULT (now()),
  `is_owner` tinyint(1) NOT NULL DEFAULT '0',
  `group_owner` smallint unsigned DEFAULT NULL,
  `group_name` varchar(100) DEFAULT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `owner_remarks` text,
  `confirmedDt` datetime DEFAULT CURRENT_TIMESTAMP,
  `bus` varchar(10) DEFAULT NULL,
  `clothingFrozen` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `group_owner` (`group_owner`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`email`) REFERENCES `logins` (`email`),
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`group_owner`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1370 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','','info@historalia.be','1985-10-02','Belg','België',0,'Markt','1','Geel','2440',NULL,'0498765432',NULL,'2019-10-10 14:39:08',0,NULL,NULL,0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'historalia'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-16 23:25:09
