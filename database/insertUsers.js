const logger = require('../logger')({
    stringUtil: require('../server/utils/string')(),
    dateUtil: require('../server/utils/date')({
        numberUtil: require('../server/utils/number')(),
    }),
});
const mysql = require('../server/connectors/mysql')({ logger });

const logins = [
    {
        email: 'hans@fanvandimpna.be',
        pwd: 'FVD123',
        confirmed: true,
    },
    {
        email: 'maarten@fanvandimpna.be',
        pwd: 'FVD123',
        confirmed: true,
    },
    {
        email: 'david@fanvandimpna.be',
        pwd: 'FVD123',
        confirmed: true,
    },
    {
        email: 'ben@fanvandimpna.be',
        pwd: 'FVD123',
        confirmed: true,
    },
    {
        email: 'guy@fanvandimpna.be',
        pwd: 'FVD123',
        confirmed: true,
    },
    {
        email: 'jo@fanvandimpna.be',
        pwd: 'FVD123',
        confirmed: true,
    },
    {
        email: 'yasmine@fanvandimpna.be',
        pwd: 'FVD123',
        confirmed: true,
    },
    {
        email: 'nadine@fanvandimpna.be',
        pwd: 'FVD123',
        confirmed: true,
    },
];

const users = [
    {
        firstName: 'Hans',
        lastName: 'Driesen',
        email: 'hans@fanvandimpna.be',
        date_of_birth: '1985-10-02',
        nationality: 'Belg',
        origin: 'België',
        sex: 0,
        streetName: 'Markt',
        houseNumber: '1',
        cityName: 'Geel',
        zipCode: '2440',
        cellPhone: '0498765432',
    },
    {
        firstName: 'Maarten',
        lastName: 'Vanuytsel',
        email: 'maarten@fanvandimpna.be',
        date_of_birth: '1985-10-02',
        nationality: 'Belg',
        origin: 'België',
        sex: 0,
        streetName: 'Markt',
        houseNumber: '1',
        cityName: 'Geel',
        zipCode: '2440',
        cellPhone: '0487654321',
    },
    {
        firstName: 'David',
        lastName: 'Verhulst',
        email: 'david@fanvandimpna.be',
        date_of_birth: '1985-06-10',
        nationality: 'Belg',
        origin: 'België',
        sex: 0,
        streetName: 'Schaliënhoevedreef',
        houseNumber: '20D',
        cityName: 'Mechelen',
        zipCode: '2000',
        cellPhone: '0477995519',
    },
    {
        firstName: 'Ben',
        lastName: 'Hendrickx',
        email: 'ben@fanvandimpna.be',
        date_of_birth: '1992-02-23',
        nationality: 'Belg',
        origin: 'België',
        sex: 0,
        streetName: 'Molenaarstraat',
        houseNumber: '10',
        cityName: 'Ekeren',
        zipCode: '2180',
        cellPhone: '0476091967',
    },
    {
        firstName: 'Guy',
        lastName: 'Gevers',
        email: 'guy@fanvandimpna.be',
        date_of_birth: '1983-08-15',
        nationality: 'Belg',
        origin: 'België',
        sex: 0,
        streetName: 'Werft',
        houseNumber: '36',
        cityName: 'Geel',
        zipCode: '2440',
        cellPhone: '0476543210',
    },
    {
        firstName: 'Jo',
        lastName: 'Vangenechten',
        email: 'jo@fanvandimpna.be',
        date_of_birth: '1981-03-25',
        nationality: 'Belg',
        origin: 'België',
        sex: 1,
        streetName: 'Patronaatstraat',
        houseNumber: '11',
        cityName: 'Geel',
        zipCode: '2440',
        cellPhone: '0484321099',
    },
    {
        firstName: 'Yasmine',
        lastName: 'Vangenechten',
        email: 'yasmine@fanvandimpna.be',
        date_of_birth: '1981-06-12',
        nationality: 'Belg',
        origin: 'België',
        sex: 1,
        streetName: 'Peperstraat',
        houseNumber: '23',
        cityName: 'Geel',
        zipCode: '2440',
        cellPhone: '0484321098',
    },
    {
        firstName: 'Nadine',
        lastName: 'Laeremans',
        email: 'nadine@fanvandimpna.be',
        date_of_birth: '1964-08-04',
        nationality: 'Belg',
        origin: 'België',
        sex: 1,
        streetName: 'Anemoonstraat',
        houseNumber: '7',
        cityName: 'Geel',
        zipCode: '2440',
        cellPhone: '0473210987',
    },
];

const userSizes = [
    {
        userId: 1,
        shirtSize: 13,
    },
    {
        userId: 2,
        shirtSize: 13,
    },
    {
        userId: 3,
        shirtSize: 14,
    },
    {
        userId: 4,
        shirtSize: 12,
    },
    {
        userId: 5,
        shirtSize: 13,
    },
    {
        userId: 6,
        shirtSize: 12,
    },
    {
        userId: 7,
        shirtSize: 12,
    },
    {
        userId: 8,
        shirtSize: 12,
    },
];

const userAppSettings = [
    {
        userId: 1,
        level: 1,
        pin: 12345,
        privacy: true,
        pictures: true,
    },
    {
        userId: 2,
        level: 1,
        pin: 23456,
        privacy: true,
        pictures: false,
    },
    {
        userId: 3,
        level: 1,
        pin: 34567,
        privacy: true,
        pictures: false,
    },
    {
        userId: 4,
        level: 1,
        pin: 45678,
        privacy: true,
        pictures: true,
    },
    {
        userId: 5,
        level: 1,
        pin: 56789,
        privacy: true,
        pictures: true,
    },
    {
        userId: 6,
        level: 1,
        pin: 67890,
        privacy: true,
        pictures: true,
    },
    {
        userId: 7,
        level: 1,
        pin: 78901,
        privacy: true,
        pictures: true,
    },
    {
        userId: 8,
        level: 1,
        pin: 89012,
        privacy: true,
        pictures: true,
    },
];

module.exports = async function() {
    for (let i = 0; i < logins.length; i++) {
        try {
            await mysql.add('logins', logins[i]);
            await mysql.add('users', users[i]);
            await mysql.add('user_sizes', userSizes[i]);
            await mysql.add('user_app_settings', userAppSettings[i]);
        } catch (e) {
            logger.error(e);
        }
    }
};
