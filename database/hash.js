const CryptoJS = require('crypto-js');
const logger = require('../logger')({
    stringUtil: require('../server/utils/string')(),
    dateUtil: require('../server/utils/date')({
        numberUtil: require('../server/utils/number')(),
    }),
});
const mysql = require('../server/connectors/mysql')({ logger });

const queries = [`SELECT * FROM logins`];

(async function() {
    for (let i = 0; i < queries.length; i++) {
        try {
            logger.success(queries[i]);
            let data = await mysql.raw(queries[i]);
            logger.success(data);
            await Promise.all(
                data.map(
                    async ({ id, pwd }) =>
                        await mysql.raw(`UPDATE logins SET pwd = '${CryptoJS.SHA512(pwd)}' WHERE id=${id}`)
                )
            );
        } catch (e) {
            logger.error(e);
        }
    }
    process.exit(0);
})();
