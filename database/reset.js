const logger = require('../logger')({
    stringUtil: require('../server/utils/string')(),
    dateUtil: require('../server/utils/date')({
        numberUtil: require('../server/utils/number')(),
    }),
});

if (process.env.NODE_ENV === 'production') {
    logger.warn('Reset of database is disabled on production, please run the scripts separately');
    process.exit(0);
}

(async function() {
    await require('./setup.js')();
    process.exit(0);
})();
