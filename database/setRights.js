const logger = require('../logger')({
    stringUtil: require('../server/utils/string')(),
    dateUtil: require('../server/utils/date')({
        numberUtil: require('../server/utils/number')(),
    }),
});
const mysql = require('../server/connectors/mysql')({ logger });

const queries = [`SELECT * FROM users INNER JOIN user_app_settings ON user_app_settings.userId=users.id`];
(async function() {
    for (let i = 0; i < queries.length; i++) {
        try {
            logger.success(queries[i]);
            let data = await mysql.raw(queries[i]);
            logger.success(data);
            await Promise.all(
                data.map(async ({ id, level }) => {
                    if (level === 0) {
                        await mysql.raw(
                            `INSERT INTO user_rights (userId, rightId, \`read\`, \`create\`, \`update\`, \`delete\`) VALUES (${id}, 2, true, true, true, true)`
                        );
                        await mysql.raw(
                            `INSERT INTO user_sub_rights (userId, subRightId, access) VALUES (${id}, 5, true)`
                        );
                        await mysql.raw(
                            `INSERT INTO user_sub_rights (userId, subRightId, access) VALUES (${id}, 6, true)`
                        );
                        await mysql.raw(
                            `INSERT INTO user_sub_rights (userId, subRightId, access) VALUES (${id}, 7, true)`
                        );
                    }
                })
            );
        } catch (e) {
            console.error(e);
        }
    }
    process.exit(0);
})();
