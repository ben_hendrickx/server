const logger = require('../logger')({
    stringUtil: require('../server/utils/string')(),
    dateUtil: require('../server/utils/date')({
        numberUtil: require('../server/utils/number')(),
    }),
});
const mysql = require('../server/connectors/mysql')({ logger });

const queries = [
    `ALTER TABLE users ADD COLUMN archived BOOL NOT NULL DEFAULT false`,
    `ALTER TABLE registrations ADD COLUMN part_of_town SMALLINT UNSIGNED`,
    `ALTER TABLE projects ADD COLUMN extended BOOL NOT NULL DEFAULT false`,
    `ALTER TABLE roles CHANGE role role varchar(30) NOT NULL`,
    `ALTER TABLE roles ADD COLUMN for_extended BOOL NOT NULL DEFAULT false`,
    `ALTER TABLE registrations ADD COLUMN owner_remarks TEXT`,
    `ALTER TABLE users ADD COLUMN owner_remarks TEXT`,
    `ALTER TABLE projects ADD COLUMN auditions_enabled BOOL NOT NULL DEFAULT false`,
    `ALTER TABLE project_roles MODIFY identifier varchar(10) NOT NULL`,
    `ALTER TABLE groupes MODIFY identifier varchar(10) NOT NULL`,
    `ALTER TABLE scenes MODIFY identifier varchar(10) NOT NULL`,
    `ALTER TABLE scenes ADD groupId SMALLINT UNSIGNED`,
    `ALTER TABLE projects ADD rolesOpen datetime`,
    `ALTER TABLE projects ADD rolesClosed datetime`,
    `ALTER TABLE projects ADD closedOffStage datetime`,
    `ALTER TABLE projects ADD editRegistrationClosed datetime`,
    `ALTER TABLE projects ADD deleteRegistrationClosed datetime`,
    `ALTER TABLE group_of_scenes MODIFY identifier varchar(10) NOT NULL`,
    `ALTER TABLE repetitions ADD COLUMN allChecked BOOL NOT NULL DEFAULT false`,
    `ALTER TABLE group_users ADD COLUMN creationDt datetime DEFAULT(now())`,
    `ALTER TABLE users ADD COLUMN confirmedDt datetime`,
    `ALTER TABLE users ADD COLUMN bus varchar(10)`,
    `ALTER TABLE user_sizes MODIFY shirtSize smallint(1)`,
    `ALTER TABLE repetitions ADD COLUMN castChecked BOOL NOT NULL DEFAULT false`,
    `ALTER TABLE articles ADD COLUMN tags TEXT`,
    `ALTER TABLE articles DROP COLUMN rack`,
    `ALTER TABLE articles ADD COLUMN rackId INT UNSIGNED`,
    `ALTER TABLE articles DROP COLUMN washing`,
    `ALTER TABLE articles ADD COLUMN status SMALLINT UNSIGNED`,
    `ALTER TABLE articles MODIFY COLUMN clothingSizeId SMALLINT UNSIGNED`,
    `ALTER TABLE logins MODIFY COLUMN pwd varchar(255) NOT NULL`,
    `ALTER TABLE categories DROP COLUMN price`,
    `ALTER TABLE rentals ADD COLUMN internal BOOL NOT NULL DEFAULT false`,
    `ALTER TABLE rentals ADD COLUMN socialSecurity VARCHAR(50)`,
    `ALTER TABLE rentals ADD COLUMN billed BOOL NOT NULL DEFAULT false`,
    `ALTER TABLE rental_items ADD COLUMN amount INT UNSIGNED NOT NULL DEFAULT 1`,
    `ALTER TABLE rental_items ADD COLUMN remarks TEXT``ALTER TABLE rental_items ADD COLUMN returned smallint(3) NOT NULL DEFAULT 0`,
    `ALTER TABLE rentals ADD COLUMN creationDt datetime DEFAULT(now())`,
];

(async function() {
    for (let i = 0; i < queries.length; i++) {
        try {
            logger.debug(queries[i]);
            await mysql.raw(queries[i]);
        } catch (e) {
            logger.error(e);
        }
    }
    process.exit(0);
})();
