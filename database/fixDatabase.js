const stringUtil = require('../server/utils/string')();
const numberUtil = require('../server/utils/number')();
const dateUtil = require('../server/utils/date')({ numberUtil });
const logger = require('../logger')({ stringUtil, dateUtil });
const mysql = require('../server/connectors/mysql')({ logger });
const dataService = require('../server/services/data')({ logger, mysql });

function mapItem(item, oldId, newId) {
    if (item.userId !== oldId) {
        return item;
    }

    item.userId = newId;
    return item;
}

(async function() {
    try {
        let [
            logins,
            users,
            sizes,
            logs,
            settings,
            repetitionUsers,
            repetitionUserPresences,
            projectRights,
            projectRegistrations,
            projectRoles,
            notifications,
            notificationsRead,
            notificationsRemoved,
            groupUsers,
        ] = await Promise.all(
            [
                'logins',
                'users',
                'user_sizes',
                'user_log',
                'user_app_settings',
                'repetition_users',
                'repetition_user_presence',
                'project_user_rights',
                'project_user_registrations',
                'project_role_users',
                'notifications',
                'notification_read',
                'notification_removed',
                'group_users',
            ].map(table => dataService.read(table))
        );

        await mysql.disconnect();
        await mysql.connect({
            host: 'database',
            port: '3306',
            user: 'fvd',
            password: 'FVD123',
            database: 'fanvandimpna',
            insecureAuth: true,
        });

        logins = logins.map((login, index) => {
            const newId = index + 1;
            login.id = newId;
            return login;
        });

        users = users.map((user, index) => {
            const { id: oldId } = user;
            const newId = index + 1;
            user.id = newId;

            sizes = sizes.map(size => mapItem(size, oldId, newId));
            logs = logs.map(log => mapItem(log, oldId, newId));
            settings = settings.map(setting => mapItem(setting, oldId, newId));
            repetitionUsers = repetitionUsers.map(repetitionUser => mapItem(repetitionUser, oldId, newId));
            repetitionUserPresences = repetitionUserPresences.map(repetitionUserPresence =>
                mapItem(repetitionUserPresence, oldId, newId)
            );
            projectRights = projectRights.map(projectRight => mapItem(projectRight, oldId, newId));
            projectRegistrations = projectRegistrations.map(projectRegistration =>
                mapItem(projectRegistration, oldId, newId)
            );
            projectRoles = projectRoles.map(projectRole => mapItem(projectRole, oldId, newId));
            notifications = notifications.map(notification => mapItem(notification, oldId, newId));
            notificationsRead = notificationsRead.map(notificationRead => mapItem(notificationRead, oldId, newId));
            notificationsRemoved = notificationsRemoved.map(notificationRemoved =>
                mapItem(notificationRemoved, oldId, newId)
            );
            groupUsers = groupUsers.map(groupUser => mapItem(groupUser, oldId, newId));
            return user;
        });

        await Promise.all(logins.map(login => dataService.create('logins', login)));
        await Promise.all(users.map(user => dataService.create('users', user)));
        await Promise.all(sizes.map(size => dataService.create('user_sizes', size)));
        await Promise.all(logs.map(log => dataService.create('user_log', log)));
        await Promise.all(settings.map(settings => dataService.create('user_app_settings', settings)));
        await Promise.all(
            repetitionUsers.map(repetitionUser => dataService.create('repetition_users', repetitionUser))
        );
        await Promise.all(
            repetitionUserPresences.map(repetitionUserPresence =>
                dataService.create('repetition_user_presence', repetitionUserPresence)
            )
        );
        await Promise.all(projectRights.map(projectRight => dataService.create('project_user_rights', projectRight)));
        await Promise.all(
            projectRegistrations.map(projectRegistration =>
                dataService.create('project_user_registrations', projectRegistration)
            )
        );
        await Promise.all(projectRoles.map(projectRole => dataService.create('project_role_users', projectRole)));
        await Promise.all(notifications.map(notification => dataService.create('notifications', notification)));
        await Promise.all(
            notificationsRead.map(notificationRead => dataService.create('notification_read', notificationRead))
        );
        await Promise.all(
            notificationsRemoved.map(notificationRemoved =>
                dataService.create('notification_removed', notificationRemoved)
            )
        );
        await Promise.all(groupUsers.map(groupUser => dataService.create('group_users', groupUser)));
    } catch (e) {
        logger.fatal(e);
    }
})();
