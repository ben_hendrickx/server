module.exports = function() {
    function twoDigits(number) {
        return number.toString().length > 1 ? number.toString() : '0' + number;
    }

    function threeDigits(number) {
        if (number.toString().length === 1) {
            return '00' + number;
        }

        if (number.toString().length === 2) {
            return '0' + number;
        }

        return number.toString();
    }

    function randomNumber(min = 1, max = 100) {
        return Math.floor(Math.random() * (max - min) + min);
    }

    return {
        twoDigits,
        threeDigits,
        randomNumber,
    };
};
