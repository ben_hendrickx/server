const momentTimezone = require('moment-timezone');

module.exports = function({ numberUtil }) {
    function dateString() {
        const d = new Date();

        const getMonth = () => numberUtil.twoDigits(d.getMonth() + 1);
        const getDay = () => numberUtil.twoDigits(d.getDate());

        return `${getDay()}/${getMonth()}/${d.getFullYear()}`;
    }

    function timeString() {
        const d = new Date();

        const getHours = () => numberUtil.twoDigits(d.getHours());
        const getMinutes = () => numberUtil.twoDigits(d.getMinutes());
        const getSeconds = () => numberUtil.twoDigits(d.getSeconds());
        const getMS = () => numberUtil.threeDigits(d.getMilliseconds());

        return `${getHours()}:${getMinutes()}:${getSeconds()}.${getMS()}`;
    }

    function dateTimeString() {
        return `${dateString()} ${timeString()}`;
    }

    function isInPast(date = new Date()) {
        return new Date(date).getTime() < Date.now();
    }

    function isInFuture(date) {
        return new Date(date).getTime() > Date.now();
    }

    function useTimezone(date) {
        date = momentTimezone(date).tz('Europe/Brussels');
        return date.format('YYYY-MM-DD HH:mm:ss');
    }

    return {
        dateString,
        timeString,
        dateTimeString,
        isInPast,
        isInFuture,
        useTimezone,
    };
};
