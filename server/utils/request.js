module.exports = function requestUtil() {
    function getToken(req) {
        return (req.headers['authorization'] || '').replace('Bearer ', '');
    }

    return {
        getToken,
    };
};
