describe('stringSpec', function() {
    beforeEach(() => {
        this.stringUtil = require('../string')();
    });

    describe('When removing chars from a string', () => {
        beforeEach(() => {
            this.result = this.stringUtil.removeChars('test', ['t', 's']);
        });

        it('Characters should be removed globally', () => {
            expect(this.result).to.be.equal('e');
        });
    });

    describe('When generating a password', () => {
        beforeEach(() => {
            this.result = this.stringUtil.generatePassword();
        });

        it('Should be 6 long', () => {
            expect(this.result.length).to.be.equal(6);
        });
    });

    describe('When cleaning an email address', () => {
        beforeEach(() => {
            this.result = this.stringUtil.removeChars(`.!#%&'-/=_\`{}~",:;<>@`, `.!#%&'-/=_\`{}~",:;<>@`.split(''));
        });

        it('Characters should be removed globally', () => {
            expect(this.result).to.be.equal('');
        });
    });

    describe('When making a string a fixedLength', () => {
        beforeEach(() => {
            this.result = this.stringUtil.fixedLength('test', 7);
        });

        it('String should be appended by spaces', () => {
            expect(this.result).to.be.equal('test   ');
        });
    });
});
