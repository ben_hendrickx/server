const sinon = require('sinon');

describe('dateSpec', function() {
    beforeEach(() => {
        this.clock = sinon.useFakeTimers(new Date(2019, 9, 10, 22, 43, 54, 500));
        this.dateUtil = require('../date')({ numberUtil: require('../number')() });
    });
    describe('When getting the dateString for the current date', () => {
        beforeEach(() => {
            this.result = this.dateUtil.dateString(new Date());
        });

        it('Should be the correct date', () => {
            expect(this.result).to.be.equal('10/10/2019');
        });
    });
    describe('When getting the timeString for the current datetime', () => {
        beforeEach(() => {
            this.result = this.dateUtil.timeString(new Date());
        });

        it('Should be the correct date', () => {
            expect(this.result).to.be.equal('22:43:54.500');
        });
    });
    describe('When getting the dateTimeString for the current datetime', () => {
        beforeEach(() => {
            this.result = this.dateUtil.dateTimeString(new Date());
        });

        it('Should be the correct date', () => {
            expect(this.result).to.be.equal('10/10/2019 22:43:54.500');
        });
    });
    afterEach(() => {
        this.clock.restore();
    });
});
