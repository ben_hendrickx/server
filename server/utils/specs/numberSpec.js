describe('numberSpec', function() {
    beforeEach(() => {
        this.numberUtil = require('../number')();
    });

    describe('When converting one digit to two digits', () => {
        beforeEach(() => {
            this.result = this.numberUtil.twoDigits(1);
        });

        it('It should be prepended with one zero', () => {
            expect(this.result).to.be.equal('01');
        });
    });

    describe('When converting one digit to three digits', () => {
        beforeEach(() => {
            this.result = this.numberUtil.threeDigits(1);
        });

        it('It should be prepended with two zeroes', () => {
            expect(this.result).to.be.equal('001');
        });
    });

    describe('When converting two digits to three digits', () => {
        beforeEach(() => {
            this.result = this.numberUtil.threeDigits(11);
        });

        it('It should be prepended with one zero', () => {
            expect(this.result).to.be.equal('011');
        });
    });

    describe('When generating a random number between 1 and 1', () => {
        beforeEach(() => {
            this.result = this.numberUtil.randomNumber(1, 1);
        });

        it('It should be 1', () => {
            expect(this.result).to.be.equal(1);
        });
    });

    describe('When generating a random number', () => {
        beforeEach(() => {
            this.result = this.numberUtil.randomNumber();
        });

        it('It should be 1', () => {
            expect(this.result)
                .to.be.greaterThan(0)
                .and.to.be.lessThan(1001);
        });
    });
});
