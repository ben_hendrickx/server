module.exports = function() {
    function getPresentProperties(data, props) {
        return Object.keys(data).filter(key => props.includes(key));
    }

    function getNestedProperty(object = {}, key) {
        if (!key.includes('.')) {
            return object[key];
        }

        const parts = key.split('.');

        return getNestedProperty(object[parts.shift()], parts.join('.'));
    }

    return {
        getPresentProperties,
        getNestedProperty,
    };
};
