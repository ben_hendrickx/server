module.exports = function() {
    function fixedLength(string, length, filler = ' ') {
        const spaces = length - string.length;

        return string + new Array(spaces).fill(filler).join('');
    }

    function removeChars(string, chars) {
        const hasDot = chars.includes('.');
        hasDot && chars.splice(chars.indexOf('.'), 1);
        const regex = new RegExp(`(${chars.join('|')})`, 'g');

        string = string.replace(regex, '');
        hasDot && (string = string.replace(/\./g, ''));
        return string;
    }

    function generatePassword(length = 6) {
        const possibilities = [
            'b',
            'c',
            'd',
            'f',
            'g',
            'h',
            'j',
            'k',
            'l',
            'm',
            'n',
            'p',
            'q',
            'r',
            's',
            't',
            'v',
            'w',
            'x',
            'y',
            'z',
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
        ];
        return [...new Array(length)]
            .map(() => possibilities[Math.floor(Math.random() * possibilities.length)])
            .join('');
    }

    return {
        fixedLength,
        removeChars,
        generatePassword,
    };
};
