const requestUtil = require('../utils/request')();
const jwt = require('jsonwebtoken');
const SECRET = '7d6ac074040ce958425d53da29f9034b6cc556e89426f2dda4fc0ad627ad7fb0';

function _fixParams(params) {
    return Object.entries(params).reduce((acc, [key, value]) => {
        if (key === 'text') {
            return acc;
        }

        if (value.includes('-')) {
            return acc;
        }

        if (value === 'true') {
            acc[key] = true;
            return acc;
        }

        if (value === 'false') {
            acc[key] = false;
            return acc;
        }

        if (isNaN(parseInt(value))) {
            return acc;
        }

        if (value.includes(',')) {
            return acc;
        }

        acc[key] = parseInt(value);
        return acc;
    }, params);
}

module.exports = function httpAdapter(app, { logger, onlineUsersService }) {
    const fallBackMiddleWare = (req, res, next) => {
        logger.info('Skipping middleware');
        next();
    };

    const handlerMiddleWare = handler => {
        let handlerFunction = handler;

        if (typeof handler !== 'function') {
            handlerFunction = () => handler;
        }

        return async (req, res) => {
            try {
                const token = requestUtil.getToken(req);
                if (token) {
                    const data = jwt.verify(token, SECRET);
                    onlineUsersService.update(data);
                    req.user = data;
                }

                req.params = _fixParams(req.params);
                req.query = _fixParams(req.query);
                const result = await handlerFunction(req, res);

                if (!result) {
                    return res.sendStatus(404);
                }

                if (typeof result === 'object' && result.status) {
                    if (result.redirect) {
                        return res.status(result.status).redirect(result.redirect);
                    }

                    if (result.data) {
                        return res.status(result.status).send(result.data);
                    }

                    return res.sendStatus(result.status);
                }

                return res.status(200).send(result);
            } catch (e) {
                const response = {
                    message: e.message || e.title,
                    stack: process.env.NODE_ENV === 'development' ? e.stack || (e.meta && e.meta.stack) : undefined,
                };
                logger.error(e, response, handlerFunction.toString());
                return res.status(e.code || 500).send(response);
            }
        };
    };

    function setupRoute(route, { method, validator, handler }) {
        switch (method) {
            case 'GET':
                app.get(route, validator || fallBackMiddleWare, handlerMiddleWare(handler));
                break;
            case 'DELETE':
                app.delete(route, validator || fallBackMiddleWare, handlerMiddleWare(handler));
                break;
            case 'POST':
                app.post(route, validator || fallBackMiddleWare, handlerMiddleWare(handler));
                break;
            case 'PUT':
                app.put(route, validator || fallBackMiddleWare, handlerMiddleWare(handler));
                break;
        }
    }

    return {
        setupRoute,
    };
};
