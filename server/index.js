const bodyParser = require('body-parser');
const express = require('express');
const helmet = require('helmet');
const multer = require('multer');
const cors = require('cors');
const moment = require('moment');
const ical = require('ical-generator');
const uuidv4 = require('uuid/v4');

const BASE_PATH = '/api';
const VALID_METHODS = ['GET', 'DELETE', 'POST', 'PUT'];

const connectedClients = {};
const presenceSubscriptions = {};

const server = function ServerBuilder({ port, hostname } = {}) {
    const compression = require('compression');
    const app = express();

    require('@cypress/code-coverage/middleware/express')(app);

    const storage = multer.diskStorage({
        destination: function(req, file, cb) {
            cb(null, 'files/uploads');
        },
        filename: function(req, file, cb) {
            cb(null, file.originalname);
        },
    });

    const upload = multer({ storage });

    const start = function(dependencies) {
        const {
            //utils
            logger,
            objectUtil,

            //services
            mailService,
            jsonService,
            validationService,
            pdfService,

            authenticationService,
            repetitionsService,
        } = dependencies;

        const server = app.listen(port, () => {
            return logger.success(`Server successfully started on port: ${port}`);
        });
        const io = require('socket.io')(server, { path: '/websocket', pingTimeout: 30000 });

        app.use(helmet());
        app.use(compression());
        app.use(
            cors({
                credentials: true,
                origin: (origin, callback) => {
                    if (
                        !origin ||
                        origin.includes('192.168.0.151') || //domain specific
                        origin.includes('fanvandimpna') || //domain specific
                        origin.includes('historalia') || //domain specific
                        origin.includes('157.245.67.126') || //prod-env ip address
                        origin.includes('68.183.61.134') || //test-env ip address
                        origin.includes('localhost') //for local development
                    ) {
                        return callback(null, true);
                    }

                    callback(new Error(`Origin ${origin} Not allowed by CORS`));
                },
            })
        );
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(bodyParser.json({ strict: false, limit: '10mb' }));

        app.use('/api/files', express.static('files'));
        app.use('/api', (req, _, next) => {
            logger.trace(`Request retrieved on ${req.originalUrl} (${req.method})`);

            if (Object.keys(req.body).length > 0) {
                logger.trace(`Request has a body ${jsonService.cleanString(req.body)}`);
            }

            next();
        });
        app.use('/api', (req, _, next) => {
            logger.file('trace.log', `${req.method}: ${req.originalUrl}`);
            next();
        });
        app.use(
            '/api/logo.png',
            /* istanbul ignore next */
            async (req, res, next) => {
                const { uuid } = req.query;

                if (uuid) {
                    const [record] = await mailService.opened.read({ uuid });
                    if (record && !record.opened) {
                        await mailService.opened.update(
                            { uuid },
                            { opened: true, openedDt: require('moment')().format('YYYY-MM-DD HH:mm:ss') }
                        );
                    } else {
                        logger.warn(`Uuid ${uuid} was not found`);
                    }
                } else {
                    logger.error('Image was requested without uuid');
                }

                next();
            },
            express.static(__dirname + '/../files/logo.png')
        );

        const httpAdapter = require('./adapters/http')(app, dependencies);

        function setupRoutes(config) {
            if (!config) {
                return;
            }

            Object.keys(config).forEach(key => {
                const methods = objectUtil.getPresentProperties(config[key], VALID_METHODS);
                methods.forEach(method => {
                    httpAdapter.setupRoute(`${BASE_PATH}/${key}`, {
                        method,
                        handler: config[key][method].handler,
                        validator: validationService.validateRoute(
                            config[key][method].validator,
                            config[key][method].requiredRights
                        ),
                        legacy: config[key][method].legacy,
                    });
                });

                setupRoutes(
                    Object.entries(config[key]).reduce((acc, [subRoute, value]) => {
                        if (!VALID_METHODS.includes(subRoute)) {
                            acc[key + '/' + subRoute] = value;
                        }
                        return acc;
                    }, {})
                );
            });
        }

        const routes = Object.entries(dependencies).reduce((acc, [key, value]) => {
            if (key.endsWith('Routes')) {
                acc[key] = value;
            }
            return acc;
        }, {});

        const serverDependencies = {
            io,
            connectedClients,
            presenceSubscriptions,
            hostname,
        };

        for (let i in routes) {
            const route = routes[i];

            setupRoutes(typeof route === 'function' ? route(serverDependencies) : route);
        }

        // VERIFIED
        app.post('/api/upload', upload.single('file'), function(req, res) {
            return res.sendStatus(200);
        });

        app.get('/api/download', function(req, res) {
            const file = `${process.cwd()}/files/uploads/${req.query.file}`;
            res.download(file);
        });

        app.get('/api/repetitions/ical', async function(req, res) {
            const { id, level } = await authenticationService.verifyToken(req.query.token);
            const repetitions = await repetitionsService.getRepetitionsForUser(id, level);
            const calendar = ical({ name: 'Repetities' });
            for (let repetition of repetitions) {
                if (!repetition.uuid) {
                    const uuid = uuidv4();
                    repetition.uuid = uuid;
                    await repetitionsService.update({ id: repetition.id }, { uuid });
                }
                calendar.createEvent({
                    id: repetition.uuid,
                    start: moment(repetition.startDt),
                    end: moment(repetition.endDt),
                    summary: `${repetition.name}`,
                    description: repetition.description,
                    location: `${repetition.location.name}\n${repetition.location.streetName} ${repetition.location.houseNumber}\n${repetition.location.zipCode} ${repetition.location.cityName}`,
                });
            }
            return calendar.serve(res);
        });

        // VERIFIED
        app.use('/api/uploads', express.static(`${process.cwd()}/files/uploads`));

        // VERIFIED
        app.get('/api/order/:id/pdf', async (req, res) => {
            const pdfData = await pdfService.getOrderDocument(parseInt(req.params.id));
            res.writeHead(200, {
                'Content-Length': Buffer.byteLength(pdfData),
                'Content-Type': 'application/pdf',
            }).end(pdfData);
        });

        app.get('/api/repetitions/pdf', async (req, res) => {
            const pdfData = await pdfService.getRepetitionsDocument(req);
            res.writeHead(200, {
                'Content-Length': Buffer.byteLength(pdfData),
                'Content-Type': 'application/pdf',
            }).end(pdfData);
        });

        app.use('/api', (req, res) => {
            logger.error(`Requested ${req.path} on /api, but route does not exist`);
            return res.sendStatus(404);
        });

        server.on('error', error => {
            return logger.error(error.code, error.message);
        });

        process.on('uncaughtException', function(err) {
            logger.fatal('Caught exception: ', err);
        });

        [`exit`, `SIGINT`, `SIGTERM`, `SIGQUIT`].forEach(eventType => {
            process.on(eventType, () => {
                io.emit('logout');
                process.exit(1);
            });
        });
    };

    return {
        start,
    };
};

module.exports = server;
