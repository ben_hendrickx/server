const onlineUsers = [];

module.exports = function({ sortingService }) {
    function remove(config) {
        const index = onlineUsers.findIndex(user => {
            return Object.entries(config).every(([key, value]) => user[key] === value);
        });

        index > -1 && onlineUsers.splice(index, 1);
    }

    function add(user) {
        const { email } = user;
        remove({ email });
        user.since = Date.now();
        user.lastActivity = Date.now();
        onlineUsers.push(user);
    }

    function get() {
        return sortingService.sortBy(onlineUsers, 'lastActivity', true);
    }

    function update(user) {
        const config = { email: user.email };
        const index = onlineUsers.findIndex(user => {
            return Object.entries(config).every(([key, value]) => user[key] === value);
        });

        if (index > -1) {
            onlineUsers[index].lastActivity = Date.now();
        } else {
            add(user);
        }
    }

    return {
        add,
        remove,
        get,
        update,
    };
};
