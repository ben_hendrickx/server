/* istanbul ignore file */
const xlsx = require('node-xlsx');
const moment = require('moment');

module.exports = function reportsService({
    mysql,
    repetitionsService,
    scenesService,
    groupsService,
    projectRolesService,
    crudService,
    repetitionUsersQuery,
    queryService,
    articlesQuery,
    projectUserArticlesQuery,
    sortingService,
}) {
    const json2Array = function(result, fields) {
        let out = [];
        let temp = [];

        // Create headers array
        fields.forEach(item => {
            temp.push(item.name);
        });

        // temp array works as column headers in .xlsx file
        out.push(temp);

        result.forEach(item => {
            let row = [];
            Object.entries(item).forEach(([key, value]) => (row[temp.indexOf(key)] = value));
            out.push(row);
        });

        return out;
    };

    async function getProjectUsersReport(query) {
        const [result, fields] = await mysql.raw(query, true);

        const data = json2Array(result, fields);

        return xlsx.build([{ name: 'artikels_projecten', data: data }]);
    }

    async function getProjectsReport() {
        const [result, fields] = await mysql.raw(
            `SELECT
                projects.id as id,
                projects.name as naam,
                projects.title as titel,
                projects.subtitle as omschrijving,
                DATE_FORMAT(projects.openFrom, '%d/%m/%Y') as Geopend_vanaf,
                DATE_FORMAT(projects.closedFrom, '%d/%m/%Y') as Onstage_open_tot_eind,
                DATE_FORMAT(projects.closedOffstage, '%d/%m/%Y') as Offstage_open_tot_eind,
                DATE_FORMAT(projects.editRegistrationClosed, '%d/%m/%Y') as Wijzigen_tot_eind,
                DATE_FORMAT(projects.deleteRegistrationClosed, '%d/%m/%Y') as Uitschrijven_tot_eind,
                DATE_FORMAT(projects.rolesOpen, '%d/%m/%Y') as Rollenschema_open_vanaf,
                DATE_FORMAT(projects.rolesClosed, '%d/%m/%Y') as Rollenschema_open_tot_eind,
                projects.extended as Uitgebreide_versie,
                projects.auditions_enabled as Heeft_audities
            FROM projects
            `,
            true
        );

        const data = json2Array(result, fields);

        return xlsx.build([{ name: 'projecten', data: data }]);
    }

    async function getUsersReport(filters) {
        const [result, fields] = await mysql.raw(
            `SELECT
                users.id as id,
                users.firstName as Voornaam,
                users.lastName as Achternaam,
                users.email as Emailadres,
                DATE_FORMAT(date_of_birth, '%d/%m/%Y') as Geboortedatum,
                YEAR(CURDATE()) - YEAR(date_of_birth) AS Leeftijd,
                IF(users.sex is null, '', IF(users.sex = 1, 'Vrouw','Man')) as Geslacht,
                users.nationality as Nationaliteit,
                users.origin as Geboorteland,
                users.streetName as Straatnaam,
                users.houseNumber as Huisnummer,
                users.zipCode as Postcode,
                users.cityName as Gemeente,
                users.phoneNumber as Telefoonnummer,
                users.cellPhone as Gsm,
                users.secondaryEmail as Tweede_mailadres,
                users.owner_remarks as Opmerkingen_over_gebruiker_van_beheerder,
                user_sizes.remarks as Opmerkingen_over_kledijmaten_van_beheerder,
                DATE_FORMAT(users.creationDt, '%d/%m/%Y') as Geregistreerd,
                DATE_FORMAT(logins.lastLoggedIn, '%d/%m/%Y') as Laatst_ingelogd,
                confirmed as Mail_bevestigd,
                subscribed as Nieuwbrief,
                height as Lichaamslengte,
                chest as Borstomtrek,
                waist as Taille,
                hipSize as Heupomtrek,
                headSize as Hoofdomtrek,
                clothingSize as Kledingmaat,
                shirt_sizes.name as Tshirtmaat,
                pin as Pincode,
                pictures as Fotos
            FROM users
            LEFT JOIN logins ON users.email=logins.email
            LEFT JOIN user_sizes ON users.id=user_sizes.userId
            LEFT JOIN user_app_settings ON users.id=user_app_settings.userId
            LEFT JOIN shirt_sizes ON user_sizes.shirtSize=shirt_sizes.id
            WHERE user_app_settings.level=0`,
            true
        );

        const data = json2Array(
            result.filter(user => {
                if (filters.minAge) {
                    if (parseInt(filters.minAge) > user.Leeftijd) {
                        return false;
                    }
                }

                if (filters.maxAge) {
                    if (parseInt(filters.maxAge) < user.Leeftijd) {
                        return false;
                    }
                }

                if (filters.sex) {
                    if (user.Geslacht !== filters.sex) {
                        return false;
                    }
                }

                return true;
            }),
            fields
        );

        return xlsx.build([{ name: 'Users', data: data }]);
    }

    async function getRegistrationsReport(id, filters) {
        const [result, fields] = await mysql.raw(
            `SELECT
                projects.name,
                users.id as id,
                users.firstName as Voornaam,
                users.lastName as Achternaam,
                users.email as Emailadres,
                DATE_FORMAT(date_of_birth, '%d/%m/%Y') as Geboortedatum,
                YEAR(CURDATE()) - YEAR(date_of_birth) AS Leeftijd,
                IF(users.sex is null, '', IF(users.sex = 1, 'Vrouw','Man')) as Geslacht,
                users.nationality as Nationaliteit,
                users.origin as Geboorteland,
                users.streetName as Straatnaam,
                users.houseNumber as Huisnummer,
                users.zipCode as Postcode,
                users.cityName as Gemeente,
                users.phoneNumber as Telefoonnummer,
                users.cellPhone as Gsm,
                users.secondaryEmail as Tweede_mailadres,
                talents as Talenten,
                users.owner_remarks as Opmerkingen_over_gebruiker_van_beheerder,
                registrations.remarks as Opmerkingen_bij_inschrijving_van_gebruiker,
                registrations.owner_remarks as Opmerkingen_bij_inschrijving_van_beheerder,
                user_sizes.remarks as Opmerkingen_over_kledijmaten_van_beheerder,
                registrations.id as registrationId,
                DATE_FORMAT(registrations.creationDt, '%d/%m/%Y') as Inschrijvingsdatum,
                IF(auditioning, 'Ja', 'Nee') as Auditie,
                height as Lichaamslengte,
                chest as Borstomtrek,
                waist as Taille,
                hipSize as Heupomtrek,
                headSize as Hoofdomtrek,
                clothingSize as Kledingmaat,
                shirt_sizes.name as Tshirtmaat,
                pin as Pincode
            FROM users
            LEFT JOIN user_sizes ON users.id=user_sizes.userId
            LEFT JOIN shirt_sizes ON user_sizes.shirtSize=shirt_sizes.id
            INNER JOIN user_app_settings ON users.id=user_app_settings.userId
            INNER JOIN project_user_registrations ON project_user_registrations.userId=users.id
            INNER JOIN projects ON projects.id=project_user_registrations.projectId
            INNER JOIN registrations ON project_user_registrations.registrationId=registrations.id
            WHERE user_app_settings.level=0 AND projects.id=${id}`,
            true
        );

        const roles = await mysql.get('roles');

        const newResult = [];
        for (let i = 0; i < result.length; i++) {
            const { registrationId } = result[i];
            const rolesForRegistration = await mysql.get('registrations_roles', { registrationId });

            const resultWithRoles = { ...result[i] };

            roles.forEach(({ id, role, onStage }) => {
                resultWithRoles[role] = rolesForRegistration.some(({ roleId }) => {
                    if (roleId === id) {
                        resultWithRoles['OnStage'] = onStage ? 'Ja' : 'Nee';
                        return true;
                    }
                    return false;
                })
                    ? 'X'
                    : '';
                !resultWithRoles['OnStage'] && (resultWithRoles['OnStage'] = 'Nee');
            });

            newResult.push(resultWithRoles);
        }

        const data = json2Array(
            newResult.filter(registration => {
                if (filters.onStage) {
                    if (filters.onStage === 'Ja' && registration.OnStage !== 'Ja') {
                        return false;
                    }

                    if (filters.onStage === 'Neen' && registration.OnStage !== 'Nee') {
                        return false;
                    }
                }

                if (filters.auditioning) {
                    if (filters.auditioning === 'Ja' && registration.Auditie !== 'Ja') {
                        return false;
                    }

                    if (filters.auditioning === 'Neen' && registration.Auditie !== 'Nee') {
                        return false;
                    }
                }

                if (filters.remarks) {
                    if (filters.remarks === 'Ja' && !registration.Opmerkingen) {
                        return false;
                    }

                    if (filters.remarks === 'Neen' && registration.Opmerkingen) {
                        return false;
                    }
                }

                if (filters.talents) {
                    if (filters.talents === 'Ja' && !registration.Talenten) {
                        return false;
                    }

                    if (filters.talents === 'Neen' && registration.Talenten) {
                        return false;
                    }
                }

                return true;
            }),
            [...fields, { name: 'OnStage' }, ...roles.map(({ role }) => ({ name: role }))]
        );

        return xlsx.build([{ name: 'Registrations', data }]);
    }

    async function getNotRegisteredReport() {
        const [result, fields] = await mysql.raw(
            `SELECT
                users.id as id,
                users.firstName as Voornaam,
                users.lastName as Achternaam,
                users.email as Emailadres,
                DATE_FORMAT(date_of_birth, '%d/%m/%Y') as Geboortedatum,
                YEAR(CURDATE()) - YEAR(date_of_birth) AS Leeftijd,
                IF(users.sex is null, '', IF(users.sex = 1, 'Vrouw','Man')) as Geslacht,
                users.nationality as Nationaliteit,
                users.origin as Geboorteland,
                users.streetName as Straatnaam,
                users.houseNumber as Huisnummer,
                users.zipCode as Postcode,
                users.cityName as Gemeente,
                users.phoneNumber as Telefoonnummer,
                users.cellPhone as Gsm,
                users.secondaryEmail as Tweede_mailadres,
                users.owner_remarks as Opmerkingen_over_gebruiker_van_beheerder,
                user_sizes.remarks as Opmerkingen_over_kledijmaten_van_beheerder,
                DATE_FORMAT(users.creationDt, '%d/%m/%Y') as Geregistreerd,
                confirmed as Mail_bevestigd,
                subscribed as Nieuwbrief,
                height as Lichaamslengte,
                chest as Borstomtrek,
                waist as Taille,
                hipSize as Heupomtrek,
                headSize as Hoofdomtrek,
                clothingSize as Kledingmaat,
                shirt_sizes.name as Tshirtmaat,
                pin as Pincode,
                pictures as Fotos
            FROM users
            INNER JOIN logins ON users.email=logins.email
            INNER JOIN user_sizes ON users.id=user_sizes.userId
            INNER JOIN user_app_settings ON users.id=user_app_settings.userId
            INNER JOIN shirt_sizes ON user_sizes.shirtSize=shirt_sizes.id
            WHERE user_app_settings.level=0`,
            true
        );

        const userRegistrations = ((await mysql.get('project_user_registrations')) || []).map(({ userId }) => userId);

        const filteredUsers = result.filter(({ id }) => !userRegistrations.includes(id));

        const data = json2Array(filteredUsers, fields);

        return xlsx.build([{ name: 'Users', data }]);
    }

    async function getRolesReport(projectId) {
        const [result, fields] = await mysql.raw(
            `SELECT
                c.name as Cast,
                pr.identifier as Volgnummer,
                pr.name as Naam,
                u.firstName as Voornaam,
                u.lastName as Achternaam,
                u.email as Emailadres,
                DATE_FORMAT(date_of_birth, '%d/%m/%Y') as Geboortedatum,
                YEAR(CURDATE()) - YEAR(date_of_birth) AS Leeftijd,
                IF(u.sex is null, '', IF(u.sex = 1, 'Vrouw','Man')) as Geslacht
            FROM project_roles as pr
            LEFT JOIN project_role_users as pru ON pr.id=pru.project_role_id
            LEFT JOIN casts as c ON pru.castId=c.id
            LEFT JOIN users as u ON pru.userId=u.id
            WHERE pr.projectId=${projectId}`,
            true
        );

        const data = json2Array(result, fields);

        return xlsx.build([{ name: 'Roles', data }]);
    }

    async function getGroupsReport(projectId) {
        const [result, fields] = await mysql.raw(
            `SELECT
                c.name as Cast,
                g.identifier as Volgnummer,
                g.name as Naam,
                u.firstName as Voornaam,
                u.lastName as Achternaam,
                u.email as Emailadres,
                DATE_FORMAT(date_of_birth, '%d/%m/%Y') as Geboortedatum,
                YEAR(CURDATE()) - YEAR(date_of_birth) AS Leeftijd,
                IF(u.sex is null, '', IF(u.sex = 1, 'Vrouw','Man')) as Geslacht
            FROM groupes as g
            LEFT JOIN group_users as gu ON gu.groupId=g.id
            LEFT JOIN casts as c ON gu.castId=c.id
            LEFT JOIN users as u ON gu.userId=u.id
            WHERE g.projectId=${projectId}`,
            true
        );

        const data = json2Array(result, fields);

        return xlsx.build([{ name: 'Groups', data }]);
    }

    async function getScenesReport(projectId) {
        const [roles, roleFields] = await mysql.raw(
            `SELECT
                s.identifier as Volgnummer,
                s.name as Scenenaam,
                pr.identifier as Rol_Groep_nummer,
                pr.name as Rol_Groep_naam,
                c.name as Cast,
                u.firstName as Voornaam,
                u.lastName as Achternaam,
                u.email as Emailadres,
                DATE_FORMAT(date_of_birth, '%d/%m/%Y') as Geboortedatum,
                YEAR(CURDATE()) - YEAR(date_of_birth) AS Leeftijd,
                IF(u.sex is null, '', IF(u.sex = 1, 'Vrouw','Man')) as Geslacht
            FROM scenes as s
                LEFT JOIN scene_roles AS sr ON sr.sceneId=s.id
                LEFT JOIN project_roles AS pr ON pr.id=sr.project_role_id
                LEFT JOIN project_role_users AS pru ON pru.project_role_id=pr.id
                LEFT JOIN users AS u ON pru.userId=u.id
                LEFT JOIN casts AS c ON pru.castId=c.id
            WHERE s.projectId=${projectId}`,
            true
        );

        const [groups] = await mysql.raw(
            `SELECT
                s.identifier as Volgnummer,
                s.name as Scenenaam,
                g.identifier as Rol_Groep_nummer,
                g.name as Rol_Groep_naam,
                c.name as Cast,
                u.firstName as Voornaam,
                u.lastName as Achternaam,
                u.email as Emailadres,
                DATE_FORMAT(date_of_birth, '%d/%m/%Y') as Geboortedatum,
                YEAR(CURDATE()) - YEAR(date_of_birth) AS Leeftijd,
                IF(u.sex is null, '', IF(u.sex = 1, 'Vrouw','Man')) as Geslacht
            FROM scenes as s
                LEFT JOIN scene_groups AS sg ON sg.sceneId=s.id
                LEFT JOIN groupes AS g ON g.id=sg.groupId
                LEFT JOIN group_users AS gu ON gu.groupId=g.id
                LEFT JOIN users AS u ON gu.userId=u.id
                LEFT JOIN casts AS c ON gu.castId=c.id
            WHERE s.projectId=${projectId}`,
            true
        );

        const result = roles.concat(groups);

        const data = json2Array(result, roleFields);

        return xlsx.build([{ name: 'Scenes', data }]);
    }

    async function getRepetitionsReport({ projectId }) {
        const [repetitions, fields] = await mysql.raw(
            `SELECT
                r.id as id,
                r.identifier as Volgnummer,
                t.name as Type,
                l.name as Locatie,
                r.name as Naam,
                r.description as Omschrijving,
                r.startDt,
                r.endDt,
                IF(r.open, 'Ja','Nee') as Geopend,
                IF(r.allChecked, 'Ja','Nee') as Iedereen_uitgenodigd
            FROM repetitions as r
            INNER JOIN locations as l ON l.id=r.locationId
            INNER JOIN types as t ON t.id=r.typeId
            WHERE r.projectId=${projectId}
            ORDER BY r.startDt ASC`,
            true
        );

        const moment = require('moment');

        const data = json2Array(
            repetitions.map(repetition => {
                let startDt = moment(repetition.startDt);
                let endDt = moment(repetition.endDt);

                repetition.Datum = startDt.format('DD-MM-YYYY');
                repetition.Start_uur = startDt.format('HH:mm');
                repetition.Eind_uur = endDt.format('HH:mm');
                return repetition;
            }),
            fields
                .concat([{ name: 'Datum' }, { name: 'Start_uur' }, { name: 'Eind_uur' }])
                .filter(({ name }) => !['startDt', 'endDt'].includes(name))
        );

        return xlsx.build([{ name: 'Repetitions', data }]);
    }

    async function getRepetitionData(repetitionId, withPresence = true) {
        let [userRows, [repetition], userPresence, locations] = await Promise.all([
            await repetitionsService.users.read({ repetitionId }),
            await repetitionsService.read({ id: repetitionId }),
            withPresence ? await repetitionsService.presence.read({ repetitionId }) : [],
            await crudService('locations').read(),
        ]);

        if (repetition.allChecked) {
            const [scenes, sceneGroups, sceneRoles, groupUsers, roleUsers] = await Promise.all([
                scenesService.getScenes(),
                scenesService.getSceneGroups(),
                scenesService.getSceneRoles(),
                groupsService.getGroupUsers(),
                projectRolesService.users.read(),
            ]);

            userRows = [];

            scenes
                .filter(scene => scene.projectId === repetition.projectId)
                .forEach(scene => {
                    const sGroups = sceneGroups
                        .filter(sceneGroup => sceneGroup.sceneId === scene.id)
                        .map(({ groupId }) => groupId);
                    const sRoles = sceneRoles
                        .filter(sceneRole => sceneRole.sceneId === scene.id)
                        .map(({ project_role_id }) => project_role_id);

                    const gUsers = groupUsers.filter(groupUser => sGroups.includes(groupUser.groupId));
                    const rUsers = roleUsers.filter(roleUser => sRoles.includes(roleUser.project_role_id));

                    gUsers.concat(rUsers).forEach(({ userId }) => {
                        if (!userRows.find(user => user.userId === userId)) {
                            userRows.push({
                                userId,
                            });
                        }
                    });
                });
        }

        let tmp = await Promise.all(userRows.map(row => repetitionsService.resolveUserRow(row)));
        tmp = tmp.flat().flat();

        const moment = require('moment');

        let startDt = moment(repetition.startDt);
        let endDt = moment(repetition.endDt);

        const location = locations.find(({ id }) => id === repetition.locationId);

        return tmp.reduce((acc, row) => {
            const gr = row.role ? row.role : row.group;
            row.users.forEach(user => {
                const birthYear = new Date(user.date_of_birth).getFullYear();
                const age = new Date().getFullYear() - birthYear;

                const item = {
                    Repetitie_volgnummer: repetition.identifier,
                    Repetitie_naam: repetition.name,
                    Repetitie_datum: startDt.format('DD-MM-YYYY'),
                    Repetitie_start_uur: startDt.format('HH:mm'),
                    Repetitie_eind_uur: endDt.format('HH:mm'),
                    Repetitie_locatie: location.name,
                    Repetitie_locatie_adres: `${location.streetName} ${location.houseNumber}, ${location.zipCode} ${location.cityName}`,
                    Scene_volgnummer: (row.scene || {}).identifier,
                    Scene_naam: (row.scene || {}).name,
                    Groep_of_rolnummer: (gr || {}).identifier,
                    Groep_of_rolnaam: (gr || {}).name,
                    Cast: (row.cast || {}).name,
                    Voornaam: user.firstName,
                    Achternaam: user.lastName,
                    Pin: user.pin || '',
                    Emailadres: user.email,
                    Geboortedatum: require('moment')(user.date_of_birth).format('DD-MM-YYYY'),
                    Leeftijd: age,
                    Geslacht: user.sex === null ? '' : user.sex === 0 ? 'Man' : 'Vrouw',
                    Nationaliteit: user.nationality,
                    Geboorteland: user.origin,
                    Straatnaam: user.streetName,
                    Huisnummer: user.houseNumber,
                    Postcode: user.zipCode,
                    Gemeente: user.cityName,
                    Telefoonnummer: user.phoneNumber,
                    Gsm: user.cellPhone,
                    Tweede_mailadres: user.secondaryEmail,
                    Opmerkingen_over_gebruiker_van_beheerder: user.owner_remarks,
                };

                if (withPresence) {
                    const hasPresence = userPresence.find(({ userId }) => userId === user.id);
                    item.Aanwezig = hasPresence
                        ? hasPresence.present
                            ? 'Aanwezig'
                            : !hasPresence.markedPresent
                            ? 'Gewettigd afwezig'
                            : new Date(repetition.startDt).getTime() + 15 * 60 * 1000 < Date.now()
                            ? 'Ongewettigd afwezig'
                            : 'Waarschijnlijk aanwezig'
                        : new Date(repetition.startDt).getTime() + 15 * 60 * 1000 < Date.now()
                        ? 'Ongewettigd afwezig'
                        : 'Waarschijnlijk aanwezig';
                }

                acc.push(item);
            });
            return acc;
        }, []);
    }

    async function getRepetitionReport({ repetitionId }) {
        const fields = [
            'Repetitie_volgnummer',
            'Repetitie_naam',
            'Repetitie_datum',
            'Repetitie_start_uur',
            'Repetitie_eind_uur',
            'Scene_volgnummer',
            'Scene_naam',
            'Groep_of_rolnummer',
            'Groep_of_rolnaam',
            'Cast',
            'Aanwezig',
            'Voornaam',
            'Achternaam',
            'Pin',
            'Emailadres',
            'Geboortedatum',
            'Leeftijd',
            'Geslacht',
            'Nationaliteit',
            'Geboorteland',
            'Straatnaam',
            'Huisnummer',
            'Postcode',
            'Gemeente',
            'Telefoonnummer',
            'Gsm',
            'Tweede_mailadres',
            'Opmerkingen_over_gebruiker_van_beheerder',
        ];

        const data = json2Array(
            await getRepetitionData(repetitionId),
            fields.map(f => ({ name: f }))
        );

        return xlsx.build([{ name: 'Repetition', data }]);
    }

    async function getCompletePresenceReport({ projectId }) {
        const fields = [
            'Repetitie_volgnummer',
            'Repetitie_naam',
            'Repetitie_datum',
            'Repetitie_start_uur',
            'Repetitie_eind_uur',
            'Repetitie_locatie',
            'Repetitie_locatie_adres',
            'Aanwezig',
            'Voornaam',
            'Achternaam',
            'Emailadres',
        ];

        const repetitions = await repetitionsService.read({ projectId });
        const locations = await crudService('locations').read();
        const rows = await Promise.all(
            repetitions.map(async r => {
                return (await repetitionsService.getUserPresence(r.id, false, false)).map(u => {
                    const location = locations.find(l => l.id === r.locationId);
                    const startDt = moment(r.startDt);
                    const endDt = moment(r.endDt);

                    return {
                        Repetitie_volgnummer: r.identifier,
                        Repetitie_naam: r.name,
                        Repetitie_datum: startDt.format('DD-MM-YYYY'),
                        Repetitie_start_uur: startDt.format('HH:mm'),
                        Repetitie_eind_uur: endDt.format('HH:mm'),
                        Repetitie_locatie: location.name,
                        Repetitie_locatie_adres: `${location.streetName} ${location.houseNumber}, ${location.zipCode} ${location.cityName}`,
                        Aanwezig: u.present
                            ? 'Aanwezig'
                            : !u.markedPresent
                            ? 'Gewettigd afwezig'
                            : startDt.add(15, 'm').isBefore(moment())
                            ? 'Ongewettigd afwezig'
                            : 'Waarschijnlijk aanwezig',
                        Voornaam: u.firstName,
                        Achternaam: u.lastName,
                        Emailadres: u.email,
                    };
                });
            })
        );

        const data = json2Array(
            rows.flat(),
            fields.map(f => ({ name: f }))
        );

        return xlsx.build([{ name: 'Presence_complete', data }]);
    }

    async function getCompleteRepetitionsReport({ projectId }) {
        const fields = [
            'Repetitie_volgnummer',
            'Repetitie_naam',
            'Repetitie_datum',
            'Repetitie_start_uur',
            'Repetitie_eind_uur',
            'Repetitie_locatie',
            'Repetitie_locatie_adres',
            'Scene_volgnummer',
            'Scene_naam',
            'Groep_of_rolnummer',
            'Groep_of_rolnaam',
            'Cast',
            'Voornaam',
            'Achternaam',
            'Emailadres',
            'Geboortedatum',
            'Leeftijd',
            'Geslacht',
            'Telefoonnummer',
            'Gsm',
        ];

        const allCheckedRolesQueryConfig = repetitionUsersQuery.allChecked.roles({ projectId });
        const allCheckedGroupsQueryConfig = repetitionUsersQuery.allChecked.groups({ projectId });
        const sceneRolesQueryConfig = repetitionUsersQuery.scenes.roles({ projectId });
        const sceneGroupsQueryConfig = repetitionUsersQuery.scenes.groups({ projectId });
        const rolesQueryConfig = repetitionUsersQuery.roles({ projectId });
        const groupsQueryConfig = repetitionUsersQuery.groups({ projectId });
        const usersQueryConfig = repetitionUsersQuery.users({ projectId });
        const { query: allCheckedRolesQuery } = await queryService(allCheckedRolesQueryConfig);
        const { query: allCheckedGroupsQuery } = await queryService(allCheckedGroupsQueryConfig);
        const { query: sceneRolesQuery } = await queryService(sceneRolesQueryConfig);
        const { query: sceneGroupsQuery } = await queryService(sceneGroupsQueryConfig);
        const { query: rolesQuery } = await queryService(rolesQueryConfig);
        const { query: groupsQuery } = await queryService(groupsQueryConfig);
        const { query: usersQuery } = await queryService(usersQueryConfig);
        let repetitions = [
            ...(await mysql.raw(allCheckedGroupsQuery)),
            ...(await mysql.raw(allCheckedRolesQuery)),
            ...(await mysql.raw(sceneRolesQuery)),
            ...(await mysql.raw(sceneGroupsQuery)),
            ...(await mysql.raw(rolesQuery)),
            ...(await mysql.raw(groupsQuery)),
            ...(await mysql.raw(usersQuery)),
        ];

        repetitions = sortingService.sortBy(repetitions.flat(), 'startDt', true, true);

        const data = json2Array(
            repetitions,
            fields.map(f => ({ name: f }))
        );

        await require('fs').promises.writeFile(
            process.cwd() + '/files/Repetitions_complete.csv',
            data.map(row => row.join(';')).join('\r\n')
        );

        //return xlsx.build([{ name: 'Repetitions_complete', data }]);
    }

    async function getUsersImportReport() {
        const fields = [
            'Voornaam',
            'Achternaam',
            'Geboortedatum',
            'Geslacht',
            'Nationaliteit',
            'Telefoonnummer',
            'Gsm nummer',
            'Mailadres',
            'Bevestig mailadres',
            'Tweede mailadres',
            'Straatnaam',
            'Huisnummer',
            'Busnummer',
            'Postcode',
            'Gemeente',
            'T-shirt maat',
            '',
            'Vereniging',
        ];

        const data = json2Array(
            [],
            fields.map(f => ({ name: f }))
        );

        return xlsx.build([{ name: 'import_users_template', data }]);
    }

    async function getRolesImportReport() {
        const fields = ['Cast', 'Rol/Groep nummer', 'Voornaam', 'Achternaam', 'Email'];

        const data = json2Array(
            [],
            fields.map(f => ({ name: f }))
        );

        return xlsx.build([{ name: 'import_role_users_template', data }]);
    }

    async function getRoleSchemeImportReport() {
        const fields = [
            'Scene nummer',
            'Scene naam',
            'Scene omschrijving',
            'Scene groep nummer',
            'Rol/Groep nummer',
            'Rol/Groep naam',
            'Rol/Groep omschrijving',
            'Is individuele rol',
            'Is sprekende rol',
        ];

        const data = json2Array(
            [],
            fields.map(f => ({ name: f }))
        );

        return xlsx.build([{ name: 'import_role_scheme_template', data }]);
    }

    async function getRoleSchemeReport(projectId) {
        const fields = [
            'Scene nummer',
            'Scene naam',
            'Scene omschrijving',
            'Scene groep nummer',
            'Rol/Groep nummer',
            'Rol/Groep naam',
            'Rol/Groep omschrijving',
            'Is individuele rol',
            'Is sprekende rol',
        ];

        const [groupOfSceneRows] = await mysql.raw(
            `SELECT
                s.identifier as 'Scene nummer',
                s.name as 'Scene naam',
                s.description as 'Scene omschrijving',
                'x' as 'Scene groep nummer',
                '' as 'Rol/Groep nummer',
                '' as 'Rol/Groep naam',
                '' as 'Rol/Groep omschrijving',
                '' as 'Is individuele rol',
                '' as 'Is sprekende rol'
            FROM group_of_scenes as s
            WHERE s.projectId=${projectId}`,
            true
        );

        const [roleRows] = await mysql.raw(
            `SELECT
                s.identifier as 'Scene nummer',
                s.name as 'Scene naam',
                s.description as 'Scene omschrijving',
                gOS.identifier as 'Scene groep nummer',
                pR.identifier as 'Rol/Groep nummer',
                pR.name as 'Rol/Groep naam',
                pR.description as 'Rol/Groep omschrijving',
                'x' as 'Is individuele rol',
                IF(pR.speaking, 'x', '') as 'Is sprekende rol'
            FROM scenes as s
            LEFT JOIN group_of_scenes as gOS ON gOS.id=s.groupId
            LEFT JOIN scene_roles as sR ON s.id=sR.sceneId
            LEFT JOIN project_roles as pR ON sR.project_role_id=pR.id
            WHERE s.projectId=${projectId}`,
            true
        );

        const [groupRows] = await mysql.raw(
            `SELECT
                s.identifier as 'Scene nummer',
                s.name as 'Scene naam',
                s.description as 'Scene omschrijving',
                gOS.identifier as 'Scene groep nummer',
                g.identifier as 'Rol/Groep nummer',
                g.name as 'Rol/Groep naam',
                g.description as 'Rol/Groep omschrijving',
                '' as 'Is individuele rol',
                '' as 'Is sprekende rol'
            FROM scenes as s
            LEFT JOIN group_of_scenes as gOS ON gOS.id=s.groupId
            LEFT JOIN scene_groups as sG ON s.id=sG.sceneId
            LEFT JOIN groupes as g ON sG.groupId=g.id
            WHERE s.projectId=${projectId}`,
            true
        );

        const data = json2Array(
            [...groupOfSceneRows, ...roleRows, ...groupRows],
            fields.map(f => ({ name: f }))
        );

        return xlsx.build([{ name: 'role_scheme', data }]);
    }

    async function getClothingSizesReport() {
        const fields = ['ID', 'Maat'];

        const [rows] = await mysql.raw(
            `SELECT
                acs.id as ID,
                acs.size as Maat
            FROM article_clothing_sizes as acs
            WHERE acs.deletedDt IS NULL
            ORDER BY acs.id ASC`,
            true
        );

        const data = json2Array(
            rows,
            fields.map(f => ({ name: f }))
        );

        return xlsx.build([{ name: 'article_clothing_sizes', data }]);
    }

    async function getFabricsReport() {
        const fields = ['ID', 'Stof'];

        const [rows] = await mysql.raw(
            `SELECT
                f.id as ID,
                f.type as Stof
            FROM fabrics as f
            WHERE f.deletedDt IS NULL
            ORDER BY f.id ASC`,
            true
        );

        const data = json2Array(
            rows,
            fields.map(f => ({ name: f }))
        );

        return xlsx.build([{ name: 'fabrics', data }]);
    }

    async function getPatternsReport() {
        const fields = ['ID', 'Motief'];

        const [rows] = await mysql.raw(
            `SELECT
                p.id as ID,
                p.name as Motief
            FROM patterns as p
            WHERE p.deletedDt IS NULL
            ORDER BY p.id ASC`,
            true
        );

        const data = json2Array(
            rows,
            fields.map(f => ({ name: f }))
        );

        return xlsx.build([{ name: 'patterns', data }]);
    }

    async function getPriceCategoriesReport() {
        const fields = ['ID', 'Naam', 'Binnen Geel', 'Buiten Geel', 'Vervangingswaarde', 'Geldig vanaf', 'Geldig tot'];

        const [rows] = await mysql.raw(
            `SELECT
                c.id as ID,
                c.name as Naam,
                p.internalPrice as 'Binnen Geel',
                p.externalPrice as 'Buiten Geel',
                p.replacementPrice as Vervangingswaarde,
                p.startDt as 'Geldig vanaf',
                p.endDt as 'Geldig tot'
            FROM categories as c
                LEFT JOIN (SELECT categoryId, max(startDt) as maxDate FROM category_prices GROUP BY categoryId) as cP ON (c.id = cP.categoryId)
                LEFT JOIN category_prices as p ON (p.categoryId = c.id) AND (cP.maxDate = p.startDt)
                ORDER BY c.id ASC`,
            true
        );

        const data = json2Array(
            rows,
            fields.map(f => ({ name: f }))
        );

        return xlsx.build([{ name: 'categories', data }]);
    }

    async function getOrdersReport() {
        const fields = [
            'ID',
            'Type huurder',
            'Naam',
            'Ten behoeve van',
            'Bedrijf',
            'Straat',
            'Huisnummer',
            'Bus',
            'Postcode',
            'Gemeente',
            'Telefoonnummer',
            'Emailadres',
            'Rekeningnummer',
            'Rijksregisternummer',
            'Ondernemingsnummer',
            'Vanaf',
            'Tot',
            'Aantal periodes',
            'Afkomstig van Geel',
            'Prijs',
            'Vervangingswaarde',
        ];

        const [rows] = await mysql.raw(
            `SELECT
                r.id as ID,
                IF(r.type = 1, 'Particulier', IF(r.type = 2, 'Vereniging','Bedrijf')) as 'Type huurder',
                r.name as Naam,
                r.isFor as 'Ten behoeve van',
                r.companyName as Bedrijf,
                r.streetName as Straat,
                r.houseNumber as Huisnummer,
                r.bus as Bus,
                r.zipCode as Postcode,
                r.cityName as Gemeente,
                r.cellPhone as Telefoonnummer,
                r.email as Emailadres,
                r.bankAccount as Rekeningnummer,
                IF(r.type = 1, r.uniqueNumber, '') as Rijksregisternummer,
                IF(r.type IN (2,3), r.uniqueNumber, '') as Ondernemingsnummer,
                DATE_FORMAT(r.fromDt, "%d/%m/%Y") as Vanaf,
                DATE_FORMAT(r.toDt, "%d/%m/%Y") as Tot,
                r.periods as 'Aantal periodes',
                r.internal as 'Afkomstig van Geel',
                r.price as Prijs,
                r.replacementPrice as Vervangingswaarde
            FROM rentals as r
            ORDER BY r.id ASC`,
            true
        );

        const data = json2Array(
            rows,
            fields.map(f => ({ name: f }))
        );

        return xlsx.build([{ name: 'orders', data }]);
    }

    async function getColorsReport() {
        const fields = ['ID', 'Kleur'];

        const [rows] = await mysql.raw(
            `SELECT
                c.id as ID,
                c.name as Kleur
            FROM colors as c
            WHERE c.deletedDt IS NULL
            ORDER BY c.id ASC`,
            true
        );

        const data = json2Array(
            rows,
            fields.map(f => ({ name: f }))
        );

        return xlsx.build([{ name: 'colors', data }]);
    }

    async function getRacksReport() {
        const fields = ['Nummer', 'Omschrijving'];

        const [rows] = await mysql.raw(
            `SELECT
                r.id as Nummer,
                r.description as Omschrijving
            FROM racks as r
            WHERE r.deletedDt IS NULL
            ORDER BY r.id ASC`,
            true
        );

        const data = json2Array(
            rows,
            fields.map(f => ({ name: f }))
        );

        return xlsx.build([{ name: 'racks', data }]);
    }

    async function getArticlesReport(q) {
        let { text, categoryId, clothingSizeId, colorId, fabricId, patternId, rackId, tags } = q;

        let active, washing;

        text = (text || '').toString().toLowerCase();

        if (text) {
            if (text.includes('niet actief')) {
                active = false;
            } else if (text.includes('actief')) {
                active = true;
            } else if (text.includes('in de was')) {
                washing = true;
            } else if (text.includes('uitgeleend')) {
                rackId = 0;
                active = true;
                washing = false;
            }

            text = text
                .toLowerCase()
                .replace('niet actief', '')
                .replace('actief', '')
                .replace('in de was', '')
                .replace('uitgeleend', '');
        }

        const fields = [
            'Artikelnummer',
            'Omschrijving',
            'Kledingmaat',
            'Motief',
            'Stof',
            'Kleur',
            'Actief',
            'In Was',
            'Tags',
            'Categorie',
            'Reknummer',
            'Rekomschrijving',
        ];

        const articlesQueryConfig = articlesQuery({
            fields: [
                { field: 'a.id', as: 'Artikelnummer' },
                { field: 'a.description', as: 'Omschrijving' },
                { field: 'acs.size', as: 'Kledingmaat' },
                { field: 'p.name', as: 'Motief' },
                { field: 'f.type', as: 'Stof' },
                { field: 'c.name', as: 'Kleur' },
                { field: 'a.active', as: 'Actief' },
                { field: 'a.washing', as: 'In Was' },
                { field: 'a.tags', as: 'Tags' },
                { field: 'cat.name', as: 'Categorie' },
                { field: 'r.id', as: 'Reknummer' },
                { field: 'r.description', as: 'Rekomschrijving' },
            ],
            where: [
                text !== undefined && {
                    key: [
                        'LPAD(a.id, 6, "0")',
                        'cat.name',
                        'tags',
                        'acs.size',
                        'f.type',
                        'p.name',
                        'c.name',
                        'r.description',
                        'a.description',
                        'a.rackId',
                    ],
                    operator: 'LIKE',
                    value: `%${text}%`,
                },
                active !== undefined && {
                    key: 'active',
                    value: active ? 1 : 0,
                },
                washing !== undefined && {
                    key: 'washing',
                    value: washing ? 1 : 0,
                },
                categoryId !== undefined && {
                    key: 'categoryId',
                    value: categoryId,
                },
                clothingSizeId !== undefined && {
                    key: 'clothingSizeId',
                    value: clothingSizeId,
                },
                colorId !== undefined && {
                    key: 'colorId',
                    value: colorId,
                },
                fabricId !== undefined && {
                    key: 'fabricId',
                    value: fabricId,
                },
                patternId !== undefined && {
                    key: 'patternId',
                    value: patternId,
                },
                rackId !== undefined && {
                    key: 'rackId',
                    value: rackId,
                },
                tags === 'accessoire' && {
                    key: 'tags',
                    operator: 'LIKE "%accessoire%"',
                    value: '',
                },
                tags?.toString() === 'false' && {
                    key: 'tags',
                    operator: 'NOT LIKE "%accessoire%"',
                    value: '',
                },
            ],
        });
        const { query } = await queryService(articlesQueryConfig);
        const articles = await mysql.raw(query);

        const data = json2Array(
            articles,
            fields.map(f => ({ name: f }))
        );

        await require('fs').promises.writeFile(
            process.cwd() + '/files/articles.csv',
            data.map(row => row.join(';')).join('\r\n')
        );
    }

    async function getProjectUserArticlesReport() {
        const fields = [
            'Voornaam',
            'Achternaam',
            'E-mailadres',
            'G.S.M.-nummer',
            'Project',
            'Scène-nummer',
            'Scène-naam',
            'Rol-nummer',
            'Rol-naam',
            'Groep-nummer',
            'Groep-naam',
            'Cast',
            'Status',
            'T-shirtmaat',
            'Artikelnummer',
            'Omschrijving',
            'Kledingmaat',
            'Motief',
            'Stof',
            'Kleur',
            'Actief',
            'In Was',
            'Tags',
            'Opmerkingen',
        ];

        const articlesQueryConfig = projectUserArticlesQuery({
            fields: [
                { field: 'u.firstName', as: 'Voornaam' },
                { field: 'u.lastName', as: 'Achternaam' },
                { field: 'u.email', as: 'E-mailadres' },
                { field: 'u.cellPhone', as: 'G.S.M.-nummer' },
                { field: 'p.name', as: 'Project' },
                { field: 's.identifier', as: 'Scène-nummer' },
                { field: 's.name', as: 'Scène-naam' },
                { field: 'pR.identifier', as: 'Rol-nummer' },
                { field: 'pR.name', as: 'Rol-naam' },
                { field: 'g.identifier', as: 'Groep-nummer' },
                { field: 'g.name', as: 'Groep-naam' },
                { field: 'c.name', as: 'Cast' },
                { field: 'pU.status', as: 'Status' },
                { field: 'sS.name', as: 'T-shirtmaat' },
                { field: 'a.id', as: 'Artikelnummer' },
                { field: 'a.description', as: 'Omschrijving' },
                { field: 'acs.size', as: 'Kledingmaat' },
                { field: 'p.name', as: 'Motief' },
                { field: 'f.type', as: 'Stof' },
                { field: 'c.name', as: 'Kleur' },
                { field: 'a.active', as: 'Actief' },
                { field: 'a.washing', as: 'In Was' },
                { field: 'a.tags', as: 'Tags' },
                { field: 'pUA.remarks', as: 'Opmerkingen' },
            ],
            joins: [
                { table: 'project_users', as: 'pU', source: 'pU.uuid', target: 'pUA.projectUserUuid' },
                { table: 'articles', as: 'a', source: 'pUA.articleId', target: 'a.id' },
                { table: 'categories', as: 'cat', source: 'a.categoryId', target: 'cat.id' },
                { table: 'article_clothing_sizes', as: 'acs', source: 'a.clothingSizeId', target: 'acs.id' },
                { table: 'fabrics', as: 'f', source: 'a.fabricId', target: 'f.id' },
                { table: 'patterns', as: 'pat', source: 'a.patternId', target: 'pat.id' },
                { table: 'colors', as: 'col', source: 'a.colorId', target: 'col.id' },
                { table: 'racks', as: 'r', source: 'a.rackId', target: 'r.id' },
                { table: 'users', as: 'u', source: 'pU.userId', target: 'u.id' },
                { table: 'user_sizes', as: 'uS', source: 'u.id', target: 'uS.userId' },
                { table: 'shirt_sizes', as: 'sS', source: 'uS.shirtSize', target: 'sS.id' },
                { table: 'projects', as: 'p', source: 'pU.projectId', target: 'p.id' },
                { table: 'scenes', as: 's', source: 'pU.sceneId', target: 's.id' },
                { table: 'project_roles', as: 'pR', source: 'pU.roleId', target: 'pR.id' },
                { table: 'groupes', as: 'g', source: 'pU.groupId', target: 'g.id' },
                { table: 'casts', as: 'c', source: 'pU.castId', target: 'c.id' },
            ],
        });
        const { query } = await queryService(articlesQueryConfig);
        const articles = await mysql.raw(query);

        const data = json2Array(
            articles,
            fields.map(f => ({ name: f }))
        );

        await require('fs').promises.writeFile(
            process.cwd() + '/files/project_users_with_articles.csv',
            data.map(row => row.join(';')).join('\r\n')
        );
    }

    return {
        getProjectsReport,
        getProjectUsersReport,
        getUsersImportReport,
        getRolesImportReport,
        getRoleSchemeImportReport,
        getRoleSchemeReport,
        getClothingSizesReport,
        getFabricsReport,
        getPatternsReport,
        getColorsReport,
        getRacksReport,
        getOrdersReport,
        getPriceCategoriesReport,

        getScenesReport,
        getGroupsReport,
        getRolesReport,
        getUsersReport,
        getRegistrationsReport,
        getNotRegisteredReport,
        getRepetitionsReport,
        getRepetitionReport,
        getCompleteRepetitionsReport,
        getCompletePresenceReport,
        getArticlesReport,
        getProjectUserArticlesReport,
    };
};
