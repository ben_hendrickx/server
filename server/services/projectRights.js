module.exports = function projectRightsService({ accessRolesService, rightsService }) {
    async function getRightsForProjectAndUser(projectId, userId) {
        const accessRolesForUser = await accessRolesService.projects.users.read({ userId });
        const accessRoleIds = accessRolesForUser.map(({ projectAccessRoleId }) => projectAccessRoleId);
        const accessRolesForProject = await accessRolesService.projects.read({ projectId, id: accessRoleIds });

        return [
            ...(await Promise.all(
                accessRolesForProject.map(accessRole => rightsService.getRightsForAccessRole(accessRole.accessRoleId))
            )),
            ...(await rightsService.getRightsForProject(projectId)),
        ].flat();
    }

    return {
        getRightsForProjectAndUser,
    };
};
