module.exports = function({ crudService }) {
    const crud = crudService('project_roles');
    const projectRoleUsersCrud = crudService('project_role_users');
    const sceneRolesCrud = crudService('scene_roles');

    async function getProjectRolesForUserInScene(userId, sceneId) {
        const sceneRoles = await sceneRolesCrud.read({ sceneId });
        const roleIds = sceneRoles.map(sR => sR.project_role_id);
        const projectRoles = await crud.read({ id: roleIds });
        const rolesForUser = await projectRoleUsersCrud.read({ userId, project_role_id: roleIds });
        const userRoleIds = rolesForUser.map(g => g.project_role_id);

        return projectRoles.filter(r => userRoleIds.includes(r.id));
    }

    async function getProjectRolesForProjectAndUser(projectId, userId) {
        const projectRolesForProject = await crud.read({ projectId });
        const projectRoleIds = projectRolesForProject.map(pR => pR.id);
        const projectRolesForUser = await projectRoleUsersCrud.read({ userId, project_role_id: projectRoleIds });
        const userProjectRoleIds = projectRolesForUser.map(pR => pR.project_role_id);

        return projectRolesForProject.filter(pR => userProjectRoleIds.includes(pR.id));
    }

    async function getProjectRolesForUser(userId) {
        const projectRoles = await crud.read();
        const projectRoleIds = projectRoles.map(pR => pR.id);
        const projectRolesForUser = await projectRoleUsersCrud.read({ userId, project_role_id: projectRoleIds });
        const userProjectRoleIds = projectRolesForUser.map(pR => pR.project_role_id);

        return projectRoles.filter(pR => userProjectRoleIds.includes(pR.id));
    }

    return {
        ...crud,
        users: {
            ...projectRoleUsersCrud,
        },
        getProjectRolesForProjectAndUser,
        getProjectRolesForUserInScene,
        getProjectRolesForUser,
    };
};
