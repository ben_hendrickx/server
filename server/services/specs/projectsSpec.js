const logger = require('../../../logger')({
    stringUtil: require('../../utils/string')(),
    dateUtil: require('../../utils/date')({
        numberUtil: require('../../utils/number')(),
    }),
});

xdescribe('projectsSpec', () => {
    beforeEach(() => {
        const dataService = {
            read: (resource, id) => (id ? this.projects.find(({ id: pId }) => id === pId) : this.projects),
            create: () => ({ success: true }),
            update: () => ({ success: true }),
            delete: () => ({ success: true }),
        };
        const crudService = function(resource) {
            return {
                read: () => dataService.read(resource),
            };
        };
        this.projectsService = require('../projects')({ crudService, dataService, logger });
    });

    describe('When I try to get a project', () => {
        describe('When I request all projects', () => {
            describe('When there are projects', () => {
                describe('When projects are sorted ascending on id', () => {
                    beforeEach(() => {
                        this.projects = [
                            { id: 1, name: 'a' },
                            { id: 2, name: 'b' },
                            { id: 3, name: 'c' },
                            { id: 4, name: 'a' },
                        ];
                    });

                    describe('When sorting on name', () => {
                        describe('Sorting ascending', () => {
                            beforeEach(() => {
                                this.result = this.projectsService.query({ method: 'GET', query: { sort: 'name' } });
                            });

                            it('First item should have name a', () => {
                                return this.result.then(projects => {
                                    return expect(projects.data[0])
                                        .to.have.property('name')
                                        .and.to.be.equal('a');
                                });
                            });
                        });

                        describe('Sorting descending', () => {
                            beforeEach(() => {
                                this.result = this.projectsService({
                                    method: 'GET',
                                    query: { sort: 'name', sortOrder: 'DESC' },
                                });
                            });

                            it('First item should have name a', () => {
                                return this.result.then(projects => {
                                    return expect(projects.data[0])
                                        .to.have.property('name')
                                        .and.to.be.equal('c');
                                });
                            });
                        });
                    });

                    describe('When sorting on id', () => {
                        describe('Sorting ascending', () => {
                            beforeEach(() => {
                                this.result = this.projectsService({
                                    method: 'GET',
                                });
                            });

                            it('First item should have id 1', () => {
                                return this.result.then(projects => {
                                    return expect(projects.data[0])
                                        .to.have.property('id')
                                        .and.to.be.equal(1);
                                });
                            });
                        });

                        describe('Sorting descending', () => {
                            beforeEach(() => {
                                this.result = this.projectsService({
                                    method: 'GET',
                                    query: { sortOrder: 'DESC' },
                                });
                            });

                            it('First item should have id 4', () => {
                                return this.result.then(projects => {
                                    return expect(projects.data[0])
                                        .to.have.property('id')
                                        .and.to.be.equal(4);
                                });
                            });
                        });
                    });
                });

                describe('When projects are randomized', () => {
                    beforeEach(() => {
                        this.projects = [
                            { id: 3, name: 'c' },
                            { id: 1, name: 'a' },
                            { id: 4, name: 'a' },
                            { id: 2, name: 'b' },
                        ];
                    });

                    describe('When sorting on name', () => {
                        describe('Sorting ascending', () => {
                            beforeEach(() => {
                                this.result = this.projectsService({ method: 'GET', query: { sort: 'name' } });
                            });

                            it('First item should have name a', () => {
                                return this.result.then(projects => {
                                    return expect(projects.data[0])
                                        .to.have.property('name')
                                        .and.to.be.equal('a');
                                });
                            });
                        });

                        describe('Sorting descending', () => {
                            beforeEach(() => {
                                this.result = this.projectsService({
                                    method: 'GET',
                                    query: { sort: 'name', sortOrder: 'DESC' },
                                });
                            });

                            it('First item should have name a', () => {
                                return this.result.then(projects => {
                                    return expect(projects.data[0])
                                        .to.have.property('name')
                                        .and.to.be.equal('c');
                                });
                            });
                        });
                    });

                    describe('When sorting on id', () => {
                        describe('Sorting ascending', () => {
                            beforeEach(() => {
                                this.result = this.projectsService({
                                    method: 'GET',
                                });
                            });

                            it('First item should have id 1', () => {
                                return this.result.then(projects => {
                                    return expect(projects.data[0])
                                        .to.have.property('id')
                                        .and.to.be.equal(1);
                                });
                            });
                        });

                        describe('Sorting descending', () => {
                            beforeEach(() => {
                                this.result = this.projectsService({
                                    method: 'GET',
                                    query: { sortOrder: 'DESC' },
                                });
                            });

                            it('First item should have id 4', () => {
                                return this.result.then(projects => {
                                    return expect(projects.data[0])
                                        .to.have.property('id')
                                        .and.to.be.equal(4);
                                });
                            });
                        });
                    });
                });
            });

            describe('When there are no projects', () => {
                beforeEach(() => {
                    this.projects = [];
                    this.result = this.projectsService({ method: 'GET' });
                });

                it('Should return status 204', () => {
                    return expect(this.result)
                        .to.eventually.have.property('status')
                        .and.to.be.equal(204);
                });
            });
        });

        describe('When I request a specific project', () => {
            describe('When there are projects for given filter', () => {
                beforeEach(() => {
                    this.projects = [{ id: 1 }];
                    this.result = this.projectsService({ method: 'GET', query: { id: 1 } });
                });

                it('Should give project with id 1', () => {
                    return expect(this.result)
                        .to.eventually.have.property('data')
                        .and.to.have.property('id')
                        .and.to.be.equal(1);
                });
            });

            describe('When there are no projects for given filter', () => {
                beforeEach(() => {
                    this.projects = [{ id: 2 }];
                    this.result = this.projectsService({ method: 'GET', query: { id: 1 } });
                });

                it('Should return status 404', () => {
                    return expect(this.result)
                        .to.eventually.have.property('status')
                        .and.to.be.equal(404);
                });
            });
        });
    });

    describe('When I try to create a project', () => {
        describe('When I pass the correct data', () => {
            beforeEach(() => {
                this.result = this.projectsService({ method: 'POST', body: { name: 'TEST' } });
            });

            it('Should succeed', () => {
                return expect(this.result)
                    .to.eventually.have.property('data')
                    .and.to.have.property('success')
                    .and.to.be.equal(true);
            });
        });

        describe('When I pass no data', () => {
            beforeEach(() => {
                this.result = this.projectsService({ method: 'POST' });
            });

            it('Should return status 400', () => {
                return expect(this.result)
                    .to.eventually.have.property('status')
                    .and.to.be.equal(400);
            });
        });
    });

    describe('When I try to update a project', () => {
        beforeEach(() => {
            this.result = this.projectsService({ method: 'PUT', body: { name: 'TEST' } });
        });

        it('Should succeed', () => {
            return expect(this.result)
                .to.eventually.have.property('data')
                .and.to.have.property('success')
                .and.to.be.equal(true);
        });
    });

    describe('When I try to delete a project', () => {
        beforeEach(() => {
            this.result = this.projectsService({ method: 'DELETE', paths: [1] });
        });

        it('Should succeed', () => {
            return expect(this.result)
                .to.eventually.have.property('data')
                .and.to.have.property('success')
                .and.to.be.equal(true);
        });
    });
});
