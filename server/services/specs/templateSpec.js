const logger = require('../../../logger')({
    stringUtil: require('../../utils/string')(),
    dateUtil: require('../../utils/date')({
        numberUtil: require('../../utils/number')(),
    }),
});

describe('templateSpec', function() {
    beforeEach(function() {
        const fileService = function(path) {
            if (path.toLowerCase().includes('error')) {
                throw new Error(`${path} could not be loaded`);
            }

            return {
                read: () => '',
            };
        };

        this.templateService = require('../template')({ fileService, logger });
    });

    describe('When loading a template that exists', function() {
        beforeEach(function() {
            this.template = this.templateService.get('default');
        });

        it('Template should load', function() {
            return expect(this.template).to.eventually.equal('');
        });
    });

    describe('When loading a template that does not exist', function() {
        beforeEach(function() {
            this.template = this.templateService.get('notWorking');
        });

        it('Template should load', function() {
            return expect(this.template).to.eventually.equal('');
        });
    });

    describe('When loading a template ends in an error', function() {
        beforeEach(function() {
            this.template = this.templateService.get('goingToError');
        });

        it('An error should be thrown', function() {
            return expect(this.template).to.be.rejectedWith('../templates/goingToError.html could not be loaded');
        });
    });
});
