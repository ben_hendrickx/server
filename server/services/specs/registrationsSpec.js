xdescribe('registrationsSpec', function() {
    beforeEach(() => {
        const dataService = {
            read: (resource, id) => {
                switch (resource) {
                    case 'project_user_registrations':
                        return this.projectUserRegistrations.filter(
                            ({ userId, projectId }) =>
                                (id.userId && id.userId === userId) || (id.projectId && id.projectId === projectId)
                        );
                    case 'registrations':
                        return [{ id: 1 }];
                    case 'projects':
                    case 'registrations_roles':
                    case 'roles':
                    case 'users':
                        return [{}];
                }
            },
        };
        const authenticationService = {
            verifyToken: token => token,
        };
        const usersService = {
            getUserImage: () => {},
        };
        this.registrationsService = require('../registrations')({ dataService, authenticationService, usersService });
    });

    describe('When I try to get the registrations for a user', () => {
        beforeEach(() => {
            this.projectUserRegistrations = [{ userId: 1 }];
            this.registrations = this.registrationsService.getRegistrationsForUser({ id: 1 });
        });

        it('Should give me the registrations', () => {
            return expect(this.registrations)
                .to.eventually.have.property('length')
                .and.to.be.equal(1);
        });
    });

    describe('When I try to get the registrations for a project as an admin', () => {
        beforeEach(() => {
            this.projectUserRegistrations = [{ projectId: 1, userId: 1 }];
            this.registrations = this.registrationsService.getRegistrationsForProject(1, { level: 1 });
        });

        it('Should give me the registrations', () => {
            return expect(this.registrations)
                .to.eventually.have.property('length')
                .and.to.be.equal(1);
        });
    });
});
