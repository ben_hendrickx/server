xdescribe('castsSpec', function() {
    beforeEach(() => {
        const dataService = {
            read: () => this.casts,
            create: () => ({ success: true }),
            update: () => ({ success: true }),
            delete: () => ({ success: true }),
        };
        this.castsService = require('../casts')({ dataService });
    });

    describe('When I request all casts', () => {
        describe('When there are casts', () => {
            beforeEach(() => {
                this.casts = [
                    { id: 1, name: 'a' },
                    { id: 2, name: 'b' },
                    { id: 3, name: 'c' },
                    { id: 4, name: 'a' },
                ];
                this.result = this.castsService.getCasts();
            });

            it('First item should have id 1', () => {
                return this.result.then(casts => {
                    return expect(casts.data[0])
                        .to.have.property('id')
                        .and.to.be.equal(1);
                });
            });

            it('Should give 4 items', () => {
                return this.result.then(casts => {
                    return expect(casts.data)
                        .to.have.property('length')
                        .and.to.be.equal(4);
                });
            });

            describe('When requesting a second time', () => {
                beforeEach(() => {
                    this.result = this.castsService.getCasts();
                });

                it('First item should have id 1', () => {
                    return this.result.then(casts => {
                        return expect(casts.data[0])
                            .to.have.property('id')
                            .and.to.be.equal(1);
                    });
                });

                it('Should give 4 items', () => {
                    return this.result.then(casts => {
                        return expect(casts.data)
                            .to.have.property('length')
                            .and.to.be.equal(4);
                    });
                });
            });
        });
    });

    describe('When there are no casts', () => {
        beforeEach(() => {
            this.casts = [];
            this.result = this.castsService.getCasts();
        });

        it('Should return status 204', () => {
            return expect(this.result)
                .to.eventually.have.property('status')
                .and.to.be.equal(204);
        });
    });
});
