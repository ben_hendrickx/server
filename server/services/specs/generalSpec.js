const errors = require('../../errors')();

describe('generalSpec', function() {
    beforeEach(() => {
        const projectsService = {
            getCurrentOpenProject: () => {
                return this.project;
            },
        };
        this.generalService = require('../general')({ errors, projectsService });
    });

    describe('When getting info', () => {
        describe('When there is a project open for registration', () => {
            beforeEach(() => {
                this.project = {
                    title: 'foo',
                    subtitle: 'bar',
                    openFrom: new Date(2000, 11, 31),
                    closedFrom: new Date(2999, 11, 31),
                };
                this.info = this.generalService.getInfo();
            });

            it('Should got project with a title', () => {
                return expect(this.info)
                    .to.eventually.have.property('title')
                    .and.to.be.equal('foo');
            });

            it('Should have subtitle', () => {
                return expect(this.info)
                    .to.eventually.have.property('subtitle')
                    .and.to.be.equal('bar');
            });
        });

        describe('When there is no project open for registration', () => {
            beforeEach(() => {
                delete this.project;
                this.info = this.generalService.getInfo();
            });

            it('Should got project with a title', () => {
                return expect(this.info).to.eventually.be.rejectedWith('Currently no projects running');
            });
        });
    });
});
