xdescribe('validationSpec', function() {
    beforeEach(() => {
        const authenticationService = {};
        this.validationService = require('../validation')({ authenticationService });
    });

    describe('When setting up validation for a route', () => {
        beforeEach(() => {
            this.validation = this.validationService.validateRoute(require('../../validators/general'));
        });

        describe('When validating the route', () => {
            beforeEach(() => {
                this.validated = this.validation({}, {}, () => {});
            });

            it('Should be valid', () => {
                return expect(this.validated).to.equal(undefined);
            });
        });
    });

    describe('When there is no config for a route', () => {
        beforeEach(() => {
            this.validation = this.validationService.validateRoute();
        });

        describe('When validating the route', () => {
            beforeEach(() => {
                this.validated = this.validation({}, {}, () => {});
            });

            it('Should be valid', () => {
                return expect(this.validated).to.equal(undefined);
            });
        });
    });
});
