const logger = require('../../../logger')({
    stringUtil: require('../../utils/string')(),
    dateUtil: require('../../utils/date')({
        numberUtil: require('../../utils/number')(),
    }),
});

xdescribe('usersSpec', () => {
    beforeEach(() => {
        const dataService = {
            read: (resource, id) => {
                let func;
                switch (resource) {
                    case 'users':
                        func = id && typeof id === 'object' ? 'filter' : 'find';
                        return id
                            ? this.users[func](({ id: uId }) => (typeof id === 'object' ? id.id : id) === uId)
                            : this.users;
                    case 'user_sizes':
                    case 'user_app_settings':
                    case 'clothing_sizes':
                    case 'shirt_sizes':
                        return [{}];
                }
            },
            create: () => ({ success: true }),
            update: () => ({ success: true }),
            delete: () => ({ success: true }),
        };
        const authenticationService = {
            verifyToken: token => token,
        };
        this.usersService = require('../users')({ dataService, logger, authenticationService });
    });

    describe('When I try to get a user', () => {
        describe('When I request all users', () => {
            describe('When there are users', () => {
                describe('When users are sorted ascending on id', () => {
                    beforeEach(() => {
                        this.users = [
                            { id: 1, name: 'a' },
                            { id: 2, name: 'b' },
                            { id: 3, name: 'c' },
                            { id: 4, name: 'a' },
                        ];
                    });

                    describe('When sorting on name', () => {
                        describe('Sorting ascending', () => {
                            beforeEach(() => {
                                this.result = this.usersService({ method: 'GET', query: { sort: 'name' } });
                            });

                            it('First item should have name a', () => {
                                return this.result.then(users => {
                                    return expect(users.data[0])
                                        .to.have.property('name')
                                        .and.to.be.equal('a');
                                });
                            });
                        });

                        describe('Sorting descending', () => {
                            beforeEach(() => {
                                this.result = this.usersService({
                                    method: 'GET',
                                    query: { sort: 'name', sortOrder: 'DESC' },
                                });
                            });

                            it('First item should have name a', () => {
                                return this.result.then(users => {
                                    return expect(users.data[0])
                                        .to.have.property('name')
                                        .and.to.be.equal('c');
                                });
                            });
                        });
                    });

                    describe('When sorting on id', () => {
                        describe('Sorting ascending', () => {
                            beforeEach(() => {
                                this.result = this.usersService({
                                    method: 'GET',
                                });
                            });

                            it('First item should have id 1', () => {
                                return this.result.then(users => {
                                    return expect(users.data[0])
                                        .to.have.property('id')
                                        .and.to.be.equal(1);
                                });
                            });
                        });

                        describe('Sorting descending', () => {
                            beforeEach(() => {
                                this.result = this.usersService({
                                    method: 'GET',
                                    query: { sortOrder: 'DESC' },
                                });
                            });

                            it('First item should have id 4', () => {
                                return this.result.then(users => {
                                    return expect(users.data[0])
                                        .to.have.property('id')
                                        .and.to.be.equal(4);
                                });
                            });
                        });
                    });
                });

                describe('When users are randomized', () => {
                    beforeEach(() => {
                        this.users = [
                            { id: 3, name: 'c' },
                            { id: 1, name: 'a' },
                            { id: 4, name: 'a' },
                            { id: 2, name: 'b' },
                        ];
                    });

                    describe('When sorting on name', () => {
                        describe('Sorting ascending', () => {
                            beforeEach(() => {
                                this.result = this.usersService({ method: 'GET', query: { sort: 'name' } });
                            });

                            it('First item should have name a', () => {
                                return this.result.then(users => {
                                    return expect(users.data[0])
                                        .to.have.property('name')
                                        .and.to.be.equal('a');
                                });
                            });
                        });

                        describe('Sorting descending', () => {
                            beforeEach(() => {
                                this.result = this.usersService({
                                    method: 'GET',
                                    query: { sort: 'name', sortOrder: 'DESC' },
                                });
                            });

                            it('First item should have name a', () => {
                                return this.result.then(users => {
                                    return expect(users.data[0])
                                        .to.have.property('name')
                                        .and.to.be.equal('c');
                                });
                            });
                        });
                    });

                    describe('When sorting on id', () => {
                        describe('Sorting ascending', () => {
                            beforeEach(() => {
                                this.result = this.usersService({
                                    method: 'GET',
                                });
                            });

                            it('First item should have id 1', () => {
                                return this.result.then(users => {
                                    return expect(users.data[0])
                                        .to.have.property('id')
                                        .and.to.be.equal(1);
                                });
                            });
                        });

                        describe('Sorting descending', () => {
                            beforeEach(() => {
                                this.result = this.usersService({
                                    method: 'GET',
                                    query: { sortOrder: 'DESC' },
                                });
                            });

                            it('First item should have id 4', () => {
                                return this.result.then(users => {
                                    return expect(users.data[0])
                                        .to.have.property('id')
                                        .and.to.be.equal(4);
                                });
                            });
                        });
                    });
                });
            });

            describe('When there are no users', () => {
                beforeEach(() => {
                    this.users = [];
                    this.result = this.usersService({ method: 'GET' });
                });

                it('Should return status 204', () => {
                    return expect(this.result)
                        .to.eventually.have.property('status')
                        .and.to.be.equal(204);
                });
            });
        });

        describe('When I request a specific user', () => {
            describe('When there are users for given filter', () => {
                beforeEach(() => {
                    this.users = [{ id: 1 }];
                    this.result = this.usersService({ method: 'GET', query: { id: 1 } });
                });

                it('Should give user with id 1', () => {
                    return expect(this.result)
                        .to.eventually.have.property('data')
                        .and.to.have.property('id')
                        .and.to.be.equal(1);
                });
            });

            describe('When there are no users for given filter', () => {
                beforeEach(() => {
                    this.users = [{ id: 2 }];
                    this.result = this.usersService({ method: 'GET', query: { id: 1 } });
                });

                it('Should return status 404', () => {
                    return expect(this.result)
                        .to.eventually.have.property('status')
                        .and.to.be.equal(404);
                });
            });
        });
    });

    describe('When I try to create a user', () => {
        describe('When I pass the correct data', () => {
            beforeEach(() => {
                this.result = this.usersService({ method: 'POST', body: { name: 'TEST' } });
            });

            it('Should succeed', () => {
                return expect(this.result)
                    .to.eventually.have.property('data')
                    .and.to.have.property('success')
                    .and.to.be.equal(true);
            });
        });

        describe('When I pass no data', () => {
            beforeEach(() => {
                this.result = this.usersService({ method: 'POST' });
            });

            it('Should return status 400', () => {
                return expect(this.result)
                    .to.eventually.have.property('status')
                    .and.to.be.equal(400);
            });
        });
    });

    describe('When I try to update a user', () => {
        beforeEach(() => {
            this.result = this.usersService({ method: 'PUT', body: { name: 'TEST' } });
        });

        it('Should succeed', () => {
            return expect(this.result)
                .to.eventually.have.property('data')
                .and.to.have.property('success')
                .and.to.be.equal(true);
        });
    });

    describe('When I try to delete a user', () => {
        beforeEach(() => {
            this.result = this.usersService({ method: 'DELETE', paths: [1] });
        });

        it('Should succeed', () => {
            return expect(this.result)
                .to.eventually.have.property('data')
                .and.to.have.property('success')
                .and.to.be.equal(true);
        });
    });

    describe('When I request a specific user', () => {
        describe('When admin requests user', () => {
            beforeEach(() => {
                this.users = [{ id: 1 }];
                this.result = this.usersService.getUserById(1, { level: 1 });
            });

            it('Should give user with id 1', () => {
                return expect(this.result)
                    .to.eventually.have.property('id')
                    .and.to.be.equal(1);
            });

            it('Should have sizes', () => {
                return expect(this.result).to.eventually.have.property('sizes');
            });

            it('Should have settings', () => {
                return expect(this.result).to.eventually.have.property('settings');
            });
        });

        describe('When user requests other user', () => {
            beforeEach(() => {
                this.users = [{ id: 1 }];
                this.result = this.usersService.getUserById(1, { level: 0, id: 2 });
            });

            it('Should throw an error', () => {
                return expect(this.result).to.be.rejectedWith('Not Authorized');
            });
        });

        describe('When setting a remark on a user', () => {
            beforeEach(() => {
                this.users = [{ id: 1 }];
                this.result = this.usersService.setRemarkOnUser(1, 'Test');
            });

            it('Should succeed', () => {
                return expect(this.result)
                    .to.eventually.have.property('success')
                    .and.to.be.equal(true);
            });
        });
    });
});
