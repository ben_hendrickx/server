const logger = require('../../../logger')({
    stringUtil: require('../../utils/string')(),
    dateUtil: require('../../utils/date')({
        numberUtil: require('../../utils/number')(),
    }),
});

describe('jsonSpec', function() {
    beforeEach(function() {
        this.jsonService = require('../json')({ logger });
    });

    describe('When cleaning a string', function() {
        beforeEach(function() {
            this.string = this.jsonService.cleanString({});
        });

        it('Should succeed', function() {
            return expect(this.string).to.equal('{}');
        });
    });

    describe('When cleaning a string that is not a valid json', function() {
        beforeEach(function() {
            this.string = this.jsonService.cleanString(undefined);
        });

        it('Should succeed', function() {
            return expect(this.string).to.equal(undefined);
        });
    });
});
