module.exports = function generalService({ errors, projectsService }) {
    async function getInfo() {
        const project = await projectsService.getCurrentOpenProject();

        if (!project) {
            throw errors.httpStatusCodeError(404, 'Currently no projects running');
        }

        return { title: project.title, subtitle: project.subtitle };
    }

    return {
        getInfo,
    };
};
