const fs = require('fs').promises;
const squel = require('squel');

module.exports = function usersService({ numberUtil, logger, stringUtil, crudService, sizesService, dataService }) {
    async function readNew(config = [], { offset, items, sort, fields }) {
        const count = squel
            .select()
            .field('count(*)', 'count')
            .from('users');
        let query = squel.select().from('users');

        if (fields) {
            for (let field of fields) {
                query.field(field);
            }
        }

        if (!sort) {
            query.order('lastName').order('firstName');
        } else {
            query.order(sort.split('|')[0], sort.split('|')[1] === 'ASC' ? true : false);
        }

        if (items !== undefined && items !== 'all') {
            query.offset(offset).limit(items);
        }

        Object.values(config).forEach(filter => {
            let { key, operator = '=', value } = filter;

            if (filter && value !== undefined) {
                if (!Array.isArray(key)) {
                    if (value !== '') {
                        count.where(`${key} ${operator} ?`, value);
                        query.where(`${key} ${operator} ?`, value);
                    } else {
                        count.where(`${key} ${operator}`);
                        query.where(`${key} ${operator}`);
                    }
                } else {
                    const expression = squel.expr();

                    key.forEach(fieldName => {
                        expression.or(`${fieldName} ${operator} ?`, value);
                    });

                    count.where(expression);
                    query.where(expression);
                }
            }
        });

        return {
            count: (await dataService.raw(count.toString())).shift().count,
            data: await dataService.raw(query.toString()),
        };
    }

    const crud = crudService('users');
    const sizesCrud = crudService('user_sizes');
    const settingsCrud = crudService('user_app_settings');

    let users;

    async function getUser(id) {
        if (!users) {
            await getAllUsers();
        }

        const { pin } = await settingsCrud.read().then(settings => settings.find(({ userId }) => userId === id) || {});

        return {
            ...(users.find(user => user.id === id) || {}),
            pin,
        };
    }

    async function getAllUsers() {
        users = await crud.read();
        return users;
    }

    async function getUsersByPartialString(partialString) {
        if (!users) {
            await getAllUsers();
        }

        const pieces = partialString.split(' ');

        return users.filter(user =>
            pieces.every(
                piece =>
                    user.firstName.toLowerCase().includes(piece.toLowerCase()) ||
                    user.lastName.toLowerCase().includes(piece.toLowerCase())
            )
        );
    }

    async function getImage(firstName, lastName, email) {
        try {
            const path = `images/${firstName}_${lastName}_${stringUtil.removeChars(
                email,
                `.!#%&'-/=_\`{}~",:;<>@`.split('')
            )}.jpg`;
            /* istanbul ignore next */
            const image = await fs.readFile(path, 'base64');
            return `data:image/jpeg;base64,${image}`;
        } catch (e) {
            logger.trace(`User ${firstName} ${lastName} has no image`);
        }
    }

    async function getUserById(id) {
        /* istanbul ignore next */
        const [user = {}] = await crud.read({ id });
        /* istanbul ignore next */
        const [sizes = {}] = await sizesCrud.read({ userId: id });
        const [settings] = await settingsCrud.read({ userId: id });
        const shirtSizes = await sizesService.shirtSizes.read();

        sizes.shirtSize = shirtSizes.find(({ id }) => id === sizes.shirtSize)?.name;

        return { ...user, sizes, settings, image: await getImage(user.firstName, user.lastName, user.email) };
    }

    async function insertAppSettings(userId, privacy = true, pictures = false, level = 0) {
        await settingsCrud.create({
            userId,
            level,
            pin: numberUtil.randomNumber(10000, 99999),
            privacy,
            pictures,
        });
    }

    function filterUsers(users, { sex, picture, minAge, maxAge }) {
        if (sex !== undefined) {
            users = users.filter(user => sex === user.sex);
        }

        if (picture !== undefined) {
            users = users.filter(user => (picture ? user.image : !user.image));
        }

        if (minAge !== undefined) {
            users = users.filter(user => {
                const diff_ms = Date.now() - new Date(user.date_of_birth).getTime();
                const age_dt = new Date(diff_ms);
                const userAge = Math.abs(age_dt.getUTCFullYear() - 1970);

                return userAge >= minAge;
            });
        }

        if (maxAge !== undefined) {
            users = users.filter(user => {
                const diff_ms = Date.now() - new Date(user.date_of_birth).getTime();
                const age_dt = new Date(diff_ms);
                const userAge = Math.abs(age_dt.getUTCFullYear() - 1970);

                return userAge <= maxAge;
            });
        }

        return users;
    }

    function sortUsers(users, sort) {
        let [sortBy, order] = sort.split('|');

        // SWAP ORDER FOR DATES
        ['registered', 'age', 'creationDt', 'date_of_birth'].includes(sortBy) &&
            (order = order === 'ASC' ? 'DESC' : 'ASC');

        return [...users].sort((a, b) => {
            if (['registered', 'age', 'creationDt', 'date_of_birth'].includes(sortBy)) {
                if (sortBy === 'registered' || sortBy === 'creationDt') {
                    a.registered = new Date(a.creationDt).getTime();
                    b.registered = new Date(b.creationDt).getTime();
                }

                if (sortBy === 'age' || sortBy === 'date_of_birth') {
                    a.age = new Date(a.date_of_birth).getTime();
                    b.age = new Date(b.date_of_birth).getTime();
                }
            }

            const aProp = a[sortBy];
            const bProp = b[sortBy];

            if (typeof aProp === 'string') {
                if (aProp === bProp) {
                    return 0;
                }

                return order === 'ASC' ? (aProp < bProp ? -1 : 1) : aProp < bProp ? 1 : -1;
            }

            return order === 'ASC' ? aProp - bProp : bProp - aProp;
        });
    }

    async function getAges() {
        return (
            await dataService.raw(
                'SELECT DISTINCT TIMESTAMPDIFF(YEAR, date_of_birth, CURDATE()) as age FROM users ORDER BY AGE ASC'
            )
        ).map(({ age }) => age);
    }

    return {
        ...crud,
        readNew,
        sizes: {
            ...sizesCrud,
        },
        settings: {
            ...settingsCrud,
        },
        getUserImage: getImage,
        insertAppSettings,
        getUser,
        getImage,
        getAllUsers,
        getUsersByPartialString,
        getUserById,

        filterUsers,
        sortUsers,

        getAges,
    };
};
