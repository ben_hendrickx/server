/* istanbul ignore file */
const moment = require('moment');

module.exports = function clothingService({ crudService, logger }) {
    const articlesCrud = crudService('articles');
    const articleClothingSizesCrud = crudService('article_clothing_sizes');
    const clothingSizesCrud = crudService('clothing_sizes');
    const patternsCrud = crudService('patterns');
    const fabricsCrud = crudService('fabrics');
    const colorsCrud = crudService('colors');
    const categoriesCrud = crudService('categories');
    const categoryPricesCrud = crudService('category_prices');
    const ordersCrud = crudService('rentals');
    const orderItemsCrud = crudService('rental_items');
    const racksCrud = crudService('racks');

    async function getArticles({ active, id } = {}) {
        const config = {};

        if (active !== undefined) {
            config.active = active;
        }

        if (id !== undefined) {
            config.id = id;
        }

        const [articles, clothingSizes, patterns, fabrics, colors, categories, racks] = await Promise.all([
            articlesCrud.read(config),
            articleClothingSizesCrud.read(),
            patternsCrud.read(),
            fabricsCrud.read(),
            colorsCrud.read(),
            categoriesCrud.read(),
            racksCrud.read(),
        ]);

        return articles.map(item => ({
            ...item,
            size: {
                ...clothingSizes.find(size => size.id === item.clothingSizeId),
            },
            pattern: {
                ...(item.patternId ? patterns.find(pattern => pattern.id === item.patternId) : {}),
            },
            fabric: {
                ...(item.fabricId ? fabrics.find(fabric => fabric.id === item.fabricId) : {}),
            },
            color: {
                ...(item.colorId ? colors.find(color => color.id === item.colorId) : {}),
            },
            rack: {
                ...(item.rackId ? racks.find(rack => rack.id === item.rackId) : {}),
            },
            category: {
                ...(item.categoryId ? categories.find(category => category.id === item.categoryId) : {}),
            },
        }));
    }

    async function _calculatePrices(items, startDt, periods, internal, price = 0, replacementPrice = 0) {
        periods = parseInt(periods);
        if (periods <= 0) {
            return {
                price,
                replacementPrice,
            };
        }

        startDt = moment(startDt);

        const categories = await categoriesCrud.read({ id: items.map(item => item.categoryId) });
        let categoryPrices = await categoryPricesCrud.read({ categoryId: categories.map(category => category.id) });

        categoryPrices = categoryPrices
            .filter(price => {
                if (moment(price.startDt).startOf('day') > startDt) {
                    return false;
                }

                if (price.endDt && price.endDt < startDt) {
                    return false;
                }

                return true;
            })
            .sort((a, b) => {
                if (new Date(a.startDt).getTime() > new Date(b.startDt).getTime()) {
                    return -1;
                } else if (new Date(a.startDt).getTime() < new Date(b.startDt).getTime()) {
                    return 1;
                } else {
                    return 0;
                }
            });

        for (let item of items) {
            const prices = categoryPrices
                .sort((a, b) => {
                    const result = new Date(b.startDt).getTime() - new Date(a.startDt).getTime();

                    if (result === 0) {
                        return new Date(a.endDt).getTime() - new Date(b.endDt).getTime();
                    }

                    return result;
                })
                .find(price => price.categoryId === item.categoryId);
            price += (prices?.[internal ? 'internalPrice' : 'externalPrice'] || 0) * item.amount;
            periods === 1 &&
                (replacementPrice += (prices?.replacementPrice || 0) * (item.amount - (item.returned || 0)));
        }

        return _calculatePrices(items, startDt.add(30, 'days'), periods - 1, internal, price, replacementPrice);
    }

    // TODO: change how rent works
    // rack doesn't exist anymore
    async function rentClothes(body) {
        const { items, internal } = body;
        delete body.items;

        const { price, replacementPrice } = await _calculatePrices(items, moment(), body.periods, internal);
        body.price = price;
        body.replacementPrice = replacementPrice;

        const rentalId = await ordersCrud.create(body);

        for (let item of items) {
            const { id: artId, amount, remarks } = item;
            await Promise.all([
                orderItemsCrud.create({ rentalId, artId, amount, remarks }),
                articlesCrud.update({ id: artId }, { stock: 0, rented: 1 }),
            ]);
        }

        return rentalId;
    }

    async function updateRental(id, body) {
        const [order] = await ordersCrud.read({ id });
        const { items, internal } = body;
        delete body.items;

        const { price, replacementPrice } = await _calculatePrices(items, order.creationDt, body.periods, internal);
        body.price = price;
        body.replacementPrice = replacementPrice;

        await ordersCrud.update(id, body);
        const articleIds = (await orderItemsCrud.read({ rentalId: id })).map(({ artId }) => artId);
        await articlesCrud.update({ id: articleIds }, { stock: 1, rented: 0 });
        await orderItemsCrud.remove({ rentalId: id });

        for (let item of items) {
            let { id: artId, amount, remarks, returned } = item;
            amount = parseInt(amount || 1);
            await Promise.all([
                orderItemsCrud.create({ rentalId: id, artId, amount, remarks, returned }),
                articlesCrud.update({ id: artId }, { stock: 0, rented: 1 }),
            ]);
        }
    }

    async function getOrders() {
        return ordersCrud.read();
    }

    async function getOrder(id) {
        const [order] = await ordersCrud.read({ id });

        if (!order) {
            return;
        }

        let items = await orderItemsCrud.read({ rentalId: id });
        const itemIds = items.map(({ artId }) => artId);
        let articles = await getArticles({ id: itemIds });
        items = items.map(item => ({
            amount: item.amount,
            remarks: item.remarks,
            returned: item.returned,
            ...articles.find(article => article.id === item.artId),
        }));

        const { price, replacementPrice } = await _calculatePrices(
            items,
            order.creationDt,
            order.periods,
            order.internal
        );

        return {
            ...order,
            price,
            replacementPrice,
            items,
        };
    }

    async function getCategories() {
        return await Promise.all([categoriesCrud.read(), categoryPricesCrud.read()]).then(([categories, prices]) => {
            return categories.map(category => {
                return {
                    ...category,
                    prices: prices.filter(({ categoryId }) => categoryId === category.id),
                };
            });
        });
    }

    async function createCategory(body) {
        const categoryId = await categoriesCrud.create({ name: body.name });
        delete body.name;
        body.categoryId = categoryId;
        body.startDt = moment()
            .startOf('day')
            .format();
        categoryPricesCrud.create({ ...body });
        return { success: true };
    }

    async function importItems(items, callback) {
        let [clothingSizes, categories, fabrics, patterns, colors, articles, racks] = await Promise.all([
            articleClothingSizesCrud.read(),
            categoriesCrud.read(),
            fabricsCrud.read(),
            patternsCrud.read(),
            colorsCrud.read(),
            articlesCrud.read(),
            racksCrud.read(),
        ]);

        let rackIds = racks.map(r => r.id);

        const itemsToImport = items.reduce(
            (acc, item) => {
                const {
                    artId,
                    description,
                    tags,
                    clothingSize,
                    fabric,
                    pattern,
                    color,
                    rackId,
                    rackDescription,
                    categorie,
                    active,
                } = item;

                if (fabric && ![...fabrics, ...acc.fabrics].some(f => f.type.toLowerCase() === fabric.toLowerCase())) {
                    acc.fabrics.push({
                        type: fabric,
                    });
                }

                if (rackId && !rackIds.includes(parseInt(rackId))) {
                    rackIds.push(parseInt(rackId));
                    acc.racks.push({
                        id: parseInt(rackId),
                        description: rackDescription || '',
                    });
                }

                if (
                    pattern &&
                    ![...patterns, ...acc.patterns].some(p => p.name.toLowerCase() === pattern.toLowerCase())
                ) {
                    acc.patterns.push({
                        name: pattern,
                    });
                }

                if (
                    categorie &&
                    ![...categories, ...acc.categories].some(
                        category => category.name.toLowerCase() === categorie.toLowerCase()
                    )
                ) {
                    acc.categories.push({
                        name: categorie,
                        internalPrice: 0,
                        externalPrice: 0,
                        replacementPrice: 0,
                    });
                }

                if (color && ![...colors, ...acc.colors].some(c => c.name.toLowerCase() === color.toLowerCase())) {
                    acc.colors.push({
                        name: color,
                    });
                }

                if (
                    ![...clothingSizes, ...acc.clothingSizes].some(
                        cS => cS.size.toLowerCase() === clothingSize.toLowerCase()
                    )
                ) {
                    acc.clothingSizes.push({
                        size: clothingSize,
                    });
                }

                acc.articles.push({
                    id: artId,
                    active,
                    rackId: rackId ? parseInt(rackId) : null,
                    stock: parseInt(1),
                    category: categorie,
                    clothingSize,
                    pattern,
                    fabric,
                    color,
                    tags,
                    description,
                    washing: true,
                });

                return acc;
            },
            {
                fabrics: [],
                patterns: [],
                colors: [],
                articles: [],
                racks: [],
                clothingSizes: [],
                categories: [],
            }
        );

        //eslint-disable-next-line
        colors = [
            ...colors,
            ...(await Promise.all(
                itemsToImport.colors.map(async body => {
                    const id = await colorsCrud.create(body);
                    return { id, ...body };
                })
            )),
        ];

        //eslint-disable-next-line
        clothingSizes = [
            ...clothingSizes,
            ...(await Promise.all(
                itemsToImport.clothingSizes.map(async body => {
                    const id = await articleClothingSizesCrud.create(body);
                    return { id, ...body };
                })
            )),
        ];

        categories = [
            ...categories,
            ...(await Promise.all(
                itemsToImport.categories.map(async body => {
                    const categoryId = await categoriesCrud.create({ name: body.name });
                    await categoryPricesCrud.create({
                        categoryId,
                        // internalPrice: parseFloat(body.internalPrice.replace(/,/g, '.')),
                        // externalPrice: parseFloat(body.externalPrice.replace(/,/g, '.')),
                        // replacementPrice: parseFloat(body.replacementPrice.replace(/,/g, '.')),
                        internalPrice: 0,
                        externalPrice: 0,
                        replacementPrice: 0,
                        startDt: moment().format(),
                    });
                    return { id: categoryId, name: body.name };
                })
            )),
        ];

        let totalImportLength = Object.values(itemsToImport).reduce((acc, items) => (acc += items.length), 0);
        let imported = colors.length;

        for (let [i, rack] of itemsToImport.racks.entries()) {
            await racksCrud.create(rack);
            imported++;
            callback(imported / totalImportLength, `Importeren van rekken (${i + 1}/${itemsToImport.racks.length})`);
        }

        for (let [i, pattern] of itemsToImport.patterns.entries()) {
            await patternsCrud.create(pattern);
            imported++;
            callback(
                imported / totalImportLength,
                `Importeren van motieven (${i + 1}/${itemsToImport.patterns.length})`
            );
        }

        for (let [i, fabric] of itemsToImport.fabrics.entries()) {
            await fabricsCrud.create(fabric);
            imported++;
            callback(imported / totalImportLength, `Importeren van stoffen (${i + 1}/${itemsToImport.fabrics.length})`);
        }

        fabrics = await fabricsCrud.read();
        colors = await colorsCrud.read();
        patterns = await patternsCrud.read();
        racks = await racksCrud.read();
        clothingSizes = await articleClothingSizesCrud.read();
        categories = await categoriesCrud.read();

        for (let [i, article] of itemsToImport.articles.entries()) {
            article.colorId = (
                colors.find(({ name }) => name?.toLowerCase() === article.color?.toLowerCase()) || {}
            ).id;
            article.clothingSizeId = (
                clothingSizes.find(
                    ({ size }) => size.toLowerCase() === article.clothingSize.toString().toLowerCase()
                ) || {}
            ).id;
            article.fabricId = (
                fabrics.find(({ type }) => type.toLowerCase() === article.fabric.toString().toLowerCase()) || {}
            ).id;
            article.patternId = (
                patterns.find(({ name }) => name.toLowerCase() === article.pattern.toString().toLowerCase()) || {}
            ).id;
            article.categoryId = (
                categories.find(({ name }) => name.toLowerCase() === article.category.toString().toLowerCase()) || {}
            ).id;

            delete article.pattern;
            delete article.fabric;
            delete article.color;
            delete article.clothingSize;
            delete article.category;

            if (articles.find(({ id }) => id === parseInt(article.id))) {
                if (article.colorId) {
                    await articlesCrud.update(
                        { id: parseInt(article.id) },
                        {
                            colorid: article.colorId,
                        }
                    );
                }
            } else {
                await articlesCrud.create(article);
            }

            imported++;
            callback(
                imported / totalImportLength,
                `Importeren van artikels (${i + 1}/${itemsToImport.articles.length})`
            );
        }

        return { success: true };
    }

    async function getClothingSizes() {
        return await clothingSizesCrud.read();
    }

    async function getArticleClothingSizes() {
        return await articleClothingSizesCrud.read();
    }

    async function getFabrics(config) {
        return await fabricsCrud.read(config);
    }

    async function getPatterns(config) {
        return await patternsCrud.read(config);
    }

    async function getColors(config) {
        return await colorsCrud.read(config);
    }

    async function createArticle(body) {
        const articles = await articlesCrud.read();
        const nextArticleId = (articles.filter(({ id }) => !id.toString().startsWith('99')).reverse()[0]?.id || 0) + 1;
        body.tags = (body.tags || []).join('|');
        body.id = nextArticleId;
        const id = await articlesCrud.create({ ...body });
        return { success: true, id };
    }

    async function updateArticle(id, body) {
        body.tags = body.tags?.join('|');
        await articlesCrud.update({ id }, { ...body });
        return { success: true };
    }

    async function createFabric(body) {
        const id = await fabricsCrud.create({ ...body });
        return { success: true, id };
    }

    async function updateFabric(id, body) {
        await fabricsCrud.update({ id }, { ...body });
    }

    async function createPattern(body) {
        const id = await patternsCrud.create({ ...body });
        return { success: true, id };
    }

    async function updatePattern(id, body) {
        await patternsCrud.update({ id }, { ...body });
    }

    async function createColor(body) {
        const id = await colorsCrud.create({ ...body });
        return { success: true, id };
    }

    async function updateColor(id, body) {
        await colorsCrud.update({ id }, { ...body });
    }

    async function getArticlesByPartialString(string) {
        function pad(value, length) {
            if ((value || '').toString().length >= length) {
                return value;
            }

            return pad('0' + value, length);
        }

        const articles = await getArticles();
        const pieces = string.split(' ');
        return articles.filter(article => {
            let {
                id,
                category: { name },
                tags,
            } = article;

            id = pad(id, 6);

            return pieces.every(
                piece =>
                    id.toLowerCase().includes(piece.toLowerCase()) ||
                    name.toLowerCase().includes(piece.toLowerCase()) ||
                    (tags || '').toLowerCase().includes(piece.toLowerCase())
            );
        });
    }

    async function getArticle(id) {
        const articles = await getArticles();
        return articles.find(article => article.id === id);
    }

    async function getRacks() {
        return await racksCrud.read();
    }

    async function getRack(id) {
        return await getRacks().then(racks => racks.find(rack => rack.id === parseInt(id)));
    }

    async function createRack(body) {
        const id = await racksCrud.create({ ...body });
        return { success: true, id };
    }

    async function updateRack(id, body) {
        await racksCrud.update({ id }, { ...body });
        return { success: true };
    }

    async function getArticlesForRack(rack) {
        return await getArticles().then(articles => articles.filter(article => article.rackId === parseInt(rack)));
    }

    async function createClothingSize({ size }) {
        return await articleClothingSizesCrud.create({ size });
    }

    return {
        categories: {
            ...categoriesCrud,
            prices: {
                ...categoryPricesCrud,
            },
        },
        orders: {
            ...ordersCrud,
        },
        articles: {
            ...articlesCrud,
        },
        getClothingSizes,
        getArticleClothingSizes,
        getFabrics,
        getPatterns,
        updatePattern,
        getColors,
        importItems,
        getArticles,
        getArticle,
        rentClothes,
        getOrders,
        getOrder,
        getCategories,
        createCategory,
        createArticle,
        updateArticle,
        createFabric,
        updateFabric,
        createPattern,
        createColor,
        updateColor,
        getArticlesByPartialString,
        getRacks,
        getRack,
        updateRack,
        createRack,
        getArticlesForRack,
        createClothingSize,
        updateRental,
        calculatePrices: _calculatePrices,
    };
};
