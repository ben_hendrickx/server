module.exports = function notificationsService({ crudService, usersService, rightsService, projectRightsService }) {
    const crud = crudService('notifications');
    const removedCrud = crudService('notification_removed');
    const readCrud = crudService('notification_read');

    const sockets = [];
    function registerSocket(socket) {
        unregisterSocket(socket);
        sockets.push(socket);
    }

    function unregisterSocket(socket) {
        while (sockets.find(s => s.id === socket.id)) {
            sockets.splice(
                sockets.findIndex(s => s.id === socket.id),
                1
            );
        }
    }

    async function add(n) {
        const { user, label, notification, type } = n;
        const notificationId = await crud.create({ userId: user.id, label, notification, type });

        //eslint-disable-next-line
        n.id = notificationId;
        //eslint-disable-next-line
        n.creationDt = new Date();

        for (let socket of sockets) {
            socket.emit('notification_added', n);
        }
    }

    async function getUnreadNotificationsForUser(userId, projectId) {
        const notifications = await getNotificationsForUser(userId, projectId);
        return notifications.filter(({ read }) => !read);
    }

    async function getTypesForUser(userId, projectId) {
        const rights = projectId
            ? await projectRightsService.getRightsForProjectAndUser(projectId, userId)
            : await rightsService.getRightsForUser(userId);
        const notificationRight = rights.find(r => r.name === 'notifications');

        if (!notificationRight.read) {
            return false;
        }

        return notificationRight.subRights
            .filter(sR => sR.access)
            .map(sR => {
                switch (sR.name) {
                    case 'new_users':
                        return 1;
                    case 'registration_updated':
                        return 2;
                    case 'registration_deleted':
                        return 3;
                    case 'repetition_signoff':
                        return 4;
                    case 'clothing_changed':
                        return 5;
                    case 'contactdetails_changed':
                        return 6;
                    case 'clothingdetails_changed':
                        return 7;
                    case 'userinfo_changed':
                        return 8;
                }
            });
    }

    async function getNotificationsForUser(userId, projectId) {
        const types = await getTypesForUser(userId, projectId);
        if (types === false) {
            return [];
        }

        const config = { type: types };
        projectId && (config.projectId = projectId);

        const notifications = await crud.read(config);

        if (!notifications.length) {
            return [];
        }

        for (let notification of notifications) {
            const [user] = await usersService.read({ id: notification.userId });
            user &&
                (notification.user = {
                    id: user.id,
                    name: user.lastName + ' ' + user.firstName,
                });
        }

        const notificationIds = notifications.map(({ id }) => id);
        const removed = await removedCrud.read({ userId, notificationId: notificationIds });
        const removedNotificationIds = removed.map(({ notificationId }) => notificationId);

        const read = await readCrud.read({ userId, notificationId: notificationIds });
        const readNotificationIds = read.map(({ notificationId }) => notificationId);

        return notifications
            .filter(({ id }) => !removedNotificationIds.includes(id))
            .map(n => ({ ...n, read: readNotificationIds.includes(n.id) }))
            .sort((a, b) => {
                if (new Date(a.creationDt).getTime() > new Date(b.creationDt).getTime()) {
                    return -1;
                } else if (new Date(a.creationDt).getTime() < new Date(b.creationDt).getTime()) {
                    return 1;
                } else {
                    return 0;
                }
            });
    }

    async function markReadForUser(userId, notificationId) {
        await readCrud.create({ userId, notificationId });
    }

    async function markUnreadForUser(userId, notificationId) {
        await readCrud.remove({ userId, notificationId });
    }

    async function markRemovedForUser(userId, notificationId) {
        await removedCrud.create({ userId, notificationId });
    }

    return {
        ...crud,
        removed: {
            ...removedCrud,
        },
        didRead: {
            ...readCrud,
        },
        add,
        getNotificationsForUser,
        markReadForUser,
        markUnreadForUser,
        markRemovedForUser,
        getUnreadNotificationsForUser,
        registerSocket,
        unregisterSocket,
    };
};
