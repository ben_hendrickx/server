module.exports = function({ crudService }) {
    const crud = crudService('access_roles');
    const usersCrud = crudService('user_access_roles');
    const rightsCrud = crudService('access_role_rights');
    const subRightsCrud = crudService('access_role_sub_rights');
    const projectsCrud = crudService('project_access_roles');
    const projectUsersCrud = crudService('project_user_access_roles');

    async function setRightsForAccessRole(accessRoleId, rights) {
        await Promise.all([rightsCrud.remove({ accessRoleId }), subRightsCrud.remove({ accessRoleId })]);

        await Promise.all(
            rights.map(async ({ id: rightId, read, create, update, delete: remove, subRights }) => {
                await rightsCrud.create({
                    accessRoleId,
                    rightId,
                    read,
                    create,
                    update,
                    delete: remove,
                });
                await Promise.all(
                    subRights.map(async ({ id: subRightId, access }) => {
                        await subRightsCrud.create({ accessRoleId, subRightId, access });
                    })
                );
            })
        );
    }

    async function copy(id) {
        const [accessRole] = await crud.read({ id });
        const [accessRoleRights, accessRoleSubRights] = await Promise.all([
            rightsCrud.read({ accessRoleId: id }),
            subRightsCrud.read({ accessRoleId: id }),
        ]);
        delete accessRole.id;

        accessRole.name += ' - Kopie';

        const newId = await crud.create(accessRole);

        await Promise.all(
            accessRoleRights.map(async accessRoleRight => {
                accessRoleRight.accessRoleId = newId;
                await rightsCrud.create(accessRoleRight);
            })
        );

        await Promise.all(
            accessRoleSubRights.map(async accessRoleSubRight => {
                accessRoleSubRight.accessRoleId = newId;
                await subRightsCrud.create(accessRoleSubRight);
            })
        );
    }

    async function getAccessRolesForUser(userId) {
        const accessRoleUsers = await usersCrud.read({ userId });

        return await crud.read({ id: accessRoleUsers.map(({ accessRoleId }) => accessRoleId) });
    }

    return {
        ...crud,
        users: {
            ...usersCrud,
        },
        rights: {
            ...rightsCrud,
        },
        subRights: {
            ...subRightsCrud,
        },
        projects: {
            ...projectsCrud,
            users: {
                ...projectUsersCrud,
            },
        },
        setRightsForAccessRole,
        copy,
        getAccessRolesForUser,
    };
};
