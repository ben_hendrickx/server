const RESOURCE_NAME = 'projects';
const moment = require('moment');

module.exports = function projectsService({ errors, crudService, dateUtil, usersService }) {
    const crud = crudService(RESOURCE_NAME);
    const partsOfTownCrud = crudService('parts_of_town');
    const projectUserRegistrationsCrud = crudService('project_user_registrations');
    const groupsCrud = crudService('groupes');
    const groupOfScenesCrud = crudService('group_of_scenes');
    const scenesCrud = crudService('scenes');
    const projectRolesCrud = crudService('project_roles');
    const projectUserAccessRolesCrud = crudService('project_user_access_roles');
    const groupUsersCrud = crudService('group_users');
    const projectRoleUsersCrud = crudService('project_role_users');
    const sceneGroupsCrud = crudService('scene_groups');
    const sceneRolesCrud = crudService('scene_roles');
    const registrationsCrud = crudService('registrations');
    const registrationsRolesCrud = crudService('registrations_roles');
    const repetitionsCrud = crudService('repetitions');
    const repetitionUserPresenceCrud = crudService('repetition_user_presence');
    const repetitionUsersCrud = crudService('repetition_users');

    function _isOngoing(project) {
        const { openFrom, closedFrom, closedOffStage } = project;

        return (
            openFrom &&
            dateUtil.isInPast(openFrom) &&
            ((closedFrom && dateUtil.isInFuture(closedFrom)) || (closedOffStage && dateUtil.isInFuture(closedOffStage)))
        );
    }

    function _sortByStartDate(a, b) {
        return moment(a.openFrom).isSameOrBefore(moment(b.openFrom)) ? 1 : 0;
    }

    async function getCurrentOpenProjects() {
        const projects = await crud.read();

        return projects.sort(_sortByStartDate).filter(_isOngoing);
    }

    async function getCurrentOpenProject() {
        const projects = await getCurrentOpenProjects();

        const project = projects.reverse().shift();

        if (!project) {
            return;
        }

        return project;
    }

    async function getUnregisteredOngoingProjectsForUser(userId) {
        const ongoingProjects = (await crud.read()).filter(_isOngoing);

        for (let project of ongoingProjects) {
            const registrations = await projectUserRegistrationsCrud.read({
                userId,
                projectId: project.id,
            });
            project.registered = registrations && registrations.length;
        }

        return ongoingProjects.filter(({ registered }) => !registered);
    }

    async function copy(projectId, callback) {
        const [project] = await crud.query({ id: projectId });

        if (!project) {
            throw errors.httpStatusCodeError(404, 'Project niet gevonden');
        }

        const [groups, groupsOfScenes, scenes, projectRoles] = await Promise.all([
            groupsCrud.read({ projectId }),
            groupOfScenesCrud.read({ projectId }),
            scenesCrud.read({ projectId }),
            projectRolesCrud.read({ projectId }),
        ]);

        const sceneIds = scenes.map(scene => scene.id);

        const [sceneGroups, sceneRoles] = await Promise.all([
            sceneIds.length ? sceneGroupsCrud.read({ sceneId: sceneIds }) : [],
            sceneIds.length ? sceneRolesCrud.read({ sceneId: sceneIds }) : [],
        ]);

        const newProject = {
            name: project.name + ' - Kopie',
            title: project.title,
            subtitle: project.subtitle,
            auditionHint: project.auditionHint,
            extended: project.extended,
            auditions_enabled: project.auditions_enabled,
        };

        const newProjectId = await crud.create(newProject);

        const mappings = {
            groups: {},
            groupsOfScenes: {},
            projectRoles: {},
            scenes: {},
        };

        let totalItemsToImport =
            groups.length +
            groupsOfScenes.length +
            projectRoles.length +
            scenes.length +
            sceneGroups.length +
            sceneRoles.length;

        let imported = 0;
        let text = 'Kopiëren van groepen';

        callback(imported / totalItemsToImport, text);
        await Promise.all(
            groups.map(async group => {
                const newGroup = { ...group };
                delete newGroup.id;
                newGroup.projectId = newProjectId;
                const newGroupId = await groupsCrud.create(newGroup);
                mappings.groups[group.id] = newGroupId;
                imported++;
                callback(imported / totalItemsToImport, text);
            })
        );

        text = 'Kopiëren van rollen';
        await Promise.all(
            projectRoles.map(async projectRole => {
                const newProjectRole = { ...projectRole };
                delete newProjectRole.id;
                newProjectRole.projectId = newProjectId;
                const newProjectRoleId = await projectRolesCrud.create(newProjectRole);
                mappings.projectRoles[projectRole.id] = newProjectRoleId;
                imported++;
                callback(imported / totalItemsToImport, text);
            })
        );

        text = 'Kopiëren van groepen van scènes';
        await Promise.all(
            groupsOfScenes.map(async groupOfScenes => {
                const newGroupOfScenes = { ...groupOfScenes };
                delete newGroupOfScenes.id;
                newGroupOfScenes.projectId = newProjectId;
                const newGroupOfScenesId = await groupOfScenesCrud.create(newGroupOfScenes);
                mappings.groupsOfScenes[groupOfScenes.id] = newGroupOfScenesId;
                imported++;
                callback(imported / totalItemsToImport, text);
            })
        );

        text = 'Kopiëren van scènes';
        await Promise.all(
            scenes.map(async scene => {
                const newScene = { ...scene };
                delete newScene.id;
                newScene.projectId = newProjectId;
                const newSceneId = await scenesCrud.create(newScene);
                mappings.scenes[scene.id] = newSceneId;
                imported++;
                callback(imported / totalItemsToImport, text);
            })
        );

        text = 'Kopiëren van groepen in scènes';
        await Promise.all(
            sceneGroups.map(async sceneGroup => {
                const newSceneGroup = { ...sceneGroup };
                newSceneGroup.sceneId = mappings.scenes[sceneGroup.sceneId];
                (newSceneGroup.groupId = mappings.groups[sceneGroup.groupId]),
                    await sceneGroupsCrud.create(newSceneGroup);
                imported++;
                callback(imported / totalItemsToImport, text);
            })
        );

        text = 'Kopiëren van rollen in scènes';
        await Promise.all(
            sceneRoles.map(async sceneRole => {
                const newSceneRole = { ...sceneRole };
                newSceneRole.sceneId = mappings.scenes[sceneRole.sceneId];
                newSceneRole.project_role_id = mappings.projectRoles[sceneRole.project_role_id];
                await sceneRolesCrud.create(newSceneRole);
                imported++;
                callback(imported / totalItemsToImport, text);
            })
        );
    }

    async function remove(projectId) {
        const [groups, projectRoles, repetitions, projectUserRegistrations] = await Promise.all([
            groupsCrud.read({ projectId }),
            projectRolesCrud.read({ projectId }),
            repetitionsCrud.read({ projectId }),
            projectUserRegistrationsCrud.read({ projectId }),
        ]);

        const registrations = projectUserRegistrations.length
            ? await registrationsCrud.read({
                  id: projectUserRegistrations.map(projectUserRegistration => projectUserRegistration.registrationId),
              })
            : [];

        const groupIds = groups.map(group => group.id);
        const projectRoleIds = projectRoles.map(projectRole => projectRole.id);
        const repetitionIds = repetitions.map(repetition => repetition.id);

        groupIds.length && (await groupUsersCrud.remove({ groupId: groupIds }));
        groupIds.length && (await sceneGroupsCrud.remove({ groupId: groupIds }));
        await groupsCrud.remove({ projectId });
        projectRoleIds.length && (await projectRoleUsersCrud.remove({ project_role_id: projectRoleIds }));
        projectRoleIds.length && (await sceneRolesCrud.remove({ project_role_id: projectRoleIds }));
        await projectRolesCrud.remove({ projectId });
        await groupOfScenesCrud.remove({ projectId });
        await scenesCrud.remove({ projectId });
        repetitionIds.length && (await repetitionUserPresenceCrud.remove({ repetitionId: repetitionIds }));
        repetitionIds.length && (await repetitionUsersCrud.remove({ repetitionId: repetitionIds }));
        await repetitionsCrud.remove({ projectId });
        registrations.length &&
            (await registrationsRolesCrud.remove({
                registrationId: registrations.map(registration => registration.id),
            }));
        await projectUserRegistrationsCrud.remove({ projectId });
        await projectUserAccessRolesCrud.remove({ projectId });

        await crud.remove(projectId);
    }

    //---------------------//

    async function getUsers(projectId) {
        const projectUserRegistrations = await projectUserRegistrationsCrud.read({ projectId });
        const userIds = projectUserRegistrations.reduce((acc, pUR) => {
            !acc.includes(pUR.userId) && acc.push(pUR.userId);
            return acc;
        }, []);

        return await usersService.read({ id: userIds });
    }

    async function getProjectsByPartialString(partialString) {
        const projects = await crud.read();
        const pieces = partialString.split(' ');

        return projects.filter(project =>
            pieces.every(piece => project.name.toLowerCase().includes(piece.toLowerCase()))
        );
    }

    return {
        ...crud,
        partsOfTown: {
            ...partsOfTownCrud,
        },
        getCurrentOpenProjects,
        getCurrentOpenProject,
        getUnregisteredOngoingProjectsForUser,
        copy,
        remove,

        getProjectsByPartialString,
        getUsers,
    };
};
