/* istanbul ignore file */
const squel = require('squel');

module.exports = function({ dataService }) {
    async function read(config = [], { offset, items }) {
        const count = squel
            .select()
            .field('count(*)', 'count')
            .from('articles', 'a')
            .left_join('categories', 'cat', 'a.categoryId=cat.id')
            .left_join('article_clothing_sizes', 'acs', 'a.clothingSizeId=acs.id')
            .left_join('fabrics', 'f', 'a.fabricId=f.id')
            .left_join('patterns', 'p', 'a.patternId=p.id')
            .left_join('colors', 'c', 'a.colorId=c.id')
            .left_join('racks', 'r', 'a.rackId=r.id');
        const query = squel
            .select()
            .from('articles', 'a')
            .field('a.*')
            .field('r.description', 'rackDescription')
            .field('f.type')
            .field('a.categoryId')
            .field('cat.name', 'categoryName')
            .field('p.name', 'patternName')
            .field('c.name', 'colorName')
            .field('acs.size')
            .left_join('categories', 'cat', 'a.categoryId=cat.id')
            .left_join('article_clothing_sizes', 'acs', 'a.clothingSizeId=acs.id')
            .left_join('fabrics', 'f', 'a.fabricId=f.id')
            .left_join('patterns', 'p', 'a.patternId=p.id')
            .left_join('colors', 'c', 'a.colorId=c.id')
            .left_join('racks', 'r', 'a.rackId=r.id');

        if (items !== undefined && items !== 'all') {
            query.offset(offset).limit(items);
        }

        Object.values(config).forEach(filter => {
            let { key, operator = '=', value } = filter;

            if (filter && value !== undefined) {
                if (!Array.isArray(key)) {
                    if (value !== '') {
                        count.where(`${key} ${operator} ?`, value);
                        query.where(`${key} ${operator} ?`, value);
                    } else {
                        count.where(`${key} ${operator}`);
                        query.where(`${key} ${operator}`);
                    }
                } else {
                    const expression = squel.expr();

                    key.forEach(fieldName => {
                        expression.or(`${fieldName} ${operator} ?`, value);
                    });

                    count.where(expression);
                    query.where(expression);
                }
            }
        });

        return {
            count: (await dataService.raw(count.toString())).shift().count,
            data: await dataService.raw(query.toString()),
        };
    }

    return {
        read,
    };
};
