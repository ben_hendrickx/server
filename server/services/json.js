module.exports = function jsonService({ logger }) {
    function cleanString(string) {
        try {
            return JSON.stringify(string)
                .replace(/\[{/g, '[\r\n    { ')
                .replace(/}]/g, ' }\r\n]')
                .replace(/},{/g, ' },\r\n    { ')
                .replace(/,"/g, ', "')
                .replace(/":/g, '": ');
        } catch (e) {
            logger.error(`Could not transform JSON to string`, e);
        }
    }
    return {
        cleanString,
    };
};
