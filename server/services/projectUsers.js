/* istanbul ignore file */
module.exports = function({ crudService, dataService, queryService }) {
    const crud = crudService('project_users');
    const articlesCrud = crudService('project_user_articles');

    async function read(config) {
        const { count, query } = queryService(config);

        return {
            count: (await dataService.raw(count)).shift().count,
            data: await dataService.raw(query),
        };
    }

    async function count(config) {
        const { count, query } = queryService(config);
        return (await dataService.raw(count)).shift().count;
    }

    return {
        ...crud,
        articles: {
            ...articlesCrud,
            read,
        },
        read,
        count,
    };
};
