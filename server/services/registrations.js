module.exports = function registrationsService({
    authenticationService,
    usersService,
    groupUsersService,
    crudService,
    rolesService,
    projectsService,
    projectRolesService,
    projectRightsService,
    groupsService,
    dateUtil,
}) {
    const crud = crudService('registrations');
    const userRegistrationsCrud = crudService('project_user_registrations');
    const registrationsRolesCrud = crudService('registrations_roles');
    const repetitionsCrud = crudService('repetitions');
    const repetitionUsersCrud = crudService('repetition_users');
    const repetitionPresenceCrud = crudService('repetition_user_presence');

    async function getUserForRegistration(registrationId) {
        const [userRegistration] = await userRegistrationsCrud.read({ registrationId });
        const [[user], [settings], [sizes]] = await Promise.all([
            usersService.read({ id: userRegistration.userId }),
            usersService.settings.read({ userId: userRegistration.userId }),
            usersService.sizes.read({ userId: userRegistration.userId }),
        ]);

        return {
            ...user,
            settings,
            sizes,
        };
    }

    async function getRolesForRegistration(registrationId) {
        const registrationRoles = await registrationsRolesCrud.read({ registrationId });
        const roleIds = registrationRoles.map(rR => rR.roleId);
        const roles = await rolesService.read({ id: roleIds });

        return roles;
    }

    async function getRolesForProjectAndUser(projectId, userId) {
        const [{ registrationId } = {}] = await userRegistrationsCrud.read({ projectId, userId });
        const registrationRoles = await registrationsRolesCrud.read({ registrationId });
        const roleIds = registrationRoles.map(rR => rR.roleId);
        const roles = await rolesService.read({ id: roleIds });

        return roles;
    }

    async function copyRegistrations(from, config, callback) {
        const { to, onStage, offStage, appearing = false } = config;

        const [userRegistrations, toUserRegistrations, roles] = await Promise.all([
            userRegistrationsCrud.read({ projectId: from }),
            userRegistrationsCrud.read({ projectId: to }),
            rolesService.read(),
        ]);

        const registrationIds = userRegistrations.map(({ registrationId }) => registrationId);
        const userIdsAlreadyInProject = toUserRegistrations.map(({ userId }) => userId);

        const registrations = await crud.read({ id: registrationIds });
        const registrationRoles = await registrationsRolesCrud.read({ registrationId: registrationIds });

        const newRegistrations = registrations.reduce((acc, registration) => {
            const { userId } = userRegistrations.find(({ registrationId }) => registrationId === registration.id);

            if (userIdsAlreadyInProject.includes(userId)) {
                return acc;
            }

            const rolesForRegistration = registrationRoles.filter(({ registrationId }) => registrationId === registration.id);
            const roleIdsToCreate = rolesForRegistration.reduce((acc, { roleId }) => {
                const role = roles.find(({ id }) => id === roleId);

                if (offStage && !role.onStage) {
                    acc.push(roleId);
                }

                if (onStage && role.onStage) {
                    if (appearing) {
                        !acc.includes(2) && acc.push(2);
                    } else {
                        acc.push(roleId);
                    }
                }

                return acc;
            }, []);

            if (!roleIdsToCreate.length) {
                return acc;
            }

            acc.push({
                ...registration,
                projectId: to,
                userId,
                roles: roleIdsToCreate,
            });

            return acc;
        }, []);

        let copied = 0;
        callback(copied / newRegistrations.length, 'Kopiëren van inschrijvingen');

        for (let registration of newRegistrations) {
            const { projectId, userId, roles, ...newRegistration } = registration;

            delete newRegistration.id;
            delete newRegistration.creationDt;

            const registrationId = await crud.create({
                ...newRegistration,
                owner_remarks: 'Deze inschrijving werd automatisch gecreëerd door het systeem',
            });

            await userRegistrationsCrud.create({
                userId,
                registrationId,
                projectId: to,
            });

            await Promise.all(roles.map(roleId => registrationsRolesCrud.create({ registrationId, roleId })));

            callback(++copied / newRegistrations.length, 'Kopiëren van inschrijvingen');
        }
    }

    async function getRegistrationsForProject(projectId) {
        const userRegistrations = await userRegistrationsCrud.read({ projectId });
        const { registrationIds, userIds } = userRegistrations.reduce(
            (acc, uR) => {
                acc.registrationIds.push(uR.registrationId);
                acc.userIds.push(uR.userId);
                return acc;
            },
            { registrationIds: [], userIds: [] }
        );

        const [registrations, users] = await Promise.all([
            crud.read({ id: registrationIds }),
            usersService.read({ id: userIds }),
        ]);

        return await Promise.all(
            registrations.map(async registration => {
                const userRegistration = userRegistrations.find(uR => uR.registrationId === registration.id);
                const user = users.find(u => u.id === userRegistration.userId);
                const image = await usersService.getUserImage(user.firstName, user.lastName, user.email);

                return {
                    ...registration,
                    user: {
                        ...user,
                        image,
                    },
                };
            })
        );
    }

    async function getRegistrationsForUser(userId, onlyActive = false) {
        const userRegistrations = await userRegistrationsCrud.read({ userId });
        const { registrationIds, projectIds } = userRegistrations.reduce(
            (acc, uR) => {
                acc.registrationIds.push(uR.registrationId);
                acc.projectIds.push(uR.projectId);
                return acc;
            },
            { registrationIds: [], projectIds: [] }
        );

        const [registrations, projects] = await Promise.all([
            crud.read({ id: registrationIds }),
            projectsService.read({ id: projectIds }),
        ]);

        const activeProjects = projects.filter(({ closedOffStage, closedFrom, projectClosed }) => {
            return (projectClosed && !dateUtil.isInPast(projectClosed)) || !dateUtil.isInPast(closedOffStage) || !dateUtil.isInPast(closedFrom);
        });

        const registrationsOfUser = await Promise.all(
            registrations.map(async registration => {
                const userRegistration = userRegistrations.find(uR => uR.registrationId === registration.id);
                const project = (onlyActive ? activeProjects : projects).find(u => u.id === userRegistration.projectId);

                if (!project) {
                    return;
                }

                const rights = await projectRightsService.getRightsForProjectAndUser(project.id, userId);

                return {
                    ...registration,
                    project: {
                        ...project,
                        rights,
                    },
                };
            })
        );

        return registrationsOfUser.filter(v => v);
    }

    async function signOffForRegistration(registrationId, token, userId) {
        const { level, id } = await authenticationService.verifyToken(token);
        const [registration] = await crud.read({ id: registrationId });
        if ((level !== 0 && !userId) || !registration) {
            throw Error('Not found');
        }

        const idToDelete = userId || id;

        const [{ projectId } = {}] = await userRegistrationsCrud.read({
            userId: idToDelete,
            registrationId,
        });

        const groupIds = (await groupsService.read({ projectId })).map(({ id }) => id);
        await groupsService.users.remove({ groupId: groupIds, userId: idToDelete });

        const projectRoleIds = (await projectRolesService.read({ projectId })).map(({ id }) => id);
        await projectRolesService.users.remove({ project_role_id: projectRoleIds, userId: idToDelete });

        const repetitionIds = (await repetitionsCrud.read({ projectId })).map(({ id }) => id);
        if (repetitionIds.length) {
            await repetitionUsersCrud.remove({ repetitionId: repetitionIds, userId: idToDelete });
            await repetitionPresenceCrud.remove({ repetitionId: repetitionIds, userId: idToDelete });
        }

        await registrationsRolesCrud.remove({ registrationId });
        await userRegistrationsCrud.remove({ userId: idToDelete, registrationId });
        await crud.remove({ id: registrationId });

        return { success: true };
    }

    // TODO: remove this
    // async function getRegistrationForProjectAndUser(projectId, userId) {
    //     const [registration] = await userRegistrationsCrud.read({ projectId, userId });

    //     const [registrationData] = await crud.read({ id: registration.registrationId });
    //     const [userData] = await usersService.read({ id: userId });

    //     const registrationRoles = await registrationsRolesCrud.read({
    //         registrationId: registration.registrationId,
    //     });

    //     let roles = [];
    //     for (let i = 0; i < registrationRoles.length; i++) {
    //         const { roleId } = registrationRoles[i];
    //         const [roleData] = await rolesService.read({ id: roleId });
    //         roleData && roles.push(roleData);
    //     }

    //     const [projectData] = await projectsService.read({ id: projectId });

    //     let canSignOff =
    //         new Date(projectData.closedFrom).getTime() + 24 * 60 * 60 * 1000 > Date.now() ||
    //         new Date(projectData.closedOffStage).getTime() + 24 * 60 * 60 * 1000 > Date.now();

    //     if (projectData.deleteRegistrationClosed) {
    //         canSignOff = new Date(projectData.deleteRegistrationClosed).getTime() + 24 * 60 * 60 * 1000 > Date.now();
    //     }

    //     return { ...registrationData, user: userData, roles, projectData, canSignOff };
    // }

    return {
        ...crud,
        users: {
            ...userRegistrationsCrud,
        },
        roles: {
            ...registrationsRolesCrud,
        },
        getRolesForProjectAndUser,
        copyRegistrations,

        getUserForRegistration,
        getRolesForRegistration,
        getRegistrationsForProject,
        getRegistrationsForUser,

        // TODO: remove this
        //getRegistrationForProjectAndUser,
        signOffForRegistration,
    };
};
