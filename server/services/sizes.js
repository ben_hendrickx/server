module.exports = function({ crudService }) {
    const clothingSizesCrud = crudService('clothing_sizes');
    const shirtSizesCrud = crudService('shirt_sizes');

    return {
        clothingSizes: {
            ...clothingSizesCrud,
        },
        shirtSizes: {
            ...shirtSizesCrud,
        },
    };
};
