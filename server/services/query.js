const squel = require('squel');

module.exports = function() {
    return function buildQueries(config) {
        const {
            countField = '*',
            table,
            fields,
            joins,
            where = [],
            offset,
            items,
            sort,
            distinct = false,
            hasDelete = false,
        } = config;

        let totalCount = squel
            .select()
            .field(`count(${countField})`, 'count')
            .from(table.name, table.as);
        let count = squel
            .select()
            .field(`count(${countField})`, 'count')
            .from(table.name, table.as);
        let query = squel.select().from(table.name, table.as);

        if (distinct) {
            query = query.distinct();
            totalCount = totalCount.distinct();
            count = count.distinct();
        }

        if (hasDelete) {
            query = query.where(`deletedDt IS NULL`);
            totalCount = totalCount.where(`deletedDt IS NULL`);
            count = count.where(`deletedDt IS NULL`);
        }

        fields
            ? fields.forEach(field => {
                  typeof field === 'object' ? query.field(field.field, field.as) : query.field(field);
              })
            : query.field('*');

        joins?.forEach(({ table, as, source, target }) => {
            totalCount.left_join(table, as, `${source}=${target}`);
            count.left_join(table, as, `${source}=${target}`);
            query.left_join(table, as, `${source}=${target}`);
        });

        sort && query.order(sort.split('|')[0], sort.split('|')[1] === 'ASC' ? true : false);
        offset && query.offset(offset);
        items && query.limit(items);

        where.forEach(filter => {
            if (typeof filter === 'string') {
                count.where(filter);
                query.where(filter);
                filter.total && totalCount.where(filter);
                return;
            }

            let { key, operator = '=', value } = filter;

            if (Array.isArray(value)) {
                operator = 'IN';
                value = value.length ? value : [''];
            }

            if (filter && value !== undefined) {
                if (!Array.isArray(key)) {
                    if (value !== '') {
                        count.where(`${key} ${operator} ?`, value);
                        query.where(`${key} ${operator} ?`, value);
                        filter.total && totalCount.where(`${key} ${operator} ?`, value);
                    } else {
                        count.where(`${key} ${operator}`);
                        query.where(`${key} ${operator}`);
                        filter.total && totalCount.where(`${key} ${operator}`);
                    }
                } else {
                    const expression = squel.expr();

                    key.forEach(fieldName => {
                        expression.or(`${fieldName} ${operator} ?`, value);
                    });

                    count.where(expression);
                    query.where(expression);
                    filter.total && totalCount.where(expression);
                }
            }
        });

        return {
            query: query.toString(),
            total: totalCount.toString(),
            count: count.toString(),
        };
    };
};
