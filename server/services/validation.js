module.exports = function validationService({ authenticationService, rightsService }) {
    const httpStatus = require('http-status');
    const validation = require('express-validation');

    validation.options({
        status: httpStatus.UNPROCESSABLE_ENTITY,
        statusText: httpStatus[httpStatus.UNPROCESSABLE_ENTITY],
        allowUnknownBody: false,
        allowUnknownHeaders: false,
        allowUnknownQuery: false,
        allowUnknownParams: false,
        allowUnknownCookies: false,
    });

    function getToken(req) {
        let token = (req.headers['authorization'] || '').replace('Bearer ', '');

        if (!token && req.query.token) {
            token = req.query.token;
        }

        return token;
    }

    function validateRoute(config = {}, requiredRights = {}) {
        return async function(req, res, next) {
            const token = getToken(req);

            if (token === 'test' && process.env.NODE_ENV === 'development') {
                next();
                return;
            }

            if (Object.keys(requiredRights).length) {
                const { id } = await authenticationService.verifyToken(token);

                const rights = await rightsService.getRightsForUser(id);

                for (let [name, right] of Object.entries(requiredRights)) {
                    if (!rights.find(r => r.name.toLowerCase() === name.toLowerCase())) {
                        return res.status(401).send('Unauthorized');
                    }

                    if (
                        !rights.find(r => {
                            const { read, create, update } = r;

                            if (r.name.toLowerCase() !== name.toLowerCase()) {
                                return false;
                            }

                            if (right.read && !read) {
                                return false;
                            }

                            if (right.create && !create) {
                                return false;
                            }

                            if (right.update && !update) {
                                return false;
                            }

                            if (right.delete && !r.delete) {
                                return false;
                            }

                            return true;
                        })
                    ) {
                        return res.status(401).send('Unauthorized');
                    }
                }
            }

            return validation(config)(req, res, next);
        };
    }

    return {
        validateRoute,
    };
};
