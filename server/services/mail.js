/* istanbul ignore file */
const {
    mail: { enable, userId, apiKey },
    server: { hostname },
} = require('../../config');
const uuid = require('uuid/v4');

module.exports = function mailService({ logger, crudService }) {
    const openedCrud = crudService('mail_opened');

    const Mailjet = require('node-mailjet').connect(userId, apiKey);

    const mailer = Mailjet.post('send');

    function send({ from, to, subject, text, html }, callback, name = 'missing') {
        if (!enable) {
            logger.warn('Mail is not enabled, not sending mail');
            return callback();
        }

        if (Array.isArray(to)) {
            to = to
                .reduce((acc, email) => {
                    !acc.includes(email) && acc.push(email.toLowerCase());
                    return acc;
                }, [])
                .map(email => ({ Email: email }));
        } else {
            to = [{ Email: to }];
        }

        to.forEach(to => _send(from, to, subject, text, html));

        async function _send(from, to, subject, text, html) {
            const uid = uuid();
            await openedCrud.create({ mail: name, email: to.Email, uuid: uid });

            try {
                const emailData = {
                    FromEmail: from.address,
                    FromName: from.name,
                    Subject: subject,
                    'Text-part': text
                        .replace(/\{hostname\}/g, hostname)
                        .replace(/\{uuid\}/g, uid)
                        .replace(/\{email\}/g, to.Email),
                    'HTML-part': html
                        .replace('{signoutlink}', `${hostname}/api/unsubscribe?email=${to}`)
                        .replace(/\{hostname\}/g, hostname)
                        .replace(/\{uuid\}/g, uid)
                        .replace(/\{email\}/g, to.Email),
                    Recipients: [to],
                };
                logger.debug(emailData);
                mailer.request(emailData);
                logger.debug('Mail succesfully sent');
                callback();
            } catch (e) {
                e && logger.error(e);
                callback(e);
            }
        }
    }

    return {
        send,
        opened: {
            ...openedCrud,
        },
    };
};
