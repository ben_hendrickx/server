/* eslint-disable require-atomic-updates */
module.exports = function groupsService({ usersService, crudService }) {
    const crud = crudService('groupes');
    const groupUsersCrud = crudService('group_users');
    const sceneGroupsCrud = crudService('scene_groups');

    async function getGroupUsersAddedBefore(groupId, castId, date = new Date()) {
        const queryConfig = { groupId };
        castId && (queryConfig.castId = castId);
        const group_users = await groupUsersCrud.read();

        return group_users.filter(groupUser => {
            if (!(new Date(groupUser.creationDt).getTime() < new Date(date).getTime())) {
                return false;
            }

            if (!(groupUser.castId === parseInt(castId))) {
                return false;
            }

            if (!(groupUser.groupId === parseInt(groupId))) {
                return false;
            }

            return true;
        });
    }

    // TODO: remove this
    // async function getGroupsForProjectAndUser(projectId, userId) {
    //     const groupsForProject = await crud.read({ projectId });
    //     const groupIds = groupsForProject.map(g => g.id);
    //     const groupsForUser = await groupUsersCrud.read({ userId, groupId: groupIds });
    //     const userGroupIds = groupsForUser.map(g => g.groupId);

    //     return groupsForProject.filter(g => userGroupIds.includes(g.id));
    // }

    // TODO: remove this
    async function getGroupsForUser(userId) {
        const groups = await crud.read();
        const groupIds = groups.map(g => g.id);
        const groupsForUser = await groupUsersCrud.read({ userId, groupId: groupIds });
        const userGroupIds = groupsForUser.map(g => g.groupId);

        return groups.filter(g => userGroupIds.includes(g.id));
    }

    async function getGroupsForUserInScene(userId, sceneId) {
        const sceneGroups = await sceneGroupsCrud.read({ sceneId });
        const groupIds = sceneGroups.map(sG => sG.groupId);
        const groups = await crud.read({ id: groupIds });
        const groupsForUser = await groupUsersCrud.read({ userId, groupId: groupIds });
        const userGroupIds = groupsForUser.map(g => g.groupId);

        return groups.filter(g => userGroupIds.includes(g.id));
    }

    async function getGroupGroupsAndUsers(groupId) {
        const users = await groupUsersCrud.read({ groupId });

        return {
            users,
        };
    }

    async function getGroups(filter) {
        return crud
            .read()
            .then(groups =>
                !filter ? groups : groups.filter(g => Object.entries(filter).every(([key, value]) => g[key] === value))
            );
    }

    async function getGroup(id) {
        return (await getGroups()).find(group => group.id === parseInt(id));
    }

    async function getUsers(groupId, castId) {
        if (!Array.isArray(groupId)) {
            groupId = [groupId];
        }

        if (!groupId.length) {
            return [];
        }

        const queryConfig = { groupId };
        castId && (queryConfig.castId = parseInt(castId));
        const group_users = await groupUsersCrud.read(queryConfig);

        const userIds = group_users.map(({ userId }) => userId);

        const users = await usersService.read({ id: userIds });
        const settings = await usersService.settings.read({ userId: userIds });

        return users.map(user => {
            const { pin } = settings.find(({ userId }) => userId === user.id) || {};
            user.pin = pin;
            return user;
        });
    }

    async function getUsersAddedBefore(groupId, castId, date = new Date()) {
        const userIds = ((await getGroupUsersAddedBefore(groupId, castId, date)) || [])
            .filter(gU => gU && gU.userId)
            .map(gU => gU.userId);

        const users = await usersService.read({ id: userIds });
        const settings = await usersService.settings.read({ userId: userIds });

        return users.map(user => {
            const { pin } = settings.find(({ userId }) => userId === user.id) || {};
            user.pin = pin;
            return user;
        });
    }

    async function getGroupUsers() {
        return await groupUsersCrud.read();
    }

    return {
        ...crud,
        users: {
            ...groupUsersCrud,
        },
        // TODO: remove this
        // getGroupsForProjectAndUser,
        getGroupsForUser,

        getGroupGroupsAndUsers,

        getUsers,
        getUsersAddedBefore,
        getGroup,
        getGroups,
        getGroupUsers,
        getGroupsForUserInScene,
    };
};
