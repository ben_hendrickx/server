module.exports = function rolesService({ usersService, crudService, projectRolesService }) {
    const crud = crudService('roles');

    async function getRole(id) {
        const roles = await crud.read();
        return roles.find(role => role.id === id);
    }

    async function getOnStageRoles() {
        const roles = await crud.read();
        return roles.filter(role => !!role.onStage);
    }

    async function getOffStageRoles() {
        const roles = await crud.read();
        return roles.filter(role => !role.onStage);
    }

    async function getUsers(roleId, castId) {
        if (!Array.isArray(roleId)) {
            roleId = [roleId];
        }

        if (!roleId.length) {
            return [];
        }

        const queryConfig = { project_role_id: roleId };
        castId && (queryConfig.castId = parseInt(castId));
        const project_role_users = await projectRolesService.users.read(queryConfig);

        const userIds = project_role_users.map(({ userId }) => userId);

        const users = await usersService.read({ id: userIds });
        const settings = await usersService.settings.read({ userId: userIds });

        return users.map(user => {
            const { pin } = settings.find(({ userId }) => userId === user.id) || {};
            user.pin = pin;
            return user;
        });
    }

    return {
        ...crud,
        getUsers,
        getRole,
        getOnStageRoles,
        getOffStageRoles,
    };
};
