module.exports = function({ crudService }) {
    const crud = crudService('countries');

    return {
        ...crud,
    };
};
