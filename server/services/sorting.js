const moment = require('moment');

module.exports = function() {
    function sortBy(array, field, isDate = false, inverse = false) {
        return array.sort((a, b) => {
            if (isDate) {
                if (inverse) {
                    return moment(a[field]).isSameOrBefore(moment(b[field])) ? -1 : 1;
                }
                return moment(a[field]).isSameOrBefore(moment(b[field])) ? 1 : -1;
            }

            const aField = typeof field === 'function' ? field(a) : a[field].toLowerCase();
            const bField = typeof field === 'function' ? field(b) : b[field].toLowerCase();

            if (Array.isArray(aField)) {
                if (aField[0] > bField[0]) {
                    return 1;
                }

                if (aField[0] < bField[0]) {
                    return 0;
                }

                return (aField[1] || 0) > (bField[1] || 0) ? 1 : (aField[1] || 0) < (bField[1] || 0) ? -1 : 0;
            }

            return aField > bField ? 1 : aField < bField ? -1 : 0;
        });
    }

    function sortScenes(a, b) {
        const aField = (a.sceneIdentifier || a.identifier)
            .replace('SC', '')
            .split('.')
            .map(v => parseInt(v));
        const bField = (b.sceneIdentifier || b.identifier)
            .replace('SC', '')
            .split('.')
            .map(v => parseInt(v));

        if (aField[0] === bField[0]) {
            return (aField[1] || 0) - (bField[1] || 0);
        }

        return aField[0] - bField[0];
    }

    function sortRoles(a, b) {
        const aField = a.identifier
            .replace('GR', '')
            .replace('INR', '')
            .split('.')
            .map(v => parseInt(v));
        const bField = b.identifier
            .replace('GR', '')
            .replace('INR', '')
            .split('.')
            .map(v => parseInt(v));

        if (aField[0] === bField[0]) {
            return (aField[1] || 0) - (bField[1] || 0);
        }

        return aField[0] - bField[0];
    }

    function sortRoleOverview(a, b) {
        if (a.projectId === b.projectId) {
            const sceneSort = sortScenes(a, b);
            if (!sceneSort) {
                return sortRoles(a, b);
            }
            return sceneSort;
        }

        return a.projectId - b.projectId;
    }

    return {
        sortBy,
        sortScenes,
        sortRoleOverview,
    };
};
