module.exports = function loginsService({ crudService }) {
    const crud = crudService('logins');
    const passwordChangeCrud = crudService('password_change_tokens');

    return {
        ...crud,
        changePasswordRequests: {
            ...passwordChangeCrud,
        },
    };
};
