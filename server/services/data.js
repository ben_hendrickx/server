const { type } = require('../../config').data || {};
const FALLBACK_CONNECTOR = 'json';
const moment = require('moment');

module.exports = function dataService({ logger, mysql: mysqlConnector }) {
    let connector;

    try {
        switch ((type || '').toUpperCase()) {
            case 'MYSQL':
            default:
                connector = mysqlConnector;
                break;
        }

        logger.success(`Data-connector for ${type || FALLBACK_CONNECTOR} successfully loaded`);
    } catch (e) {
        logger.debug(`Error with loading data-connector`, e);
        logger.error(`Could not load connector for ${type || FALLBACK_CONNECTOR}`);
        process.exit(9000);
    }

    function create(resource, data, keys) {
        data = Object.entries(data).reduce((acc, [key, value]) => {
            if (value instanceof Date || (typeof value === 'string' && value.endsWith('.000Z'))) {
                acc[key] = moment(value).format();

                if (key === 'date_of_birth') {
                    acc[key] = moment(value).format('YYYY-MM-DD');
                }
            }
            return acc;
        }, data);

        return connector.add(resource, data, keys);
    }

    async function read(resource, config) {
        return await connector.get(resource, config);
    }

    function count(resource, config) {
        return connector.get(resource, config, true);
    }

    async function update(resource, id, delta) {
        delta = Object.entries(delta).reduce((acc, [key, value]) => {
            if (typeof value === 'string' && value.endsWith('.000Z')) {
                acc[key] = moment(value).format();
            }
            return acc;
        }, delta);

        // const [data] = await connector.get(resource, { id });
        const updatedData = Object.assign({}, delta);
        return connector.set(resource, typeof id === 'object' ? id : { id }, updatedData);
    }

    function remove(resource, id) {
        return connector.remove(resource, id);
    }

    function transaction(transactions) {
        transactions = transactions.map(t => {
            if (t.method === 'delete') {
                t.method = 'remove';
            }
            return t;
        });
        return connector.transaction(transactions);
    }

    function raw(query) {
        return connector.raw(query);
    }

    return {
        create,
        read,
        count,
        update,
        delete: remove,
        transaction,
        raw,
    };
};
