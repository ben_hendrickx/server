const {
    mail: { colors, urls, client, mail },
    server: { hostnameShort },
} = require('../../config');

module.exports = function templateService({ fileService, logger }) {
    async function get(name, object = {}) {
        object.primaryColor = colors.primary;
        object.secondaryColor = colors.secondary;
        object.facebook = urls.facebook;
        object.instagram = urls.instagram;
        object.hostnameShort = hostnameShort;
        object.client = client;
        object.mail = mail;

        try {
            let string = await fileService(`../templates/${name}.html`).read();

            return Object.entries(object).reduce((acc, [key, value]) => {
                while (acc.includes(`{${key}}`)) {
                    acc = acc.replace(`{${key}}`, value);
                }
                return acc;
            }, string);
        } catch (e) {
            logger.error(`Could not load template ${name}`, e.message);
            throw e;
        }
    }

    async function getAll() {
        const files = await fileService(`../templates`).read();

        return files
            .filter(file => file.includes('.html'))
            .map(file => {
                return {
                    label: file,
                    value: file,
                };
            });
    }

    return {
        get,
        getAll,
    };
};
