/* istanbul ignore file */
const moment = require('moment');
const localization = require('moment/locale/nl-be');

function pad(value, length) {
    if ((value || '').toString().length >= length) {
        return value;
    }

    return pad('0' + value, length);
}

function getCurrentPrice(category, date = Date.now()) {
    const prices = category.prices
        .filter(price => {
            if (moment(price.startDt).startOf('day') > date) {
                return false;
            }

            if (price.endDt && price.endDt < date) {
                return false;
            }

            return true;
        })
        .sort((a, b) => {
            if (new Date(a.startDt).getTime() > new Date(b.startDt).getTime()) {
                return -1;
            } else if (new Date(a.startDt).getTime() < new Date(b.startDt).getTime()) {
                return 1;
            } else {
                return 0;
            }
        });

    return prices[0];
}

module.exports = function pdfService({ templateService, clothingService, repetitionsService, authenticationService }) {
    async function getOrderDocument(id) {
        const {
            internal,
            name,
            socialSecurity,
            companyName,
            uniqueNumber,
            isFor,
            streetName,
            houseNumber,
            bus,
            zipCode,
            cityName,
            cellPhone,
            type,
            email,
            items,
            periods,
            info,
            creationDt,
            fromDt,
            toDt,
        } = await clothingService.getOrder(id);
        const categories = await clothingService.getCategories();

        let field1, field2, field3, field4, field5;

        switch (type) {
            case 1:
                field1 = internal ? 'Gelenaar' : 'Niet-Gelenaar';
                field2 = name || '';
                field3 = socialSecurity || '';
                field4 = '';
                field5 = '';
                break;
            case 2:
                field1 = internal ? 'Geelse vereniging' : 'Niet-Geelse vereniging';
                field2 = companyName || '';
                field3 = name || '';
                field4 = uniqueNumber || '';
                field5 = !uniqueNumber ? isFor || '' : '';
                break;
            case 3:
                field1 = internal ? 'Geels bedrijf' : 'Niet-Geels bedrijf';
                field2 = companyName || '';
                field3 = name || '';
                field4 = uniqueNumber || isFor || '';
                field5 = !uniqueNumber ? isFor || '' : '';
                break;
        }

        const prices = await clothingService.calculatePrices(items, creationDt, periods, internal);

        const content = await templateService.get('order', {
            orderNumber: id,
            field1,
            field2,
            field3,
            field4,
            field5,
            address1: `${streetName} ${houseNumber}${bus || ''}`,
            address2: `${zipCode} ${cityName}`,
            cellPhone,
            email,

            columns: `<tr>${['Art.', 'Naam', 'Eenheidsprijs', 'Aantal', 'Totaal', 'Totale VW']
                .map(col => `<th style="width: 10%">${col}</th>`)
                .join('')}</tr>`,
            items: items
                .map(({ id, pattern, color, fabric, size, category, amount, remarks }) => {
                    const { internalPrice, externalPrice, replacementPrice } = getCurrentPrice(
                        categories.find(({ id }) => id === category?.id),
                        moment(creationDt)
                    );
                    const price = internal ? internalPrice : externalPrice;
                    return `<tr>${[
                        pad(id, 6),
                        category.name || '',
                        `€ ${price.toFixed(2)}`,
                        amount,
                        `€ ${(price * amount * periods).toFixed(2)}`,
                        `€ ${(replacementPrice * amount).toFixed(2)}`,
                    ]
                        .map(col => `<td>${col}</td>`)
                        .join('')}</tr>
                        <tr>
                            <td class="small"></td>
                            <td class="small" colspan="5" style="text-align: left;">
                                <b>Motief: </b>${pattern.name || 'nvt'}&nbsp;&nbsp;&nbsp;
                                <b>Kleur: </b>${color.name || 'nvt'}&nbsp;&nbsp;&nbsp;
                                <b>Stof: </b>${fabric.type || 'nvt'}&nbsp;&nbsp;&nbsp;
                                <b>Maat: </b>${size.size || 'nvt'}&nbsp;&nbsp;&nbsp;
                            </td>
                        ${
                            remarks
                                ? `<tr><td class="small"></td><td class="small" colspan="5" style="text-align: left;"><b>Opmerkingen: </b>${remarks}</td></tr>`
                                : ''
                        }`;
                })
                .join(''),

            today: moment().format('DD/MM/YYYY'),
            orderDate: moment(creationDt).format('DD/MM/YYYY'),
            fromDt: moment(fromDt).format('DD/MM/YYYY'),
            toDt: moment(toDt).format('DD/MM/YYYY'),
            returnDt: moment(toDt)
                .add(14, 'days')
                .format('DD/MM/YYYY'),
            periods,
            excl: `€ ${prices.price.toFixed(2)}`,
            vat: `€ ${(prices.price * 0.21).toFixed(2)}`,
            incl: `€ ${(prices.price * 1.21).toFixed(2)}`,
            replexcl: `€ ${prices.replacementPrice.toFixed(2)}`,
            replvat: `€ ${(prices.replacementPrice * 0.21).toFixed(2)}`,
            replincl: `€ ${(prices.replacementPrice * 1.21).toFixed(2)}`,
            remarks: info ? `<b>Opmerking: </b> ${info}` : '',
        });

        const axios = require('axios');
        const FormData = require('form-data');

        const formData = new FormData();
        formData.append(
            'instructions',
            JSON.stringify({
                parts: [
                    {
                        html: 'index.html',
                    },
                ],
            })
        );
        formData.append('index.html', content);
        try {
            const response = await axios.post('https://api.pspdfkit.com/build', formData, {
                headers: formData.getHeaders({
                    Authorization: 'Bearer pdf_live_lKlo0EJayCyhWJaET4KndGKqVKmvk5INHyN9ujevCXp',
                }),
                responseType: 'arraybuffer',
            });
            return response.data;
        } catch (e) {
            const errorString = await streamToString(e.response.data);
            throw errorString;
        }

        function streamToString(stream) {
            const chunks = [];
            return new Promise((resolve, reject) => {
                stream.on('data', chunk => chunks.push(Buffer.from(chunk)));
                stream.on('error', err => reject(err));
                stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')));
            });
        }
    }

    async function getRepetitionsDocument(req) {
        const { id, level, firstName, lastName } = await authenticationService.verifyToken(req.query.token);
        const repetitions = await repetitionsService.getRepetitionsForUser(id, level);

        const content = await templateService.get('repetitions', {
            user: `${lastName} ${firstName}`,
            items: repetitions
                .map(r => {
                    return `
                    <div class="repetition">
                    <div class="header">
                        <div class="info">${[
                            `${r.name} (${r.type?.name})`,
                            `${moment(r.startDt)
                                .locale('nl', localization)
                                .format('dddd DD-MM-YYYY')} ${moment(r.startDt).format('HH:mm')} - ${moment(
                                r.endDt
                            ).format('HH:mm')}`,
                        ]
                            .map(col => `<span>${col}</span>`)
                            .join('')}
                        </div>
                        <div class="address">${r.location?.name}&nbsp;&nbsp;&nbsp;&nbsp;${r.location?.streetName} ${
                        r.location?.houseNumber
                    }, ${r.location?.zipCode} ${r.location?.cityName}</td>
                        </div>
                    </div>
                    ${
                        !!r.allChecked || !!r.castChecked
                            ? r.description
                                ? `<div class="roles">${r.description}</div>`
                                : ''
                            : `<div class="roles">
                                ${(r.scenes || [])
                                    .map(s =>
                                        s.roles
                                            .map(
                                                r =>
                                                    `<div class="role"><span><b>Scene</b>: ${s.identifier} - ${s.name}</span><span><b>Rol</b>: ${r.identifier} - ${r.name}</span></div>`
                                            )
                                            .join('')
                                    )
                                    .join('')}
                                ${(r.roles || [])
                                    .map(
                                        r =>
                                            `<div class="role"><span><b>Rol</b>: ${r.identifier} - ${r.name}</span></div>`
                                    )
                                    .join('')}
                            </div>`
                    }
                    </div>`;
                })
                .join(''),
        });

        const axios = require('axios');
        const FormData = require('form-data');

        const formData = new FormData();
        formData.append(
            'instructions',
            JSON.stringify({
                parts: [
                    {
                        html: 'index.html',
                    },
                ],
            })
        );
        formData.append('index.html', content);
        try {
            const response = await axios.post('https://api.pspdfkit.com/build', formData, {
                headers: formData.getHeaders({
                    Authorization: 'Bearer pdf_live_lKlo0EJayCyhWJaET4KndGKqVKmvk5INHyN9ujevCXp',
                }),
                responseType: 'arraybuffer',
            });
            return response.data;
        } catch (e) {
            const errorString = await streamToString(e.response.data);
            throw errorString;
        }

        function streamToString(stream) {
            const chunks = [];
            return new Promise((resolve, reject) => {
                stream.on('data', chunk => chunks.push(Buffer.from(chunk)));
                stream.on('error', err => reject(err));
                stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')));
            });
        }
    }

    return {
        getOrderDocument,
        getRepetitionsDocument,
    };
};
