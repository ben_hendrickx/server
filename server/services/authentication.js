const jwt = require('jsonwebtoken');
const CryptoJS = require('crypto-js');
const moment = require('moment');

// Base64 van: fanvandimpna in samenwerking met kommaboard
// --> ZmFudmFuZGltcG5hIGluIHNhbWVud2Vya2luZyBtZXQga29tbWFib2FyZA
// --> Sha256 van bovenstaande base64
// TODO: move to .env
const SECRET = '7d6ac074040ce958425d53da29f9034b6cc556e89426f2dda4fc0ad627ad7fb0';

// TODO: save to users instead of saving it here
const userTokens = {};

module.exports = function authenticationService({ errors, crudService, logger, loginsService, onlineUsersService }) {
    const usersService = {
        ...crudService('users'),
        settings: {
            ...crudService('user_app_settings'),
        },
    };

    // TODO: implement crudService for logins?
    async function authenticate(email, password) {
        if (!email || !password) {
            throw errors.httpStatusCodeError(400, 'Gelieve emailadres én wachtwoord in te vullen');
        }

        const pwd = CryptoJS.SHA512(password).toString();
        const users = await loginsService.read({ email, pwd });
        const user = users.length ? users[0] : undefined;

        if (!user) {
            logger.warn(`Found no user for email: ${email} and password: ${password}`);
            // TODO: return custom NotFoundError
            throw errors.httpStatusCodeError(401, 'Verkeerd emailadres of wachtwoord');
        }

        if (!user.confirmed) {
            throw errors.httpStatusCodeError(
                401,
                'Dit e-mailadres werd nog niet bevestigd, gelieve dit eerst te bevestigen via de mail die u werd toegezonden'
            );
        }

        const profiles = await usersService.read({ email });
        const [{ id, firstName, lastName, confirmedDt, clothingFrozen }] = profiles;
        // TODO: create helper for days ago (momentjs)
        const shouldVerify = new Date(confirmedDt).getTime() <= Date.now() - 30 * 24 * 60 * 60 * 1000;

        const settings = await usersService.settings.read({ userId: id });
        const [profileSettings] = settings && settings.length ? settings : [{}];
        const { level } = profileSettings;

        const token = jwt.sign(
            {
                email,
                firstName,
                lastName,
                level,
                id,
                since: Date.now(),
            },
            SECRET,
            {
                expiresIn: '10h',
            }
        );

        onlineUsersService.add(profiles[0]);

        let userToken = userTokens[`${firstName}_${lastName}_${email}`];
        userToken ? userToken.push(token) : (userTokens[`${firstName}_${lastName}_${email}`] = [token]);

        const canEditClothingSizes = !clothingFrozen || moment(clothingFrozen).isBefore(moment());

        return {
            token,
            firstName,
            lastName,
            level,
            email,
            id,
            shouldVerify,
            canEditClothingSizes,
        };
    }

    //------------------------//

    async function switchProfile(token, id) {
        id = parseInt(id);
        try {
            const { email, firstName: fName, lastName: lName } = jwt.verify(token, SECRET);

            const user = await loginsService.read({ email });

            if (!user || !user.length) {
                logger.warn(`Found no user for email: ${email}`);
                // TODO: return custom NotFoundError
                return { status: 404 };
            }

            logger.debug(`Received request to switch to profile ${id}`);
            if (!token) {
                // TODO: split out to validation system
                return { status: 400 };
            }

            const profiles = await usersService.read({ email, id });
            const [{ id: userId, firstName, lastName, confirmedDt }] = profiles;
            const shouldVerify = new Date(confirmedDt).getTime() <= Date.now() - 30 * 24 * 60 * 60 * 1000;
            //const shouldVerify = new Date(confirmedDt).getTime() <= Date.now() - 2 * 60 * 60 * 1000;

            const settings = await usersService.settings.read({ userId });
            const [profileSettings] = settings && settings.length ? settings : [{}];
            const { level } = profileSettings;

            logger.debug(firstName, lastName, level, email);

            onlineUsersService.remove({ email });
            onlineUsersService.add(profiles[0]);

            const index = userTokens[`${firstName}_${lastName}_${email}`]
                ? userTokens[`${firstName}_${lastName}_${email}`].indexOf(token)
                : -1;

            token = jwt.sign(
                {
                    email,
                    firstName,
                    lastName,
                    level,
                    id: userId,
                    since: Date.now(),
                },
                SECRET,
                {
                    expiresIn: '10h',
                }
            );

            if (index > -1) {
                userTokens[`${firstName}_${lastName}_${email}`].splice(index, 1, token);
            } else {
                // eslint-disable-next-line require-atomic-updates
                userTokens[`${firstName}_${lastName}_${email}`] = [token];
            }

            logger.success(`Found user ${JSON.stringify(user[0])}`);
            return {
                success: true,
                token,
                firstName,
                lastName,
                level,
                email,
                id: userId,
                shouldVerify,
            };
        } catch (e) {
            return { status: 404 };
        }
    }

    async function verifyToken(token) {
        // const [userIdentifier, savedToken] =
        //     Object.entries(userTokens).find(([, savedToken]) => {
        //         return savedToken.includes(token);
        //     }) || [];

        // if (!userIdentifier || !savedToken || !savedToken.includes(token)) {
        //     return { status: 404 };
        // }
        let data;
        try {
            data = jwt.verify(token, SECRET);
        } catch (e) {
            return { status: 404 };
        }
        const profiles = await usersService.read({ email: data.email, id: data.id });
        if (!profiles?.length) {
            return;
        }

        const [{ confirmedDt }] = profiles;
        // TODO: shouldVerify should not be managed by token
        const shouldVerify = !confirmedDt || new Date(confirmedDt).getTime() <= Date.now() - 30 * 24 * 60 * 60 * 1000;

        return {
            ...data,
            shouldVerify,
        };
    }

    async function logout(token) {
        const { email } = await verifyToken(token);
        onlineUsersService.remove({ email });
        return { success: true };

        // const index = userTokens[`${firstName}_${lastName}_${email}`]?.indexOf(token);

        // if (index > -1) {
        //     userTokens[`${firstName}_${lastName}_${email}`].splice(index, 1);
        // }

        // if (
        //     userTokens[`${firstName}_${lastName}_${email}`] &&
        //     !userTokens[`${firstName}_${lastName}_${email}`].length
        // ) {
        //     delete userTokens[`${firstName}_${lastName}_${email}`];
        // }
        // return { success: true };
    }

    return {
        authenticate,
        verifyToken,
        switchProfile,
        logout,
    };
};
