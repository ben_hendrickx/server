module.exports = function groupUsersService({ groupsService }) {
    async function deleteGroupUsers(groupId, castId, userId) {
        const config = {};
        groupId && (config.groupId = groupId);
        castId && (config.castId = castId);
        userId && (config.userId = userId);
        await groupsService.users.remove(config);
    }

    return {
        deleteGroupUsers,
    };
};
