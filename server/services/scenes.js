module.exports = function scenesService({ groupsService, rolesService, projectRolesService, crudService }) {
    const crud = crudService('scenes');
    const sceneGroupsCrud = crudService('scene_groups');
    const sceneRolesCrud = crudService('scene_roles');

    // TODO: remove this
    // async function getScenesForProjectAndUser(projectId, userId) {
    //     const scenesForProject = await crud.read({ projectId });
    //     const sceneIds = scenesForProject.map(s => s.id);
    //     const sceneGroups = await sceneGroupsCrud.read({ sceneId: sceneIds });
    //     const sceneRoles = await sceneRolesCrud.read({ sceneId: sceneIds });

    //     const userGroups = await groupsService.getGroupsForProjectAndUser(projectId, userId);
    //     const userGroupIds = userGroups.map(uG => uG.id);
    //     const userRoles = await projectRolesService.getProjectRolesForProjectAndUser(projectId, userId);
    //     const userRoleIds = userRoles.map(uR => uR.id);

    //     return scenesForProject.filter(scene => {
    //         const sceneGroupIds = sceneGroups.filter(sG => sG.sceneId === scene.id).map(sG => sG.groupId);
    //         const sceneRoleIds = sceneRoles.filter(sG => sG.sceneId === scene.id).map(sG => sG.project_role_id);

    //         return (
    //             sceneGroupIds.some(sGId => userGroupIds.includes(sGId)) ||
    //             sceneRoleIds.some(sRId => userRoleIds.includes(sRId))
    //         );
    //     });
    // }

    // TODO: remove this
    async function getScenesForUser(userId) {
        const scenes = await crud.read();
        const sceneIds = scenes.map(s => s.id);
        const sceneGroups = await sceneGroupsCrud.read({ sceneId: sceneIds });
        const sceneRoles = await sceneRolesCrud.read({ sceneId: sceneIds });

        const userGroups = await groupsService.getGroupsForUser(userId);
        const userGroupIds = userGroups.map(uG => uG.id);
        const userRoles = await projectRolesService.getProjectRolesForUser(userId);
        const userRoleIds = userRoles.map(uR => uR.id);

        return scenes.filter(scene => {
            const sceneGroupIds = sceneGroups.filter(sG => sG.sceneId === scene.id).map(sG => sG.groupId);
            const sceneRoleIds = sceneRoles.filter(sG => sG.sceneId === scene.id).map(sG => sG.project_role_id);

            return (
                sceneGroupIds.some(sGId => userGroupIds.includes(sGId)) ||
                sceneRoleIds.some(sRId => userRoleIds.includes(sRId))
            );
        });
    }

    async function getScenes() {
        return crud.read();
    }

    async function getScene(id) {
        return await getScenes().then(scenes => {
            const scene = { ...(scenes.find(scene => scene.id === id) || {}) };
            delete scene.groups;
            delete scene.roles;
            return scene;
        });
    }

    async function getAllScenesForProject(projectId) {
        return await getScenes().then(scenes => scenes.filter(scene => scene.projectId === parseInt(projectId)));
    }

    function sortByIdentifier(scenes) {
        return [...scenes].sort((a, b) => {
            const aI = parseInt(a.identifier.replace('SC', ''));
            const bI = parseInt(b.identifier.replace('SC', ''));

            if (aI === bI) {
                const aF = a.identifier.includes('.')
                    ? parseInt(a.identifier.substr(a.identifier.indexOf('.') + 1))
                    : 0;
                const bF = b.identifier.includes('.')
                    ? parseInt(b.identifier.substr(b.identifier.indexOf('.') + 1))
                    : 0;

                return aF - bF;
            }

            return aI - bI;
        });
    }

    async function getUsers(sceneId, castId) {
        if (!Array.isArray(sceneId)) {
            sceneId = [sceneId];
        }

        if (!sceneId.length) {
            return [];
        }

        const [scene_groups = [], scene_roles = []] = await Promise.all([
            sceneGroupsCrud.read({ sceneId }),
            sceneRolesCrud.read({ sceneId }),
        ]);

        const groupIds = scene_groups.map(({ groupId }) => groupId);
        const roleIds = scene_roles.map(({ project_role_id }) => project_role_id);

        const [groupUsers, roleUsers] = await Promise.all([
            groupsService.getUsers(groupIds, castId),
            rolesService.getUsers(roleIds, castId),
        ]);

        const allUsers = groupUsers.concat(roleUsers);

        return allUsers.reduce((acc, user) => {
            if (!acc.some(({ id }) => id === user.id)) {
                acc.push(user);
            }

            return acc;
        }, []);
    }

    async function getSceneGroups() {
        return await sceneGroupsCrud.read();
    }

    async function getSceneRoles() {
        return await sceneRolesCrud.read();
    }

    return {
        ...crud,
        roles: {
            ...sceneRolesCrud,
        },
        groups: {
            ...sceneGroupsCrud,
        },
        // TODO: remove this
        //getScenesForProjectAndUser,
        // TODO: remove this
        getScenesForUser,

        getAllScenesForProject,
        utils: {
            sortByIdentifier,
        },
        getUsers,
        getScene,
        getScenes,
        getSceneGroups,
        getSceneRoles,
    };
};
