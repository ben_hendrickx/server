module.exports = function({ crudService }) {
    const crud = crudService('casts');

    return {
        ...crud,
    };
};
