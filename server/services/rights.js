module.exports = function rightsService({ crudService }) {
    const crud = crudService('rights');
    const subRightsCrud = crudService('sub_rights');
    const userRightsCrud = crudService('user_rights');
    const userSubRightsCrud = crudService('user_sub_rights');
    const projectRightsCrud = crudService('project_user_rights');
    const projectSubRightsCrud = crudService('project_user_sub_rights');
    const projectAccessRolesCrud = crudService('project_access_roles');
    const projectUserAccessRolesCrud = crudService('project_user_access_roles');
    const accessRoleRightsCrud = crudService('access_role_rights');
    const accessRoleSubRightsCrud = crudService('access_role_sub_rights');
    const userAccessRolesCrud = crudService('user_access_roles');

    async function getUserIdsWithRight(rightName) {
        const [right] = await crud.read({ name: rightName });

        if (!right) {
            return [];
        }

        const { id: rightId } = right;
        const userRights = await userRightsCrud.read({ rightId });
        const usersWithRight = userRights.filter(right => right.read);
        const userIdsWithRight = usersWithRight.map(({ userId }) => userId);

        const accessRoles = await accessRoleRightsCrud.read({ rightId });
        const accessRolesWithRight = accessRoles.filter(accessRole => accessRole.read);
        const accessRoleIds = accessRolesWithRight.map(({ accessRoleId }) => accessRoleId);
        const usersWithAccessRole = await userAccessRolesCrud.read({ accessRoleId: accessRoleIds });
        const userIdsWithAccessRole = usersWithAccessRole.map(({ userId }) => userId);

        const projectAccessRolesWithRight = await projectAccessRolesCrud.read({ accessRoleId: accessRoleIds });
        const projectAccessRoleIds = projectAccessRolesWithRight.map(({ id }) => id);
        const usersWithProjectAccessRole = await projectUserAccessRolesCrud.read({
            projectAccessRoleId: projectAccessRoleIds,
        });
        const userIdsWithProjectAccessRole = usersWithProjectAccessRole.map(({ userId }) => userId);

        return [...userIdsWithRight, ...userIdsWithAccessRole, ...userIdsWithProjectAccessRole].filter(v => v);
    }

    async function getRights() {
        const rights = await crud.read();
        const subRights = await subRightsCrud.read();

        return rights.map(right => {
            const rightSubRights = subRights
                .filter(subRight => {
                    return subRight.rightId === right.id;
                })
                .map(subRight => {
                    return {
                        ...subRight,
                    };
                });

            return {
                ...right,
                subRights: rightSubRights,
            };
        });
    }

    async function getRightsForProject(projectId) {
        const rights = await crud.read();
        const subRights = await subRightsCrud.read();
        const projectRights = await projectRightsCrud.query({ projectId });
        const projectSubRights = await projectSubRightsCrud.query({ projectId });

        return rights.map(right => {
            const projectRight = projectRights.find(accessRoleRight => accessRoleRight.rightId === right.id);
            const rightSubRights = subRights
                .filter(subRight => {
                    return subRight.rightId === right.id;
                })
                .map(subRight => {
                    const projectSubRight = projectSubRights.find(
                        accessRoleSubRight => accessRoleSubRight.subRightId === subRight.id
                    );
                    return {
                        ...subRight,
                        access: !!projectSubRight?.access,
                    };
                });

            return {
                ...right,
                read: !!projectRight?.read,
                create: !!projectRight?.create,
                update: !!projectRight?.update,
                delete: !!projectRight?.delete,
                subRights: rightSubRights,
            };
        });
    }

    async function getRightsForAccessRole(accessRoleId) {
        const rights = await crud.read();
        const subRights = await subRightsCrud.read();
        const accessRoleRights = await accessRoleRightsCrud.query({ accessRoleId });
        const accessRoleSubRights = await accessRoleSubRightsCrud.query({ accessRoleId });

        return rights.map(right => {
            const accessRoleRight = accessRoleRights.find(accessRoleRight => accessRoleRight.rightId === right.id);
            const rightSubRights = subRights
                .filter(subRight => {
                    return subRight.rightId === right.id;
                })
                .map(subRight => {
                    const accessRoleSubRight = accessRoleSubRights.find(
                        accessRoleSubRight => accessRoleSubRight.subRightId === subRight.id
                    );
                    return {
                        ...subRight,
                        access: !!accessRoleSubRight?.access,
                    };
                });

            return {
                ...right,
                read: !!accessRoleRight?.read,
                create: !!accessRoleRight?.create,
                update: !!accessRoleRight?.update,
                delete: !!accessRoleRight?.delete,
                subRights: rightSubRights,
            };
        });
    }

    async function getRightsForUser(userId) {
        let [rights, subRights, userRights, userSubRights, userAccessRoles] = await Promise.all([
            crud.read(),
            subRightsCrud.read(),
            userRightsCrud.read({ userId }),
            userSubRightsCrud.read({ userId }),
            userAccessRolesCrud.read({ userId }),
        ]);

        userRights = rights.map(right => {
            const userRight = userRights.find(accessRoleRight => accessRoleRight.rightId === right.id);
            const rightSubRights = subRights
                .filter(subRight => {
                    return subRight.rightId === right.id;
                })
                .map(subRight => {
                    const userSubRight = userSubRights.find(
                        accessRoleSubRight => accessRoleSubRight.subRightId === subRight.id
                    );
                    return {
                        ...subRight,
                        access: !!userSubRight?.access,
                    };
                });

            return {
                ...right,
                read: !!userRight?.read,
                create: !!userRight?.create,
                update: !!userRight?.update,
                delete: !!userRight?.delete,
                subRights: rightSubRights,
            };
        });

        return (
            await Promise.all([
                ...userAccessRoles.map(({ accessRoleId }) => getRightsForAccessRole(accessRoleId)),
                ...userRights,
            ])
        ).flat();
    }

    return {
        ...crud,
        user: {
            rights: {
                ...userRightsCrud,
            },
            subRights: {
                ...userSubRightsCrud,
            },
        },
        getUserIdsWithRight,
        getRights,
        getRightsForAccessRole,
        getRightsForProject,
        getRightsForUser,
    };
};
