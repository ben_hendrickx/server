module.exports = function CRUD({ dataService }) {
    const cachedCruds = {};

    return function(resource) {
        if (cachedCruds[resource]) {
            return cachedCruds[resource];
        }

        let promise;

        async function read(config = {}) {
            if (!promise) {
                promise = dataService.read(resource);
            }

            const data = (await promise) || [];

            return data
                .filter(item => {
                    return Object.entries(config).every(([key, value]) => {
                        if (Array.isArray(value)) {
                            return value.includes(item[key]);
                        }

                        if (typeof item[key] === 'string') {
                            return item[key].trim().toLowerCase() === (value || '').trim().toLowerCase();
                        }

                        return item[key] === value;
                    });
                })
                .map(item => ({ ...item }));
        }

        async function count(config) {
            return await dataService.count(resource, config);
        }

        async function query(config) {
            const data = await read();

            return data.filter(row => {
                for (let [key, value] of Object.entries(config)) {
                    if (row[key] !== value) {
                        return false;
                    }
                }

                return true;
            });
        }

        async function update(id, data) {
            await dataService.update(resource, id, data);
            clear();
        }

        async function create(data, keys) {
            const id = await dataService.create(resource, data, keys);
            clear();
            return id;
        }

        async function remove(id) {
            await dataService.delete(resource, id);
            clear();
        }

        async function archive(id) {
            await dataService.update(resource, id, { archive: 1 });
            clear();
        }

        function clear() {
            promise = undefined;
        }

        cachedCruds[resource] = {
            count,
            read,
            query,
            update,
            create,
            remove,
            archive,
            clear,
        };

        return cachedCruds[resource];
    };
};
