module.exports = function fileService({ logger }) {
    return function(pathName) {
        const fs = require('fs');
        const path = require('path');

        pathName = path.resolve(__dirname, pathName);

        // TODO: create util for making callback functions promiseable
        // TODO: replace all return new Promise by the new util (for using async await)
        function read() {
            return new Promise((resolve, reject) => {
                const isDirectory = fs.lstatSync(pathName).isDirectory();

                if (isDirectory) {
                    return fs.readdir(pathName, (err, files) => {
                        if (err) {
                            logger.error(`Error with reading file on path ${pathName}, ${err}`);
                            return reject(err);
                        }

                        return resolve(files);
                    });
                }

                fs.readFile(pathName, 'utf-8', (err, data) => {
                    if (err) {
                        logger.error(`Error with reading file on path ${pathName}, ${err}`);
                        return reject(err);
                    }

                    return resolve(data);
                });
            });
        }

        function write(data) {
            return new Promise((resolve, reject) => {
                fs.writeFile(pathName, data, err => {
                    if (err) {
                        logger.error(`Error with writing data to file on path ${pathName}, ${err}`);
                        return reject(err);
                    }

                    return resolve();
                });
            });
        }

        return {
            read,
            write,
        };
    };
};
