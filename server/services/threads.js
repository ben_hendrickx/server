module.exports = function threadsService({ crudService }) {
    const threadsCrud = crudService('threads');
    const threadUsersCrud = crudService('thread_users');
    const threadMessagesCrud = crudService('thread_messages');
    const threadActionsCrud = crudService('thread_actions');
    const threadActionUsersCrud = crudService('thread_action_users');

    return {
        ...threadsCrud,
        users: {
            ...threadUsersCrud,
        },
        messages: {
            ...threadMessagesCrud,
        },
        actions: {
            ...threadActionsCrud,
            users: {
                ...threadActionUsersCrud,
            },
        },
    };
};
