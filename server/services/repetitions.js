const { OFF_STAGE_REPETITIONS } = process.env;

module.exports = function repetitionsService({
    usersService,
    groupsService,
    rolesService,
    scenesService,
    castsService,
    crudService,
    projectsService,
    projectRolesService,
    dataService,
    queryService,
    sortingService,
    repetitionSceneRoleUsersQuery,
    repetitionSceneGroupUsersQuery,
    repetitionSceneUsersQuery,
    repetitionRoleUsersQuery,
    repetitionGroupUsersQuery,
}) {
    const crud = crudService('repetitions');
    const usersCrud = crudService('repetition_users');
    const presenceCrud = crudService('repetition_user_presence');
    const locationsCrud = crudService('locations');
    const typesCrud = crudService('types');

    async function resolveUserRow({ sceneId, groupId, roleId, castId, userId }) {
        const promises = [scenesService.getScene(sceneId), castsService.read({ id: castId })];

        let users;

        promises.push(groupId ? groupsService.getGroup(groupId) : Promise.resolve());
        promises.push(roleId ? projectRolesService.read({ id: roleId }) : Promise.resolve([]));

        const [scene, cast, group, [role]] = await Promise.all(promises);

        if (userId) {
            users = [await usersService.getUser(userId)];
        } else {
            if (sceneId && !groupId && !roleId) {
                users = await scenesService.getUsers(sceneId, castId);

                if (!users.length) {
                    return {
                        scene,
                        cast,
                        group,
                        role,
                        users,
                    };
                }

                users = await Promise.all(
                    users.map(async user => {
                        const groups = await groupsService.getGroupsForUserInScene(user.id, sceneId);
                        const roles = await projectRolesService.getProjectRolesForUserInScene(user.id, sceneId);

                        if (!groups.length && !roles.length) {
                            return {
                                scene,
                                cast,
                                group,
                                role,
                                users,
                            };
                        }

                        return [
                            ...groups.map(group => ({
                                scene,
                                cast,
                                group,
                                role,
                                users: [user],
                            })),
                            ...roles.map(role => ({
                                scene,
                                cast,
                                role,
                                group,
                                users: [user],
                            })),
                        ];
                    })
                );

                return users;
            } else if (groupId) {
                users = await groupsService.getUsers(groupId, castId);
            } else if (roleId) {
                users = await rolesService.getUsers(roleId, castId);
            } else {
                users = [];
            }
        }

        return {
            scene,
            cast,
            group,
            role,
            users,
        };
    }

    async function getRoleUsersForRepetitionsAndUser(userId, repetitionIds) {
        const queryConfig = repetitionRoleUsersQuery({
            userId: parseInt(userId),
            repetitionId: repetitionIds,
        });
        const { query } = await queryService(queryConfig);
        return await dataService.raw(query);
    }

    async function getGroupUsersForRepetitionsAndUser(userId, repetitionIds) {
        const queryConfig = repetitionGroupUsersQuery({
            userId: parseInt(userId),
            repetitionId: repetitionIds,
        });
        const { query } = await queryService(queryConfig);
        return await dataService.raw(query);
    }

    async function getSceneRoleUsersForRepetitionsAndUser(userId, repetitionIds) {
        const queryConfig = repetitionSceneRoleUsersQuery({
            userId: parseInt(userId),
            repetitionId: repetitionIds,
        });
        const { query } = await queryService(queryConfig);
        return await dataService.raw(query);
    }

    async function getSceneGroupUsersForRepetitionsAndUser(userId, repetitionIds) {
        const queryConfig = repetitionSceneGroupUsersQuery({
            userId: parseInt(userId),
            repetitionId: repetitionIds,
        });
        const { query } = await queryService(queryConfig);
        return await dataService.raw(query);
    }

    async function getSceneUsersForRepetitionsAndUser(userId, repetitionIds) {
        const rolesQueryConfig = repetitionSceneUsersQuery.roles({
            userId: parseInt(userId),
            repetitionId: repetitionIds,
        });
        const groupsQueryConfig = repetitionSceneUsersQuery.groups({
            userId: parseInt(userId),
            repetitionId: repetitionIds,
        });
        const { query: rolesQuery } = await queryService(rolesQueryConfig);
        const { query: groupsQuery } = await queryService(groupsQueryConfig);
        const data = await Promise.all([dataService.raw(rolesQuery), dataService.raw(groupsQuery)]);

        return data.flat();
    }

    // TODO: rework???
    async function getRepetitionsForUser(userId, level, past) {
        const futureRepetitions = (await crud.read()).filter(repetition => {
            if (level === 0 && !repetition.open) {
                return false;
            }

            if (level === 0 && past === undefined) {
                return new Date(repetition.endDt).getTime() > Date.now();
            }

            if (past === 2) {
                return true;
            }

            if (past === undefined) {
                return true;
            }

            if (past === 1) {
                return new Date(repetition.endDt).getTime() < Date.now();
            }

            return new Date(repetition.endDt).getTime() > Date.now();
        });
        const repetitionIds = futureRepetitions.map(({ id }) => id);

        const projectIds = futureRepetitions.map(({ projectId }) => projectId);
        const projects = await projectsService.read({ id: projectIds });
        const presences = await presenceCrud.read({ repetitionId: repetitionIds, userId });
        const locations = await locationsCrud.read();
        const types = await typesCrud.read();

        const data = await Promise.all([
            getSceneUsersForRepetitionsAndUser(userId, repetitionIds),
            getSceneRoleUsersForRepetitionsAndUser(userId, repetitionIds),
            getSceneGroupUsersForRepetitionsAndUser(userId, repetitionIds),
            getRoleUsersForRepetitionsAndUser(userId, repetitionIds),
            getGroupUsersForRepetitionsAndUser(userId, repetitionIds),
        ]);

        const result = data.flat().reduce((acc, row) => {
            if (!acc[row.id]) {
                const repetition = futureRepetitions.find(({ id }) => id === row.id);
                const presence = !!(
                    presences.find(({ repetitionId }) => repetitionId === repetition.id) || { markedPresent: true }
                ).markedPresent;
                const location = locations.find(({ id }) => id === repetition.locationId);
                const type = types.find(({ id }) => id === repetition.typeId);
                const project = projects.find(({ id }) => id === repetition.projectId);

                acc[row.id] = {
                    ...repetition,
                    presence,
                    location,
                    type,
                    project,
                    scenes: [],
                    roles: [],
                    groups: [],
                };
            }

            let scene;
            let group;
            let role;

            if (row.sceneIdentifier) {
                scene = {
                    identifier: row.sceneIdentifier,
                    name: row.sceneName,
                };
            }

            if (row.roleIdentifier) {
                role = {
                    identifier: row.roleIdentifier,
                    name: row.roleName,
                };
            }

            if (row.groupIdentifier) {
                role = {
                    identifier: row.groupIdentifier,
                    name: row.groupName,
                };
            }

            if (
                scene &&
                role &&
                acc[row.id].scenes.some(
                    s => s.identifier === scene.identifier && s.roles?.some(r => r.identifier === role.identifier)
                )
            ) {
                return acc;
            }

            if (
                scene &&
                group &&
                acc[row.id].scenes.some(
                    s => s.identifier === scene.identifier && s.groups?.some(g => g.identifier === group.identifier)
                )
            ) {
                return acc;
            }

            if (!scene && role && acc[row.id].roles.some(r => r.identifier === role.identifier)) {
                return acc;
            }

            if (!scene && group && acc[row.id].groups.some(g => g.identifier === group.identifier)) {
                return acc;
            }

            if (scene) {
                if (role) {
                    scene.roles = [role];
                }
                if (group) {
                    scene.groups = [group];
                }
                acc[row.id].scenes.push(scene);
            } else if (role) {
                acc[row.id].roles.push(role);
            } else if (group) {
                acc[row.id].groups.push(group);
            }

            return acc;
        }, {});

        const users = await usersCrud.read({ repetitionId: repetitionIds, userId });

        for (let repetition of futureRepetitions) {
            if (repetition.allChecked || users.find(({ repetitionId }) => repetition.id === repetitionId)) {
                const presence = !!(
                    presences.find(({ repetitionId }) => repetitionId === repetition.id) || { markedPresent: true }
                ).markedPresent;
                const location = locations.find(({ id }) => id === repetition.locationId);
                const type = types.find(({ id }) => id === repetition.typeId);
                const project = projects.find(({ id }) => id === repetition.projectId);
                const projectUserRegistrations = await dataService.raw(
                    `SELECT * FROM project_user_registrations as pRU WHERE pRU.projectId=${project.id} AND pRU.userId=${userId}`
                );
                const offStageUserIds = (
                    await dataService.raw(`SELECT DISTINCT pRU.userId FROM roles
                    LEFT JOIN registrations_roles as rR ON rR.roleId=roles.id
                    LEFT JOIN project_user_registrations as pRU ON pRU.registrationId=rR.registrationId
                    WHERE onStage=0 AND projectId=${project.id};`)
                ).map(({ userId }) => userId);

                if (
                    projectUserRegistrations.length &&
                    (OFF_STAGE_REPETITIONS === 'true' || !offStageUserIds.includes(userId))
                ) {
                    result[repetition.id] = {
                        ...repetition,
                        presence,
                        location,
                        type,
                        project,
                    };
                }
            }
        }

        return Object.values(result)
            .map(repetition => {
                if (repetition.scenes) {
                    repetition.scenes = repetition.scenes.sort(sortingService.sortScenes);
                }
                return repetition;
            })
            .sort((a, b) => new Date(a.startDt).getTime() - new Date(b.startDt).getTime());
    }

    async function getUsers(repetition, short = false) {
        const repetition_users = await usersCrud.read({ repetitionId: repetition.id || repetition });

        const data = repetition_users.reduce(
            (acc, { sceneId, groupId, roleId, castId, userId }) => {
                if (userId) {
                    if (!acc.users.includes(userId)) {
                        acc.users.push(userId);
                    }
                    return acc;
                }

                if (roleId) {
                    if (!acc.roles[castId]) {
                        acc.roles[castId] = [roleId];
                    } else if (!acc.roles[castId].includes(roleId)) {
                        acc.roles[castId].push(roleId);
                    }
                    return acc;
                }

                if (groupId) {
                    if (!acc.groups[castId]) {
                        acc.groups[castId] = [groupId];
                    } else if (!acc.groups[castId].includes(groupId)) {
                        acc.groups[castId].push(groupId);
                    }
                    return acc;
                }

                if (sceneId) {
                    if (!acc.scenes[castId]) {
                        acc.scenes[castId] = [sceneId];
                    } else if (!acc.scenes[castId].includes(sceneId)) {
                        acc.scenes[castId].push(sceneId);
                    }
                    return acc;
                }

                return acc;
            },
            {
                scenes: {},
                groups: {},
                roles: {},
                users: [],
            }
        );

        delete repetition_users;

        const promises = [];

        if (!short && repetition.allChecked) {
            const allCheckedRepetition = await getRepetition(repetition.id);
            const scenes = await scenesService.getAllScenesForProject(allCheckedRepetition.projectId);
            const sceneIds = scenes.map(({ id }) => id);
            promises.push(scenesService.getUsers(sceneIds));
        }

        for (const [cast, scenes] of Object.entries(data.scenes)) {
            promises.push(scenesService.getUsers(scenes, cast));
        }

        for (const [cast, roles] of Object.entries(data.roles)) {
            promises.push(rolesService.getUsers(roles, cast));
        }

        for (const [cast, groups] of Object.entries(data.groups)) {
            groups.forEach(group => promises.push(groupsService.getUsersAddedBefore(group, cast, repetition.startDt)));
        }

        data.users?.length && promises.push(usersService.read({ id: data.users }));

        const allUserArrays = await Promise.all(promises);

        return allUserArrays.flat().reduce((acc, user) => {
            if (!acc.some(u => (short ? u : u.id) === user.id)) {
                acc.push(short ? user.id : user);
            }

            return acc;
        }, []);
    }

    async function markPresent(repetitionId, userId, present) {
        const presence = await presenceCrud.read({ repetitionId, userId });

        if (!presence || !presence.length) {
            await presenceCrud.create({ repetitionId, userId, markedPresent: present });
        } else {
            await presenceCrud.update({ repetitionId, userId }, { markedPresent: present });
        }
    }

    async function getUserPresence(repetitionId, withSettings = true, withRoleUser = true) {
        const [repetition] = (await crud.read({ id: repetitionId })) || [];
        const users = await getUsers(repetition);

        const roleIds = !withRoleUser
            ? []
            : (await projectRolesService.read())
                  .filter(({ projectId }) => projectId === repetition.projectId)
                  .map(({ id }) => id);
        const projectUserRoles = !withRoleUser
            ? []
            : ((await projectRolesService.users.read()) || [])
                  .filter(({ project_role_id }) => roleIds.includes(project_role_id))
                  .map(({ userId }) => userId);

        const presence = (await presenceCrud.read({ repetitionId })) || [];

        return await Promise.all(
            users.map(async user => {
                const userPresence = presence.find(({ userId }) => userId === user.id);

                user.markedPresent = userPresence ? !!userPresence.markedPresent : true;
                user.present = userPresence ? !!userPresence.present : false;

                const [settings] = !withSettings ? [] : (await usersService.settings.read({ userId: user.id })) || [];
                const isRoleUser = !withRoleUser ? undefined : projectUserRoles.includes(user.id);

                return { ...user, settings, isRoleUser };
            })
        );
    }

    async function getScenes(repetitionId) {
        const users = (await usersCrud.read({ repetitionId })) || [];

        const sceneIds = users.reduce((acc, { sceneId }) => {
            if (sceneId && !acc.includes(sceneId)) {
                acc.push(sceneId);
            }

            return acc;
        }, []);

        if (!sceneIds.length) {
            return [];
        }

        return (await Promise.all(sceneIds.map(sceneId => scenesService.getScene(sceneId)))).sort((a, b) => {
            const aI = parseInt(a.identifier.replace('SC', ''));
            const bI = parseInt(b.identifier.replace('SC', ''));

            if (aI === bI) {
                const aF = a.identifier.includes('.')
                    ? parseInt(a.identifier.substr(a.identifier.indexOf('.') + 1))
                    : 0;
                const bF = b.identifier.includes('.')
                    ? parseInt(b.identifier.substr(b.identifier.indexOf('.') + 1))
                    : 0;

                return aF - bF;
            }

            return aI - bI;
        });
    }

    async function getRepetitions() {
        return crud.read();
    }

    async function getRepetition(id) {
        const repetitions = await getRepetitions();
        return repetitions.find(repetition => repetition.id === parseInt(id));
    }

    async function getRepetitionsForProject(projectId) {
        const repetitions = await getRepetitions();
        return repetitions.filter(repetition => repetition.projectId === parseInt(projectId));
    }

    async function getRepetitionsByLocation(locationId) {
        const repetitions = await getRepetitions();
        return repetitions.filter(repetition => repetition.locationId === parseInt(locationId));
    }

    async function getRepetitionsForProjectAndUser(projectId, userId) {
        const userRepetitions = await usersCrud.read({ userId });
        const userRepetitionIds = userRepetitions.map(uR => uR.repetitionId);
        const repetitions = await crud.read({ id: userRepetitionIds, projectId });

        return repetitions;
    }

    return {
        ...crud,
        users: {
            ...usersCrud,
        },
        presence: {
            ...presenceCrud,
        },
        getRepetitionsForProjectAndUser,
        getRepetitionsForUser,

        getRepetition,
        getRepetitions,
        getRepetitionsForProject,
        getRepetitionsByLocation,

        getUsers,
        markPresent,
        getUserPresence,
        resolveUserRow,
        getScenes,
    };
};
