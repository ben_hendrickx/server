const uuidv4 = require('uuid/v4');
const moment = require('moment');

const ArtTrack = `<svg class="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M14 5c0-1.1-.9-2-2-2h-1V2c0-.55-.45-1-1-1H6c-.55 0-1 .45-1 1v1H4c-1.1 0-2 .9-2 2v15c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2h8V5h-8zm-2 13h-2v-2h2v2zm0-9h-2V7h2v2zm4 9h-2v-2h2v2zm0-9h-2V7h2v2zm4 9h-2v-2h2v2zm0-9h-2V7h2v2z"></path></svg>`;
const Group = `<svg class="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M16 11c1.66 0 2.99-1.34 2.99-3S17.66 5 16 5c-1.66 0-3 1.34-3 3s1.34 3 3 3zm-8 0c1.66 0 2.99-1.34 2.99-3S9.66 5 8 5C6.34 5 5 6.34 5 8s1.34 3 3 3zm0 2c-2.33 0-7 1.17-7 3.5V19h14v-2.5c0-2.33-4.67-3.5-7-3.5zm8 0c-.29 0-.62.02-.97.05 1.16.84 1.97 1.97 1.97 3.45V19h6v-2.5c0-2.33-4.67-3.5-7-3.5z"></path></svg>`;
const Role = `<svg class="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M22 13h-8v-2h8v2zm0-6h-8v2h8V7zm-8 10h8v-2h-8v2zm-2-8v6c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V9c0-1.1.9-2 2-2h6c1.1 0 2 .9 2 2zm-1.5 6l-2.25-3-1.75 2.26-1.25-1.51L3.5 15h7z"></path></svg>`;
const User = `<svg class="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"></path></svg>`;

module.exports = function projectRoutes({
    requestUtil,
    dateUtil,
    projectsService,
    authenticationService,
    queryService,
    dataService,
    projectUsersQuery,
    projectUsersService,
    sortingService,
    usersService,
    notificationsService,
    castsService,
    scenesService,
    projectRolesService,
    groupsService,
    crudService,
}) {
    return function({ io }) {
        return {
            projects: {
                GET: {
                    // TODO: re-enable when rights are enabled on users with level=0
                    // requiredRights: {
                    //     projects: {
                    //         read: true,
                    //     },
                    // },
                    // VERIFIED
                    handler: async req => {
                        let { inactive, all, items } = req.query;
                        typeof inactive === 'string' && (inactive = !!parseInt(inactive));

                        const token = requestUtil.getToken(req);
                        const data = await authenticationService.verifyToken(token);

                        const { level, id } = data;

                        if (!all && items !== 'all' && level === 0) {
                            const projects = await projectsService.getUnregisteredOngoingProjectsForUser(id);

                            return {
                                data: projects,
                                total: projects.length,
                            };
                        }

                        let projects = await projectsService.read();

                        const filterFn = ({ openFrom, closedFrom, closedOffStage, projectClosed }) => {
                            if (inactive === undefined) {
                                return true;
                            }

                            const openInFuture = dateUtil.isInFuture(openFrom);
                            const closedInPast = !!projectClosed && dateUtil.isInPast(projectClosed);
                            const onStageInPast = dateUtil.isInPast(closedFrom);
                            const offStageInPast = dateUtil.isInPast(closedOffStage);

                            if (inactive) {
                                if (projectClosed !== null) {
                                    return openInFuture || closedInPast;
                                }

                                return openInFuture || (onStageInPast && offStageInPast);
                            }

                            if (projectClosed !== null) {
                                return !openInFuture && !closedInPast;
                            }

                            return !openInFuture && (!onStageInPast || !offStageInPast);
                        };

                        projects = projects.filter(filterFn);

                        return {
                            data: projects,
                            items: projects.length,
                            total: await projectsService.count(),
                        };
                    },
                },
                POST: {
                    // requiredRights: {
                    //     projects: {
                    //         create: true,
                    //     },
                    // },
                    handler: async req => {
                        await projectsService.create(req.body);

                        return {
                            success: true,
                        };
                    },
                },
            },
            project: {
                ':id': {
                    import: {
                        scheme: {
                            users: {
                                POST: {
                                    // VERIFIED
                                    handler: async req => {
                                        const { id: projectId } = req.params;
                                        const { body } = req;

                                        for (let row of body) {
                                            const [[cast], [role], [group], [user]] = await Promise.all([
                                                castsService.read({ name: row.cast }),
                                                projectRolesService.read({ projectId, identifier: row.identifier }),
                                                groupsService.read({ projectId, identifier: row.identifier }),
                                                usersService.read({
                                                    firstName: row.firstName,
                                                    lastName: row.lastName,
                                                    email: row.email,
                                                }),
                                            ]);

                                            if (!cast || !user) {
                                                continue;
                                            }

                                            if (role) {
                                                const exists = await projectRolesService.users.read({
                                                    project_role_id: role.id,
                                                    userId: user.id,
                                                });
                                                if (!exists.length) {
                                                    await projectRolesService.users.create({
                                                        userId: user.id,
                                                        project_role_id: role.id,
                                                        castId: cast.id,
                                                    });
                                                }
                                                continue;
                                            }

                                            if (group) {
                                                const exists = await groupsService.users.read({
                                                    groupId: group.id,
                                                    userId: user.id,
                                                });
                                                if (!exists.length) {
                                                    await groupsService.users.create({
                                                        userId: user.id,
                                                        groupId: group.id,
                                                        castId: cast.id,
                                                    });
                                                }
                                                continue;
                                            }
                                        }

                                        return { success: true };
                                    },
                                },
                            },
                            POST: {
                                // VERIFIED
                                handler: async req => {
                                    const { id: projectId } = req.params;
                                    const { body } = req;

                                    for (let row of body) {
                                        let sceneId;
                                        const [scene] = await scenesService.read({
                                            projectId,
                                            identifier: row.scene.identifier,
                                        });

                                        if (!scene) {
                                            const group = row.scene.group;
                                            delete row.scene.group;
                                            if (group === 'x') {
                                                const [sceneGroup] = await crudService('group_of_scenes').read({
                                                    identifier: row.scene.identifier,
                                                    projectId,
                                                });
                                                !sceneGroup &&
                                                    (await crudService('group_of_scenes').create({
                                                        ...row.scene,
                                                        projectId,
                                                    }));
                                                continue;
                                            } else {
                                                const [sceneGroup] = await crudService('group_of_scenes').read({
                                                    identifier: group,
                                                    projectId,
                                                });
                                                sceneId = await scenesService.create({
                                                    ...row.scene,
                                                    projectId,
                                                    groupId: sceneGroup?.id,
                                                });
                                            }
                                        } else {
                                            sceneId = scene.id;
                                        }

                                        if (row.role?.identifier) {
                                            const [role] = await projectRolesService.read({
                                                projectId,
                                                identifier: row.role.identifier,
                                            });

                                            if (!role) {
                                                const roleId = await projectRolesService.create({
                                                    ...row.role,
                                                    projectId,
                                                });
                                                await scenesService.roles.create({ project_role_id: roleId, sceneId });
                                            } else {
                                                await scenesService.roles.create({ project_role_id: role.id, sceneId });
                                            }
                                        }

                                        if (row.group?.identifier) {
                                            const [group] = await groupsService.read({
                                                projectId,
                                                identifier: row.group.identifier,
                                            });

                                            if (!group) {
                                                const groupId = await groupsService.create({ ...row.group, projectId });
                                                await scenesService.groups.create({ groupId, sceneId });
                                            } else {
                                                await scenesService.groups.create({ groupId: group.id, sceneId });
                                            }
                                        }
                                    }

                                    return { success: true };
                                },
                            },
                        },
                    },
                    GET: {
                        // TODO: re-enable when rights are enabled on users with level=0
                        // requiredRights: {
                        //     projects: {
                        //         read: true,
                        //     },
                        // },
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            const [project] = await projectsService.read({ id });

                            return project;
                        },
                    },
                    PUT: {
                        // requiredRights: {
                        //     projects: {
                        //         update: true,
                        //     },
                        // },
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;

                            delete req.body.rights;

                            let projects = await projectsService.read();
                            const currentProject = projects.find(project => project.id === id);

                            await projectsService.update(id, req.body);
                            projects = await projectsService.read();

                            projects = projects.filter(project => project.projectClosed);
                            projects = sortingService.sortBy(projects, 'projectClosed', true);

                            if (projects?.length) {
                                const [lastProject] = projects;

                                await usersService.update(
                                    { clothingFrozen: moment(currentProject.projectClosed).format() },
                                    { clothingFrozen: moment(lastProject.projectClosed).format() }
                                );
                            }

                            return { success: true };
                        },
                    },
                    DELETE: {
                        // requiredRights: {
                        //     projects: {
                        //         delete: true,
                        //     },
                        // },
                        handler: async req => {
                            const { id } = req.params;

                            await projectsService.remove(id);

                            return { success: true };
                        },
                    },
                    copy: {
                        POST: {
                            // requiredRights: {
                            //     projects: {
                            //         create: true,
                            //     },
                            // },
                            handler: async req => {
                                req.setTimeout(10 * 60 * 1000);

                                const { id } = req.params;
                                const { uuid } = req.body;

                                let lastTimeSent;

                                function callback(progress, text) {
                                    if (
                                        progress === 1 ||
                                        (progress > 0.9 && Date.now() - 100 > lastTimeSent) ||
                                        !lastTimeSent ||
                                        Date.now() - 1000 > lastTimeSent
                                    ) {
                                        lastTimeSent = Date.now();
                                        io.emit('copy_project_progress', { uuid, progress, text });
                                    }
                                }

                                await projectsService.copy(id, callback);

                                return { success: true };
                            },
                        },
                    },
                    transfer: {
                        POST: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;
                                const { user } = req;

                                await Promise.all(
                                    req.body.map(projectUser =>
                                        projectUsersService.create({ uuid: uuidv4(), projectId: id, ...projectUser })
                                    )
                                );

                                const { length } = req.body;
                                const [casts, scenes, users] = await Promise.all([
                                    castsService.read(),
                                    scenesService.read({ projectId: id }),
                                    await usersService.read(),
                                ]);
                                const changes = (
                                    await Promise.all(
                                        req.body.map(async item => {
                                            const cast = casts.find(({ id }) => id === item.castId);
                                            const scene = scenes.find(({ id }) => id === item.sceneId);
                                            const user = users.find(({ id }) => id === item.userId);
                                            const [roleOrGroup] = await (item.roleId
                                                ? projectRolesService.read({ id: item.roleId })
                                                : groupsService.read({ id: item.groupId }));
                                            return `<li>
                                            <div>${ArtTrack}<span>${scene.identifier} - ${scene.name}</span></div>
                                            <div>${item.roleId ? Role : Group}<span>${roleOrGroup.identifier} - ${
                                                roleOrGroup.name
                                            } (Cast: ${cast.name})</span></div>
                                            <div>${User}<span>${user.lastName} ${user.firstName}</span></div>
                                        </li>`;
                                        })
                                    )
                                ).join('');
                                let notification = `<div>Er zijn ${length} nieuwe rijen doorgeschreven naar kledijbeheer:<ul class="user-changes">${changes}</ul></div>`;

                                await notificationsService.add({
                                    user: {
                                        id: user.id,
                                        name: user.lastName + ' ' + user.firstName,
                                    },
                                    label: `Wijzigingen in kledijbeheer`,
                                    notification,
                                    type: 5,
                                });

                                return { success: true };
                            },
                        },
                        GET: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;
                                const { scene, role, group, cast } = req.query;

                                //WHERE pR.projectId=38;
                                const rolesQueryConfig = {
                                    table: {
                                        name: 'project_role_users',
                                        as: 'pRU',
                                    },
                                    fields: [
                                        'pRU.*',
                                        'u.firstName',
                                        'u.lastName',
                                        { field: 's.identifier', as: 'sceneIdentifier' },
                                        { field: 's.name', as: 'sceneName' },
                                        { field: 's.id', as: 'sceneId' },
                                        { field: 'pR.identifier', as: 'roleIdentifier' },
                                        { field: 'pR.name', as: 'roleName' },
                                        { field: 'c.name', as: 'castName' },
                                    ],
                                    joins: [
                                        {
                                            table: 'project_roles',
                                            as: 'pR',
                                            source: 'pRU.project_role_id',
                                            target: 'pR.id',
                                        },
                                        {
                                            table: 'scene_roles',
                                            as: 'sR',
                                            source: 'pRU.project_role_id',
                                            target: 'sR.project_role_id',
                                        },
                                        { table: 'users', as: 'u', source: 'pRU.userId', target: 'u.id' },
                                        { table: 'scenes', as: 's', source: 'sR.sceneId', target: 's.id' },
                                        { table: 'casts', as: 'c', source: 'pRU.castId', target: 'c.id' },
                                    ],
                                    where: [
                                        { key: 'sR.sceneId', operator: 'IS NOT', value: null },
                                        { key: 'pR.projectId', value: id },
                                        scene && { key: 's.id', operator: `IN (${scene.toString()})`, value: '' },
                                        role && { key: 'pR.id', operator: `IN (${role.toString()})`, value: '' },
                                        cast && { key: 'c.id', operator: `IN (${cast.toString()})`, value: '' },
                                    ].filter(v => v),
                                };

                                const groupsQueryConfig = {
                                    table: {
                                        name: 'group_users',
                                        as: 'gU',
                                    },
                                    fields: [
                                        'gU.*',
                                        'u.firstName',
                                        'u.lastName',
                                        { field: 's.identifier', as: 'sceneIdentifier' },
                                        { field: 's.name', as: 'sceneName' },
                                        { field: 's.id', as: 'sceneId' },
                                        { field: 'g.identifier', as: 'groupIdentifier' },
                                        { field: 'g.name', as: 'groupName' },
                                        { field: 'c.name', as: 'castName' },
                                    ],
                                    joins: [
                                        {
                                            table: 'groupes',
                                            as: 'g',
                                            source: 'gU.groupId',
                                            target: 'g.id',
                                        },
                                        {
                                            table: 'scene_groups',
                                            as: 'sG',
                                            source: 'gU.groupId',
                                            target: 'sG.groupId',
                                        },
                                        { table: 'users', as: 'u', source: 'gU.userId', target: 'u.id' },
                                        { table: 'scenes', as: 's', source: 'sG.sceneId', target: 's.id' },
                                        { table: 'casts', as: 'c', source: 'gU.castId', target: 'c.id' },
                                    ],
                                    where: [
                                        { key: 'sG.sceneId', operator: 'IS NOT', value: null },
                                        { key: 'g.projectId', value: id },
                                        scene && { key: 's.id', operator: `IN (${scene.toString()})`, value: '' },
                                        group && { key: 'g.id', operator: `IN (${group.toString()})`, value: '' },
                                        cast && { key: 'c.id', operator: `IN (${cast.toString()})`, value: '' },
                                    ].filter(v => v),
                                };

                                const { query: rolesQuery } = queryService(rolesQueryConfig);
                                const { query: groupsQuery } = queryService(groupsQueryConfig);

                                const projectRoles = group ? [] : await dataService.raw(rolesQuery);
                                const projectGroups = role ? [] : await dataService.raw(groupsQuery);

                                let newProjectUsers = [...projectRoles, ...projectGroups];

                                const queryConfig = projectUsersQuery({
                                    project: id,
                                });
                                const currentProjectUsers = await projectUsersService.read(queryConfig);

                                newProjectUsers = newProjectUsers.filter(projectUser => {
                                    return !currentProjectUsers.data.some(
                                        ({ sceneId, roleId, groupId, castId, userId }) => {
                                            return (
                                                projectUser.sceneId === sceneId &&
                                                (projectUser.project_role_id === roleId ||
                                                    projectUser.groupId === groupId) &&
                                                projectUser.castId === castId &&
                                                projectUser.userId === userId
                                            );
                                        }
                                    );
                                });

                                return {
                                    total: newProjectUsers.length,
                                    data: newProjectUsers.sort(sortingService.sortScenes),
                                };
                            },
                        },
                    },
                },
            },
        };
    };
};
