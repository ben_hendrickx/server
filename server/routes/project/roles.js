module.exports = function({ projectRolesService }) {
    return {
        project: {
            ':projectId': {
                roles: {
                    GET: {
                        //TODO: add rights
                        // VERIFIED
                        handler: async req => {
                            const { projectId } = req.params;

                            return await projectRolesService.read({ projectId });
                        },
                    },
                },
            },
        },
    };
};
