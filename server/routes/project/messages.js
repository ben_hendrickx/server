module.exports = function messages({ crudService, usersService }) {
    const messageReadCrud = crudService('message_read');

    return {
        projects: {
            ':id': {
                messages: {
                    ':messageId': {
                        users: {
                            GET: {
                                handler: async req => {
                                    const { messageId } = req.params;
                                    const { text, offset, items } = req.query;

                                    const pieces = [];
                                    text !== undefined && pieces.push(...text.split(' '));

                                    const messageRead = await messageReadCrud.read({ messageId });
                                    const total = messageRead.length;
                                    const userIds = messageRead.map(({ userId }) => userId);

                                    const users = await usersService.readNew(
                                        [
                                            {
                                                key: 'id',
                                                operator: `IN (${userIds.join(',')})`,
                                                value: ``,
                                            },
                                            ...pieces.map(piece => ({
                                                key: ['firstName', 'lastName', 'email', 'YEAR(date_of_birth)'],
                                                operator: 'LIKE',
                                                value: `%${piece}%`,
                                            })),
                                        ],
                                        { offset, items }
                                    );

                                    const { data, count } = users;

                                    return {
                                        total,
                                        data: data.map(u => {
                                            const { openedDt, readDt } = messageRead.find(
                                                ({ userId }) => userId === u.id
                                            );
                                            return {
                                                ...u,
                                                openedDt,
                                                readDt,
                                            };
                                        }),
                                        count,
                                    };
                                },
                            },
                        },
                    },
                },
            },
        },
    };
};
