const moment = require('moment');

module.exports = function({
    projectsService,
    registrationsService,
    usersService,
    sizesService,
    queryService,
    dataService,
}) {
    return {
        project: {
            ':projectId': {
                onstage: {
                    users: {
                        GET: {
                            // VERIFIED
                            handler: async req => {
                                const { projectId } = req.params;
                                const { sex, picture, minAge, maxAge, sort = 'lastName|ASC' } = req.query;

                                let users = await projectsService.getUsers(projectId);

                                users = [...users].sort((a, b) => {
                                    const aProp = a['lastName'];
                                    const bProp = b['lastName'];

                                    if (aProp === bProp) {
                                        return 0;
                                    }

                                    return aProp < bProp ? -1 : 1;
                                });

                                users = await Promise.all(
                                    users.map(async user => {
                                        const roles = await registrationsService.getRolesForProjectAndUser(
                                            projectId,
                                            user.id
                                        );
                                        const image = await usersService.getUserImage(
                                            user.firstName,
                                            user.lastName,
                                            user.email
                                        );
                                        return {
                                            ...user,
                                            image,
                                            isOnStage: roles.some(role => role.onStage),
                                        };
                                    })
                                );

                                users = users
                                    .filter(user => user.isOnStage)
                                    .map(user => {
                                        delete user.isOnStage;
                                        return user;
                                    });

                                const total = users.length;

                                users = usersService.filterUsers(users, { picture, minAge, maxAge, sex });
                                users = usersService.sortUsers(users, sort);

                                return {
                                    data: users,
                                    total,
                                    filters: {
                                        age: await usersService.getAges(),
                                    },
                                };
                            },
                        },
                    },
                },
                offstage: {
                    users: {
                        GET: {
                            // VERIFIED
                            handler: async req => {
                                const { projectId } = req.params;
                                const { sex, picture, minAge, maxAge, sort = 'lastName|ASC' } = req.query;

                                let users = await projectsService.getUsers(projectId);

                                users = [...users].sort((a, b) => {
                                    const aProp = a['lastName'];
                                    const bProp = b['lastName'];

                                    if (aProp === bProp) {
                                        return 0;
                                    }

                                    return aProp < bProp ? -1 : 1;
                                });

                                users = await Promise.all(
                                    users.map(async user => {
                                        const roles = await registrationsService.getRolesForProjectAndUser(
                                            projectId,
                                            user.id
                                        );
                                        const image = await usersService.getUserImage(
                                            user.firstName,
                                            user.lastName,
                                            user.email
                                        );
                                        return {
                                            ...user,
                                            image,
                                            isOnStage: roles.some(role => role.onStage),
                                        };
                                    })
                                );

                                users = users
                                    .filter(user => !user.isOnStage)
                                    .map(user => {
                                        delete user.isOnStage;
                                        return user;
                                    });

                                const total = users.length;

                                users = usersService.filterUsers(users, { picture, minAge, maxAge, sex });
                                users = usersService.sortUsers(users, sort);

                                return {
                                    data: users,
                                    total,
                                    filters: {
                                        age: await usersService.getAges(),
                                    },
                                };
                            },
                        },
                    },
                },
                users: {
                    GET: {
                        //TODO: add rights
                        // VERIFIED
                        handler: async req => {
                            const { projectId } = req.params;

                            let users = await projectsService.getUsers(projectId);

                            users = [...users].sort((a, b) => {
                                const aProp = a['lastName'];
                                const bProp = b['lastName'];

                                if (aProp === bProp) {
                                    return 0;
                                }

                                return aProp < bProp ? -1 : 1;
                            });

                            return {
                                data: users,
                                total: users.length,
                                filters: {
                                    age: await usersService.getAges(),
                                },
                            };
                        },
                    },
                    POST: {
                        handler: async req => {
                            const { projectId } = req.params;

                            const registrationsForProject = await registrationsService.users.read({ projectId });
                            const userIdsAlreadyInProject = registrationsForProject.map(({ userId }) => userId);
                            const userIdsToRegister = req.body.filter(id => !userIdsAlreadyInProject.includes(id));

                            for (let userId of userIdsToRegister) {
                                const registrationId = await registrationsService.create({
                                    auditioning: false,
                                    remarks: 'Automatisch gegenereerde inschrijving',
                                });

                                await registrationsService.users.create({
                                    projectId,
                                    userId,
                                    registrationId,
                                });

                                await registrationsService.roles.create({
                                    registrationId,
                                    roleId: 2,
                                });
                            }

                            return { success: true };
                        },
                    },
                },
                'not-registered': {
                    users: {
                        GET: {
                            // VERIFIED
                            handler: async req => {
                                const { projectId } = req.params;
                                const { sort, text, sex, minAge, maxAge, offset = 0, items = 10 } = req.query;

                                const pieces = [];
                                text !== undefined && pieces.push(...text.split(' '));

                                const queryConfig = {
                                    distinct: true,
                                    table: {
                                        name: 'users',
                                        as: 'u',
                                    },
                                    fields: [{ field: 'u.*' }],
                                    where: [
                                        {
                                            total: true,
                                            key: 'u.id',
                                            operator: `NOT IN (SELECT DISTINCT pUR.userId FROM project_user_registrations as pUR WHERE pUR.projectId = ${projectId})`,
                                            value: '',
                                        },
                                        sex !== undefined && { key: 'sex', value: sex },
                                        minAge !== undefined && {
                                            key: 'date_of_birth',
                                            operator: '<',
                                            value: moment()
                                                .subtract(minAge, 'years')
                                                .add(1, 'day')
                                                .startOf('day')
                                                .format(),
                                        },
                                        maxAge !== undefined && {
                                            key: 'date_of_birth',
                                            operator: '>',
                                            value: moment()
                                                .subtract(maxAge, 'years')
                                                .subtract(1, 'years')
                                                .add(1, 'day')
                                                .startOf('day')
                                                .format(),
                                        },
                                        ...pieces.map(piece => ({
                                            key: ['firstName', 'lastName', 'email', 'YEAR(date_of_birth)'],
                                            operator: 'LIKE',
                                            value: `%${piece}%`,
                                        })),
                                    ],
                                    sort,
                                    offset,
                                    items,
                                };

                                const { query, total, count } = queryService(queryConfig);

                                return {
                                    offset,
                                    data: await dataService.raw(query),
                                    items: (await dataService.raw(count.toString())).shift().count,
                                    total: (await dataService.raw(total.toString())).shift().count,
                                    filters: {
                                        age: await usersService.getAges(),
                                    },
                                };
                            },
                        },
                    },
                },
                user: {
                    ':userId': {
                        POST: {
                            // VERIFIED
                            handler: async req => {
                                const { projectId, userId } = req.params;

                                const {
                                    roles = [],
                                    auditioning,
                                    talents,
                                    remarks,
                                    height,
                                    chest,
                                    waist,
                                    hipSize,
                                    headSize,
                                    clothingSize,
                                    shirtSize,
                                } = req.body;

                                let { partOfTown } = req.body;

                                const registrationsForProject = await registrationsService.users.read({ projectId });
                                const userIsRegistered = registrationsForProject.some(
                                    registration => registration.userId === userId
                                );

                                if (userIsRegistered) {
                                    throw 'Je kan je geen 2 maal inschrijven voor dit project';
                                }

                                const registration = {
                                    auditioning: auditioning || false,
                                    talents,
                                    remarks,
                                    part_of_town: partOfTown,
                                };

                                // const partsOfTown = await projectsService.partsOfTown.read();
                                // if (partOfTown) {
                                //     registration.part_of_town = (
                                //         partsOfTown.find(({ name }) => name === partOfTown) || {}
                                //     ).id;
                                // }

                                const registrationId = await registrationsService.create(registration);

                                if (roles.length) {
                                    await Promise.all(
                                        roles.map(roleId =>
                                            registrationsService.roles.create({ registrationId, roleId })
                                        )
                                    );
                                } else {
                                    await registrationsService.roles.create({ registrationId, roleId: 2 });
                                }

                                let newShirtSize = ((await sizesService.shirtSizes.read()) || []).find(
                                    ({ name }) => name === shirtSize.toString()
                                )?.id;

                                await usersService.sizes.update(
                                    { userId },
                                    {
                                        height,
                                        chest,
                                        waist,
                                        hipSize,
                                        headSize,
                                        clothingSize,
                                        shirtSize: newShirtSize,
                                    }
                                );
                                await registrationsService.users.create({ registrationId, userId, projectId });
                                return { success: true };
                            },
                        },
                    },
                },
            },
        },
    };
};
