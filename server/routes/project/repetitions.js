module.exports = function (dependencies) {
    const { repetitionsService, dateUtil } = dependencies;

    return {
        project: {
            ':projectId': {
                repetitions: {
                    GET: {
                        // TODO: add rights
                        // handler: async req => {
                        //     const repetitionsWorker = await spawn(new Worker("../../workers/repetitions"));
                        //     const result = await repetitionsWorker.getRepetitions(req.params, req.query);
                        //     await Thread.terminate(repetitionsWorker);
                        //     return result;
                        // },
                        // VERIFIED
                        handler: async req => {
                            const { projectId } = req.params;
                            const { past, offset, items, text } = req.query;

                            let repetitions = await repetitionsService.read({ projectId });
                            const total = repetitions.length;

                            if (past !== undefined) {
                                repetitions = repetitions.filter(repetition =>
                                    past ? dateUtil.isInPast(repetition.endDt) : dateUtil.isInFuture(repetition.endDt)
                                );
                            }

                            if (text !== undefined) {
                                repetitions = repetitions.filter(repetition => (
                                    repetition.identifier.toLowerCase().includes(text.toLowerCase()) ||
                                    repetition.name.toLowerCase().includes(text.toLowerCase())
                                ))
                            }

                            repetitions = repetitions.sort((a, b) => {
                                return new Date(a.startDt).getTime() - new Date(b.startDt).getTime();
                            });

                            const filtered = repetitions.length;

                            repetitions = repetitions.slice(offset, offset + items);

                            // repetitions = await Promise.all(
                            //     repetitions.map(async r => {
                            //         const users = await repetitionsService.getUsers(r, true);

                            //         return {
                            //             ...r,
                            //             users: users.length,
                            //         };
                            //     })
                            // );

                            return {
                                offset,
                                items: filtered,
                                data: repetitions,
                                total,
                            };
                        },
                    },
                },
                user: {
                    ':userId': {
                        repetitions: {
                            GET: {
                                //TODO: add rights
                                // VERIFIED
                                handler: async req => {
                                    const { projectId, userId } = req.params;
                                    const { past = 2 } = req.query;

                                    let repetitions = await repetitionsService.getRepetitionsForUser(
                                        userId,
                                        undefined,
                                        past
                                    );
                                    repetitions = repetitions.filter(({ project }) => project.id === projectId);

                                    return {
                                        data: repetitions,
                                        total: repetitions.length,
                                    };
                                },
                            },
                        },
                    },
                },
            },
        },
    };
};
