module.exports = function({ scenesService, crudService, groupsService, projectRolesService, usersService }) {
    const groupOfScenesCrud = crudService('group_of_scenes');

    return {
        project: {
            ':projectId': {
                scenes: {
                    groups: {
                        GET: {
                            // VERIFIED
                            handler: async req => {
                                const { projectId } = req.params;
                                return await groupOfScenesCrud.read({ projectId });
                            },
                        },
                    },
                    group: {
                        ':id': {
                            DELETE: {
                                handler: async req => {
                                    const { projectId, id } = req.params;
                                    await groupOfScenesCrud.remove({ projectId, id });
                                    await scenesService.update({ projectId, groupId: id }, { groupId: null });
                                    return { success: true };
                                },
                            },
                        },
                    },
                    GET: {
                        //TODO: add rights
                        // VERIFIED
                        handler: async req => {
                            const { projectId } = req.params;

                            let groupsOfScenes = await groupOfScenesCrud.read({ projectId });
                            groupsOfScenes = groupsOfScenes.map(groupOfScenes => ({ scenes: [], ...groupOfScenes }));
                            let scenes = await scenesService.read({ projectId });

                            scenes = await Promise.all(
                                scenes.map(async scene => {
                                    const groups = await scenesService.groups.count({ sceneId: scene.id });
                                    const roles = await scenesService.roles.count({ sceneId: scene.id });

                                    return {
                                        ...scene,
                                        groups,
                                        roles,
                                    };
                                })
                            );

                            let sortedScenes = [...groupsOfScenes, ...scenes].sort((a, b) => {
                                if (a.identifier === b.identifier) {
                                    return 0;
                                }

                                return a.identifier < b.identifier ? -1 : 1;
                            });

                            sortedScenes
                                .filter(({ groupId }) => groupId)
                                .forEach(scene => {
                                    const parent = sortedScenes.find(({ id }) => id === scene.groupId);
                                    parent?.scenes?.push(scene);
                                });

                            sortedScenes = sortedScenes.filter(({ groupId }) => !groupId);

                            return {
                                data: sortedScenes,
                                total: scenes.length,
                            };
                        },
                    },
                },
                scene: {
                    ':id': {
                        users: {
                            GET: {
                                // VERIFIED
                                handler: async req => {
                                    const { id } = req.params;
                                    const { sex, picture, minAge, maxAge, sort = 'lastName|ASC' } = req.query;

                                    let [sceneGroups, sceneRoles] = await Promise.all([
                                        scenesService.groups.read({ sceneId: id }),
                                        scenesService.roles.read({ sceneId: id }),
                                    ]);

                                    let groupIds = sceneGroups.map(sceneGroup => sceneGroup.groupId);
                                    let roleIds = sceneRoles.map(sceneRole => sceneRole.project_role_id);

                                    let [groupUsers, roleUsers, users] = await Promise.all([
                                        groupsService.users.read({ groupId: groupIds }),
                                        projectRolesService.users.read({ project_role_id: roleIds }),
                                        usersService.read(),
                                    ]);

                                    let userIds = [...groupUsers, ...roleUsers].map(({ userId }) => userId);

                                    users = users.filter(user => userIds.includes(user.id));
                                    const total = users.length;

                                    users = await Promise.all(
                                        users.map(async user => {
                                            const image = await usersService.getUserImage(
                                                user.firstName,
                                                user.lastName,
                                                user.email
                                            );

                                            return {
                                                ...user,
                                                image,
                                            };
                                        })
                                    );

                                    users = usersService.filterUsers(users, { sex, picture, minAge, maxAge });
                                    users = usersService.sortUsers(users, sort);

                                    return {
                                        data: users,
                                        total,
                                    };
                                },
                            },
                        },
                    },
                },
                // TODO: remove this
                // user: {
                //     ':userId': {
                //         scenes: {
                //             GET: {
                //                 // TODO: add rights
                //                 handler: async req => {
                //                     const { projectId, userId } = req.params;
                //                     const scenes = await scenesService.getScenesForProjectAndUser(projectId, userId);

                //                     return {
                //                         data: scenes,
                //                         total: scenes.length,
                //                     };
                //                 },
                //             },
                //         },
                //     },
                // },
            },
        },
    };
};
