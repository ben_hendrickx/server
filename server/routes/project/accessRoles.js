module.exports = function projectAccessRoles({ accessRolesService }) {
    return {
        project: {
            ':id': {
                accessRoles: {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;

                            const accessRoles = await accessRolesService.projects.read({ projectId: id });

                            return {
                                data: accessRoles,
                                total: accessRoles.length,
                            };
                        },
                    },
                    POST: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;

                            await accessRolesService.projects.create({ projectId: id, ...req.body });

                            return { success: true };
                        },
                    },
                },
                accessRole: {
                    ':accessRoleId': {
                        users: {
                            GET: {
                                // VERIFIED
                                handler: async req => {
                                    const { accessRoleId } = req.params;

                                    const users = await accessRolesService.projects.users.read({
                                        projectAccessRoleId: accessRoleId,
                                    });

                                    return {
                                        data: users,
                                        total: users.length,
                                    };
                                },
                            },
                            POST: {
                                // VERIFIED
                                handler: async req => {
                                    const { accessRoleId } = req.params;
                                    const users = req.body;

                                    await accessRolesService.projects.users.remove({
                                        projectAccessRoleId: accessRoleId,
                                    });
                                    await Promise.all(
                                        users.map(userId =>
                                            accessRolesService.projects.users.create({
                                                projectAccessRoleId: accessRoleId,
                                                userId,
                                            })
                                        )
                                    );

                                    return { success: true };
                                },
                            },
                        },
                        PUT: {
                            // VERIFIED
                            handler: async req => {
                                const { accessRoleId } = req.params;

                                await accessRolesService.projects.update({ id: accessRoleId }, req.body);

                                return { success: true };
                            },
                        },
                        DELETE: {
                            // VERIFIED
                            handler: async req => {
                                const { accessRoleId } = req.params;

                                await accessRolesService.projects.users.remove({ projectAccessRoleId: accessRoleId });
                                await accessRolesService.projects.remove({ id: accessRoleId });

                                return { success: true };
                            },
                        },
                    },
                },
            },
        },
    };
};
