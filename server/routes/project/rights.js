module.exports = function({ projectRightsService, accessRolesService }) {
    return {
        project: {
            ':projectId': {
                user: {
                    ':userId': {
                        rights: {
                            GET: {
                                handler: async req => {
                                    const { projectId, userId } = req.params;

                                    return await projectRightsService.getRightsForProjectAndUser(projectId, userId);
                                },
                            },
                        },
                        access_roles: {
                            GET: {
                                // TODO: add rights
                                // VERIFIED
                                handler: async req => {
                                    const { projectId, userId } = req.params;

                                    const projectAccessRoles = await accessRolesService.projects.read({
                                        projectId,
                                    });

                                    const accessRoleIds = projectAccessRoles.map(({ id }) => id);

                                    const projectUserAccessRoles = await accessRolesService.projects.users.read({
                                        projectAccessRoleId: accessRoleIds,
                                        userId,
                                    });

                                    const userAccessRoleIds = projectUserAccessRoles.map(
                                        ({ projectAccessRoleId }) => projectAccessRoleId
                                    );

                                    const accessRoles = await accessRolesService.projects.read({
                                        id: userAccessRoleIds,
                                    });

                                    return {
                                        data: accessRoles,
                                        total: accessRoles.length,
                                    };
                                },
                            },
                            POST: {
                                // VERIFIED
                                handler: async req => {
                                    const { userId } = req.params;

                                    await accessRolesService.projects.users.create({
                                        userId,
                                        projectAccessRoleId: req.body,
                                    });

                                    return {
                                        success: true,
                                    };
                                },
                            },
                        },
                        access_role: {
                            ':id': {
                                DELETE: {
                                    handler: async req => {
                                        const { userId, id } = req.params;

                                        await accessRolesService.projects.users.remove({
                                            projectAccessRoleId: id,
                                            userId,
                                        });

                                        return {
                                            success: true,
                                        };
                                    },
                                },
                            },
                        },
                    },
                },
                access_roles: {
                    GET: {
                        handler: async req => {
                            const { projectId } = req.params;

                            return {
                                data: await accessRolesService.projects.read({ projectId }),
                                total: await accessRolesService.projects.count({ projectId }),
                            };
                        },
                    },
                },
            },
        },
    };
};
