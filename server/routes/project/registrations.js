module.exports = function({ registrationsService, objectUtil }) {
    return function({ io }) {
        return {
            project: {
                ':projectId': {
                    registrations: {
                        GET: {
                            // requiredRights: {
                            //     projects: {
                            //         read: true,
                            //     },
                            //     registrations: {
                            //         read: true,
                            //     },
                            // },
                            // VERIFIED
                            handler: async req => {
                                const { projectId } = req.params;
                                const {
                                    sort = 'user.lastName|ASC',
                                    sex,
                                    minAge,
                                    maxAge,
                                    onStage,
                                    auditioning,
                                    remarks,
                                    talents,
                                } = req.query;

                                let registrations = await registrationsService.getRegistrationsForProject(projectId);
                                const total = registrations.length;

                                if (sex !== undefined) {
                                    registrations = registrations.filter(({ user }) => user.sex === sex);
                                }

                                if (minAge !== undefined) {
                                    registrations = registrations.filter(({ user }) => {
                                        const diff_ms = Date.now() - new Date(user.date_of_birth).getTime();
                                        const age_dt = new Date(diff_ms);
                                        const userAge = Math.abs(age_dt.getUTCFullYear() - 1970);

                                        return userAge >= minAge;
                                    });
                                }

                                if (maxAge !== undefined) {
                                    registrations = registrations.filter(({ user }) => {
                                        const diff_ms = Date.now() - new Date(user.date_of_birth).getTime();
                                        const age_dt = new Date(diff_ms);
                                        const userAge = Math.abs(age_dt.getUTCFullYear() - 1970);

                                        return userAge <= maxAge;
                                    });
                                }

                                if (onStage !== undefined) {
                                    registrations = await Promise.all(
                                        registrations.map(async registration => {
                                            const roles = await registrationsService.getRolesForRegistration(
                                                registration.id
                                            );
                                            return {
                                                ...registration,
                                                isOnStage: roles.some(role => role.onStage),
                                            };
                                        })
                                    );

                                    registrations = registrations
                                        .filter(registration => registration.isOnStage === !!onStage)
                                        .map(registration => {
                                            delete registration.isOnStage;
                                            return registration;
                                        });
                                }

                                if (auditioning !== undefined) {
                                    registrations = registrations.filter(registration =>
                                        auditioning ? registration.auditioning : !registration.auditioning
                                    );
                                }

                                if (remarks !== undefined) {
                                    registrations = registrations.filter(registration =>
                                        remarks ? registration.remarks : !registration.remarks
                                    );
                                }

                                if (talents !== undefined) {
                                    registrations = registrations.filter(registration =>
                                        talents ? registration.talents : !registration.talents
                                    );
                                }

                                let [sortBy, order] = sort.split('|');

                                // SWAP ORDER FOR DATES
                                ['registered', 'user.age'].includes(sortBy) &&
                                    (order = order === 'ASC' ? 'DESC' : 'ASC');

                                registrations = [...registrations].sort((a, b) => {
                                    if (['registered', 'user.age'].includes(sortBy)) {
                                        if (sortBy === 'registered') {
                                            a.registered = new Date(a.creationDt).getTime();
                                            b.registered = new Date(b.creationDt).getTime();
                                        }

                                        if (sortBy === 'user.age') {
                                            a.user.age = new Date(a.user.date_of_birth).getTime();
                                            b.user.age = new Date(b.user.date_of_birth).getTime();
                                        }
                                    }

                                    const aProp = objectUtil.getNestedProperty(a, sortBy);
                                    const bProp = objectUtil.getNestedProperty(b, sortBy);

                                    if (typeof aProp === 'string') {
                                        if (aProp === bProp) {
                                            return 0;
                                        }

                                        return order === 'ASC' ? (aProp < bProp ? -1 : 1) : aProp < bProp ? 1 : -1;
                                    }

                                    return order === 'ASC' ? aProp - bProp : bProp - aProp;
                                });

                                return {
                                    data: registrations,
                                    total: total,
                                };
                            },
                        },
                        copy: {
                            POST: {
                                // VERIFIED
                                handler: async req => {
                                    const { projectId } = req.params;
                                    const { uuid } = req.body;

                                    let lastTimeSent;

                                    function callback(progress, text) {
                                        if (
                                            progress === 1 ||
                                            (progress > 0.9 && Date.now() - 100 > lastTimeSent) ||
                                            !lastTimeSent ||
                                            Date.now() - 1000 > lastTimeSent
                                        ) {
                                            lastTimeSent = Date.now();
                                            io.emit('copy_registrations_progress', { uuid, progress, text });
                                        }
                                    }

                                    await registrationsService.copyRegistrations(projectId, req.body, callback);

                                    return { success: true };
                                },
                            },
                        },
                    },
                },
            },
        };
    };
};
