module.exports = function({ groupsService, scenesService, repetitionsService, usersService, castsService }) {
    return {
        project: {
            ':projectId': {
                groups: {
                    GET: {
                        //TODO: add rights
                        // VERIFIED
                        handler: async req => {
                            const { projectId } = req.params;
                            const { sort = 'identifier|ASC', scene } = req.query;

                            let groups = await groupsService.read({ projectId });
                            const total = groups.length;
                            const casts = await castsService.read();

                            groups = await Promise.all(
                                groups.map(g =>
                                    groupsService.getGroupGroupsAndUsers(g.id).then(({ groups, users }) => {
                                        return {
                                            ...g,
                                            groups,
                                            users,
                                            userCount: users,
                                            usersPerCast: users.reduce((acc, user) => {
                                                const cast = casts.find(cast => cast.id === user.castId);

                                                acc[cast.name]++;

                                                return acc;
                                            }, casts.sort((a, b) => {
                                                const aName = a.name.toLowerCase();
                                                const bName = b.name.toLowerCase();
                                                return aName > bName ? 1 : aName < bName ? -1 : 0;
                                            }).reduce((acc, cast) => {
                                                if (!acc[cast.name]) {
                                                    acc[cast.name] = 0;
                                                };
                                                return acc;
                                            }, {}))
                                        };
                                    })
                                )
                            );

                            if (scene !== undefined) {
                                groups = await Promise.all(
                                    groups.map(async group => {
                                        const scenes = await scenesService.groups.read({ groupId: group.id });

                                        return {
                                            ...group,
                                            scenes: scenes.map(scene => scene.sceneId),
                                        };
                                    })
                                );

                                groups = groups
                                    .filter(group => group.scenes.includes(scene))
                                    .map(group => {
                                        delete group.scenes;
                                        return group;
                                    });
                            }

                            let [sortBy, order] = sort.split('|');
                            groups = [...groups].sort((a, b) => {
                                const aProp = a[sortBy];
                                const bProp = b[sortBy];

                                if (typeof aProp === 'string') {
                                    if (aProp === bProp) {
                                        return 0;
                                    }

                                    return order === 'ASC' ? (aProp < bProp ? -1 : 1) : aProp < bProp ? 1 : -1;
                                }

                                return order === 'ASC' ? aProp - bProp : bProp - aProp;
                            });

                            return {
                                data: groups.map(group => {
                                    delete group.userCount;
                                    group.users = group.users?.length || 0;
                                    return group;
                                }),
                                total,
                            };
                        },
                    },
                    POST: {
                        // VERIFIED
                        handler: async req => {
                            const { projectId } = req.params;
                            const { identifier, name, description } = req.body;
                            await groupsService.create({ identifier, name, description, projectId });
                            return { success: true };
                        },
                    },
                },
                group: {
                    ':id': {
                        group_users: {
                            GET: {
                                // VERIFIED
                                handler: async req => {
                                    const { id } = req.params;
                                    return await groupsService.users.read({ groupId: id });
                                },
                            },
                        },
                        users: {
                            PUT: {
                                // VERIFIED
                                handler: async req => {
                                    const { id } = req.params;
                                    const users = req.body;

                                    const groupUsers = await groupsService.users.read({ groupId: id });
                                    
                                    // Look for users that are not already in db
                                    const newUsers = users.filter(u => !groupUsers.some(gU => u.userId === gU.userId && u.castId === gU.castId));
                                    const usersToDelete = groupUsers.filter(gU => !users.some(u => gU.userId === u.userId && gU.castId === u.castId));
                                    await Promise.all(
                                        newUsers.map(({ userId, castId }) =>
                                            groupsService.users.create({ groupId: id, userId, castId })
                                        )
                                    );

                                    await Promise.all(
                                        usersToDelete.map(({ userId, castId }) =>
                                            groupsService.users.remove({ groupId: id, userId, castId })
                                        )
                                    );

                                    return {
                                        success: true,
                                    };
                                },
                            },
                            GET: {
                                // VERIFIED
                                handler: async req => {
                                    const { id } = req.params;
                                    const { sex, picture, minAge, maxAge, sort = 'lastName|ASC' } = req.query;

                                    let [groupUsers, users] = await Promise.all([
                                        groupsService.users.read({ groupId: id }),
                                        usersService.read(),
                                    ]);

                                    const groupUserIds = groupUsers.map(({ userId }) => userId);

                                    users = users.filter(user => groupUserIds.includes(user.id));
                                    const total = users.length;

                                    users = await Promise.all(
                                        users.map(async user => {
                                            const image = await usersService.getUserImage(
                                                user.firstName,
                                                user.lastName,
                                                user.email
                                            );

                                            return {
                                                ...user,
                                                image,
                                            };
                                        })
                                    );

                                    users = usersService.filterUsers(users, { sex, picture, minAge, maxAge });
                                    users = usersService.sortUsers(users, sort);

                                    return {
                                        data: users,
                                        total,
                                    };
                                },
                            },
                        },
                        PUT: {
                            handler: async req => {
                                const { id } = req.params;
                                const { identifier, name, description } = req.body;
                                await groupsService.update({ id }, { identifier, name, description });
                                return { success: true };
                            },
                        },
                        DELETE: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;

                                await groupsService.users.remove({ groupId: id });
                                await repetitionsService.users.remove({ groupId: id });
                                await scenesService.groups.remove({ groupId: id });
                                await groupsService.remove({ id });

                                return { success: true };
                            },
                        },
                    },
                },
                user: {
                    ':userId': {
                        groups: {
                            GET: {
                                // TODO: add rights
                                handler: async req => {
                                    const { projectId, userId } = req.params;
                                    const { scene } = req.query;

                                    const [casts, allGroups, scenes, sceneGroups] = await Promise.all([
                                        castsService.read(),
                                        groupsService.read(),
                                        scenesService.read(),
                                        scenesService.groups.read(),
                                    ]);

                                    const groupIds = allGroups.map(group => group.id);
                                    const groupsForUser = await groupsService.users.read({
                                        userId,
                                        groupId: groupIds,
                                    });

                                    const groups = groupsForUser.reduce((acc, groupUser) => {
                                        const group = allGroups.find(({ id }) => id === groupUser.groupId);

                                        if (group.projectId === projectId) {
                                            const cast = casts.find(({ id }) => id === groupUser.castId);
                                            const sceneIds = sceneGroups
                                                .filter(({ groupId }) => groupId === group.id)
                                                .map(({ sceneId }) => sceneId);

                                            sceneIds.forEach(sceneId => {
                                                if (!scene || scene === sceneId) {
                                                    acc.push({
                                                        scene: scenes.find(({ id }) => id === sceneId),
                                                        cast,
                                                        ...group,
                                                    });
                                                }
                                            });

                                            if (!scene && !sceneIds.length) {
                                                acc.push({
                                                    cast,
                                                    ...group,
                                                });
                                            }
                                        }

                                        return acc;
                                    }, []);

                                    return {
                                        data: groups,
                                        total: groups.length,
                                    };
                                },
                            },
                        },
                    },
                },
            },
        },
    };
};
