const squel = require('squel');

module.exports = function({
    projectRolesService,
    scenesService,
    castsService,
    usersService,
    queryService,
    projectRolesQuery,
    groupsQuery,
    dataService,
    sortingService,
}) {
    return {
        project: {
            ':projectId': {
                project_roles: {
                    GET: {
                        // TODO: add rights
                        // VERIFIED
                        handler: async req => {
                            const { projectId } = req.params;
                            const { sort = 'identifier|ASC', scene } = req.query;

                            let roles = await projectRolesService.read({ projectId });
                            const total = roles.length;

                            roles = await Promise.all(
                                roles.map(async role => {
                                    let roleUsers = await projectRolesService.users.read({ project_role_id: role.id });
                                    const users = await usersService.read({ id: roleUsers.map(u => u.userId) });
                                    const casts = await castsService.read();

                                    return {
                                        ...role,
                                        users: roleUsers.reduce((acc, { castId, userId }) => {
                                            const cast = casts.find(cast => cast.id === castId);
                                            const user = users.find(user => user.id === userId);

                                            if (!acc[cast.name]) {
                                                acc[cast.name] = [];
                                            }

                                            if (
                                                !acc[cast.name].some(
                                                    ({ firstName, lastName }) =>
                                                        firstName === user.firstName && lastName === user.lastName
                                                )
                                            ) {
                                                acc[cast.name].push({
                                                    firstName: user.firstName,
                                                    lastName: user.lastName,
                                                });
                                            }

                                            return acc;
                                        }, {}),
                                        userCount: roleUsers.length,
                                    };
                                })
                            );

                            if (scene !== undefined) {
                                roles = await Promise.all(
                                    roles.map(async role => {
                                        const scenes = await scenesService.roles.read({ project_role_id: role.id });

                                        return {
                                            ...role,
                                            scenes: scenes.map(scene => scene.sceneId),
                                        };
                                    })
                                );

                                roles = roles
                                    .filter(role => role.scenes.includes(scene))
                                    .map(role => {
                                        delete role.scenes;
                                        return role;
                                    });
                            }

                            let [sortBy, order] = sort.split('|');
                            roles = [...roles].sort((a, b) => {
                                const aProp = a[sortBy];
                                const bProp = b[sortBy];

                                if (typeof aProp === 'string') {
                                    if (aProp === bProp) {
                                        return 0;
                                    }

                                    return order === 'ASC' ? (aProp < bProp ? -1 : 1) : aProp < bProp ? 1 : -1;
                                }

                                return order === 'ASC' ? aProp - bProp : bProp - aProp;
                            });

                            return {
                                data: roles,
                                total,
                            };
                        },
                    },
                    POST: {
                        // VERIFIED
                        handler: async req => {
                            const { projectId } = req.params;

                            await projectRolesService.create({
                                projectId,
                                ...req.body,
                            });

                            return { success: true };
                        },
                    },
                },
                project_role: {
                    ':id': {
                        PUT: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;

                                await projectRolesService.update({ id }, req.body);

                                return { success: true };
                            },
                        },
                        users: {
                            PUT: {
                                // VERIFIED
                                handler: async req => {
                                    const { id } = req.params;
                                    const users = req.body;

                                    await projectRolesService.users.remove({ project_role_id: id });
                                    await Promise.all(
                                        users.map(({ userId, castId }) =>
                                            projectRolesService.users.create({ project_role_id: id, userId, castId })
                                        )
                                    );

                                    return {
                                        success: true,
                                    };
                                },
                            },
                            GET: {
                                // VERIFIED
                                handler: async req => {
                                    const { id } = req.params;
                                    return await projectRolesService.users.read({ project_role_id: id });
                                },
                            },
                        },
                    },
                },
                user: {
                    ':userId': {
                        project_roles: {
                            GET: {
                                // TODO: add rights
                                // VERIFIED
                                handler: async req => {
                                    const { projectId, userId } = req.params;
                                    const { scene } = req.query;

                                    const { query: roles } = queryService(
                                        projectRolesQuery({ projectId, userId, scene })
                                    );
                                    const { query: groups } = queryService(groupsQuery({ projectId, userId, scene }));

                                    const allRoles = [
                                        (await dataService.raw(roles)).map(role => ({ ...role, type: 'role' })),
                                        (await dataService.raw(groups)).map(group => ({ ...group, type: 'group' })),
                                    ].flat();

                                    const casts = await castsService.read();

                                    return {
                                        total: allRoles.flat().length,
                                        data: allRoles
                                            .map(role => {
                                                const { castId } = role;
                                                delete role.castId;

                                                const cast = casts.find(({ id }) => id === castId);

                                                return {
                                                    ...role,
                                                    cast,
                                                };
                                            })
                                            .sort(sortingService.sortScenes),
                                    };
                                },
                            },
                        },
                    },
                },
            },
        },
    };
};
