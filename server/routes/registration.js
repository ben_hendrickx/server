module.exports = function registrationRoutes({
    authenticationService,
    notificationsService,
    registrationsService,
    projectsService,
    requestUtil,
    crudService,
    usersService,
}) {
    const projectUserRegistrationsCrud = crudService('project_user_registrations');

    return {
        registrations: {
            others: {
                PUT: {
                    // VERIFIED
                    handler: async req => {
                        const token = requestUtil.getToken(req);
                        const { level } = await authenticationService.verifyToken(token);
                        let { id, remarks, talents, ownerRemarks, partOfTown } = req.body;
                        id = parseInt(id);

                        if (level === 0) {
                            const user = await authenticationService.verifyToken(token);
                            const [registration] = await registrationsService.read({ id });
                            const [pUR] = await projectUserRegistrationsCrud.read({
                                registrationId: id,
                            });
                            const [project] = await projectsService.read({ id: pUR.projectId });
                            const partsOfTown = await projectsService.partsOfTown.read();

                            if (!registration) {
                                return;
                            }

                            const changes = [];

                            const oldPartOfTown =
                                (partsOfTown.find(pOT => pOT.id === parseInt(registration.part_of_town)) || {}).name ||
                                'Ik woon niet in Geel';
                            const newPartOfTown =
                                (partsOfTown.find(pOT => pOT.id === parseInt(partOfTown)) || {}).name ||
                                'Ik woon niet in Geel';

                            let notification = `<div>Medewerker ${user.firstName} ${user.lastName} heeft volgende zaken gewijzigd aan zijn inschrijving voor het project ${project.name}<br /><ul>`;

                            remarks !== undefined &&
                                remarks !== registration.remarks &&
                                changes.push({ key: 'Opmerkingen', old: registration.remarks, new: remarks });
                            talents !== undefined &&
                                talents !== registration.talents &&
                                changes.push({ key: 'Talenten', old: registration.talents, new: talents });
                            partOfTown !== undefined &&
                                oldPartOfTown !== newPartOfTown &&
                                changes.push({ key: 'Deeldorp', old: oldPartOfTown, new: newPartOfTown });

                            for (let change of changes) {
                                notification += `<li>${change.key} werd veranderd `;
                                change.old && (notification += `van ${change.old} `);
                                notification += `naar ${change.new}</li>`;
                            }

                            notification += '</ul>';

                            await notificationsService.add({
                                user: {
                                    id: user.id,
                                    name: user.lastName + ' ' + user.firstName,
                                },
                                label: `Wijziging inschrijving: ${project.name}`,
                                notification,
                                type: 2,
                                projectId: project.id,
                            });
                        }

                        await registrationsService.update(
                            { id },
                            {
                                remarks,
                                talents,
                                owner_remarks: ownerRemarks,
                                part_of_town: partOfTown,
                            }
                        );
                        return { success: true };
                    },
                },
            },
            GET: {
                handler: req => {
                    const token = requestUtil.getToken(req);
                    return registrationsService.getRegistrationsForUser(token);
                },
            },
            ':id': {
                GET: {
                    handler: req => {
                        const { id } = req.params;
                        return registrationsService.getRegistrationsForProject(id);
                    },
                },
                roles: {
                    PUT: {
                        // VERIFIED
                        handler: async req => {
                            const { roles, auditioning } = req.body;
                            const { id: registrationId } = req.params;

                            if (auditioning === undefined && roles === undefined) {
                                return;
                            }

                            auditioning !== undefined &&
                                (await registrationsService.update({ id: registrationId }, { auditioning }));

                            if (roles && roles.length) {
                                await registrationsService.roles.remove({ registrationId });
                                await Promise.all(
                                    roles.map(roleId => registrationsService.roles.create({ registrationId, roleId }))
                                );
                            }

                            return { success: true };
                        },
                    },
                },
            },
            // TODO: remove this
            // ':projectId/:userId': {
            //     GET: {
            //         handler: req => {
            //             const { projectId, userId } = req.params;
            //             return registrationsService.getRegistrationForProjectAndUser(projectId, userId);
            //         },
            //     },
            // },
            signoff: {
                ':registrationId': {
                    DELETE: {
                        handler: async req => {
                            const token = requestUtil.getToken(req);
                            const { id } = await authenticationService.verifyToken(token);
                            const { registrationId } = req.params;
                            const [registration] = await registrationsService.users.read({ registrationId });
                            const [project] = await projectsService.read({ id: registration.projectId });
                            const [user] = await usersService.read({ id });

                            let notification = `<div>Medewerker ${user.firstName} ${user.lastName} heeft zich uitgeschreven voor het project ${project.name}`;

                            await notificationsService.add({
                                user: {
                                    id: user.id,
                                    name: user.lastName + ' ' + user.firstName,
                                },
                                label: `Uitschrijving: ${project.name}`,
                                notification,
                                type: 3,
                                projectId: project.id,
                            });

                            return await registrationsService.signOffForRegistration(registrationId, token);
                        },
                    },
                },
                ':registrationId/:userId': {
                    DELETE: {
                        handler: async req => {
                            const token = requestUtil.getToken(req);
                            const person = await authenticationService.verifyToken(token);
                            const { registrationId, userId } = req.params;

                            const [user] = await usersService.read({ id: userId });

                            const [registration] = await projectUserRegistrationsCrud.read({
                                registrationId,
                            });
                            const [project] = await projectsService.read({ id: registration.projectId });

                            let notification = `<div>Medewerker ${user.firstName} ${user.lastName} heeft zich uitgeschreven voor het project ${project.name}`;

                            if (user.id !== person.id) {
                                notification = `<div>Medewerker ${person.firstName} ${person.lastName} heeft ${user.firstName} ${user.lastName} uitgeschreven voor het project ${project.name}`;
                            }

                            await notificationsService.add({
                                user: {
                                    id: person.id,
                                    name: person.lastName + ' ' + person.firstName,
                                },
                                label: `Uitschrijving: ${project.name}`,
                                notification,
                                type: 3,
                                projectId: project.id,
                            });

                            return await registrationsService.signOffForRegistration(registrationId, token, userId);
                        },
                    },
                },
            },
        },

        registration: {
            ':id': {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;
                        const [userRegistration] = await registrationsService.users.read({ registrationId: id });
                        const [registration] = await registrationsService.read({ id });
                        const [project] = await projectsService.read({ id: userRegistration.projectId });

                        return {
                            ...registration,
                            project,
                        };
                    },
                },
                user: {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            const user = await registrationsService.getUserForRegistration(id);
                            return usersService.getUserById(user.id);
                        },
                    },
                },
                roles: {
                    GET: {
                        // TODO: add rights
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            return await registrationsService.getRolesForRegistration(id);
                        },
                    },
                },
            },
        },
    };
};
