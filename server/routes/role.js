const moment = require('moment');

module.exports = function roleRoutes({
    repetitionsService,
    projectRolesService,
    scenesService,
    rolesService,
    groupsService,
}) {
    return {
        roles: {
            GET: {
                // TODO: add rights
                // VERIFIED
                handler: async () => {
                    return rolesService.read();
                },
            },
        },
        // TODO: rewrite this
        project_roles: {
            switch: {
                POST: {
                    handler: async req => {
                        const { userId, castId, roleIds, groupIds } = req.body;

                        roleIds?.length &&
                            (await projectRolesService.users.update({ userId, project_role_id: roleIds }, { castId }));
                        groupIds?.length &&
                            (await groupsService.users.update({ userId, groupId: groupIds }, { castId }));

                        return { success: true };
                    },
                },
            },
            GET: {
                // VERIFIED
                handler: async req => {
                    let { project } = req.query;
                    const config = project
                        ? {
                              projectId: project
                                  .toString()
                                  .split(',')
                                  .map(v => parseInt(v)),
                          }
                        : {};

                    const projectRoles = (await projectRolesService.read(config)) || [];

                    return {
                        data: projectRoles.sort((a, b) =>
                            a.identifier > b.identifier ? 1 : a.identifier < b.identifier ? -1 : 0
                        ),
                        total: await projectRolesService.count(),
                    };
                },
            },
        },
        groups: {
            GET: {
                // VERIFIED
                handler: async req => {
                    let { project } = req.query;
                    const config = project
                        ? {
                              projectId: project
                                  .toString()
                                  .split(',')
                                  .map(v => parseInt(v)),
                          }
                        : {};

                    const groups = (await groupsService.read(config)) || [];

                    return {
                        data: groups.sort((a, b) =>
                            a.identifier > b.identifier ? 1 : a.identifier < b.identifier ? -1 : 0
                        ),
                        total: await groupsService.count(),
                    };
                },
            },
        },
        scenes: {
            GET: {
                handler: async req => {
                    let { project } = req.query;
                    const config = project
                        ? {
                              projectId: project
                                  .toString()
                                  .split(',')
                                  .map(v => parseInt(v)),
                          }
                        : {};

                    const scenes = (await scenesService.read(config)) || [];

                    return {
                        data: scenes.sort((a, b) =>
                            a.identifier > b.identifier ? 1 : a.identifier < b.identifier ? -1 : 0
                        ),
                        total: await scenesService.count(),
                    };
                },
            },
        },
        project_role: {
            ':id': {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;
                        const [role] = await projectRolesService.read({ id });
                        return role;
                    },
                },
                DELETE: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;
                        await repetitionsService.users.remove({ roleId: id });
                        await projectRolesService.users.remove({ project_role_id: id });
                        await scenesService.roles.remove({ project_role_id: id });
                        await projectRolesService.remove({ id });
                        return { success: true };
                    },
                },
            },
        },
    };
};
