module.exports = function({ sortingService, threadsService, usersService }) {
    return {
        threads: {
            action: {
                ':id': {
                    DELETE: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            await threadsService.actions.update({ id }, { archived: 1 });
                            return { success: true };
                        },
                    },
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            const [action] = await threadsService.actions.read({ id });
                            const users = await threadsService.actions.users.read({ threadActionId: id });
                            const allUsers = await usersService.read();

                            const { responsible, participants } = users.reduce(
                                (acc, user) => {
                                    const { isResponsible } = user;
                                    user = allUsers.find(u => u.id === user.userId);
                                    if (user) {
                                        const { id, firstName, lastName } = user;
                                        isResponsible && acc.responsible.push(`${lastName} ${firstName} (${id})`);
                                        !isResponsible && acc.participants.push(`${lastName} ${firstName} (${id})`);
                                    }
                                    return acc;
                                },
                                {
                                    responsible: [],
                                    participants: [],
                                }
                            );

                            return {
                                ...action,
                                responsible,
                                participants,
                            };
                        },
                    },
                    PUT: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            let { name, defaultMessage, responsible = [], participants = [] } = req.body;
                            await threadsService.actions.update(
                                { id },
                                {
                                    name,
                                    defaultMessage,
                                }
                            );

                            await threadsService.actions.users.remove({
                                threadActionId: id,
                            });

                            await Promise.all(
                                responsible.map(responsible =>
                                    threadsService.actions.users.create({
                                        threadActionId: id,
                                        userId: responsible,
                                        isResponsible: true,
                                    })
                                )
                            );

                            participants = participants.filter(participant => !responsible.includes(participant));

                            await Promise.all(
                                participants.map(participant =>
                                    threadsService.actions.users.create({
                                        threadActionId: id,
                                        userId: participant,
                                    })
                                )
                            );

                            return { success: true, id };
                        },
                    },
                },
            },
            actions: {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        const data = await threadsService.actions.read({ archived: 0 });

                        return {
                            total: data.length,
                            data: sortingService.sortBy(data, 'name'),
                        };
                    },
                },
                POST: {
                    // VERIFIED
                    handler: async req => {
                        let { name, defaultMessage, responsible = [], participants = [] } = req.body;
                        const threadActionId = await threadsService.actions.create({
                            name,
                            defaultMessage,
                        });

                        await Promise.all(
                            responsible.map(responsible =>
                                threadsService.actions.users.create({
                                    threadActionId,
                                    userId: responsible,
                                    isResponsible: true,
                                })
                            )
                        );

                        participants = participants.filter(participant => !responsible.includes(participant));

                        await Promise.all(
                            participants.map(participant =>
                                threadsService.actions.users.create({
                                    threadActionId,
                                    userId: participant,
                                })
                            )
                        );

                        return { success: true, id: threadActionId };
                    },
                },
            },
        },
        thread: {
            ':id': {
                DELETE: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;

                        await threadsService.users.archive({ threadId: id });
                        await threadsService.archive(id);

                        return { success: true };
                    },
                },
            },
        },
    };
};
