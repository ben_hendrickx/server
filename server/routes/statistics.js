// TODO: abstract dataservice

module.exports = function statisticsRoutes({
    onlineUsersService,
    projectsService,
    registrationsService,
    rolesService,
    projectRolesService,
    scenesService,
    groupsService,
    usersService,
    clothingService,
    dateUtil,
    repetitionsService,
}) {
    return {
        statistics: {
            users: {
                online: {
                    GET: {
                        handler: async () => {
                            return onlineUsersService.get();
                        },
                    },
                },
            },
            overview: {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        const [projects, users, articles, rentals, online] = await Promise.all([
                            projectsService.read(),
                            usersService.settings.count({ level: 0 }),
                            clothingService.articles.count(),
                            clothingService.orders.count(),
                            onlineUsersService.get(),
                        ]);

                        return {
                            articles: articles,
                            rentals: rentals,
                            projects: projects.length,
                            employees: users,
                            online: online.length,
                        };
                    },
                },
                ':id': {
                    GET: {
                        //TODO: cleanup
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;

                            const [
                                registrations = [],
                                allRoles = [],
                                registrationRoles = [],
                                projectRoles = 0,
                                scenes = 0,
                                groupes = 0,
                                repetitions = [],
                            ] = await Promise.all([
                                registrationsService.users.read({ projectId: id }),
                                rolesService.read(),
                                registrationsService.roles.read(),
                                projectRolesService.count({ projectId: id }),
                                scenesService.count({ projectId: id }),
                                groupsService.count({ projectId: id }),
                                repetitionsService.read({ projectId: id }),
                            ]);

                            const registrationIds = registrations.map(({ registrationId }) => registrationId);

                            const roles = allRoles.reduce((acc, { id, onStage }) => {
                                acc[id] = onStage;
                                return acc;
                            }, {});

                            const stageData = registrationRoles.reduce(
                                (acc, { registrationId, roleId }) => {
                                    if (!acc.ids.includes(registrationId) && registrationIds.includes(registrationId)) {
                                        const label = roles[roleId] ? 'onStage' : 'offStage';
                                        acc[label]++;
                                        acc.ids.push(registrationId);
                                    }
                                    return acc;
                                },
                                { onStage: 0, offStage: 0, ids: [] }
                            );

                            return {
                                registrations: registrationIds.length,
                                onStage: stageData.onStage,
                                offStage: stageData.offStage,
                                roles: projectRoles,
                                scenes: scenes,
                                groups: groupes,
                                repetitions: {
                                    total: repetitions.length,
                                    past: repetitions.filter(repetition => dateUtil.isInPast(repetition.endDt)).length,
                                    future: repetitions.filter(repetition => dateUtil.isInFuture(repetition.endDt))
                                        .length,
                                },
                            };
                        },
                    },
                },
            },
        },
    };
};
