module.exports = function typeRoutes({ crudService }) {
    const typesCrud = crudService('types');

    return {
        types: {
            GET: {
                // VERIFIED
                handler: async () => {
                    const types = await typesCrud.read();
                    const total = types.length;

                    return {
                        data: types.sort((a, b) => {
                            return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
                        }),
                        total,
                    };
                },
            },
            POST: {
                // VERIFIED
                handler: async req => {
                    const { name } = req.body;
                    await typesCrud.create({ name });
                    return { success: true };
                },
            },
        },
        type: {
            ':id': {
                PUT: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;
                        const { name } = req.body;
                        await typesCrud.update({ id }, { name });
                        return { success: true };
                    },
                },
                DELETE: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;
                        await typesCrud.remove({ id });
                        return { success: true };
                    },
                },
            },
        },
    };
};
