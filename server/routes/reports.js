/* istanbul ignore file */
module.exports = function reportsRoutes({
    errors,
    authenticationService,
    reportsService,
    projectUsersQuery,
    queryService,
}) {
    return {
        templates: {
            'import_users.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        return await reportsService.getUsersImportReport();
                    },
                },
            },
            'import_role_scheme_users.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        return await reportsService.getRolesImportReport();
                    },
                },
            },
            'import_role_scheme.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        return await reportsService.getRoleSchemeImportReport();
                    },
                },
            },
        },
        reports: {
            'articles.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        await reportsService.getArticlesReport(req.query);

                        return { status: 301, redirect: '/api/files/articles.csv' };
                    },
                },
            },
            'project_users_with_articles.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        await reportsService.getProjectUserArticlesReport();

                        return { status: 301, redirect: '/api/files/project_users_with_articles.csv' };
                    },
                },
            },
            'role_scheme.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { projectId } = req.query;
                        return await reportsService.getRoleSchemeReport(projectId);
                    },
                },
            },
            'clothing_sizes.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        return await reportsService.getClothingSizesReport();
                    },
                },
            },
            'fabrics.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        return await reportsService.getFabricsReport();
                    },
                },
            },
            'patterns.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        return await reportsService.getPatternsReport();
                    },
                },
            },
            'categories.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        return await reportsService.getPriceCategoriesReport();
                    },
                },
            },
            // TODO: add orders.xlsx export
            'orders.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        return await reportsService.getOrdersReport();
                    },
                },
            },
            'colors.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        return await reportsService.getColorsReport();
                    },
                },
            },
            'racks.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        return await reportsService.getRacksReport();
                    },
                },
            },
            'project_users.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { token } = req.query;
                        const { level } = await authenticationService.verifyToken(token);

                        if (level === 0) {
                            throw errors.httpStatusCodeError(401, 'Unauthorized');
                        }

                        const queryConfig = projectUsersQuery(req.query);

                        queryConfig.fields = [
                            { field: 'p.name', as: 'Project' },
                            { field: 's.identifier', as: 'Scène-nummer' },
                            { field: 's.name', as: 'Scène' },
                            { field: 'pR.identifier', as: 'Rol-nummer' },
                            { field: 'pR.name', as: 'Rol' },
                            { field: 'g.identifier', as: 'Groep-nummer' },
                            { field: 'g.name', as: 'Groep' },
                            { field: 'c.name', as: 'Cast' },
                            { field: 'u.firstName', as: 'Voornaam' },
                            { field: 'u.lastName', as: 'Achternaam' },
                        ];

                        const { query } = await queryService(queryConfig);

                        return await reportsService.getProjectUsersReport(query);
                    },
                },
            },
            'projecten.xlsx': {
                GET: {
                    handler: async req => {
                        const { token } = req.query;
                        const { level } = await authenticationService.verifyToken(token);

                        if (level === 0) {
                            throw errors.httpStatusCodeError(401, 'Unauthorized');
                        }

                        return await reportsService.getProjectsReport();
                    },
                },
            },

            'repetitions.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        return await reportsService.getRepetitionsReport(req.query);
                    },
                },
            },
            'presence_complete.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        return await reportsService.getCompletePresenceReport(req.query);
                    },
                },
            },
            'repetitions_complete.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        await reportsService.getCompleteRepetitionsReport(req.query);

                        return { status: 301, redirect: '/api/files/Repetitions_complete.csv' };
                    },
                },
            },
            'repetition.xlsx': {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { level } = await authenticationService.verifyToken(req.query.token);

                        if (level === 0) {
                            throw new Error('Unauthorized');
                        }

                        return await reportsService.getRepetitionReport(req.query);
                    },
                },
            },
            'users.xlsx': {
                GET: {
                    requiredRights: {
                        users: {
                            read: true,
                        },
                    },
                    // VERIFIED
                    handler: async req => {
                        return await reportsService.getUsersReport(req.query);
                    },
                },
            },
            'registrations.xlsx': {
                GET: {
                    requiredRights: {
                        registrations: {
                            read: true,
                        },
                    },
                    // VERIFIED
                    handler: async req => {
                        return await reportsService.getRegistrationsReport(req.query.projectId, req.query);
                    },
                },
            },
            'roles.xlsx': {
                GET: {
                    requiredRights: {
                        roles: {
                            read: true,
                        },
                    },
                    // VERIFIED
                    handler: async req => {
                        return await reportsService.getRolesReport(req.query.projectId, req.query);
                    },
                },
            },
            'groups.xlsx': {
                GET: {
                    requiredRights: {
                        groups: {
                            read: true,
                        },
                    },
                    handler: async req => {
                        return await reportsService.getGroupsReport(req.query.projectId, req.query);
                    },
                },
            },
            'scenes.xlsx': {
                GET: {
                    requiredRights: {
                        scenes: {
                            read: true,
                        },
                    },
                    // VERIFIED
                    handler: async req => {
                        return await reportsService.getScenesReport(req.query.projectId, req.query);
                    },
                },
            },
            'notregistered.xlsx': {
                GET: {
                    requiredRights: {
                        users: {
                            read: true,
                        },
                    },
                    // VERIFIED
                    handler: async () => {
                        return await reportsService.getNotRegisteredReport();
                    },
                },
            },
        },
    };
};
