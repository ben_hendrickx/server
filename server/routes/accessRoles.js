module.exports = function({ accessRolesService, rightsService, usersService }) {
    return {
        access_roles: {
            GET: {
                // TODO: add offset and items
                // VERIFIED
                handler: async () => {
                    return {
                        data: await accessRolesService.read(),
                        total: await accessRolesService.count(),
                    };
                },
            },
            POST: {
                // VERIFIED
                handler: async req => {
                    const { name, description, rights } = req.body;

                    const accessRoleId = await accessRolesService.create({ name, description });
                    await accessRolesService.setRightsForAccessRole(accessRoleId, rights);

                    return { success: true };
                },
            },
            rights: {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        const rights = await rightsService.getRights();

                        return {
                            data: rights,
                            total: rights.length,
                        };
                    },
                },
            },
        },
        access_role: {
            ':id': {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;

                        const [accessRole] = await accessRolesService.read({ id });

                        accessRole.rights = await rightsService.getRightsForAccessRole(id);

                        return accessRole;
                    },
                },
                PUT: {
                    // VERIFIED
                    handler: async req => {
                        const { name, description, rights } = req.body;
                        const { id } = req.params;

                        await accessRolesService.update({ id }, { name, description });
                        await accessRolesService.setRightsForAccessRole(id, rights);

                        return { success: true };
                    },
                },
                DELETE: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;

                        await accessRolesService.rights.remove({ accessRoleId: id });
                        await accessRolesService.subRights.remove({ accessRoleId: id });
                        await accessRolesService.remove({ id });

                        return { success: true };
                    },
                },
                copy: {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;

                            await accessRolesService.copy(id);

                            return { success: true };
                        },
                    },
                },
                //TODO: users calls, rework all of them with something generic
                users: {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;

                            let { sex, picture, minAge, maxAge, sort = 'lastName|ASC' } = req.query;

                            let [accessRoleUsers, users] = await Promise.all([
                                accessRolesService.users.read({ accessRoleId: id }),
                                usersService.read(),
                            ]);

                            const accessRoleUserIds = accessRoleUsers.map(({ userId }) => userId);

                            users = users.filter(user => accessRoleUserIds.includes(user.id));
                            const total = users.length;

                            users = await Promise.all(
                                users.map(async user => {
                                    const image = await usersService.getUserImage(
                                        user.firstName,
                                        user.lastName,
                                        user.email
                                    );

                                    return {
                                        ...user,
                                        image,
                                    };
                                })
                            );

                            users = usersService.filterUsers(users, { sex, picture, minAge, maxAge });
                            users = usersService.sortUsers(users, sort);

                            return {
                                data: users,
                                total,
                            };
                        },
                    },
                },
            },
        },
    };
};
