module.exports = function generalRoutes({
    generalService,
    usersService,
    projectsService,
    clothingService,
    crudService,
}) {
    const settingsCrud = crudService('app_settings');

    return {
        settings: {
            GET: {
                // VERIFIED
                handler: async () => {
                    const settings = await settingsCrud.read();

                    return settings.reduce((acc, { key, value }) => {
                        acc[key] = value;
                        return acc;
                    }, {});
                },
            },
            PUT: {
                // VERIFIED
                handler: async req => {
                    const { title, description } = req.body;

                    const titleSettingExists = (await settingsCrud.read({ key: 'title' }))?.length;
                    const descriptionSettingExists = (await settingsCrud.read({ key: 'description' }))?.length;

                    if (!titleSettingExists) {
                        settingsCrud.create({ key: 'title', value: title });
                    } else {
                        settingsCrud.update({ key: 'title' }, { value: title });
                    }

                    if (!descriptionSettingExists) {
                        settingsCrud.create({ key: 'description', value: description });
                    } else {
                        settingsCrud.update({ key: 'description' }, { value: description });
                    }

                    return { success: true };
                },
            },
        },
        general: {
            info: {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        let settings = await settingsCrud.read();

                        if (!settings.length) {
                            return await generalService.getInfo();
                        }

                        settings = settings.reduce((acc, { key, value }) => {
                            acc[key] = value;
                            return acc;
                        }, {});

                        return {
                            title: settings.title,
                            subtitle: settings.description,
                        };
                    },
                },
            },
            search: {
                POST: {
                    // VERIFIED
                    handler: async req => {
                        const { search } = req.body;

                        const [users, projects, articles] = await Promise.all([
                            usersService.getUsersByPartialString(search),
                            projectsService.getProjectsByPartialString(search),
                            clothingService.getArticlesByPartialString(search),
                        ]);

                        return [
                            ...users.map(user => ({
                                type: 'user',
                                ...user,
                            })),
                            ...projects.map(project => ({
                                type: 'project',
                                ...project,
                            })),
                            ...articles.map(article => ({
                                type: 'article',
                                ...article,
                            })),
                        ];
                    },
                },
            },
        },
    };
};
