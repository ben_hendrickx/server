module.exports = function locationRoutes({ crudService, sortingService }) {
    const locationsCrud = crudService('locations');

    return {
        locations: {
            GET: {
                // VERIFIED
                handler: async () => {
                    const locations = await locationsCrud.read();

                    return {
                        data: sortingService.sortBy(locations, 'name'),
                        total: locations.length,
                    };
                },
            },
            POST: {
                // VERIFIED
                handler: async req => {
                    const { name, identifier, streetName, houseNumber, cityName, zipCode } = req.body;
                    await locationsCrud.create({
                        name,
                        identifier,
                        streetName,
                        houseNumber,
                        cityName,
                        zipCode,
                    });
                    return { success: true };
                },
            },
        },
        location: {
            ':id': {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;
                        const [location] = await locationsCrud.read({ id });
                        return location;
                    },
                },
                PUT: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;
                        const { name, identifier, streetName, houseNumber, cityName, zipCode } = req.body;
                        await locationsCrud.update(
                            { id },
                            { name, identifier, streetName, houseNumber, cityName, zipCode }
                        );
                        return { success: true };
                    },
                },
                DELETE: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;
                        await locationsCrud.remove({ id });
                        return { success: true };
                    },
                },
            },
        },
    };
};
