//TODO: rework so that this file can be removed
module.exports = function groupRoutes({ groupsService, usersService, castsService }) {
    return {
        group: {
            ':id': {
                copy: {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;

                            const [group] = await groupsService.read({ id });
                            if (!group) {
                                return { success: false };
                            }

                            const groupUsers = await groupsService.users.read({ groupId: id });
                            const newGroup = {
                                ...group,
                                name: `${group.name} - Kopie`,
                            };
                            delete newGroup.id;
                            const newGroupId = await groupsService.create(newGroup);

                            await Promise.all(
                                groupUsers.map(groupUser => {
                                    delete groupUser.creationDt;
                                    return groupsService.users.create({
                                        ...groupUser,
                                        groupId: newGroupId,
                                    });
                                })
                            );

                            return { success: true };
                        },
                    },
                },
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;
                        const [group] = await groupsService.read({ id });
                        const groupUsers = (await groupsService.users.read({ groupId: id })) || [];

                        return {
                            ...group,
                            users: await Promise.all(
                                groupUsers.map(async row => {
                                    const user = await usersService.getUser(row.userId);
                                    const [cast] = await castsService.read({ id: row.castId });

                                    return {
                                        ...user,
                                        cast,
                                    };
                                })
                            ),
                        };
                    },
                },
            },
        },
    };
};
