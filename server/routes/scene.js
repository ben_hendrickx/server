module.exports = function sceneRoutes({ scenesService, groupsService, crudService }) {
    const groupOfScenesCrud = crudService('group_of_scenes');
    const projectRolesCrud = crudService('project_roles');

    return {
        scenes: {
            GET: {
                // VERIFIED
                handler: async req => {
                    let { project } = req.query;
                    const config = project ? { projectId: project.split(',').map(v => parseInt(v)) } : {};

                    const scenes = (await scenesService.read(config)) || [];

                    return {
                        data: scenes.sort((a, b) =>
                            a.identifier > b.identifier ? 1 : a.identifier < b.identifier ? -1 : 0
                        ),
                        total: await scenesService.count(),
                    };
                },
            },
            group: {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { projectId } = req.query;
                        return (await groupOfScenesCrud.read({ projectId })) || [];
                    },
                },
                POST: {
                    // VERIFIED
                    handler: async req => {
                        const { identifier, name, description, projectId } = req.body;
                        await groupOfScenesCrud.create({
                            identifier,
                            name,
                            description,
                            projectId: parseInt(projectId),
                        });

                        return { success: true };
                    },
                },
                ':id': {
                    PUT: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            const { identifier, name, description, projectId } = req.body;
                            await groupOfScenesCrud.update(
                                { id },
                                { identifier, name, description, projectId: parseInt(projectId) }
                            );

                            return { success: true };
                        },
                    },
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            const [scene] = await groupOfScenesCrud.read({ id });
                            return { success: true, ...scene };
                        },
                    },
                },
            },
            POST: {
                // VERIFIED
                handler: async req => {
                    const { identifier, name, description, projectId, groupId } = req.body;
                    await scenesService.create({
                        identifier,
                        name,
                        description,
                        projectId: parseInt(projectId),
                        groupId,
                    });

                    return { success: true };
                },
            },
            ':id': {
                PUT: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;
                        const { identifier, name, description, projectId, groupId } = req.body;
                        await scenesService.update(
                            { id },
                            { identifier, name, description, projectId, groupId: parseInt(groupId) }
                        );

                        return { success: true };
                    },
                },
            },
        },
        scene: {
            ':id': {
                DELETE: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;

                        await scenesService.groups.remove({ sceneId: id });
                        await scenesService.roles.remove({ sceneId: id });
                        await scenesService.remove({ id });

                        return { success: true };
                    },
                },
                roles: {
                    add: {
                        POST: {
                            handler: async req => {
                                const { id } = req.params;
                                let { roleId } = req.body;

                                if (!Array.isArray(roleId)) {
                                    roleId = [roleId];
                                }

                                await Promise.all(
                                    roleId.map(role =>
                                        scenesService.roles.create({ sceneId: id, project_role_id: role })
                                    )
                                );

                                return { success: true };
                            },
                        },
                    },
                    DELETE: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            let { roleId } = req.body;
                            await scenesService.roles.remove({ sceneId: id, project_role_id: roleId });

                            return { success: true };
                        },
                    },
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            const roles = await scenesService.roles.read({ sceneId: id });
                            const roleIds = roles.map(({ project_role_id }) => project_role_id);

                            return await projectRolesCrud.read({ id: roleIds });
                        },
                    },
                },
                groups: {
                    add: {
                        POST: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;
                                let { groupId } = req.body;

                                if (!Array.isArray(groupId)) {
                                    groupId = [groupId];
                                }

                                await Promise.all(
                                    groupId.map(group => scenesService.groups.create({ sceneId: id, groupId: group }))
                                );

                                return { success: true };
                            },
                        },
                    },
                    DELETE: {
                        handler: async req => {
                            const { id } = req.params;
                            let { groupId } = req.body;

                            await scenesService.groups.remove({ sceneId: id, groupId });

                            return { success: true };
                        },
                    },
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            const groups = await scenesService.groups.read({ sceneId: id });
                            const groupIds = groups.map(({ groupId }) => groupId);

                            return await groupsService.read({ id: groupIds });
                        },
                    },
                },
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;
                        const [scene] = await scenesService.read({ id });
                        return scene;
                    },
                },
            },
        },
        scene_roles: {
            GET: {
                // VERIFIED
                handler: async () => {
                    return {
                        success: true,
                        data: await scenesService.roles.read(),
                    };
                },
            },
        },
        scene_groups: {
            GET: {
                // VERIFIED
                handler: async () => {
                    return {
                        success: true,
                        data: await scenesService.groups.read(),
                    };
                },
            },
        },
    };
};
