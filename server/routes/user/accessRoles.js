module.exports = function({ accessRolesService }) {
    return {
        user: {
            ':userId': {
                access_roles: {
                    GET: {
                        // TODO: add rights
                        // VERIFIED
                        handler: async req => {
                            const { userId } = req.params;

                            const accessRoles = await accessRolesService.getAccessRolesForUser(userId);

                            return {
                                data: accessRoles,
                                total: accessRoles.length,
                            };
                        },
                    },
                    POST: {
                        handler: async req => {
                            const { userId } = req.params;

                            await accessRolesService.users.create({ userId, accessRoleId: req.body });

                            return { success: true };
                        },
                    },
                },
                access_role: {
                    ':id': {
                        DELETE: {
                            handler: async req => {
                                const { userId, id } = req.params;

                                await accessRolesService.users.remove({ accessRoleId: id, userId });

                                return { success: true };
                            },
                        },
                    },
                },
            },
        },
    };
};
