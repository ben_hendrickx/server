const { v4: uuidv4 } = require('uuid');
const moment = require('moment');
const { mail, server: { hostnameShort } } = require('../../../config');

module.exports = function({ threadsService, usersService, projectsService, templateService, mailService, logger }) {
    logger.fatal('THREADS STARTED');

    return function({ io }) {
        const CronJob = require('cron').CronJob;
        const job = new CronJob(
            '0 25 11 * * *',
            async function() {
                const userIds = (await threadsService.users.read()).map(({ userId }) => userId);
                const sentTo = [];
                // get userids for threads
                userIds
                    .reduce((acc, id) => {
                        !acc.includes(id) && acc.push(id);
                        return acc;
                    }, [])
                    .forEach(async userId => {
                        const count = await getUnreadCountForUser(userId);

                        if (!count) {
                            return;
                        }

                        const [user] = await usersService.read({ id: userId });

                        if (sentTo.includes(user.email)) {
                            return;
                        }

                        sentTo.push(user.email);

                        templateService.get('threads').then(html => {
                            html = html.replace(':email', user.email);
                            html = html.replace('{threads}', count);
                            mailService.send(
                                {
                                    from: {
                                        name: mail.name,
                                        address: mail.noReply,
                                    },
                                    to: [user.email],
                                    subject: `Nieuwe berichten op ${hostnameShort}`,
                                    // TODO: make templateService function to convert html to text-version
                                    text:
                                        'Dankjewel voor je registratie bij Fan van Dimpna. Elk jaar zetten we met Fan van Dimpna de stad Geel in de kijker met onze Kerstmusical. Dit jaar zijn we natuurlijk weer van de partij. Om al dit in goede banen te leiden gaan we jouw hulp goed kunnen gebruiken!',
                                    html,
                                },
                                e => {
                                    if (e) {
                                        return {
                                            status: 500,
                                            data: e.toString(),
                                        };
                                    }

                                    return { status: 200 };
                                },
                                'threads'
                            );
                        });
                    });
            },
            null,
            true,
            'Europe/Brussels'
        );
        job.start();

        io.on('connection', function(socket) {
            socket.on('message_read', data => markRead(data));

            socket.on('threads_unread', async userId => {
                socket.emit('threads_unread', await getUnreadCountForUser(userId));
            });
        });

        async function getUnreadCountForUser(userId) {
            const threads = await threadsService.read();
            const projects = await projectsService.read();

            const dontSendForThreadIds = threads
                .filter(({ projectId }) => {
                    if (!projectId) {
                        return false;
                    }

                    const project = projects.find(({ id }) => id === projectId);
                    if (!project) {
                        return false;
                    }

                    const { projectClosed } = project;
                    return moment(projectClosed).isBefore(moment());
                })
                .map(({ id }) => id);

            const threadsForUser = await threadsService.users.read({ userId });
            const unreadThreads = await Promise.all(
                threadsForUser.map(async ({ threadId, lastRead, archive }) => {
                    if (archive || dontSendForThreadIds.includes(threadId)) {
                        return;
                    }

                    const messages = await threadsService.messages.read({ threadId });

                    return messages.some(({ createdDt }) => createdDt.getTime() > (lastRead?.getTime() || 0));
                })
            );

            return unreadThreads.filter(v => v).length;
        }

        async function markRead({ threadId, userId }) {
            await threadsService.users.update({ userId, threadId }, { lastRead: moment().format() });

            const threadUsers = await threadsService.users.read({ threadId });
            const messages = await threadsService.messages.read({ threadId });

            if (!messages.length) {
                return;
            }

            const message = messages.pop();

            if (threadUsers.every(user => new Date(user.lastRead).getTime() >= new Date(message.createdDt).getTime())) {
                io.emit('thread_update', { threadId, status: 'read' });
            }
        }

        return {
            user: {
                ':userId': {
                    threads: {
                        GET: {
                            // TODO: add rights
                            // VERIFIED
                            handler: async req => {
                                const { userId } = req.params;
                                const { archive } = req.query;

                                let threads = await threadsService.read();
                                const threadsForUser = await threadsService.users.read({ userId });
                                const projects = await projectsService.read();
                                const threadIdsForUser = threadsForUser.map(({ threadId }) => threadId);

                                threads = threads.filter(({ id }) => threadIdsForUser.includes(id));

                                const total = threads.length;
                                let data = threads.filter(t => archive === undefined || t.archive === archive);

                                const messages = await threadsService.messages.read({ threadId: data.map(t => t.id) });
                                messages.sort(
                                    (a, b) => new Date(b.createdDt).getTime() - new Date(a.createdDt).getTime()
                                );

                                data = await Promise.all(
                                    data.map(async thread => {
                                        const lastMessage = messages.find(message => message.threadId === thread.id);
                                        thread.unread = false;

                                        thread.project = projects.find(project => project.id === thread.projectId);

                                        if (lastMessage) {
                                            const [user] = await usersService.read({ id: lastMessage.userId });
                                            delete lastMessage.userId;
                                            lastMessage.user = user;
                                            thread.unread = threadsForUser
                                                .filter(user => thread.id === user.threadId)
                                                .some(
                                                    user =>
                                                        new Date(user.lastRead).getTime() <
                                                        new Date(lastMessage.createdDt).getTime()
                                                );
                                            thread.lastMessage = lastMessage;
                                        }

                                        return thread;
                                    })
                                );

                                return {
                                    data: data.sort((a, b) => {
                                        return (
                                            new Date(b.lastMessage.createdDt).getTime() -
                                            new Date(a.lastMessage.createdDt).getTime()
                                        );
                                    }),
                                    total,
                                };
                            },
                        },
                        POST: {
                            // VERIFIED
                            handler: async req => {
                                const { userId } = req.params;
                                let {
                                    name: title,
                                    projectId,
                                    defaultMessage,
                                    responsible,
                                    participants,
                                    actionId,
                                } = req.body;

                                const threadId = await threadsService.create({
                                    title,
                                    projectId: projectId || null,
                                    actionId,
                                });
                                await threadsService.users.create({ userId, threadId, isResponsible: true });
                                await threadsService.messages.create({
                                    id: uuidv4(),
                                    userId,
                                    threadId,
                                    message: defaultMessage,
                                });

                                await Promise.all(
                                    responsible
                                        .filter(responsible => responsible !== userId)
                                        .map(responsible =>
                                            threadsService.users.create({
                                                threadId,
                                                userId: responsible,
                                                isResponsible: true,
                                            })
                                        )
                                );

                                participants = participants.filter(participant => !responsible.includes(participant));

                                await Promise.all(
                                    participants.map(participant =>
                                        threadsService.users.create({
                                            threadId,
                                            userId: participant,
                                        })
                                    )
                                );

                                return { success: true };
                            },
                        },
                        ':id': {
                            PUT: {
                                handler: async req => {
                                    const { id: threadId } = req.params;
                                    let { responsible, participants } = req.body;

                                    await threadsService.users.remove({ threadId });

                                    await Promise.all(
                                        responsible.map(responsible =>
                                            threadsService.users.create({
                                                threadId,
                                                userId: responsible,
                                                isResponsible: true,
                                            })
                                        )
                                    );

                                    participants = participants.filter(
                                        participant => !responsible.includes(participant)
                                    );

                                    await Promise.all(
                                        participants.map(participant =>
                                            threadsService.users.create({
                                                threadId,
                                                userId: participant,
                                            })
                                        )
                                    );

                                    return { success: true };
                                },
                            },
                            GET: {
                                // VERIFIED
                                handler: async req => {
                                    const { userId, id } = req.params;

                                    const threadUsers = await threadsService.users.read({ threadId: id });
                                    const threadUserIds = threadUsers.map(user => user.userId);
                                    const users = await usersService.read({ id: threadUserIds });
                                    const threadUsersMapped = threadUsers.map(tU => {
                                        const { firstName, lastName } = users.find(u => u.id === tU.userId);

                                        return {
                                            ...tU,
                                            firstName,
                                            lastName,
                                        };
                                    });
                                    let messages = await threadsService.messages.read({ threadId: id });

                                    messages = await Promise.all(
                                        messages.map(async message => {
                                            let user = users.find(user => user.id === message.userId);
                                            if (!user) {
                                                user = (await usersService.read({ id: message.userId }))[0];
                                            }
                                            const status = threadUsers.every(
                                                user =>
                                                    new Date(message.createdDt).getTime() <
                                                    new Date(user.lastRead).getTime()
                                            )
                                                ? 'read'
                                                : 'received';

                                            message.user = {
                                                id: user.id,
                                                firstName: user.firstName,
                                                lastName: user.lastName,
                                            };

                                            message.status = status;

                                            return message;
                                        })
                                    );

                                    const [thread] = await threadsService.read({ id });

                                    thread.responsible = threadUsersMapped
                                        .filter(tU => tU.isResponsible && tU.threadId === thread.id)
                                        .map(
                                            ({ firstName, lastName, userId }) => `${lastName} ${firstName} (${userId})`
                                        );
                                    thread.participants = threadUsersMapped
                                        .filter(tU => !tU.isResponsible && tU.threadId === thread.id)
                                        .map(
                                            ({ firstName, lastName, userId }) => `${lastName} ${firstName} (${userId})`
                                        );

                                    markRead({ threadId: id, userId });

                                    return {
                                        ...thread,
                                        messages,
                                    };
                                },
                            },
                            messages: {
                                POST: {
                                    // VERIFIED
                                    handler: async req => {
                                        const { userId, id } = req.params;
                                        const { message, replyOn, file } = req.body;

                                        const messageId = uuidv4();
                                        await threadsService.messages.create({
                                            id: messageId,
                                            userId,
                                            threadId: id,
                                            message,
                                            replyOn,
                                            file,
                                        });

                                        const [newMessage] = await threadsService.messages.read({ id: messageId });
                                        const [user] = await usersService.read({ id: userId });
                                        delete newMessage.userId;
                                        newMessage.user = user;
                                        newMessage.status = 'received';
                                        io.emit('message_received', newMessage);

                                        markRead({ threadId: id, userId });

                                        return { success: true };
                                    },
                                },
                            },
                        },
                    },
                },
            },
        };
    };
};
