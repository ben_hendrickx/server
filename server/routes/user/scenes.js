module.exports = function({ scenesService, projectsService }) {
    return {
        user: {
            ':userId': {
                scenes: {
                    GET: {
                        // TODO: add rights
                        // VERIFIED
                        handler: async req => {
                            const { userId } = req.params;
                            const { project } = req.query;

                            const [projects, scenes] = await Promise.all([
                                projectsService.read(),
                                scenesService.getScenesForUser(userId),
                            ]);

                            return {
                                data: scenes
                                    .filter(({ projectId }) => !project || projectId === project)
                                    .map(scene => {
                                        scene.project = projects.find(({ id }) => id === scene.projectId);
                                        return scene;
                                    }),
                                total: scenes.length,
                            };
                        },
                    },
                },
            },
        },
    };
};
