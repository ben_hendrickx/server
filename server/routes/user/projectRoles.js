module.exports = function({
    queryService,
    dataService,
    castsService,
    projectRolesQuery,
    groupsQuery,
    projectsService,
    dateUtil,
    sortingService,
}) {
    function _isOngoing(project) {
        const { openFrom, closedFrom, closedOffStage } = project;

        return (
            openFrom &&
            dateUtil.isInPast(openFrom) &&
            ((closedFrom && dateUtil.isInFuture(closedFrom)) || (closedOffStage && dateUtil.isInFuture(closedOffStage)))
        );
    }

    function _roleSchemeOpen(project) {
        const { rolesOpen, rolesClosed } = project;

        return rolesOpen && dateUtil.isInPast(rolesOpen) && rolesClosed && dateUtil.isInFuture(rolesClosed);
    }

    return {
        user: {
            ':userId': {
                project_roles: {
                    GET: {
                        // TODO: add rights
                        // VERIFIED
                        handler: async req => {
                            const { userId } = req.params;
                            let { project, scene, open } = req.query;

                            if (open === true) {
                                project = (await projectsService.read())
                                    .filter(_isOngoing)
                                    .filter(_roleSchemeOpen)
                                    .map(({ id }) => id);
                            }

                            const { query: roles } = queryService(
                                projectRolesQuery({ projectId: project, userId, scene })
                            );
                            const { query: groups } = queryService(groupsQuery({ projectId: project, userId, scene }));

                            let [_roles, _groups] = await Promise.all([
                                dataService.raw(roles),
                                dataService.raw(groups),
                            ]);

                            const allRoles = [
                                _roles.map(role => ({ ...role, type: 'role' })),
                                _groups.map(group => ({ ...group, type: 'group' })),
                            ].flat();
                            // .reduce((acc, data) => {
                            //     if (acc.find(({ id, type }) => data.type === type && data.id === id)) {
                            //         return acc;
                            //     }

                            //     acc.push(data);
                            //     return acc;
                            // }, []);

                            const casts = await castsService.read();
                            const projects = await projectsService.read();

                            return {
                                total: allRoles.flat().length,
                                data: allRoles.sort(sortingService.sortRoleOverview).map(role => {
                                    const { castId, projectId } = role;
                                    delete role.castId;

                                    const cast = casts.find(({ id }) => id === castId);
                                    const project = projects.find(({ id }) => id === projectId);

                                    return {
                                        ...role,
                                        cast,
                                        project,
                                    };
                                }),
                            };
                        },
                    },
                },
            },
        },
    };
};
