module.exports = function({ repetitionsService }) {
    return {
        user: {
            ':userId': {
                repetitions: {
                    GET: {
                        //TODO: add rights
                        // VERIFIED
                        handler: async req => {
                            const { userId } = req.params;
                            const { past } = req.query;

                            const repetitions = await repetitionsService.getRepetitionsForUser(userId, undefined, past);

                            return {
                                data: repetitions,
                                total: repetitions.length,
                            };
                        },
                    },
                },
            },
        },
    };
};
