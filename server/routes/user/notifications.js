module.exports = function notificationRoutes({ notificationsService }) {
    return {
        user: {
            ':userId': {
                notifications: {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { userId } = req.params;
                            const { read, category: type, project } = req.query;

                            let notifications = await notificationsService.getNotificationsForUser(userId, project);
                            const total = notifications.length;
                            if (read !== undefined) {
                                notifications = notifications.filter(n => n.read == read);
                            }

                            if (type !== undefined) {
                                notifications = notifications.filter(n => n.type === type);
                            }

                            return {
                                data: notifications,
                                total,
                            };
                        },
                    },
                    DELETE: {
                        // VERIFIED
                        handler: async req => {
                            const { userId } = req.params;

                            await Promise.all(
                                req.body.map(id => notificationsService.removed.create({ notificationId: id, userId }))
                            );
                            await notificationsService.clear();

                            return { success: true };
                        },
                    },
                },
                notification: {
                    ':id': {
                        DELETE: {
                            // VERIFIED
                            handler: async req => {
                                const { id, userId } = req.params;

                                await notificationsService.removed.create({ notificationId: id, userId });
                                await notificationsService.clear();

                                return { success: true };
                            },
                        },
                    },
                },
            },
        },
    };
};
