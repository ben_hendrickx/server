const moment = require('moment');
const CryptoJS = require('crypto-js');
const { mail } = require('../../../config');

module.exports = function userRoutes({
    stringUtil,
    logger,
    templateService,
    mailService,
    requestUtil,
    usersService,
    notificationsService,
    authenticationService,
    rightsService,
    sizesService,
    registrationsService,
    loginsService,
    projectsService,
    crudService,
    sortingService,
}) {
    return {
        profile: {
            POST: {
                handler: async req => {
                    const token = requestUtil.getToken(req);
                    const { status } = await authenticationService.verifyToken(token);

                    if (status === 404) {
                        return {
                            status: 401,
                        };
                    }

                    const { firstName, lastName, email, image } = req.body;
                    const users = await usersService.read({ firstName, lastName, email });

                    if (users?.length) {
                        return {
                            status: 500,
                            data: 'Er bestaat reeds een profiel met deze gegevens',
                        };
                    }

                    if (image) {
                        const type = image.includes('image/png') ? 'png' : 'jpg';
                        const base64 = image.replace(/^data:image\/(png|jpeg);base64,/, '');

                        require('fs').writeFile(
                            `images/${firstName}_${lastName}_${stringUtil.removeChars(
                                email,
                                `.!#%&'-/=_\`{}~",:;<>@`.split('')
                            )}.${type}`,
                            base64,
                            'base64',
                            err => err && logger.error(err)
                        );
                    }

                    try {
                        const { body } = req;
                        let { tShirt: shirtSize, privacy, pictures, sex } = req.body;
                        body.sex = ['man', 'vrouw'].indexOf(sex.toLowerCase());
                        shirtSize = ((await sizesService.shirtSizes.read()) || []).find(
                            ({ name }) => name === shirtSize.toString()
                        )?.id;

                        delete body.tShirt;
                        delete body.image;
                        delete body.pwd;
                        delete body.privacy;
                        delete body.pictures;

                        const userId = await usersService.create(body);
                        await usersService.sizes.create({ userId, shirtSize });
                        await usersService.insertAppSettings(userId, privacy, pictures);

                        return { userId };
                    } catch (e) {
                        return {
                            status: 500,
                            data: e.toString(),
                        };
                    }
                },
            },
        },
        users: {
            GET: {
                requiredRights: {
                    users: {
                        read: true,
                    },
                },
                // VERIFIED
                handler: async req => {
                    const { sort, text, sex, minAge, maxAge, offset = 0, items = 10, threads } = req.query;

                    const pieces = [];
                    text !== undefined && pieces.push(...text.split(' '));

                    let users = await usersService.readNew(
                        [
                            sex !== undefined && { key: 'sex', value: sex },
                            minAge !== undefined && {
                                key: 'date_of_birth',
                                operator: '<',
                                value: moment()
                                    .subtract(minAge, 'years')
                                    .add(1, 'day')
                                    .startOf('day')
                                    .format(),
                            },
                            maxAge !== undefined && {
                                key: 'date_of_birth',
                                operator: '>',
                                value: moment()
                                    .subtract(maxAge, 'years')
                                    .subtract(1, 'years')
                                    .add(1, 'day')
                                    .startOf('day')
                                    .format(),
                            },
                            ...pieces.map(piece => ({
                                key: ['firstName', 'lastName', 'email', 'YEAR(date_of_birth)'],
                                operator: 'LIKE',
                                value: `%${piece}%`,
                            })),
                        ],
                        {
                            offset,
                            items,
                            sort,
                            fields: ['id', 'firstName', 'lastName', 'date_of_birth', 'email', 'sex'],
                        }
                    );

                    items !== 'all' &&
                        !threads &&
                        (users.data = await Promise.all(
                            users.data.map(async user => {
                                user.image = await usersService.getUserImage(user.firstName, user.lastName, user.email);
                                return user;
                            })
                        ));

                    if (threads) {
                        const userIdsWithRight = await rightsService.getUserIdsWithRight('threads');
                        const usersWithRights = users.data.filter(({ id }) => userIdsWithRight.includes(id));

                        return {
                            offset: 0,
                            data: usersWithRights,
                            items: usersWithRights.length,
                            total: usersWithRights.length,
                        };
                    }

                    return {
                        offset,
                        data: users.data,
                        items: users.count,
                        total: await usersService.count(),
                        filters: {
                            age: await usersService.getAges(),
                        },
                    };
                },
            },
            POST: {
                handler: async req => {
                    return await usersService.create(req.body);
                },
            },
        },

        'parts-of-town': {
            GET: {
                // VERIFIED
                handler: async () => {
                    return await projectsService.partsOfTown.read();
                },
            },
        },
        user: {
            ':id': {
                remarks: {
                    PUT: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            const { remarks } = req.body;
                            await usersService.update({ id }, { owner_remarks: remarks });
                            return { success: true };
                        },
                    },
                },
                profiles: {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;

                            const [user] = await usersService.read({ id });
                            const { email } = user;

                            return await usersService.read({ email });
                        },
                    },
                },
                'contact-details': {
                    PUT: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;

                            const token = requestUtil.getToken(req);
                            const { level, id: requestingUserId } = await authenticationService.verifyToken(token);

                            if (level !== 1 && id !== requestingUserId) {
                                throw new Error('Unauthorized');
                            }

                            const {
                                phoneNumber,
                                cellPhone,
                                email,
                                newEmail,
                                secondaryEmail,
                                streetName,
                                houseNumber,
                                bus,
                                zipCode,
                                cityName,
                            } = req.body || {};

                            try {
                                const [[alreadyExists = false], [login], [user]] = await Promise.all([
                                    loginsService.read({ email: newEmail }),
                                    loginsService.read({ email }),
                                    usersService.read({ id }),
                                ]);

                                if (level === 0 && email.toLowerCase() !== newEmail.toLowerCase() && alreadyExists) {
                                    throw new Error('Er is al een gebruiker met dit e-mailadres');
                                }

                                const changes = [];

                                phoneNumber !== user.phoneNumber &&
                                    changes.push({ key: 'Telefoonnummer', old: user.phoneNumber, new: phoneNumber });
                                cellPhone !== user.cellPhone &&
                                    changes.push({ key: 'G.S.M.-nummer', old: user.cellPhone, new: cellPhone });
                                level === 0 &&
                                    newEmail &&
                                    newEmail !== user.email &&
                                    changes.push({ key: 'Email', old: user.email, new: newEmail });
                                secondaryEmail &&
                                    secondaryEmail !== user.secondaryEmail &&
                                    changes.push({
                                        key: 'Tweede e-mailadres',
                                        old: user.secondaryEmail,
                                        new: secondaryEmail,
                                    });
                                streetName &&
                                    streetName !== user.streetName &&
                                    changes.push({ key: 'Straatnaam', old: user.streetName, new: streetName });
                                houseNumber &&
                                    houseNumber !== user.houseNumber &&
                                    changes.push({ key: 'Huisnummer', old: user.houseNumber, new: houseNumber });
                                bus && bus !== user.bus && changes.push({ key: 'Busnummer', old: user.bus, new: bus });
                                zipCode &&
                                    zipCode !== user.zipCode &&
                                    changes.push({ key: 'Postcode', old: user.zipCode, new: zipCode });
                                cityName &&
                                    cityName !== user.cityName &&
                                    changes.push({ key: 'Gemeente', old: user.cityName, new: cityName });

                                if (changes && changes.length) {
                                    //eslint-disable-next-line
                                    let notification = `<div>Medewerker ${user.firstName} ${user.lastName} heeft volgende contactgegevens gewijzigd:<br \><ul>`;
                                    for (let change of changes) {
                                        notification += `<li>${change.key} werd veranderd van ${change.old} naar ${change.new}</li>`;
                                    }

                                    notification += '</ul></div>';

                                    await notificationsService.add({
                                        user: {
                                            id: requestingUserId,
                                            userId: id,
                                            name: user.lastName + ' ' + user.firstName,
                                        },
                                        label: 'Wijziging contactgegevens',
                                        notification,
                                        type: 6,
                                    });
                                }

                                if (newEmail !== email) {
                                    login && delete login.id;
                                    !login.lastLoggedIn && delete login.lastLoggedIn;
                                    login.lastLoggedIn &&
                                        (login.lastLoggedIn = require('moment')(login.lastLoggedIn).format(
                                            'YYYY-MM-DD'
                                        ));
                                    !alreadyExists && (await loginsService.create({ ...login, email: newEmail }));
                                    await usersService.update({ id }, { email: newEmail });
                                    const profiles = await usersService.read({ email });
                                    !profiles.length && (await loginsService.remove({ email }));
                                }

                                await usersService.update(
                                    { id },
                                    {
                                        phoneNumber,
                                        cellPhone,
                                        secondaryEmail,
                                        streetName,
                                        houseNumber,
                                        bus,
                                        zipCode,
                                        cityName,
                                    }
                                );

                                if (newEmail !== email) {
                                    const users = await usersService.read({ email: newEmail });

                                    for (let i = 0; i < users.length; i++) {
                                        const cleanOldEmail = stringUtil.removeChars(
                                            email,
                                            `.!#%&'-/=_\`{}~",:;<>@`.split('')
                                        );
                                        const cleanNewEmail = stringUtil.removeChars(
                                            newEmail,
                                            `.!#%&'-/=_\`{}~",:;<>@`.split('')
                                        );
                                        const oldPath = `images/${users[i].firstName}_${users[i].lastName}_${cleanOldEmail}.jpg`;
                                        const newPath = `images/${users[i].firstName}_${users[i].lastName}_${cleanNewEmail}.jpg`;
                                        try {
                                            require('fs').renameSync(oldPath, newPath);
                                        } catch (e) {
                                            logger.error(e);
                                        }
                                    }
                                }

                                return { success: true };
                            } catch (e) {
                                throw new Error(e.message);
                            }
                        },
                    },
                },
                settings: {
                    GET: {
                        handler: async req => {
                            const { id } = req.params;

                            return await crudService('user_app_settings').read({ userId: id });
                        },
                    },
                },
                sizes: {
                    // TODO: add rights
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;

                            const [[userSizes = {}], shirtSizes = []] = await Promise.all([
                                usersService.sizes.read({ userId: id }),
                                sizesService.shirtSizes.read(),
                            ]);

                            const shirtSize = (shirtSizes.find(s => s.id === userSizes.shirtSize) || {}).name;

                            return {
                                ...userSizes,
                                shirtSize,
                            };
                        },
                    },
                },
                rights: {
                    GET: {
                        // TODO: add rights
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            return await rightsService.getRightsForUser(id);
                        },
                    },
                },
                PUT: {
                    handler: async req => {
                        const { id } = req.params;
                        return await usersService.update({ id }, req.body);
                    },
                },
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const token = requestUtil.getToken(req);
                        const { id } = req.params;
                        const user = await usersService.getUserById(id, token);

                        user.clothingFrozen && (user.clothingFrozen = moment(user.clothingFrozen).isAfter(moment()));

                        return user;
                    },
                },
                DELETE: {
                    handler: async req => {
                        const { id } = req.params;
                        const userId = id;

                        const [user] = await usersService.read({ id });

                        if (!user) {
                            return;
                        }

                        await Promise.all([
                            crudService('user_sub_rights').remove({ userId }),
                            crudService('user_rights').remove({ userId }),
                            crudService('user_log').remove({ userId }),
                            crudService('user_app_settings').remove({ userId }),
                            crudService('user_sizes').remove({ userId }),
                            crudService('user_access_roles').remove({ userId }),
                            crudService('thread_users').remove({ userId }),
                            crudService('thread_messages').remove({ userId }),
                            crudService('thread_action_users').remove({ userId }),
                            crudService('repetition_users').remove({ userId }),
                            crudService('repetition_user_presence').remove({ userId }),
                            crudService('project_user_sub_rights').remove({ userId }),
                            crudService('project_user_rights').remove({ userId }),
                            crudService('project_user_registrations').remove({ userId }),
                            crudService('project_user_access_roles').remove({ userId }),
                            crudService('project_role_users').remove({ userId }),
                            crudService('group_users').remove({ userId }),
                        ]);

                        const projectUserUuid = (await crudService('project_users').read({ userId })).map(
                            ({ projectUserUuid }) => projectUserUuid
                        );
                        projectUserUuid.length &&
                            (await crudService('project_user_articles').remove({ projectUserUuid }));

                        await Promise.all([crudService('project_users').remove({ userId })]);

                        await usersService.remove({ id });

                        const { email } = user;
                        const profiles = await usersService.read({ email });

                        if (!profiles.length) {
                            await crudService('logins').remove({ email });
                        }

                        return { success: true };
                    },
                },
            },
        },
        image: {
            POST: {
                handler: async req => {
                    const { email, firstName, lastName } = req.body;
                    try {
                        return await usersService.getUserImage(firstName, lastName, email);
                    } catch (e) {
                        return '';
                    }
                },
            },
            PUT: {
                // VERIFIED
                handler: req => {
                    const { image = '', firstName, lastName, email } = req.body || {};

                    if (!image) {
                        throw new Error('Bad request');
                    }

                    const type = image.includes('image/png') ? 'png' : 'jpg';
                    const base64 = image.replace(/^data:image\/(png|jpeg);base64,/, '');

                    try {
                        require('fs').writeFile(
                            `images/${firstName}_${lastName}_${stringUtil.removeChars(
                                email,
                                `.!#%&'-/=_\`{}~",:;<>@`.split('')
                            )}.${type}`,
                            base64,
                            'base64',
                            err => err && logger.error(err)
                        );
                        return { success: true };
                    } catch (e) {
                        throw new Error('Server error');
                    }
                },
            },
        },
        'clothing-sizes': {
            PUT: {
                // VERIFIED
                handler: async req => {
                    const token = requestUtil.getToken(req);
                    const { level, id: userId } = await authenticationService.verifyToken(token);
                    const { id, height, chest, waist, hipSize, headSize, clothingSize, remarks } = req.body || {};
                    let { shirtSize, clothingFrozen } = req.body || {};

                    if (level !== 1 && userId !== id) {
                        throw new Error('Unauthorized');
                    }

                    const [user] = await usersService.read({ id });
                    const [sizes] = await usersService.sizes.read({ userId });
                    const changes = [];

                    if (level === 1) {
                        if (clothingFrozen) {
                            let projects = await projectsService.read();
                            projects = projects.filter(
                                project => project.projectClosed && moment(project.projectClosed).isAfter(moment())
                            );
                            projects = sortingService.sortBy(projects, 'projectClosed', true);

                            if (projects?.length) {
                                await usersService.update(
                                    { id },
                                    { clothingFrozen: moment(projects[0].projectClosed).format() }
                                );
                            }
                        } else {
                            await usersService.update({ id }, { clothingFrozen: null });
                        }
                    }

                    const currentShirtSize = ((await sizesService.shirtSizes.read()) || []).find(
                        ({ id }) => id === sizes.shirtSize
                    )?.name;

                    height !== sizes.height && changes.push({ key: 'Lichaamslengte', old: sizes.height, new: height });
                    chest !== sizes.chest && changes.push({ key: 'Borstomtrek', old: sizes.chest, new: chest });
                    waist !== sizes.waist && changes.push({ key: 'Taille', old: sizes.waist, new: waist });
                    hipSize !== sizes.hipSize && changes.push({ key: 'Heupomtrek', old: sizes.hipSize, new: hipSize });
                    headSize !== sizes.headSize &&
                        changes.push({ key: 'Hoofdomtrek', old: sizes.headSize, new: headSize });
                    clothingSize !== sizes.clothingSize &&
                        changes.push({ key: 'Kledingmaat', old: sizes.clothingSize, new: clothingSize });
                    shirtSize !== currentShirtSize &&
                        changes.push({ key: 'T-shirt maat', old: currentShirtSize, new: shirtSize });

                    if (level === 0 && changes && changes.length) {
                        //eslint-disable-next-line
                        let notification = `<div>Medewerker ${user.firstName} ${user.lastName} heeft volgende kledij-gegevens gewijzigd:<br \><ul>`;
                        for (let change of changes) {
                            notification += `<li>${change.key} werd veranderd van ${change.old} naar ${change.new}</li>`;
                        }

                        notification += '</ul></div>';

                        await notificationsService.add({
                            user: {
                                id: userId,
                                name: user.lastName + ' ' + user.firstName,
                            },
                            label: 'Wijziging kledij-gegevens',
                            notification,
                            type: 7,
                        });
                    }

                    try {
                        const shirtSizes = await sizesService.shirtSizes.read();
                        shirtSize = shirtSizes.find(({ name }) => name === shirtSize.toString())?.id;

                        await usersService.sizes.update(
                            { userId: id },
                            {
                                height,
                                chest,
                                waist,
                                hipSize,
                                headSize,
                                shirtSize,
                                clothingSize: parseInt(clothingSize || 0),
                                remarks,
                            }
                        );

                        return { success: true };
                    } catch (e) {
                        throw new Error(e.message);
                    }
                },
            },
        },
        privacy: {
            PUT: {
                // VERIFIED
                handler: async req => {
                    const token = requestUtil.getToken(req);
                    const { level, id: userId } = await authenticationService.verifyToken(token);
                    const { id, pictures } = req.body || {};

                    if (level !== 1 && userId !== id) {
                        throw new Error('Unauthorized');
                    }

                    try {
                        await usersService.settings.update(
                            { userId: id },
                            {
                                pictures,
                            }
                        );

                        return { success: true };
                    } catch (e) {
                        throw new Error(e.message);
                    }
                },
            },
        },
        information: {
            PUT: {
                // VERIFIED
                handler: async req => {
                    const token = requestUtil.getToken(req);
                    const { level, id: userId } = await authenticationService.verifyToken(token);
                    const { id, firstName, lastName, date_of_birth, sex, nationality, origin } = req.body || {};

                    if (level !== 1 && userId !== id) {
                        throw new Error('Unauthorized');
                    }

                    const [user] = await usersService.read({ id });
                    const changes = [];

                    firstName !== user.firstName &&
                        changes.push({ key: 'Voornaam', old: user.firstName, new: firstName });
                    lastName !== user.lastName &&
                        changes.push({ key: 'Achternaam', old: user.lastName, new: lastName });
                    date_of_birth &&
                        date_of_birth !== require('moment')(user.date_of_birth).format('YYYY-MM-DD') &&
                        changes.push({
                            key: 'Geboortedatum',
                            old: require('moment')(user.date_of_birth).format('YYYY-MM-DD'),
                            new: date_of_birth,
                        });
                    sex !== user.sex &&
                        changes.push({
                            key: 'Geslacht',
                            old: user.sex ? 'Vrouw' : 'Man',
                            new: sex ? 'Vrouw' : 'Man',
                        });
                    nationality !== user.nationality &&
                        changes.push({ key: 'Nationaliteit', old: user.nationality, new: nationality });
                    origin !== user.origin && changes.push({ key: 'Geboorteland', old: user.origin, new: origin });

                    if (changes && changes.length) {
                        //eslint-disable-next-line
                        let notification = `<div>Medewerker ${user.firstName} ${user.lastName} heeft volgende persoonsgegevens gewijzigd:<br \><ul>`;
                        for (let change of changes) {
                            notification += `<li>${change.key} werd veranderd van ${change.old} naar ${change.new}</li>`;
                        }

                        notification += '</ul></div>';

                        await notificationsService.add({
                            user: {
                                id: userId,
                                name: user.lastName + ' ' + user.firstName,
                            },
                            label: 'Wijziging persoonsgegevens',
                            notification,
                            type: 8,
                        });
                    }

                    try {
                        await usersService.update(
                            { id },
                            {
                                firstName,
                                lastName,
                                date_of_birth,
                                sex,
                                nationality,
                                origin,
                            }
                        );

                        return { success: true };
                    } catch (e) {
                        throw new Error(e.message);
                    }
                },
            },
        },
        import: {
            resend: {
                POST: {
                    handler: async req => {
                        const { ids = [] } = req.body;
                        const users = await usersService.read();
                        const importedUsers = users.filter(({ id }) => ids.includes(id));
                        const notRegisteredUsers = [];

                        for (let user of importedUsers) {
                            const [registration] =
                                (await registrationsService.users.read({
                                    projectId: 1,
                                    userId: user.id,
                                })) || [];

                            if (!registration) {
                                if (!notRegisteredUsers.find(({ email }) => email === user.email)) {
                                    const [login] = (await loginsService.read({ email: user.email })) || [];
                                    if (login && login.pwd.length === 8) {
                                        notRegisteredUsers.push(login);
                                    }
                                }
                            }
                        }

                        const shouldSend = notRegisteredUsers.length;
                        let sent = 0;
                        let failed = 0;
                        for (let newUser of notRegisteredUsers) {
                            try {
                                logger.info(`Sending mail to ${newUser.email}`);
                                await templateService.get('imported').then(async html => {
                                    html = html.replace('{email}', newUser.email);
                                    html = html.replace('{pwd}', newUser.pwd);
                                    await new Promise(resolve => {
                                        mailService.send(
                                            {
                                                from: {
                                                    name: mail.name,
                                                    address: mail.noReply,
                                                },
                                                to: newUser.email,
                                                subject: 'Vervolledig uw inschrijving',
                                                // TODO: make templateService function to convert html to text-version
                                                text:
                                                    'Beste danser, Via Ann Cuyvers, coördinator dans van de Geelse academie, hebben jullie je ingeschreven om mee te dansen tijdens de voorstellingen van het totaalspektakel GheelaMania dat in mei 2020 in Geel gespeeld zal worden. Je ontvangt deze mail omdat we je hebben toegevoegd aan FanvanDimpna, het online systeem waarin we de gegevens van iedereen die meewerkt aan GheelaMania verzamelen. We gaan dit systeem in de toekomst gebruiken om de rolverdeling van GheelaMania mee te maken, om een persoonlijk repetitieschema voor jou te maken en om aan de hand van je kledijgegevens enkele prachtige kostuums voor jou te maken. Nu is het aan jou! Op basis van de lijst die Ann Cuyvers aan ons bezorgd heeft hebben we dus al een persoonlijke account voor jou aangemaakt. Je kan deze bekijken op www.fanvandimpna.be en in te loggen met je verkregen mailadres en wachtwoord.',
                                                html,
                                            },
                                            e => {
                                                if (e) {
                                                    failed++;
                                                    logger.error(`Could not send mail due to: ${e.message}`);
                                                } else {
                                                    sent++;
                                                    logger.info(`Mail sent`);
                                                }
                                                resolve();
                                            },
                                            'imported'
                                        );
                                    });
                                });
                            } catch (e) {
                                logger.error(`Could not send mail or load template due to: ${e.message}`);
                            }
                        }

                        return {
                            shouldSend,
                            sent,
                            failed,
                        };
                    },
                },
            },
            POST: {
                handler: async req => {
                    let { users } = req.body;
                    const loginEmails = ((await loginsService.read()) || []).map(({ email }) => email.toLowerCase());
                    let groupOwner;
                    const logins = users
                        .filter(({ mailadres }) => !loginEmails.includes(mailadres.toLowerCase()))
                        .reduce((acc, user) => {
                            if (!acc.find(({ mailadres }) => mailadres === user.mailadres)) {
                                if (user.groepsverantwoordelijke !== undefined) {
                                    acc.unshift(user);
                                } else {
                                    acc.push(user);
                                }
                            }
                            return acc;
                        }, []);

                    logger.info(`Importing ${logins.length} accounts`);

                    for (let user of logins) {
                        const password = stringUtil.generatePassword(8);
                        logger.info(`Creating login`, { email: user.mailadres, pwd: password, confirmed: 1 });
                        await loginsService.create({
                            email: user.mailadres,
                            pwd: CryptoJS.SHA512(password).toString(),
                            confirmed: 1,
                        });

                        try {
                            logger.info(`Sending mail to ${user.mailadres}`);
                            // const [login] = await loginsService.read({ email: to });
                            // if (!login || (login && login.subscribed)) {
                            templateService.get('imported').then(html => {
                                html = html.replace('{email}', user.mailadres);
                                html = html.replace('{pwd}', password);
                                mailService.send(
                                    {
                                        from: {
                                            name: mail.name,
                                            address: mail.noReply,
                                        },
                                        to: user.mailadres,
                                        subject: 'Vervolledig uw inschrijving',
                                        // TODO: make templateService function to convert html to text-version
                                        text:
                                            'Beste danser, Via Ann Cuyvers, coördinator dans van de Geelse academie, hebben jullie je ingeschreven om mee te dansen tijdens de voorstellingen van het totaalspektakel GheelaMania dat in mei 2020 in Geel gespeeld zal worden. Je ontvangt deze mail omdat we je hebben toegevoegd aan FanvanDimpna, het online systeem waarin we de gegevens van iedereen die meewerkt aan GheelaMania verzamelen. We gaan dit systeem in de toekomst gebruiken om de rolverdeling van GheelaMania mee te maken, om een persoonlijk repetitieschema voor jou te maken en om aan de hand van je kledijgegevens enkele prachtige kostuums voor jou te maken. Nu is het aan jou! Op basis van de lijst die Ann Cuyvers aan ons bezorgd heeft hebben we dus al een persoonlijke account voor jou aangemaakt. Je kan deze bekijken op www.fanvandimpna.be en in te loggen met je verkregen mailadres en wachtwoord.',
                                        html,
                                    },
                                    e => {
                                        if (e) {
                                            logger.error(`Could not send mail due to: ${e.message}`);
                                        } else {
                                            logger.info(`Mail sent`);
                                        }
                                    },
                                    'imported'
                                );
                            });
                        } catch (e) {
                            logger.error(`Could not send mail or load template due to: ${e.message}`);
                        }
                    }

                    const userNames = (
                        (await usersService.read({ email: users.map(user => user.mailadres) })) || []
                    ).map(
                        ({ firstName, lastName }) =>
                            firstName.trim().toLowerCase() + ' ' + lastName.trim().toLowerCase()
                    );
                    users = users.filter(
                        ({ voornaam, naam }) =>
                            !userNames.includes(voornaam.trim().toLowerCase() + ' ' + naam.trim().toLowerCase())
                    );

                    logger.info(`Creating ${users.length} profiles`);

                    let imported = [];
                    for (let user of users) {
                        const userId = await usersService.create({
                            firstName: user.voornaam,
                            lastName: user.naam,
                            email: user.mailadres,
                            date_of_birth: moment(user.geboortedatum).format('YYYY-MM-DD'),
                            nationality: user.nationaliteit,
                            origin: user.geboorteland || 'België',
                            sex: ['m', 'man'].includes(user.geslacht.toLowerCase()) ? 0 : 1,
                            streetName: user.straatnaam,
                            houseNumber: user.huisnummer + (user.busnummer ? ' ' + user.busnummer : ''),
                            cityName: user.gemeente,
                            zipCode: user.postcode,
                            phoneNumber: user.telefoonnummer,
                            cellPhone: user.gsm_nummer,
                            secondaryEmail: user.tweede_mailadres,
                            group_owner: groupOwner,
                            owner_remarks: user.vereniging,
                        });

                        if (!groupOwner && user.groepsverantwoordelijke) {
                            groupOwner = userId;
                        }

                        const [shirtSize] = (await sizesService.shirtSizes.read({ name: user.tshirt_maat })) || [];
                        const [clothingSize] = !user.kledingmaat
                            ? []
                            : (await sizesService.clothingSizes.read({ size: user.kledingmaat })) || [];

                        (shirtSize || clothingSize || user.lengte_in_cm) &&
                            (await usersService.sizes.create({
                                userId,
                                shirtSize: shirtSize?.id || null,
                                clothingSize: clothingSize?.id || null,
                                height: user.lengte_in_cm?.split(',')?.[0] || null,
                            }));

                        await usersService.insertAppSettings(userId, true, false, 0);

                        logger.info(`User ${user.voornaam} ${user.naam} created`);
                        imported.push(userId);
                    }

                    return { success: true, users: imported, projects: await projectsService.getCurrentOpenProjects() };
                },
            },
        },
    };
};
