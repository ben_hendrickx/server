module.exports = function({ registrationsService }) {
    return {
        user: {
            ':userId': {
                registrations: {
                    GET: {
                        // TODO: re-enable when rights are enabled on users with level=0
                        // requiredRights: {
                        //     projects: {
                        //         read: true,
                        //     },
                        //     registrations: {
                        //         read: true,
                        //     },
                        // },
                        // VERIFIED
                        handler: async req => {
                            const { userId } = req.params;

                            let registrations = await registrationsService.getRegistrationsForUser(userId, true);

                            return {
                                data: registrations,
                                total: registrations.length,
                            };
                        },
                    },
                },
            },
        },
    };
};
