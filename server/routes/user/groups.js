const moment = require('moment');
module.exports = function({ groupsService, projectsService, scenesService, castsService }) {
    return {
        user: {
            ':userId': {
                groups: {
                    GET: {
                        // TODO: add rights
                        handler: async req => {
                            const { userId } = req.params;
                            let { project, scene, open, past } = req.query;

                            const [projects, casts, allGroups, scenes, sceneGroups] = await Promise.all([
                                projectsService.read(),
                                castsService.read(),
                                groupsService.read(),
                                scenesService.read(),
                                scenesService.groups.read(),
                            ]);

                            const groupIds = allGroups.map(group => group.id);
                            const groupsForUser = await groupsService.users.read({
                                userId,
                                groupId: groupIds,
                            });

                            const groups = groupsForUser.reduce((acc, groupUser) => {
                                const group = allGroups.find(({ id }) => id === groupUser.groupId);

                                if (!project || group.projectId === project) {
                                    const project = projects
                                        .filter(({ closedFrom, closedOffStage, rolesClosed }) => {
                                            if (past) {
                                                return true;
                                            }

                                            const now = moment();

                                            if ((open && !rolesClosed) || moment(rolesClosed).isBefore(now)) {
                                                return false;
                                            }

                                            return (
                                                moment(closedFrom).isAfter(now) || moment(closedOffStage).isAfter(now)
                                            );
                                        })
                                        .find(({ id }) => id === group.projectId);

                                    if (!past && !project) {
                                        return acc;
                                    }

                                    const cast = casts.find(({ id }) => id === groupUser.castId);
                                    const sceneIds = sceneGroups
                                        .filter(({ groupId }) => groupId === group.id)
                                        .map(({ sceneId }) => sceneId);

                                    sceneIds.forEach(sceneId => {
                                        if (!scene || scene === sceneId) {
                                            acc.push({
                                                scene: scenes.find(({ id }) => id === sceneId),
                                                cast,
                                                project,
                                                ...group,
                                            });
                                        }
                                    });

                                    if (!scene && !sceneIds.length) {
                                        acc.push({
                                            project,
                                            cast,
                                            ...group,
                                        });
                                    }
                                }

                                return acc;
                            }, []);

                            return {
                                data: groups,
                                total: groups.length,
                            };
                        },
                    },
                },
            },
        },
    };
};
