module.exports = function({ sizesService }) {
    return {
        sizes: {
            shirts: {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        return sizesService.shirtSizes.read();
                    },
                },
            },
            clothing: {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        return sizesService.clothingSizes.read();
                    },
                },
            },
        },
    };
};
