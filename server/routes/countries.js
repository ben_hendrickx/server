module.exports = function({ countriesService }) {
    return {
        countries: {
            GET: {
                // VERIFIED
                handler: async () => {
                    return countriesService.read();
                },
            },
        },
    };
};
