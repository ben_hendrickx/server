module.exports = function castRoutes({ castsService, sortingService }) {
    return {
        casts: {
            GET: {
                // VERIFIED
                handler: async () => {
                    const casts = await castsService.read();

                    return {
                        data: sortingService.sortBy(casts, 'name'),
                        total: casts.length,
                    };
                },
            },
            POST: {
                // VERIFIED
                handler: async req => {
                    const { name } = req.body;
                    await castsService.create({ name });
                    return { success: true };
                },
            },
        },
        cast: {
            ':id': {
                GET: {
                    handler: async req => {
                        const { id } = req.params;
                        const [cast] = await castsService.read({ id });
                        return cast;
                    },
                },
                PUT: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;
                        const { name } = req.body;
                        await castsService.update({ id }, { name });
                        return { success: true };
                    },
                },
                DELETE: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;
                        await castsService.remove({ id });
                        return { success: true };
                    },
                },
            },
        },
    };
};
