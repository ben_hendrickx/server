/* istanbul ignore file */
const moment = require('moment');

module.exports = function clothingRoutes({ dataService, clothingService, crudService, articlesService }) {
    const articlesCrud = crudService('articles');
    const clothingSizesCrud = crudService('article_clothing_sizes');
    const fabricsCrud = crudService('fabrics');
    const patternsCrud = crudService('patterns');
    const colorsCrud = crudService('colors');
    const racksCrud = crudService('racks');
    const ordersCrud = crudService('rentals');
    const orderItemsCrud = crudService('rental_items');

    return function({ io }) {
        io.on('connection', function(socket) {
            socket.on('clothing_receive_item', async ({ orderId, id, amount, receive }) => {
                const [item] = await orderItemsCrud.read({ rentalId: orderId, artId: id });
                const returned = receive ? item.returned + amount : amount;
                const washing = returned === item.amount;
                const rented = !washing;
                await articlesCrud.update({ id }, { washing, rented });
                await orderItemsCrud.update({ rentalId: orderId, artId: id }, { returned });
                io.emit('clothing_received_item', { orderId, id, amount: returned, washing, rented });
            });

            socket.on('clothing_receive_items', async ({ orderId, ids }) => {
                const items = await orderItemsCrud.read({ rentalId: orderId, artId: ids });
                await Promise.all(
                    items.map(async item => {
                        await articlesCrud.update({ id: item.artId }, { washing: true, rented: 0 });
                        await orderItemsCrud.update(
                            { rentalId: orderId, artId: item.artId },
                            { returned: item.amount }
                        );
                        io.emit('clothing_received_item', {
                            orderId,
                            id: item.artId,
                            amount: item.amount,
                            washing: true,
                            rented: false,
                        });
                    })
                );
            });
        });
        return {
            orders: {
                price: {
                    POST: {
                        // VERIFIED
                        handler: async req => {
                            const { startDt, internal = false, periods, items } = req.body;
                            return await clothingService.calculatePrices(items, startDt, parseInt(periods), internal);
                        },
                    },
                },
                GET: {
                    // VERIFIED
                    handler: async req => {
                        let { return: returnBefore, billed, returned } = req.query;
                        let orders = await ordersCrud.read();

                        orders = await Promise.all(orders.map(order => clothingService.getOrder(order.id)));

                        if (billed !== undefined) {
                            orders = orders.filter(order => !!order.billed === !!billed);
                        }

                        if (returnBefore !== undefined) {
                            orders = orders.filter(order => {
                                const now = Date.now();
                                const check = now + returnBefore * 24 * 60 * 60 * 1000;
                                const maxReturnDate = new Date(order.toDt).getTime() + 14 * 24 * 60 * 60 * 1000;

                                return maxReturnDate > now && maxReturnDate < check;
                            });
                        }

                        if (returned !== undefined) {
                            orders = await Promise.all(
                                orders.map(async order => ({
                                    ...order,
                                    items: await orderItemsCrud.read({ rentalId: order.id }),
                                }))
                            );

                            if (returned) {
                                orders = orders.filter(order => order.items.every(item => !!item.returned));
                            } else {
                                orders = orders.filter(order => order.items.some(item => !item.returned));
                            }
                        }

                        return {
                            data: orders,
                            total: await ordersCrud.count(),
                        };
                    },
                },
                POST: {
                    // VERIFIED
                    handler: async req => {
                        const rentalId = await clothingService.rentClothes(req.body);
                        return {
                            success: true,
                            rentalId,
                        };
                    },
                },
            },
            order: {
                ':id': {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            return clothingService.getOrder(id);
                        },
                    },
                    PUT: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;

                            if (req.body.billed) {
                                await clothingService.orders.update(id, req.body);
                                return { success: true };
                            }

                            await clothingService.updateRental(id, req.body);
                            return { success: true };
                        },
                    },
                },
            },
            categories: {
                GET: {
                    // VERIFIED
                    handler: async () => {
                        const data = await clothingService.getCategories();
                        const total = data.length;

                        return {
                            data: data.sort((a, b) => {
                                return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
                            }),
                            total,
                        };
                    },
                },
            },
            category: {
                ':id': {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            const data = await clothingService.getCategories();

                            return data.find(category => category.id === id);
                        },
                    },
                    prices: {
                        POST: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;

                                const categories = await clothingService.categories.prices.read({
                                    categoryId: id,
                                });
                                if (categories.length) {
                                    const category = categories.pop();
                                    await clothingService.categories.prices.update(
                                        {
                                            categoryId: category.categoryId,
                                            startDt: category.startDt,
                                        },
                                        {
                                            endDt: moment(req.body.startDt).format(),
                                        }
                                    );
                                }
                                await clothingService.categories.prices.create({
                                    categoryId: id,
                                    ...req.body,
                                });

                                return { success: true };
                            },
                        },
                    },
                    PUT: {
                        handler: async req => {
                            const { id } = req.params;
                            await clothingService.categories.update({ id }, req.body);
                            return { success: true };
                        },
                    },
                },
            },

            articles: {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        let {
                            offset = 0,
                            items = 10,
                            text,
                            categoryId,
                            clothingSizeId,
                            colorId,
                            fabricId,
                            patternId,
                            rackId,
                            tags,
                        } = req.query;

                        let active, washing, rented;

                        offset = parseInt(offset);
                        items = parseInt(items);

                        text = (text || '').toString().toLowerCase();

                        if (text) {
                            if (text.includes('niet actief')) {
                                active = false;
                            } else if (text.includes('actief')) {
                                active = true;
                            }

                            if (text.includes('in de was')) {
                                washing = true;
                            }

                            if (text.includes('uitgeleend')) {
                                rented = 1;
                            }

                            text = text
                                .toLowerCase()
                                .replace('niet actief', '')
                                .replace('actief', '')
                                .replace('in de was', '')
                                .replace('uitgeleend', '');
                        }

                        let articles = await articlesService.read(
                            [
                                text !== undefined && {
                                    key: [
                                        'LPAD(a.id, 6, "0")',
                                        'cat.name',
                                        'tags',
                                        'f.type',
                                        'p.name',
                                        'c.name',
                                        'r.description',
                                        'a.description',
                                    ],
                                    operator: 'LIKE',
                                    value: `%${text}%`,
                                },
                                rented !== undefined && {
                                    key: 'rented',
                                    value: rented,
                                },
                                active !== undefined && {
                                    key: 'active',
                                    value: active ? 1 : 0,
                                },
                                washing !== undefined && {
                                    key: 'washing',
                                    value: washing ? 1 : 0,
                                },
                                categoryId !== undefined && {
                                    key: 'categoryId',
                                    value: categoryId,
                                },
                                clothingSizeId !== undefined && {
                                    key: 'clothingSizeId',
                                    value: clothingSizeId,
                                },
                                colorId !== undefined && {
                                    key: 'colorId',
                                    value: colorId,
                                },
                                fabricId !== undefined && {
                                    key: 'fabricId',
                                    value: fabricId,
                                },
                                patternId !== undefined && {
                                    key: 'patternId',
                                    value: patternId,
                                },
                                rackId !== undefined && {
                                    key: 'rackId',
                                    value: rackId,
                                },
                                tags === 'accessoire' && {
                                    key: 'tags',
                                    operator: 'LIKE "%accessoire%"',
                                    value: '',
                                },
                                tags?.toString() === 'false' && {
                                    key: 'tags',
                                    operator: 'NOT LIKE "%accessoire%"',
                                    value: '',
                                },
                            ],
                            { offset, items }
                        );
                        articles.data = articles.data.map(a => {
                            const {
                                id,
                                active,
                                washing,
                                stock,
                                rackId,
                                clothingSizeId,
                                patternId,
                                fabricId,
                                colorId,
                                tags,
                                description,
                                rackDescription,
                                type,
                                categoryId,
                                categoryName,
                                patternName,
                                colorName,
                                size,
                                rented,
                            } = a;
                            return {
                                id,
                                active,
                                washing,
                                stock,
                                rackId,
                                clothingSizeId,
                                categoryId,
                                patternId,
                                fabricId,
                                colorId,
                                tags,
                                description,
                                rack: {
                                    id: rackId,
                                    description: rackDescription,
                                },
                                fabric: {
                                    id: fabricId,
                                    type,
                                },
                                size: {
                                    id: clothingSizeId,
                                    size,
                                },
                                pattern: {
                                    id: patternId,
                                    name: patternName,
                                },
                                color: {
                                    id: colorId,
                                    name: colorName,
                                },
                                category: {
                                    id: categoryId,
                                    name: categoryName,
                                },
                                rented,
                            };
                        });
                        return {
                            data: articles.data,
                            items: articles.count,
                            total: await articlesCrud.count(),
                            offset,
                        };
                    },
                },
                POST: {
                    // VERIFIED
                    handler: async req => {
                        return clothingService.createArticle(req.body);
                    },
                },
            },

            article: {
                ':id': {
                    GET: {
                        handler: async req => {
                            const { id } = req.params;
                            return clothingService.getArticle(id);
                        },
                    },
                    PUT: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            return clothingService.updateArticle(id, req.body);
                        },
                    },
                    DELETE: {
                        handler: async req => {
                            const { id } = req.params;
                            return clothingService.updateArticle(id, { active: false });
                        },
                    },
                },
            },

            clothingsizes: {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { archived } = req.query;
                        let clothingSizes = await clothingSizesCrud.read();

                        clothingSizes.sort((a, b) => {
                            const aSize = a.size.toLowerCase();
                            const bSize = b.size.toLowerCase();
                            return aSize > bSize ? 1 : aSize < bSize ? -1 : 0;
                        });

                        clothingSizes = clothingSizes.filter(i => {
                            return archived ? i.deletedDt : !i.deletedDt;
                        });

                        return {
                            data: clothingSizes,
                            total: await clothingSizesCrud.count({ deletedDt: null }),
                        };
                    },
                },
            },
            clothingsize: {
                ':id': {
                    DELETE: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            await clothingSizesCrud.update({ id }, { deletedDt: moment().format() });
                            return { success: true };
                        },
                    },
                },
            },

            patterns: {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { archived } = req.query;
                        let patterns = await patternsCrud.read();

                        patterns.sort((a, b) => {
                            const aName = a.name.toLowerCase();
                            const bName = b.name.toLowerCase();
                            return aName > bName ? 1 : aName < bName ? -1 : 0;
                        });

                        patterns = patterns.filter(i => {
                            return archived ? i.deletedDt : !i.deletedDt;
                        });

                        return {
                            data: patterns,
                            total: await patternsCrud.count({ deletedDt: null }),
                        };
                    },
                },
            },
            pattern: {
                ':id': {
                    DELETE: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            await patternsCrud.update({ id }, { deletedDt: moment().format() });
                            return { success: true };
                        },
                    },
                },
            },

            fabrics: {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { archived } = req.query;
                        let fabrics = await fabricsCrud.read();

                        fabrics.sort((a, b) => {
                            const aType = a.type.toLowerCase();
                            const bType = b.type.toLowerCase();
                            return aType > bType ? 1 : aType < bType ? -1 : 0;
                        });

                        fabrics = fabrics.filter(i => {
                            return archived ? i.deletedDt : !i.deletedDt;
                        });

                        return {
                            data: fabrics,
                            total: await fabricsCrud.count({ deletedDt: null }),
                        };
                    },
                },
            },
            fabric: {
                ':id': {
                    DELETE: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            await fabricsCrud.update({ id }, { deletedDt: moment().format() });
                            return { success: true };
                        },
                    },
                },
            },

            colors: {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { archived } = req.query;
                        let colors = await colorsCrud.read();

                        colors.sort((a, b) => {
                            const aName = a.name.toLowerCase();
                            const bName = b.name.toLowerCase();
                            return aName > bName ? 1 : aName < bName ? -1 : 0;
                        });

                        colors = colors.filter(i => {
                            return archived ? i.deletedDt : !i.deletedDt;
                        });

                        return {
                            data: colors,
                            total: await colorsCrud.count({ deletedDt: null }),
                        };
                    },
                },
            },
            color: {
                ':id': {
                    DELETE: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            await colorsCrud.update({ id }, { deletedDt: moment().format() });
                            return { success: true };
                        },
                    },
                },
            },

            racks: {
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { archived } = req.query;
                        let racks = await racksCrud.read();

                        racks.sort((a, b) => {
                            const aId = a.id;
                            const bId = b.id;
                            return aId > bId ? 1 : aId < bId ? -1 : 0;
                        });

                        racks = racks.filter(i => {
                            return archived ? i.deletedDt : !i.deletedDt;
                        });

                        return {
                            data: racks,
                            total: await racksCrud.count({ deletedDt: null }),
                        };
                    },
                },
                ':id': {
                    articles: {
                        GET: {
                            handler: async req => {
                                const { id } = req.params;
                                let {
                                    offset = 0,
                                    items = 10,
                                    text,
                                    categoryId,
                                    clothingSizeId,
                                    colorId,
                                    fabricId,
                                    patternId,
                                } = req.query;

                                let active, washing;

                                if (text) {
                                    if (text.includes('niet actief')) {
                                        active = false;
                                    } else if (text.includes('actief')) {
                                        active = true;
                                    } else if (text.includes('in de was')) {
                                        washing = true;
                                    } else if (text.includes('uitgeleend')) {
                                        active = true;
                                        washing = false;
                                    }

                                    text = text
                                        .toLowerCase()
                                        .replace('niet actief', '')
                                        .replace('actief', '')
                                        .replace('in de was', '')
                                        .replace('uitgeleend', '');
                                }

                                let articles = await articlesService.read(
                                    [
                                        text !== undefined && {
                                            key: [
                                                'cat.name',
                                                'tags',
                                                'acs.size',
                                                'f.type',
                                                'p.name',
                                                'c.name',
                                                'r.description',
                                            ],
                                            operator: 'LIKE',
                                            value: `%${text}%`,
                                        },
                                        active !== undefined && {
                                            key: 'active',
                                            value: active ? 1 : 0,
                                        },
                                        washing !== undefined && {
                                            key: 'washing',
                                            value: washing ? 1 : 0,
                                        },
                                        categoryId !== undefined && {
                                            key: 'categoryId',
                                            value: categoryId,
                                        },
                                        clothingSizeId !== undefined && {
                                            key: 'clothingSizeId',
                                            value: clothingSizeId,
                                        },
                                        colorId !== undefined && {
                                            key: 'colorId',
                                            value: colorId,
                                        },
                                        fabricId !== undefined && {
                                            key: 'fabricId',
                                            value: fabricId,
                                        },
                                        patternId !== undefined && {
                                            key: 'patternId',
                                            value: patternId,
                                        },
                                        {
                                            key: 'rackId',
                                            value: id,
                                        },
                                    ],
                                    { offset, items }
                                );
                                articles.data = articles.data.map(a => {
                                    const {
                                        id,
                                        active,
                                        washing,
                                        stock,
                                        rackId,
                                        clothingSizeId,
                                        patternId,
                                        fabricId,
                                        colorId,
                                        tags,
                                        rackDescription,
                                        description,
                                        type,
                                        categoryId,
                                        categoryName,
                                        patternName,
                                        colorName,
                                        size,
                                    } = a;
                                    return {
                                        id,
                                        active,
                                        washing,
                                        stock,
                                        rackId,
                                        clothingSizeId,
                                        patternId,
                                        fabricId,
                                        colorId,
                                        tags,
                                        description,
                                        rack: {
                                            id: rackId,
                                            description: rackDescription,
                                        },
                                        fabric: {
                                            id: fabricId,
                                            type,
                                        },
                                        category: {
                                            id: categoryId,
                                            name: categoryName,
                                        },
                                        size: {
                                            id: clothingSizeId,
                                            size,
                                        },
                                        pattern: {
                                            id: patternId,
                                            name: patternName,
                                        },
                                        color: {
                                            id: colorId,
                                            name: colorName,
                                        },
                                    };
                                });
                                return {
                                    data: articles.data,
                                    items: articles.count,
                                    total: await articlesCrud.count(),
                                    offset,
                                };
                            },
                        },
                    },
                },
            },
            rack: {
                ':id': {
                    DELETE: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            await racksCrud.update({ id }, { deletedDt: moment().format() });
                            return { success: true };
                        },
                    },
                },
            },

            clothing: {
                tags: {
                    GET: {
                        // VERIFIED
                        handler: async () => {
                            const result = (await dataService.raw('SELECT tags FROM articles')) || [];
                            return result.reduce((acc, row) => {
                                for (let tag of (row.tags || '').split('|')) {
                                    if (tag) {
                                        !acc.includes(tag.toLowerCase()) && acc.push(tag.toLowerCase());
                                    }
                                }
                                return acc;
                            }, []);
                        },
                    },
                },
                clothingSizes: {
                    POST: {
                        // VERIFIED
                        handler: async req => {
                            await clothingService.createClothingSize(req.body);
                            return { success: true };
                        },
                    },
                    GET: {
                        handler: async () => {
                            return await clothingService.getClothingSizes();
                        },
                    },
                    ':id': {
                        GET: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;
                                const [size] = await clothingSizesCrud.read({ id });
                                return size;
                            },
                        },
                        PUT: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;
                                let { size } = req.body;
                                size = parseInt(size);

                                await clothingSizesCrud.update({ id }, { size });

                                return { success: true };
                            },
                        },
                    },
                },
                articleClothingSizes: {
                    GET: {
                        handler: async () => {
                            return await clothingService.getArticleClothingSizes();
                        },
                    },
                },
                fabrics: {
                    ':id': {
                        GET: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;
                                const [fabric] = await clothingService.getFabrics({ id });
                                return fabric;
                            },
                        },
                    },
                    GET: {
                        handler: async () => {
                            return await clothingService.getFabrics();
                        },
                    },
                },
                patterns: {
                    GET: {
                        handler: async () => {
                            return await clothingService.getPatterns();
                        },
                    },
                },
                colors: {
                    GET: {
                        handler: async () => {
                            return await clothingService.getColors();
                        },
                    },
                },
                import: {
                    POST: {
                        // VERIFIED
                        handler: async req => {
                            req.setTimeout(10 * 60 * 1000);
                            const { items, uuid } = req.body;

                            let lastTimeSent;

                            function callback(progress, text) {
                                if (progress > 0.99 || !lastTimeSent || Date.now() - 1000 > lastTimeSent) {
                                    lastTimeSent = Date.now();
                                    io.emit('clothing_item_progress', { uuid, progress, text });
                                }
                            }

                            return await clothingService.importItems(items, callback);
                        },
                    },
                },
                racks: {
                    GET: {
                        handler: async () => {
                            return clothingService.getRacks();
                        },
                    },
                },
                rack: {
                    POST: {
                        // VERIFIED
                        handler: async req => {
                            return clothingService.createRack(req.body);
                        },
                    },
                    ':id': {
                        GET: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;
                                return clothingService.getRack(id);
                            },
                        },
                        PUT: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;
                                return clothingService.updateRack(id, req.body);
                            },
                        },
                        articles: {
                            GET: {
                                handler: async req => {
                                    const { id } = req.params;
                                    return clothingService.getArticlesForRack(id);
                                },
                            },
                        },
                    },
                },
                fabric: {
                    POST: {
                        // VERIFIED
                        handler: async req => {
                            return clothingService.createFabric(req.body);
                        },
                    },
                    ':id': {
                        PUT: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;

                                await clothingService.updateFabric(id, req.body);
                                return { success: true };
                            },
                        },
                    },
                },
                pattern: {
                    ':id': {
                        GET: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;

                                const [pattern] = await clothingService.getPatterns({ id });
                                return pattern;
                            },
                        },
                        PUT: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;

                                await clothingService.updatePattern(id, req.body);
                                return { success: true };
                            },
                        },
                    },
                    POST: {
                        // VERIFIED
                        handler: async req => {
                            return clothingService.createPattern(req.body);
                        },
                    },
                },
                color: {
                    ':id': {
                        GET: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;

                                const [color] = await clothingService.getColors({ id });
                                return color;
                            },
                        },
                        PUT: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;

                                await clothingService.updateColor(id, req.body);
                                return { success: true };
                            },
                        },
                    },
                    POST: {
                        // VERIFIED
                        handler: async req => {
                            return clothingService.createColor(req.body);
                        },
                    },
                },
                category: {
                    POST: {
                        // VERIFIED
                        handler: async req => {
                            return clothingService.createCategory(req.body);
                        },
                    },
                },
            },
        };
    };
};
