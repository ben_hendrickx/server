const CryptoJS = require('crypto-js');
const moment = require('moment');
const {
    mail,
    server: { hostnameShort },
} = require('../../config');

module.exports = function authenticationRoutes({
    authenticationService,
    loginsService,
    templateService,
    mailService,
    usersService,
    requestUtil,
    logger,
    stringUtil,
    countriesService,
    sizesService,
    repetitionsService,
    notificationsService,
    rightsService,
}) {
    return function({ io, hostname, connectedClients, presenceSubscriptions }) {
        io.on('connection', function(socket) {
            logger.info('Client has connected to socket');

            socket.on('connected', email => {
                connectedClients[socket.id] = email;
            });

            socket.on('repetition_presence', async ({ repetitionId }) => {
                presenceSubscriptions[repetitionId]
                    ? presenceSubscriptions[repetitionId].push(socket)
                    : (presenceSubscriptions[repetitionId] = [socket]);
                socket.emit('repetition_presence', await repetitionsService.getUserPresence(parseInt(repetitionId)));
            });

            socket.on('notifications', async userId => {
                socket.emit('notifications', await notificationsService.getNotificationsForUser(userId));
            });

            socket.on('notifications_unread', async ({ userId, projectId }) => {
                socket.emit(
                    'notifications_unread',
                    await notificationsService.getUnreadNotificationsForUser(userId, projectId)
                );
                notificationsService.registerSocket(socket);
            });

            socket.on('notification_read', async ({ userId, notificationId }) => {
                await notificationsService.markReadForUser(userId, notificationId);
                socket.emit('notification_read', notificationId);
            });

            socket.on('notification_unread', async ({ userId, notificationId }) => {
                await notificationsService.markUnreadForUser(userId, notificationId);
                socket.emit('notifications_unread', await notificationsService.getUnreadNotificationsForUser(userId));
            });

            socket.on('notification_removed', async ({ userId, notificationId }) => {
                await notificationsService.markRemovedForUser(userId, notificationId);
                socket.emit('notification_removed', notificationId);
            });

            socket.on('error', e => logger.fatal(e));
            socket.on('disconnect', () => {
                delete connectedClients[socket.id];
                notificationsService.unregisterSocket(socket);
            });

            socket.on('VERIFY_TOKEN', async function(token) {
                logger.trace('Received VERIFY_TOKEN on socket');
                const data = await authenticationService.verifyToken(token);

                if (!data) {
                    socket.emit('VERIFY_TOKEN', { status: 404 });
                    return;
                }

                const [user] = await usersService.read({ id: data.id });
                data.canEditClothingSizes = !user?.clothingFrozen || moment(user?.clothingFrozen).isBefore(moment());

                socket.emit('VERIFY_TOKEN', data);
                const image = await usersService.getUserImage(data.firstName, data.lastName, data.email);
                data && image && socket.emit('USER_IMAGE', image);
            });
        });

        return {
            auth: {
                POST: {
                    // VERIFIED
                    handler: async req => {
                        const { email, password } = req.body;

                        const data = await authenticationService.authenticate(email, password);
                        data.image = await usersService.getUserImage(data.firstName, data.lastName, data.email);

                        return data;
                    },
                },
                switchProfile: {
                    ':id': {
                        GET: {
                            handler: async req => {
                                const token = requestUtil.getToken(req);
                                const { id } = req.params;
                                const result = await authenticationService.switchProfile(token, id);

                                if (result.success) {
                                    result.image = await usersService.getUserImage(
                                        result.firstName,
                                        result.lastName,
                                        result.email
                                    );
                                }

                                const [user] = await usersService.read({ id });
                                result.canEditClothingSizes =
                                    !user.clothingFrozen || moment(user.clothingFrozen).isBefore(moment());

                                return result;
                            },
                        },
                    },
                },
            },
            verify: {
                ':userId': {
                    GET: {
                        handler: async req => {
                            const { userId } = req.params;

                            await usersService.update(
                                { id: userId },
                                { confirmedDt: require('moment')().format('YYYY-MM-DD HH:mm:ss') }
                            );

                            return {
                                success: true,
                            };
                        },
                    },
                },
            },
            // VERIFIED
            logout: {
                POST: {
                    handler: async req => {
                        const token = requestUtil.getToken(req);
                        return await authenticationService.logout(token);
                    },
                },
            },
            //TODO: cleanup
            'forgot-password': {
                POST: {
                    // VERIFIED
                    handler: async req => {
                        const { email } = req.body;
                        const token = require('uuid/v4')();

                        const [login] = await loginsService.read({ email });
                        if (!login) {
                            return {
                                status: 404,
                                data: 'Er bestaat geen gebruiker met dit mailadres',
                            };
                        }

                        await loginsService.changePasswordRequests.remove({ email });
                        await loginsService.changePasswordRequests.create({
                            email,
                            token,
                        });

                        await templateService.get('password').then(html => {
                            html = html.replace(':email', email);
                            html = html.replace(':token', token);
                            mailService.send(
                                {
                                    from: {
                                        name: mail.name,
                                        address: mail.noReply,
                                    },
                                    to: [email],
                                    subject: 'Wachtwoord vergeten',
                                    // TODO: make templateService function to convert html to text-version
                                    text:
                                        'Dankjewel voor je registratie bij Fan van Dimpna. Elk jaar zetten we met Fan van Dimpna de stad Geel in de kijker met onze Kerstmusical. Dit jaar zijn we natuurlijk weer van de partij. Om al dit in goede banen te leiden gaan we jouw hulp goed kunnen gebruiken!',
                                    html,
                                },
                                e => {
                                    if (e) {
                                        return {
                                            status: 500,
                                            data: e.toString(),
                                        };
                                    }

                                    return { status: 200 };
                                },
                                'password'
                            );
                        });

                        return { success: true };
                    },
                },
            },
            //TODO: cleanup
            'change-password': {
                POST: {
                    // VERIFIED
                    handler: async req => {
                        const { email, token, password } = req.body;

                        const { level, email: userEmail } = await authenticationService.verifyToken(token);

                        if (level !== undefined && userEmail !== undefined) {
                            const [login] = await loginsService.read({ email: userEmail });
                            if (!login) {
                                return {
                                    status: 404,
                                    data: 'Er bestaat geen gebruiker met dit mailadres',
                                };
                            }

                            await loginsService.update(
                                { email: userEmail },
                                { pwd: CryptoJS.SHA512(password).toString() }
                            );
                            return { success: true };
                        }

                        const [login] = await loginsService.read({ email });
                        if (!login) {
                            return {
                                status: 404,
                                data: 'Er bestaat geen gebruiker met dit mailadres',
                            };
                        }

                        const changeRequests = await loginsService.changePasswordRequests.read({ email, token });
                        if (!changeRequests || !changeRequests.length) {
                            return {
                                status: 500,
                                data: 'De link uit de mail is verlopen, gelieve opnieuw te proberen',
                            };
                        }

                        if (new Date(changeRequests[0].creationDt).getTime() < Date.now() - 24 * 60 * 60 * 1000) {
                            return {
                                status: 500,
                                data: 'De link uit de mail is verlopen, gelieve opnieuw te proberen',
                            };
                        }

                        await loginsService.update({ email }, { pwd: CryptoJS.SHA512(password).toString() });
                        await loginsService.changePasswordRequests.remove({ email, token });
                        return { success: true };
                    },
                },
            },
            unsubscribe: {
                GET: {
                    handler: async req => {
                        const { email } = req.query;
                        if (!email) {
                            return {
                                status: 400,
                            };
                        }

                        await loginsService.update({ email }, { subscribed: false });
                        return {
                            status: 301,
                            redirect: `${hostname}/?unsubscribed=true`,
                        };
                    },
                },
            },
            //TODO: cleanup
            confirm: {
                ':email': {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { email } = req.params;
                            const logins = await loginsService.read({ email });
                            const [{ confirmed } = {}] = logins || [];
                            if (!confirmed) {
                                await loginsService.update({ email }, { confirmed: true });
                                await templateService.get('signup').then(html => {
                                    mailService.send(
                                        {
                                            from: {
                                                name: mail.name,
                                                address: mail.noReply,
                                            },
                                            to: [email],
                                            subject: `Registratie ${hostnameShort}`,
                                            // TODO: make templateService function to convert html to text-version
                                            text:
                                                'Dankjewel voor je registratie bij Fan van Dimpna. Elk jaar zetten we met Fan van Dimpna de stad Geel in de kijker met onze Kerstmusical. Dit jaar zijn we natuurlijk weer van de partij. Om al dit in goede banen te leiden gaan we jouw hulp goed kunnen gebruiken!',
                                            html,
                                        },
                                        () => {},
                                        'signup'
                                    );
                                });
                            }
                            return {
                                status: 301,
                                redirect: `${hostname}/?emailConfirmed=true`,
                            };
                        },
                    },
                },
            },
            //TODO: cleanup
            signup: {
                POST: {
                    // VERIFIED
                    handler: async req => {
                        const { body } = req;
                        const { firstName, lastName } = body;

                        const keysToCheckAgainstSymbols = [
                            'firstName',
                            'lastName',
                            'email',
                            'origin',
                            'nationality',
                            'phone',
                            'cell',
                            'dateOfBirth',
                            'secondaryMail',
                        ];
                        const translations = [
                            'Voornaam',
                            'Achternaam',
                            'E-mail',
                            'Geboorteland',
                            'Nationaliteit',
                            'Telefoonnummer',
                            'G.S.M.-nummer',
                            'Geboortedatum',
                            'Tweede e-mailadres',
                        ];

                        const isNotValid = keysToCheckAgainstSymbols.find(key => {
                            if (body[key]) {
                                // eslint-disable-next-line no-useless-escape
                                return /[`~\^\*\[\{\}\]\|;,<>€£¥₹]+/.test(body[key]);
                            }
                            return false;
                        });

                        if (isNotValid) {
                            return {
                                status: 400,
                                data: `${
                                    translations[keysToCheckAgainstSymbols.indexOf(isNotValid)]
                                } bevat ongeldige tekens`,
                            };
                        }

                        if (!firstName || firstName.length < 2) {
                            return {
                                status: 400,
                                data: 'Voornaam moet minstens 2 tekens lang zijn',
                            };
                        }

                        logger.info('Going to try to create new login and user: ', body);

                        if (body.image) {
                            const type = (body.image || '').includes('image/png') ? 'png' : 'jpg';
                            const base64 = body.image.replace(/^data:image\/(png|jpeg);base64,/, '');

                            require('fs').writeFile(
                                `images/${body.firstName}_${body.lastName}_${stringUtil.removeChars(
                                    body.email,
                                    `.!#%&'-/=_\`{}~",:;<>@`.split('')
                                )}.${type}`,
                                base64,
                                'base64',
                                err => err && logger.error(err)
                            );
                        }

                        try {
                            let { tShirt: shirtSize, email, privacy, pictures, pwd } = body;

                            delete body.tShirt;
                            delete body.image;
                            delete body.pwd;
                            delete body.privacy;
                            delete body.pictures;

                            body.origin = ((await countriesService.read()) || []).find(
                                ({ id }) => id === body.origin
                            )?.name;

                            const CryptoJS = require('crypto-js');

                            await loginsService.create({
                                email,
                                pwd: CryptoJS.SHA512(pwd).toString(),
                                confirmed: process.env.NODE_ENV === 'development' ? true : false,
                            });
                            const userId = await usersService.create(body);
                            shirtSize = ((await sizesService.shirtSizes.read()) || []).find(
                                ({ name }) => name === shirtSize.toString()
                            )?.id;
                            await usersService.sizes.create({ userId, shirtSize });
                            await usersService.insertAppSettings(userId, privacy, pictures);
                            await rightsService.user.rights.create({
                                userId,
                                rightId: 2,
                                read: true,
                                create: true,
                                update: true,
                                delete: true,
                            });
                            await rightsService.user.rights.create({
                                userId,
                                rightId: 26,
                                read: true,
                                create: false,
                                update: false,
                                delete: false,
                            });
                            await rightsService.user.subRights.create({
                                userId,
                                subRightId: 5,
                                access: true,
                            });
                            await rightsService.user.subRights.create({
                                userId,
                                subRightId: 6,
                                access: true,
                            });
                            await rightsService.user.subRights.create({
                                userId,
                                subRightId: 7,
                                access: true,
                            });
                            let notification = `<div>${firstName} ${lastName} heeft zich net geregistreerd</div>`;
                            await notificationsService.add({
                                user: {
                                    id: userId,
                                    userId,
                                    name: lastName + ' ' + firstName,
                                },
                                label: 'Nieuwe gebruiker',
                                notification,
                                type: 1,
                            });

                            logger.debug(`Added user with id ${userId}`);

                            try {
                                await templateService.get('confirm').then(html => {
                                    html = html.replace(':email', email);
                                    mailService.send(
                                        {
                                            from: {
                                                name: mail.name,
                                                address: mail.noReply,
                                            },
                                            to: [email],
                                            subject: 'Gelieve uw e-mailadres te bevestigen',
                                            // TODO: make templateService function to convert html to text-version
                                            text: `Om je digitale veiligheid te garanderen kan er in onze applicatie niet worden aangemeld zonder eerst te bevestigen dat jij de rechtmatige eigenaar bent van dit e-mailadres.
Gelieve op onderstaande link te klikken om je registratie bij ${hostnameShort} te bevestigen.

Heb jij je niet geregistreerd bij ${hostnameShort} dan mag je deze mail verwijderen, klik dan ook zeker niet op onderstaande link.

{hostname}/api/confirm/{email}

Kan je niet op deze link klikken? Kopi&euml;er hem dan en open de link door hem te plakken in de adresbalk in je browser.`,
                                            html,
                                        },
                                        e => {
                                            if (e) {
                                                logger.error(`Could not send mail due to: ${e.message}`);
                                                return {
                                                    status: 500,
                                                    data: e.toString(),
                                                };
                                            }

                                            return {
                                                status: 200,
                                            };
                                        },
                                        'confirm'
                                    );
                                });
                            } catch (e) {
                                logger.error(`Could not send mail or load template due to: ${e.message}`);
                                return {
                                    status: 500,
                                    data: e.toString(),
                                };
                            }
                            return {
                                status: 200,
                            };
                        } catch (e) {
                            logger.error(`Could not create user due to: ${e.message}`);
                            if (e.code === 'ER_DUP_ENTRY') {
                                return {
                                    status: 500,
                                    data: 'Er bestaat reeds een gebruiker met dit mailadres',
                                };
                            }
                            return {
                                status: 500,
                                data: e.toString(),
                            };
                        }
                    },
                },
            },
        };
    };
};
