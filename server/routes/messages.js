const moment = require('moment');

module.exports = function({
    authenticationService,
    crudService,
    requestUtil,
    dataService,
    queryService,
    messagesQuery,
    usersService,
    projectsService,
    logger,
}) {
    const messagesCrud = crudService('messages');
    const messageReadCrud = crudService('message_read');
    const messageGroupsCrud = crudService('message_groups');
    const messageFilesCrud = crudService('message_files');
    const userProjectsCrud = crudService('project_user_registrations');

    return {
        projects: {
            ':id': {
                messages: {
                    GET: {
                        handler: async req => {
                            const { id: projectId } = req.params;
                            const { id: userId } = await authenticationService.verifyToken(requestUtil.getToken(req));
                            const { offset, items, text, read } = req.query;

                            const pieces = [];
                            text !== undefined && pieces.push(...text.split(' '));

                            const { query, total, count } = queryService(
                                messagesQuery({
                                    offset,
                                    items,
                                    where: [
                                        ...pieces.map(piece => ({
                                            key: ['m.title', 'm.description'],
                                            operator: 'LIKE',
                                            value: `%${piece}%`,
                                        })),
                                        {
                                            key: 'mG.projectId',
                                            operator: '=',
                                            value: projectId,
                                        },
                                        read === 1 && {
                                            key: 'mR.readDt',
                                            operator: 'IS NOT NULL',
                                            value: '',
                                        },
                                        read === 1 && {
                                            key: 'mR.userId',
                                            operator: '=',
                                            value: userId,
                                        },
                                        read === 0 &&
                                            `(mR.userId = ${userId} AND mR.readDt IS NULL) OR mR.userId IS NULL`,
                                    ].filter(v => v),
                                })
                            );

                            return {
                                offset,
                                data: await dataService.raw(query),
                                items: (await dataService.raw(count.toString())).shift().count,
                                total: (await dataService.raw(total.toString())).shift().count,
                            };
                        },
                    },
                    POST: {
                        handler: async req => {
                            const { id: projectId } = req.params;
                            const { id: userId } = await authenticationService.verifyToken(requestUtil.getToken(req));
                            const { title, description, files } = req.body;

                            const message = {
                                userId,
                                title,
                                description,
                            };

                            const messageId = await messagesCrud.create(message);
                            await messageReadCrud.create({ userId, messageId, readDt: moment().format() });
                            await messageGroupsCrud.create({ messageId, projectId });

                            files?.length &&
                                (await Promise.all(
                                    files.map(
                                        async file =>
                                            await messageFilesCrud.create({
                                                messageId,
                                                fileUrl: file,
                                            })
                                    )
                                ));

                            return { success: true };
                        },
                    },
                },
            },
        },
        messages: {
            unread: {
                GET: {
                    handler: async req => {
                        const { id: userId } = await authenticationService.verifyToken(requestUtil.getToken(req));

                        const userProjects = await userProjectsCrud.read({ userId });
                        const userProjectIds = userProjects.map(({ projectId }) => projectId);
                        const projects = await projectsService.read({ id: userProjectIds });
                        const ongoingUserProjects = projects.filter(({ projectClosed }) =>
                            moment(projectClosed).isSameOrAfter(moment())
                        );
                        const ongoingUserProjectIds = ongoingUserProjects.map(({ id }) => id);

                        const messageGroups = await messageGroupsCrud.read({ projectId: ongoingUserProjectIds });
                        const messageIds = messageGroups.map(({ messageId }) => messageId);

                        const readMessages = await messageReadCrud.read({ userId, messageId: messageIds });
                        const readMessageIds = readMessages.map(({ messageId }) => messageId);

                        const unreadMessageIds = messageIds.filter(messageId => !readMessageIds.includes(messageId));

                        return {
                            success: true,
                            data: unreadMessageIds.length,
                        };
                    },
                },
            },
            ':id': {
                GET: {
                    handler: async req => {
                        const { id } = req.params;

                        async function getMessage() {
                            const [[message] = [], files] = await Promise.all([
                                messagesCrud.read({ id }),
                                messageFilesCrud.read({ messageId: id }),
                            ]);

                            return {
                                ...message,
                                files,
                            };
                        }
                        try {
                            const { id: userId } = await authenticationService.verifyToken(requestUtil.getToken(req));
                            const [user] = await usersService.read({ id: userId });
                            const message = await getMessage();

                            message.description = message.description
                                .replace(/\[voornaam\]/gi, user.firstName)
                                .replace(/\[achternaam\]/gi, user.lastName)
                                .replace(/\[geboortedatum\]/gi, moment(user.date_of_birth).format('DD/MM/YYYY'))
                                .replace(/\[geboorteland\]/gi, user.origin)
                                .replace(/\[gemeente\]/gi, user.cityName)
                                .replace(/\[datum\]/gi, moment(message.creationDt).format('DD/MM/YYYY'));

                            const [read] = await messageReadCrud.read({ userId, messageId: id });

                            return {
                                ...message,
                                read: !!read?.readDt || false,
                            };
                        } catch (e) {
                            logger.fatal(e);
                            return await getMessage();
                        }
                    },
                },
                opened: {
                    POST: {
                        handler: async req => {
                            const { userId } = req.query;
                            const { id: messageId } = req.params;

                            const messages = await messageReadCrud.read({ userId, messageId });

                            if (!messages.length) {
                                await messageReadCrud.create({ userId, messageId, readDt: null });
                            } else {
                                await messageReadCrud.update({ userId, messageId }, { openedDt: moment().format() });
                            }

                            return { success: true };
                        },
                    },
                },
                read: {
                    POST: {
                        handler: async req => {
                            const { userId } = req.query;
                            const { id: messageId } = req.params;

                            const messages = await messageReadCrud.read({ userId, messageId });

                            if (!messages.length) {
                                await messageReadCrud.create({ userId, messageId, readDt: moment().format() });
                            } else {
                                await messageReadCrud.update({ userId, messageId }, { readDt: moment().format() });
                            }

                            return { success: true };
                        },
                    },
                },
            },
            GET: {
                handler: async req => {
                    const { id: userId } = await authenticationService.verifyToken(requestUtil.getToken(req));
                    const { offset, items, text, project } = req.query;

                    const userProjects = await userProjectsCrud.read({ userId });
                    const userProjectIds = userProjects.map(({ projectId }) => projectId);
                    const projects = await projectsService.read({ id: userProjectIds });
                    const ongoingUserProjects = projects.filter(({ projectClosed }) =>
                        moment(projectClosed).isSameOrAfter(moment())
                    );
                    const ongoingUserProjectIds = ongoingUserProjects.map(({ id }) => id);

                    const userProjectIdsFiltered = !project
                        ? ongoingUserProjectIds
                        : ongoingUserProjectIds.filter(id =>
                              project
                                  .toString()
                                  .split(',')
                                  .includes(id.toString())
                          );

                    if (!userProjectIdsFiltered.length) {
                        return {
                            offset,
                            data: [],
                            items: 0,
                            total: 0,
                        };
                    }

                    const pieces = [];
                    text !== undefined && pieces.push(...text.split(' '));

                    const { query, total, count } = queryService(
                        messagesQuery({
                            offset,
                            items,
                            where: [
                                ...pieces.map(piece => ({
                                    key: ['m.title', 'm.description'],
                                    operator: 'LIKE',
                                    value: `%${piece}%`,
                                })),
                                userProjectIdsFiltered.length && {
                                    key: 'mG.projectId',
                                    operator: `IN (${userProjectIdsFiltered.join(',')})`,
                                    value: '',
                                    total: true,
                                },
                            ].filter(v => v),
                        })
                    );

                    const messages = await dataService.raw(query);
                    const messageIds = messages.map(({ id }) => id);

                    const readMessages = await messageReadCrud.read({ userId, messageId: messageIds });
                    const readMessageIds = readMessages.map(({ messageId }) => messageId);

                    const unreadMessageIds = messageIds.filter(messageId => !readMessageIds.includes(messageId));

                    return {
                        offset,
                        data: messages.map(message => {
                            message.read = !unreadMessageIds.includes(message.id);
                            return message;
                        }),
                        items: (await dataService.raw(count.toString())).shift().count,
                        total: (await dataService.raw(total.toString())).shift().count,
                    };
                },
            },
        },
        message: {
            ':id': {
                DELETE: {
                    handler: async req => {
                        const { id } = req.params;

                        await messagesCrud.update({ id }, { deletedDt: new Date() });

                        return { success: true };
                    },
                },
            },
        },
    };
};
