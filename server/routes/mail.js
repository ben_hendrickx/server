/* istanbul ignore file */
const { mail } = require('../../config');

module.exports = function mailRoutes({ templateService, mailService, logger }) {
    return function(hostname) {
        return {
            mail: {
                templates: {
                    GET: {
                        handler: async () => {
                            return {
                                success: true,
                                templates: await templateService.getAll(),
                            };
                        },
                    },
                },
                template: {
                    ':name': {
                        GET: {
                            handler: async req => {
                                let { name } = req.params;
                                name = name.replace('.html', '');

                                let template = await templateService.get(name);
                                template = template.replace(/\{hostname\}/g, hostname);

                                return {
                                    success: true,
                                    template,
                                };
                            },
                        },
                    },
                },
                send: {
                    POST: {
                        handler: async req => {
                            const { html, addresses, subject } = req.body;
                            await mailService.send(
                                {
                                    from: {
                                        name: mail.name,
                                        address: mail.noReply,
                                    },
                                    to: addresses,
                                    subject,
                                    html,
                                },
                                e => {
                                    if (e) {
                                        logger.error(`Could not send mail due to: ${e.message}`);
                                    } else {
                                        logger.info(`Mail sent`);
                                    }
                                },
                                'manual'
                            );

                            return { success: true };
                        },
                    },
                },
                POST: {
                    handler: async req => {
                        const { body } = req;
                        // TODO: move default values to .env
                        const {
                            template = 'default',
                            to = ['ben@kommaboard.be'],
                            subject = 'Uitnodiging om mee te doen met GheelaMania en Ommegang',
                        } = body;

                        try {
                            await templateService.get(template).then(html => {
                                mailService.send(
                                    {
                                        from: {
                                            name: mail.name,
                                            address: mail.noReply,
                                        },
                                        to,
                                        subject,
                                        text:
                                            'Elk jaar zetten we met Fan van Dimpna de stad Geel in de kijker met onze Kerstmusical. Dit jaar zijn we natuurlijk weer van de partij. Om al dit in goede banen te leiden kunnen we jou hulp goed gebruiken, ben je geïntresseerd om ons te helpen, on- stage of back - stage ? Schrijf je dan voor 30 oktober 2020 in !',
                                        html,
                                    },
                                    e => {
                                        if (e) {
                                            throw e;
                                        }

                                        return {
                                            status: 200,
                                        };
                                    },
                                    'default'
                                );
                            });
                        } catch (e) {
                            logger.error(`Could not send mail or load template due to: ${e.message}`);
                            return {
                                status: 500,
                                data: e.toString(),
                            };
                        }
                    },
                },
            },
        };
    };
};
