const moment = require('moment');
const uuid = require('uuid/v4');

module.exports = function repetitionRoutes({
    scenesService,
    repetitionsService,
    authenticationService,
    requestUtil,
    usersService,
    castsService,
    groupsService,
    projectRolesService,
    notificationsService,
    projectsService,
    logger,
}) {
    return function({ presenceSubscriptions }) {
        return {
            repetition: {
                ':id': {
                    scenes: {
                        GET: {
                            // VERIFIED
                            handler: async req => {
                                const { id } = req.params;
                                return await repetitionsService.getScenes(id);
                            },
                        },
                    },
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            const [repetition] = (await repetitionsService.read({ id })) || [];
                            return repetition;
                        },
                    },
                    DELETE: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            await repetitionsService.users.remove({ repetitionId: id });
                            await repetitionsService.presence.remove({ repetitionId: id });
                            await repetitionsService.remove({ id });
                            return { success: true };
                        },
                    },
                },
            },
            repetitions: {
                all: {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { projectId } = req.query;
                            const data = await (projectId
                                ? repetitionsService.read({ projectId })
                                : repetitionsService.read());

                            return {
                                data: data.map(({ id, identifier, startDt, endDt, projectId }) => ({
                                    id,
                                    identifier,
                                    startDt,
                                    endDt,
                                    projectId,
                                })),
                            };
                        },
                    },
                },
                DELETE: {
                    handler: async req => {
                        const { ids } = req.body;

                        for (let id of ids) {
                            await repetitionsService.users.remove({ repetitionId: id });
                            await repetitionsService.presence.remove({ repetitionId: id });
                            await repetitionsService.remove({ id });
                        }

                        return { success: true };
                    },
                },
                selectorData: {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { projectId, repetitionId } = req.query;

                            let [users = [], scenes = [], casts = []] = await Promise.all([
                                await usersService.read(),
                                await scenesService
                                    .getAllScenesForProject(projectId)
                                    .then(scenesService.utils.sortByIdentifier),
                                await castsService.read(),
                            ]);

                            const sceneIds = scenes.map(({ id }) => id);
                            const all_scene_groups = (await scenesService.groups.read({ sceneId: sceneIds })) || [];
                            const all_scene_roles = (await scenesService.roles.read({ sceneId: sceneIds })) || [];

                            await Promise.all(
                                scenes.map(async scene => {
                                    const scene_groups = all_scene_groups.filter(({ sceneId }) => scene.id === sceneId);
                                    const groupIds = scene_groups.map(({ groupId }) => groupId);
                                    if (groupIds.length) {
                                        const groups = (await groupsService.read({ id: groupIds })) || [];
                                        const group_users =
                                            (await groupsService.users.read({ groupId: groupIds })) || [];

                                        scene.groups = groups.map(group => {
                                            const userIds = group_users
                                                .filter(({ groupId }) => groupId === group.id)
                                                .map(({ userId }) => userId);
                                            const userCasts = group_users
                                                .filter(({ groupId }) => groupId === group.id)
                                                .reduce((acc, { userId, castId }) => {
                                                    acc[userId] = castId;
                                                    return acc;
                                                }, {});
                                            const groupUsers = users
                                                .filter(({ id }) => userIds.includes(id))
                                                .map(({ id, firstName, lastName }) => ({
                                                    id,
                                                    firstName,
                                                    lastName,
                                                    castId: userCasts[id],
                                                }));

                                            return {
                                                id: group.id,
                                                identifier: group.identifier,
                                                name: group.name,
                                                users: groupUsers,
                                            };
                                        });
                                    }

                                    const scene_roles = all_scene_roles.filter(({ sceneId }) => scene.id === sceneId);
                                    const roleIds = scene_roles.map(({ project_role_id }) => project_role_id);

                                    if (roleIds.length) {
                                        const roles = (await projectRolesService.read({ id: roleIds })) || [];
                                        const role_users =
                                            (await projectRolesService.users.read({
                                                project_role_id: roleIds,
                                            })) || [];

                                        scene.roles = roles.map(role => {
                                            const userIds = role_users
                                                .filter(({ project_role_id }) => project_role_id === role.id)
                                                .map(({ userId }) => userId);
                                            const userCasts = role_users
                                                .filter(({ project_role_id }) => project_role_id === role.id)
                                                .reduce((acc, { userId, castId }) => {
                                                    acc[userId] = castId;
                                                    return acc;
                                                }, {});
                                            const roleUsers = users
                                                .filter(({ id }) => userIds.includes(id))
                                                .map(({ id, firstName, lastName }) => ({
                                                    id,
                                                    firstName,
                                                    lastName,
                                                    castId: userCasts[id],
                                                }));

                                            return {
                                                id: role.id,
                                                identifier: role.identifier,
                                                name: role.name,
                                                users: roleUsers,
                                            };
                                        });
                                    }

                                    return scene;
                                })
                            );

                            let checked = [];

                            if (repetitionId) {
                                checked = await repetitionsService.users.read({ repetitionId });
                                checked = checked.map(check => {
                                    const item = {};

                                    check.sceneId && (item.sceneId = check.sceneId);
                                    check.groupId && (item.groupId = check.groupId);
                                    check.roleId && (item.roleId = check.roleId);
                                    check.userId && (item.userId = check.userId);
                                    check.castId && (item.castId = check.castId);

                                    return item;
                                });
                            }

                            return {
                                scenes: scenes.filter(
                                    ({ groups, roles }) => (groups && groups.length) || (roles && roles.length)
                                ),
                                casts,
                                checked,
                            };
                        },
                    },
                },
                open: {
                    PUT: {
                        handler: async req => {
                            const { ids } = req.body;

                            for (let id of ids) {
                                await repetitionsService.update({ id }, { open: 1 });
                            }

                            return { success: true };
                        },
                    },
                },
                close: {
                    PUT: {
                        handler: async req => {
                            const { ids } = req.body;

                            for (let id of ids) {
                                await repetitionsService.update({ id }, { open: 0 });
                            }

                            return { success: true };
                        },
                    },
                },
                'copy/:id': {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;

                            const repetition = await repetitionsService.getRepetition(id);

                            if (repetition) {
                                delete repetition.id;
                                delete repetition.users;

                                repetition.startDt = moment(repetition.startDt).format('YYYY-MM-DD HH:mm:ss');
                                repetition.endDt = moment(repetition.endDt).format('YYYY-MM-DD HH:mm:ss');
                                repetition.name += ' - Kopie';
                                repetition.open = false;
                                const newRepetitionId = await repetitionsService.create(repetition);

                                const repetition_users =
                                    (await repetitionsService.users.read({ repetitionId: id })) || [];

                                for (let user of repetition_users) {
                                    user.repetitionId = newRepetitionId;
                                    await repetitionsService.users.create(user);
                                }
                            }

                            return { success: !!repetition };
                        },
                    },
                },
                POST: {
                    // VERIFIED
                    handler: async req => {
                        const {
                            identifier,
                            description,
                            name,
                            startDt,
                            endDt,
                            locationId,
                            register,
                            notifications,
                            typeId,
                            data,
                            projectId,
                            all,
                            cast,
                        } = req.body;
                        const repetitionId = await repetitionsService.create({
                            uuid: uuid(),
                            identifier,
                            description,
                            name,
                            startDt,
                            endDt,
                            locationId,
                            register,
                            typeId,
                            projectId,
                            allChecked: all,
                            castChecked: cast,
                            notifications,
                        });

                        if (all) {
                            return { success: true };
                        }

                        data &&
                            data.length &&
                            (await repetitionsService.users.create(
                                data.map(row => ({
                                    sceneId: row.sceneId || null,
                                    groupId: row.groupId || null,
                                    roleId: row.roleId || null,
                                    userId: row.userId || null,
                                    castId: row.castId || null,
                                    repetitionId,
                                })),
                                ['castId', 'sceneId', 'groupId', 'roleId', 'userId', 'repetitionId']
                            ));

                        return { success: true };
                    },
                },
                GET: {
                    // VERIFIED
                    handler: async req => {
                        const { id, level } = await authenticationService.verifyToken(requestUtil.getToken(req));

                        if (!level) {
                            return {
                                data: await repetitionsService.getRepetitionsForUser(id, level),
                            };
                        }

                        return {
                            data: await repetitionsService.getRepetitionsForUser(id, level),
                        };
                    },
                },
                ':id': {
                    location: {
                        GET: {
                            handler: async req => {
                                const { id: repetitionId } = req.params;

                                const repetition = await repetitionsService.getRepetition(repetitionId);
                                let { startDt } = repetition;

                                const repetitions = (
                                    await repetitionsService.getRepetitionsByLocation(repetition.locationId)
                                ).filter(repetition => {
                                    if (!repetition.register) {
                                        return false;
                                    }

                                    if (new Date(repetition.startDt).getTime() < new Date(startDt).getTime()) {
                                        return false;
                                    }

                                    if (
                                        new Date(repetition.startDt).setHours(0, 0, 0, 0) !==
                                        new Date(startDt).setHours(0, 0, 0, 0)
                                    ) {
                                        return false;
                                    }

                                    return true;
                                });

                                return repetitions;
                            },
                        },
                    },
                    enterPin: {
                        POST: {
                            handler: async req => {
                                const { id } = req.params;
                                let { pin, present, repetitionIds = [] } = req.body;

                                repetitionIds.push(id);
                                repetitionIds = repetitionIds
                                    .filter(repetitionId => repetitionId)
                                    .map(repetitionId => parseInt(repetitionId));

                                logger.info(
                                    `Entering pin ${pin} ${
                                        present ? '(aanwezig)' : '(afwezig)'
                                    } for repetitions : ${repetitionIds.join(',')}`
                                );

                                pin = parseInt(pin);

                                const [settings] = (await usersService.settings.read({ pin })) || [];
                                if (!settings) {
                                    return;
                                }

                                const user = await usersService.getUser(settings.userId);
                                if (!user) {
                                    return;
                                }

                                const results = await Promise.all(
                                    repetitionIds.map(async repetitionId => {
                                        const repetition = await repetitionsService.getRepetition(repetitionId);
                                        const requestedUsers = await repetitionsService.getUsers(repetition);

                                        if (!requestedUsers.some(user => user.id === settings.userId)) {
                                            return 0;
                                        }

                                        const [userPresence] =
                                            (await repetitionsService.presence.read({
                                                repetitionId,
                                                userId: user.id,
                                            })) || [];

                                        if (!userPresence) {
                                            await repetitionsService.presence.create({
                                                repetitionId: repetition.id,
                                                userId: user.id,
                                                present,
                                            });

                                            return 1;
                                        }

                                        if (userPresence.present && present) {
                                            return 2;
                                        }

                                        await repetitionsService.presence.update(
                                            { repetitionId: repetition.id, userId: user.id },
                                            { present }
                                        );
                                        return 1;
                                    })
                                );

                                if (!results.some(v => v)) {
                                    return {
                                        success: false,
                                        message: `${user.firstName} ${user.lastName}, u bent niet uitgenodigd voor deze repetitie`,
                                        ...user,
                                    };
                                }

                                if (results.filter(v => v).every(v => v === 2)) {
                                    return {
                                        success: false,
                                        message: `${user.firstName} ${user.lastName}, u bent eerder al aangemeld voor deze repetitie`,
                                        ...user,
                                    };
                                }

                                for (let repetitionId of repetitionIds) {
                                    if (presenceSubscriptions[repetitionId]) {
                                        const presence = await repetitionsService.getUserPresence(repetitionId);
                                        for (let socket of presenceSubscriptions[repetitionId]) {
                                            socket.emit('repetition_presence', { repetitionId, presence });
                                        }
                                    }
                                }

                                return {
                                    success: true,
                                    ...user,
                                };
                            },
                        },
                    },
                    presence: {
                        GET: {
                            // VERIFIED
                            handler: async req => {
                                const { id: repetitionId } = req.params;

                                return await repetitionsService.getUserPresence(repetitionId);
                            },
                        },
                    },
                    'mark-presence': {
                        PUT: {
                            // VERIFIED
                            handler: async req => {
                                const token = requestUtil.getToken(req);
                                let { id: userId } = await authenticationService.verifyToken(token);

                                userId = userId || req.body.userId;

                                if (!userId) {
                                    return;
                                }

                                const { id: repetitionId } = req.params;
                                const { present } = req.body;

                                const [repetition] = await repetitionsService.read({ id: repetitionId });

                                if (!req.body.userId && !req.body.present && repetition?.notifications) {
                                    const [project] = await projectsService.read({ id: repetition.projectId });
                                    const [user] = await usersService.read({ id: userId });

                                    let notification = `<div>Medewerker ${user.firstName} ${user.lastName} heeft zich afgemeld voor repetitie ${repetition.identifier}: ${repetition.name} van project ${project.name}`;

                                    await notificationsService.add({
                                        user: {
                                            id: user.id,
                                            name: user.lastName + ' ' + user.firstName,
                                        },
                                        label: `Afmelding voor repetitie`,
                                        notification,
                                        type: 4,
                                        projectId: repetition.projectId,
                                    });
                                }

                                await repetitionsService.markPresent(repetitionId, userId, present);

                                return { success: true };
                            },
                        },
                    },
                    PUT: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            const {
                                identifier,
                                description,
                                name,
                                startDt,
                                endDt,
                                locationId,
                                register,
                                notifications,
                                typeId,
                                data,
                                all,
                                cast,
                            } = req.body;

                            await repetitionsService.update(
                                { id },
                                {
                                    identifier,
                                    description,
                                    name,
                                    startDt,
                                    endDt,
                                    locationId,
                                    register,
                                    typeId,
                                    allChecked: all,
                                    castChecked: cast,
                                    notifications,
                                }
                            );

                            await repetitionsService.users.remove({ repetitionId: id });

                            if (all) {
                                return { success: true };
                            }

                            data &&
                                data.length &&
                                (await repetitionsService.users.create(
                                    data.map(row => ({
                                        sceneId: row.sceneId || null,
                                        groupId: row.groupId || null,
                                        roleId: row.roleId || null,
                                        userId: row.userId || null,
                                        castId: row.castId || null,
                                        repetitionId: id,
                                    })),
                                    ['castId', 'sceneId', 'groupId', 'roleId', 'userId', 'repetitionId']
                                ));

                            return { success: true };
                        },
                    },
                },
            },
        };
    };
};
