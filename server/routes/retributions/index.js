/* istanbul ignore file */
const uuidv4 = require('uuid/v4');
const moment = require('moment');

module.exports = function({
    projectUsersService,
    projectUsersQuery,
    projectUserArticlesQuery,
    articlesService,
    rentalWithItemsQuery,
    projectArticlesQuery,
    queryService,
    dataService,
    crudService,
    sortingService,
}) {
    const projectUsersCrud = crudService('project_users');
    const projectUserArticlesCrud = crudService('project_user_articles');

    return {
        project_users: {
            GET: {
                // VERIFIED
                handler: async req => {
                    const queryConfig = projectUsersQuery(req.query);
                    const projectUsers = await projectUsersService.read(queryConfig);
                    const { offset } = req.query;

                    return {
                        data: projectUsers.data.sort(sortingService.sortScenes),
                        items: projectUsers.count,
                        total: await projectUsersService.count(projectUsersQuery({})),
                        offset,
                    };
                },
            },
            DELETE: {
                // VERIFIED
                handler: async req => {
                    await projectUsersService.articles.remove({ projectUserUuid: req.body });
                    await projectUsersService.remove({ uuid: req.body });

                    return { success: true };
                },
            },
        },
        project_user: {
            ':id': {
                PUT: {
                    // VERIFIED
                    handler: async req => {
                        const { id } = req.params;
                        await projectUsersService.update({ uuid: id }, { ...req.body, updatedDt: moment().format() });
                        return { success: true };
                    },
                },
                scenes: {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { id } = req.params;
                            const { articleUuid } = req.query;

                            const [projectUser] = await projectUsersCrud.read({ uuid: id });

                            const { uuid } = projectUser;
                            delete projectUser.uuid;
                            delete projectUser.updatedDt;
                            delete projectUser.sceneId;

                            const queryConfig = projectUsersQuery({
                                project: projectUser.projectId.toString(),
                                group: projectUser.groupId ? projectUser.groupId.toString() : undefined,
                                role: projectUser.roleId ? projectUser.roleId.toString() : undefined,
                                cast: projectUser.castId.toString(),
                                user: projectUser.userId.toString(),
                            });
                            const { query } = queryService(queryConfig);
                            const data = await dataService.raw(query);

                            if (articleUuid) {
                                const [{ articleId } = {}] = await projectUserArticlesCrud.read({
                                    uuid: articleUuid,
                                });
                                if (articleId) {
                                    const projectUserArticles = await projectUserArticlesCrud.read({
                                        articleId,
                                        projectUserUuid: data.map(({ uuid }) => uuid),
                                    });
                                    const uuids = projectUserArticles.map(({ projectUserUuid }) => projectUserUuid);
                                    return data.filter(pU => pU.uuid !== uuid && uuids.includes(pU.uuid));
                                }
                            }

                            return data.filter(pU => pU.uuid !== uuid);
                        },
                    },
                },
                DELETE: {
                    // VERIFIED
                    handler: async req => {
                        await projectUsersService.articles.remove({ projectUserUuid: req.params.id });
                        await projectUsersService.remove({ uuid: req.params.id });

                        return { success: true };
                    },
                },
                articles: {
                    GET: {
                        // VERIFIED
                        handler: async req => {
                            const { text } = req.query;
                            const queryConfig = projectUserArticlesQuery({ uuid: req.params.id, text });
                            const projectUserArticles = await projectUsersService.articles.read(queryConfig);

                            return {
                                data: projectUserArticles.data.map(item => {
                                    item = {
                                        ...item,
                                        category: {
                                            id: item.categoryId,
                                            name: item.categoryName,
                                        },
                                        rack: {
                                            id: item.rackId,
                                            description: item.rackDescription,
                                        },
                                        fabric: {
                                            id: item.fabricId,
                                            type: item.type,
                                        },
                                        size: {
                                            id: item.clothingSizeId,
                                            size: item.size,
                                        },
                                        pattern: {
                                            id: item.patternId,
                                            name: item.patternName,
                                        },
                                        color: {
                                            id: item.colorId,
                                            name: item.colorName,
                                        },
                                    };

                                    delete item.colorName;
                                    delete item.patternName;
                                    delete item.type;
                                    delete item.categoryName;

                                    return item;
                                }),
                                total: await projectUsersService.articles.count({ projectUserUuid: req.params.id }),
                            };
                        },
                    },
                    POST: {
                        // VERIFIED
                        handler: async req => {
                            const { articleId, force, userId, other } = req.body;

                            if (!force && articleId) {
                                const {
                                    data: [article],
                                } = await articlesService.read(
                                    [
                                        {
                                            key: 'a.id',
                                            value: articleId,
                                        },
                                    ],
                                    { offset: 0, items: 'all' }
                                );

                                if (!article || !article.active) {
                                    return {
                                        success: false,
                                        title: 'Inactief artikel',
                                        message:
                                            'Artikel is inactief, indien je dit toch wil koppelen gelieve dan te bevestigen',
                                    };
                                }

                                const queryConfig = rentalWithItemsQuery({ articleId, returned: 0 });
                                const { query } = queryService(queryConfig);
                                const rentals = await dataService.raw(query);

                                if (rentals.length) {
                                    return {
                                        success: false,
                                        title: 'Artikel uitgeleend',
                                        message:
                                            'Artikel is momenteel uitgeleend, indien je dit toch wil koppelen gelieve dan te bevestigen',
                                    };
                                }

                                const articlesQueryConfig = projectArticlesQuery({ articleId, userId });
                                const { query: articlesQuery } = queryService(articlesQueryConfig);
                                const articles = await dataService.raw(articlesQuery);

                                if (articles.length) {
                                    const [article] = articles;
                                    const inUse = `(${article.name}: ${article.lastName} ${article.firstName})`;

                                    return {
                                        success: false,
                                        title: 'Artikel in gebruik',
                                        message: `Artikel is momenteel reeds in gebruik ${inUse}, indien je dit toch wil koppelen gelieve dan te bevestigen. LET OP: het artikel wordt van de ander persoon verwijderd`,
                                        data: {
                                            uuid: articles.map(({ uuid }) => uuid),
                                        },
                                    };
                                }
                            }

                            if (force && req.body.uuid) {
                                await projectUsersService.articles.remove({
                                    uuid: req.body.uuid,
                                });
                            }

                            await projectUsersService.update(
                                {
                                    uuid: req.params.id,
                                },
                                {
                                    updatedDt: moment().format(),
                                }
                            );

                            await projectUsersService.articles.create({
                                uuid: uuidv4(),
                                projectUserUuid: req.params.id,
                                articleId: articleId ? parseInt(articleId) : undefined,
                                status: req.body.statusId,
                                remarks: req.body.remarks,
                            });

                            if (other) {
                                const artId = articleId ? parseInt(articleId) : undefined;
                                const projectUserArticles = await projectUserArticlesCrud.read({
                                    articleId: artId,
                                    projectUserUuid: other,
                                });
                                const alreadyHaveArticle = projectUserArticles.map(
                                    ({ projectUserUuid }) => projectUserUuid
                                );
                                const newProjectUserArticles = other.filter(uuid => !alreadyHaveArticle.includes(uuid));
                                await Promise.all(
                                    newProjectUserArticles.map(async projectUserUuid =>
                                        projectUsersService.articles.create({
                                            uuid: uuidv4(),
                                            projectUserUuid,
                                            articleId: artId,
                                            status: req.body.statusId,
                                            remarks: req.body.remarks,
                                        })
                                    )
                                );
                            }

                            return { success: true };
                        },
                    },
                },
                article: {
                    ':articleId': {
                        DELETE: {
                            // VERIFIED
                            handler: async req => {
                                const { id, articleId } = req.params;
                                const { projectUserUuids } = req.body;

                                const [article] = await projectUserArticlesCrud.read({
                                    uuid: articleId,
                                });

                                await projectUsersService.update(
                                    {
                                        uuid: id,
                                    },
                                    {
                                        updatedDt: moment().format(),
                                    }
                                );

                                if (!article.articleId) {
                                    await projectUsersService.articles.remove({
                                        projectUserUuid: [id, ...(projectUserUuids || [])],
                                        uuid: articleId,
                                    });
                                } else {
                                    await projectUsersService.articles.remove({
                                        projectUserUuid: [id, ...(projectUserUuids || [])],
                                        articleId: article.articleId,
                                    });
                                }

                                return { success: true };
                            },
                        },
                    },
                },
            },
        },
    };
};
