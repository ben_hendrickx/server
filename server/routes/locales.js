module.exports = function locales({ fileService }) {
    return {
        locales: {
            ':locale': {
                GET: {
                    handler: async req => {
                        const { locale } = req.params;

                        return await fileService(`../locales/${locale}.json`).read();
                    },
                },
            },
        },
    };
};
