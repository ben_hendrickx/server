module.exports = function({ userModel, registrationModel, projectModel }) {
    return function() {
        userModel.hasMany(registrationModel, { foreignKey: 'userId' });
        projectModel.hasMany(registrationModel, { foreignKey: 'projectId' });
        registrationModel.belongsTo(userModel, { as: 'user', foreignKey: 'userId' });
        registrationModel.belongsTo(projectModel, { as: 'project', foreignKey: 'projectId' });
    };
};
