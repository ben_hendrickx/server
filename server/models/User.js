const { DataTypes, Model } = require('sequelize');

module.exports = function userModel() {
    class User extends Model {}

    User.initModel = sequelize => {
        User.init(
            {
                firstName: {
                    type: DataTypes.STRING,
                    allowNull: false,
                },
                lastName: {
                    type: DataTypes.STRING,
                    allowNull: false,
                },
                email: {
                    type: DataTypes.STRING,
                    allowNull: false,
                },
                date_of_birth: {
                    type: DataTypes.DATEONLY,
                    allowNull: false,
                },
                nationality: {
                    type: DataTypes.STRING,
                    allowNull: false,
                },
                origin: {
                    type: DataTypes.STRING,
                    allowNull: false,
                },
                sex: {
                    type: DataTypes.BOOLEAN,
                    allowNull: false,
                },
                streetName: {
                    type: DataTypes.STRING,
                    allowNull: false,
                },
                houseNumber: {
                    type: DataTypes.STRING,
                    allowNull: false,
                },
                bus: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },
                cityName: {
                    type: DataTypes.STRING,
                    allowNull: false,
                },
                zipCode: {
                    type: DataTypes.STRING,
                    allowNull: false,
                },
                phoneNumber: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },
                cellPhone: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },
                secondaryEmail: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },
                owner_remarks: {
                    type: DataTypes.TEXT,
                    allowNull: true,
                },
                confirmedDt: {
                    type: DataTypes.DATE,
                    allowNull: true,
                },
                createdAt: {
                    type: DataTypes.DATE,
                    allowNull: true,
                    defaultValue: null,
                },
                updatedAt: {
                    type: DataTypes.DATE,
                    allowNull: true,
                    defaultValue: null,
                },
            },
            {
                sequelize,
                modelName: 'User',
                tableName: 'users',
                paranoid: true,
            }
        );
    };

    return User;
};
