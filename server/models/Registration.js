const { DataTypes, Model } = require('sequelize');

module.exports = function registrationModel() {
    class Registration extends Model {}

    Registration.initModel = sequelize => {
        Registration.init(
            {
                auditioning: {
                    type: DataTypes.BOOLEAN,
                    allowNull: false,
                },
                talents: {
                    type: DataTypes.TEXT,
                    allowNull: true,
                },
                remarks: {
                    type: DataTypes.TEXT,
                    allowNull: true,
                },
                part_of_town: {
                    type: DataTypes.SMALLINT,
                    allowNull: true,
                },
                owner_remarks: {
                    type: DataTypes.TEXT,
                    allowNull: true,
                },
                userId: {
                    type: DataTypes.SMALLINT.UNSIGNED,
                    allowNull: true,
                },
                projectId: {
                    type: DataTypes.SMALLINT.UNSIGNED,
                    allowNull: true,
                },
                createdAt: {
                    type: DataTypes.DATE,
                    allowNull: true,
                    defaultValue: null,
                },
                updatedAt: {
                    type: DataTypes.DATE,
                    allowNull: true,
                    defaultValue: null,
                },
            },
            {
                sequelize,
                modelName: 'Registration',
                tableName: 'registrations',
                paranoid: true,
            }
        );
    };

    return Registration;
};
