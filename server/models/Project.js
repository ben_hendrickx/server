const { DataTypes, Model } = require('sequelize');

module.exports = function projectModel() {
    class Project extends Model {}

    Project.initModel = sequelize => {
        Project.init(
            {
                name: {
                    type: DataTypes.STRING,
                    allowNull: false,
                },
                title: {
                    type: DataTypes.STRING,
                    allowNull: false,
                },
                subtitle: {
                    type: DataTypes.STRING,
                    allowNull: false,
                },
                auditionHint: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },
                auditions_enabled: {
                    type: DataTypes.BOOLEAN,
                    allowNull: false,
                },
                extended: {
                    type: DataTypes.BOOLEAN,
                    allowNull: false,
                },
                openFrom: {
                    type: DataTypes.DATE,
                    allowNull: true,
                },
                closedFrom: {
                    type: DataTypes.DATE,
                    allowNull: true,
                },
                closedOffStage: {
                    type: DataTypes.DATE,
                    allowNull: true,
                },
                rolesOpen: {
                    type: DataTypes.DATE,
                    allowNull: true,
                },
                rolesClosed: {
                    type: DataTypes.DATE,
                    allowNull: true,
                },
                editRegistrationsClosed: {
                    type: DataTypes.DATE,
                    allowNull: true,
                },
                deleteRegistrationsClosed: {
                    type: DataTypes.DATE,
                    allowNull: true,
                },
                createdAt: {
                    type: DataTypes.DATE,
                    allowNull: true,
                    defaultValue: null,
                },
                updatedAt: {
                    type: DataTypes.DATE,
                    allowNull: true,
                    defaultValue: null,
                },
            },
            {
                sequelize,
                modelName: 'Project',
                tableName: 'projects',
                paranoid: true,
            }
        );
    };

    return Project;
};
