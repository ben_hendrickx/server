const {
    mysql: { credentials },
} = require('../../config');

module.exports = function({ logger }) {
    const mysql = require('mysql2/promise');
    let connection;

    async function disconnect() {
        connection && connection.end();
        connection && (connection = undefined);
    }

    async function connect(cred) {
        try {
            logger.trace('Connecting', cred || credentials);
            connection = await mysql.createConnection(cred || credentials);
            connection.on('error', async e => {
                logger.error(e);
                if (e.code === 'PROTOCOL_CONNECTION_LOST' || e.fatal) {
                    connection && (await connection.destroy());
                    // eslint-disable-next-line require-atomic-updates
                    connection && (connection = null);
                    // eslint-disable-next-line require-atomic-updates
                    connect();
                } else {
                    throw e;
                }
            });
        } catch (e) {
            logger.fatal('Could not connect to database');
            process.exit(0);
        }
    }

    process.on('exit', () => {
        logger.debug('Closing mysql connection');
        connection && connection.end();
    });

    async function add(resource, config, keys) {
        try {
            logger.debug(config);

            let valuesToAdd = [];

            if (!Array.isArray(config)) {
                config = [config];
            }

            const keysToAdd =
                keys ||
                Object.keys(config[0])
                    .filter(k => config[0][k] !== undefined && config[0][k] !== null)
                    .map(key => `\`${key}\``)
                    .join(',');

            config.forEach(cfg => {
                valuesToAdd.push(
                    keys
                        ? keys.map(key => {
                              const value = cfg[key];

                              switch (typeof value) {
                                  case 'string':
                                      return `${mysql.escape(value.trim())}`;
                              }

                              return value || 'null';
                          })
                        : Object.values(cfg)
                              .filter(v => v !== undefined && (keys || v !== null))
                              .map(value => {
                                  switch (typeof value) {
                                      case 'string':
                                          return `${mysql.escape(value.trim())}`;
                                  }

                                  return value;
                              })
                              .join(',')
                );
            });

            const addString = valuesToAdd.map(str => `(${str})`).join(',');

            const query = `INSERT INTO ${resource} (${keysToAdd}) VALUES ${addString}`;
            const results = await raw(query);
            return results.insertId;
        } catch (e) {
            logger.error('Error while executing query: ', e);
            throw e;
        }
    }

    async function get(resource, config = {}, count = false) {
        try {
            const where =
                typeof config === 'object'
                    ? Object.entries(config)
                          .map(([key, value]) => {
                              if (Array.isArray(value)) {
                                  return `\`${key}\` IN (${value.join(',')})`;
                              }

                              if (value === null) {
                                  return `\`${key}\` IS NULL`;
                              }

                              return `\`${key}\`=${mysql.escape(value)}`;
                          })
                          .join(' AND ')
                    : `id=${config}`;

            const query = `SELECT ${count ? 'count(*) as count' : '*'} FROM ${resource}${
                where ? ` WHERE ${where}` : ''
            }`;
            const result = await raw(query);
            return count ? result[0].count : result;
        } catch (e) {
            logger.error('Error while executing query: ', e);
            throw e;
        }
    }

    async function set(resource, config, data) {
        try {
            const where = Object.entries(config)
                .map(([key, value]) => {
                    if (Array.isArray(value)) {
                        return `\`${key}\` IN (${value.join(',')})`;
                    }

                    if (value === null) {
                        return `\`${key}\` IS NULL`;
                    }

                    return `\`${key}\`=${mysql.escape(value)}`;
                })
                .join(' AND ');

            const update = Object.entries(data)
                .filter(([, value]) => value !== undefined)
                .map(([key, value]) => {
                    if (typeof value === 'string') {
                        return `\`${key}\`=${mysql.escape(value)}`;
                    }

                    return `\`${key}\`=${mysql.escape(value)}`;
                })
                .join(', ');

            const query = `UPDATE ${resource} SET${update ? ` ${update}` : ''}${where ? ` WHERE ${where}` : ''}`;
            return await raw(query);
        } catch (e) {
            logger.error('Error while executing query: ', e);
            throw e;
        }
    }

    async function remove(resource, config) {
        try {
            const where =
                typeof config === 'object'
                    ? Object.entries(config)
                          .filter(([, value]) => ![undefined, null].includes(value))
                          .map(([key, value]) => {
                              if (Array.isArray(value)) {
                                  return `\`${key}\` IN (${value
                                      .map(v => mysql.escape(typeof v === 'string' ? v.trim() : v))
                                      .join(',')})`;
                              }

                              if (typeof value === 'string') {
                                  return `\`${key}\`=${mysql.escape(value)}`;
                              }

                              return `\`${key}\`=${mysql.escape(value)}`;
                          })
                          .join(' AND ')
                    : `id=${config}`;

            const query = `DELETE FROM ${resource} ${where ? ` WHERE ${where}` : ''}`;
            return await raw(query);
        } catch (e) {
            logger.error('Error while executing query: ', e);
        }
    }

    async function raw(query, fullResponse) {
        try {
            if (!connection || connection.connection._fatalError) {
                await connect();
            }

            logger.debug('Going to execute query', query);
            const results = await connection.query(query);
            logger.debug(`Result for query: ${query}`, results[0]);

            if (fullResponse) {
                return results;
            }

            return results ? results[0] : undefined;
        } catch (e) {
            logger.error('Error while executing query: ', query, e);
            throw e;
        }
    }

    async function transaction(transactions) {
        try {
            if (!connection) {
                await connect();
            }

            await connection.beginTransaction();

            logger.debug('Transaction started');

            for (let i = 0; i < transactions.length; i++) {
                const t = transactions[i];
                try {
                    await this[t.method](t.resource, t.config, t.data);
                } catch (e) {
                    logger.fatal(e);
                    try {
                        return await connection.rollback();
                    } catch (e) {
                        logger.fatal(e);
                    }
                }
            }

            try {
                await connection.commit();
            } catch (e) {
                try {
                    logger.error('Error during transaction', e);
                    return await connection.rollback();
                } catch (e) {
                    logger.fatal(e);
                }
            }
        } catch (e) {
            logger.fatal(e);
        }
    }

    return {
        add,
        get,
        set,
        remove,
        raw,
        transaction,
        connect,
        disconnect,
    };
};
