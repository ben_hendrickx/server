function httpStatusCodeError(code, message = '') {
    this.name = 'HttpStatusCodeError';
    this.code = code;
    this.message = message;

    return this;
}
httpStatusCodeError.prototype = Error.prototype;

module.exports = function() {
    return {
        httpStatusCodeError,
    };
};
