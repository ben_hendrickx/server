module.exports = function() {
    return function(config = {}) {
        let { userId, repetitionId } = config;

        return {
            distinct: true,
            table: {
                name: 'repetition_users',
                as: 'rU',
            },
            fields: [
                { field: 'r.id' },
                { field: 'pR.identifier', as: 'roleIdentifier' },
                { field: 'pR.name', as: 'roleName' },
            ],
            joins: [
                { table: 'project_roles', as: 'pR', source: 'pR.id', target: 'rU.roleId' },
                { table: 'project_role_users', as: 'pRU', source: 'pRU.project_role_id', target: 'pR.id' },
                { table: 'repetitions', as: 'r', source: 'r.id', target: 'rU.repetitionId' },
            ],
            where: [
                { key: 'rU.sceneId', operator: 'IS NULL', value: '' },
                { key: 'rU.roleId', operator: 'IS NOT NULL', value: '' },
                { key: 'pRU.userId', value: userId },
                { key: 'pRU.castId', operator: `=rU.castId`, value: '' },
                { key: 'r.id', value: repetitionId }
            ],
        };
    };
};

