const moment = require('moment');

module.exports = function() {
    return function(config = {}) {
        let { sort, offset, items, scene, projectId, userId } = config;

        return {
            table: {
                name: 'groupes',
                as: 'g',
            },
            fields: ['g.*', { field: 's.identifier', as: 'sceneIdentifier' }, { field: 's.name', as: 'sceneName' }, 'gU.castId'],
            joins: [
                { table: 'group_users', as: 'gU', source: 'gU.groupId', target: 'g.id' },
                { table: 'scene_groups', as: 'sG', source: 'sG.groupId', target: 'g.id' },
                { table: 'scenes', as: 's', source: 's.id', target: 'sG.sceneId' },
            ],
            sort,
            offset,
            items,
            where: [
                { key: 'gU.userId', value: userId },
                projectId !== undefined && { key: 'g.projectId', value: projectId },
                scene !== undefined && { key: 's.id', value: scene }
            ],
        };
    };
};
