const moment = require('moment');

module.exports = function() {
    return function(config = {}) {
        let { sort, offset, items, scene, projectId, userId } = config;

        return {
            table: {
                name: 'project_roles',
                as: 'pR',
            },
            fields: ['pR.*', { field: 's.identifier', as: 'sceneIdentifier' }, { field: 's.name', as: 'sceneName' }, 'pRU.castId'],
            joins: [
                { table: 'project_role_users', as: 'pRU', source: 'pRU.project_role_id', target: 'pR.id' },
                { table: 'scene_roles', as: 'sR', source: 'sR.project_role_id', target: 'pR.id' },
                { table: 'scenes', as: 's', source: 's.id', target: 'sR.sceneId' },
            ],
            sort,
            offset,
            items,
            where: [
                { key: 'pRU.userId', value: userId },
                projectId !== undefined && { key: 'pR.projectId', value: projectId },
                scene !== undefined && { key: 's.id', value: scene }
            ],
        };
    };
};
