module.exports = function() {
    return function(config = {}) {
        let { sort, items, offset, articleId, returned } = config;

        return {
            table: {
                name: 'rental_items',
                as: 'rI',
            },
            fields: ['*'],
            joins: [{ table: 'rentals', as: 'r', source: 'rI.rentalId', target: 'r.id' }],
            sort,
            offset,
            items,
            where: [
                articleId !== undefined && { key: 'rI.artId', value: articleId },
                returned !== undefined && { key: 'rI.returned', value: returned },
            ],
        };
    };
};
