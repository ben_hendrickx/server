/* istanbul ignore file */
module.exports = function() {
    return function(config = {}) {
        let {
            uuid,
            text,
            fields = [
                'pUA.*',
                'a.*',
                {
                    field: 'pUA.status',
                    as: 'status',
                },
                {
                    field: 'r.description',
                    as: 'rackDescription',
                },
                'f.type',
                {
                    field: 'cat.name',
                    as: 'categoryName',
                },
                {
                    field: 'pat.name',
                    as: 'patternName',
                },
                {
                    field: 'col.name',
                    as: 'colorName',
                },
                'acs.size',
            ],
            joins = [
                { table: 'articles', as: 'a', source: 'pUA.articleId', target: 'a.id' },
                { table: 'categories', as: 'cat', source: 'a.categoryId', target: 'cat.id' },
                { table: 'article_clothing_sizes', as: 'acs', source: 'a.clothingSizeId', target: 'acs.id' },
                { table: 'fabrics', as: 'f', source: 'a.fabricId', target: 'f.id' },
                { table: 'patterns', as: 'pat', source: 'a.patternId', target: 'pat.id' },
                { table: 'colors', as: 'col', source: 'a.colorId', target: 'col.id' },
                { table: 'racks', as: 'r', source: 'a.rackId', target: 'r.id' },
            ],
        } = config;

        const pieces = [];
        text !== undefined && pieces.push(...text.split(' '));

        return {
            table: { name: 'project_user_articles', as: 'pUA' },
            fields,
            joins,
            where: [
                { key: 'projectUserUuid', value: uuid },
                ...pieces.map(piece => ({
                    key: [
                        'a.description',
                        'pUA.remarks',
                        'a.id',
                        'a.tags',
                        'acs.size',
                        'pat.name',
                        'cat.name',
                        'r.description',
                        'f.type',
                    ],
                    operator: 'LIKE',
                    value: `%${piece}%`,
                })),
            ],
            sort: 'a.id|ASC',
        };
    };
};
