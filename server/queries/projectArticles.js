/* istanbul ignore file */
const moment = require('moment');

module.exports = function() {
    return function(config = {}) {
        let { sort, items, offset, articleId, userId } = config;

        return {
            table: {
                name: 'project_user_articles',
                as: 'pUA',
            },
            fields: ['*', { field: 'pUA.uuid', as: 'uuid' }, { field: 'pU.uuid', as: 'projectUserUuid' }],
            joins: [
                { table: 'project_users', as: 'pU', source: 'pUA.projectUserUuid', target: 'pU.uuid' },
                { table: 'projects', as: 'p', source: 'pU.projectId', target: 'p.id' },
                { table: 'users', as: 'u', source: 'pU.userId', target: 'u.id' },
            ],
            sort,
            offset,
            items,
            where: [
                { key: 'pU.userId', operator: '!=', value: userId },
                { key: 'closedFrom', operator: '>=', value: moment().format() },
                { key: 'closedOffStage', operator: '>=', value: moment().format() },
                articleId !== undefined && { key: 'pUA.articleId', value: articleId },
            ],
        };
    };
};
