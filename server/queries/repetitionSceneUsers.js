module.exports = function() {
    return {
        roles: function(config = {}) {
            let { userId, repetitionId } = config;

            return {
                distinct: true,
                table: {
                    name: 'repetition_users',
                    as: 'rU',
                },
                fields: [
                    { field: 'r.id' },
                    { field: 's.identifier', as: 'sceneIdentifier' },
                    { field: 's.name', as: 'sceneName' },
                    { field: 'pR.identifier', as: 'roleIdentifier' },
                    { field: 'pR.name', as: 'roleName' },
                ],
                joins: [
                    { table: 'scenes', as: 's', source: 'rU.sceneId', target: 's.id' },
                    { table: 'scene_roles', as: 'sR', source: 's.id', target: 'sR.sceneId' },
                    { table: 'project_roles', as: 'pR', source: 'pR.id', target: 'sR.project_role_id' },
                    { table: 'project_role_users', as: 'pRU', source: 'pRU.project_role_id', target: 'pR.id' },
                    { table: 'repetitions', as: 'r', source: 'r.id', target: 'rU.repetitionId' },
                ],
                where: [
                    { key: 'rU.groupId', operator: 'IS NULL', value: '' },
                    { key: 'rU.roleId', operator: 'IS NULL', value: '' },
                    { key: 'pRU.userId', value: userId },
                    { key: 'pRU.castId', operator: `=rU.castId`, value: '' },
                    { key: 'r.id', value: repetitionId }
                ],
            };
        },
        groups: function(config = {}) {
            let { userId, repetitionId } = config;

            return {
                distinct: true,
                table: {
                    name: 'repetition_users',
                    as: 'rU',
                },
                fields: [
                    { field: 'r.id' },
                    { field: 's.identifier', as: 'sceneIdentifier' },
                    { field: 's.name', as: 'sceneName' },
                    { field: 'g.identifier', as: 'groupIdentifier' },
                    { field: 'g.name', as: 'groupName' },
                ],
                joins: [
                    { table: 'scenes', as: 's', source: 'rU.sceneId', target: 's.id' },
                    { table: 'scene_groups', as: 'sG', source: 's.id', target: 'sG.sceneId' },
                    { table: 'groupes', as: 'g', source: 'g.id', target: 'sG.groupId' },
                    { table: 'group_users', as: 'gU', source: 'gU.groupId', target: 'g.id' },
                    { table: 'repetitions', as: 'r', source: 'r.id', target: 'rU.repetitionId' },
                ],
                where: [
                    { key: 'rU.groupId', operator: 'IS NULL', value: '' },
                    { key: 'rU.roleId', operator: 'IS NULL', value: '' },
                    { key: 'gU.userId', value: userId },
                    { key: 'gU.castId', operator: `=rU.castId`, value: '' },
                    { key: 'gU.creationDt', operator: '<= r.startDt', value: '' },
                    { key: 'r.id', value: repetitionId }
                ],
            };
        },
    };
};
