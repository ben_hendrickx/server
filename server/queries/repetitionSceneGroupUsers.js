module.exports = function() {
    return function(config = {}) {
        let { userId, repetitionId } = config;

        return {
            distinct: true,
            table: {
                name: 'repetition_users',
                as: 'rU',
            },
            fields: [
                { field: 'r.id' },
                { field: 's.identifier', as: 'sceneIdentifier' },
                { field: 's.name', as: 'sceneName' },
                { field: 'g.identifier', as: 'groupIdentifier' },
                { field: 'g.name', as: 'groupName' },
            ],
            joins: [
                { table: 'groupes', as: 'g', source: 'g.id', target: 'rU.groupId' },
                { table: 'group_users', as: 'gU', source: 'gU.groupId', target: 'g.id' },
                { table: 'scenes', as: 's', source: 'rU.sceneId', target: 's.id' },
                { table: 'repetitions', as: 'r', source: 'r.id', target: 'rU.repetitionId' },
            ],
            where: [
                { key: 'rU.groupId', operator: 'IS NOT NULL', value: '' },
                { key: 'gU.userId', value: userId },
                { key: 'gU.castId', operator: `=rU.castId`, value: '' },
                { key: 'gU.creationDt', operator: '<= r.startDt', value: '' },
                { key: 'r.id', value: repetitionId }
            ],
        };
    };
};
