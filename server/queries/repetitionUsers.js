module.exports = function() {
    return {
        allChecked: {
            roles: function(config = {}) {
                let { projectId } = config;

                return {
                    table: {
                        name: 'repetitions',
                        as: 'r',
                    },
                    fields: [
                        { field: 'r.identifier', as: 'Repetitie_volgnummer' },
                        { field: 'r.name', as: 'Repetitie_naam' },
                        { field: 'r.startDt', as: 'startDt' },
                        { field: 'DATE_FORMAT(r.startDt, "%d/%m/%Y")', as: 'Repetitie_datum' },
                        { field: 'DATE_FORMAT(r.startDt, "%H:%i")', as: 'Repetitie_start_uur' },
                        { field: 'DATE_FORMAT(r.endDt, "%H:%i")', as: 'Repetitie_eind_uur' },
                        { field: 'l.name', as: 'Repetitie_locatie' },
                        {
                            field: `CONCAT_WS('', l.streetName, ' ', l.houseNumber, ' ', l.cityName, ' ', l.zipCode)`,
                            as: 'Repetitie_locatie_adres',
                        },
                        { field: 's.identifier', as: 'Scene_volgnummer' },
                        { field: 's.name', as: 'Scene_naam' },
                        { field: 'pR.identifier', as: 'Groep_of_rolnummer' },
                        { field: 'pR.name', as: 'Groep_of_rolnaam' },
                        { field: 'c.name', as: 'Cast' },
                        { field: 'u.firstName', as: 'Voornaam' },
                        { field: 'u.lastName', as: 'Achternaam' },
                        { field: 'uAS.pin', as: 'Pin' },
                        { field: 'u.email', as: 'Emailadres' },
                        { field: 'DATE_FORMAT(u.date_of_birth, "%d/%m/%Y")', as: 'Geboortedatum' },
                        { field: 'YEAR(CURDATE()) - YEAR(u.date_of_birth)', as: 'Leeftijd' },
                        { field: `IF(u.sex is null, '', IF(u.sex = 1, 'Vrouw','Man'))`, as: 'Geslacht' },
                        { field: 'u.phoneNumber', as: 'Telefoonnummer' },
                        { field: 'u.cellPhone', as: 'Gsm' },
                    ],
                    joins: [
                        { table: 'scenes', as: 's', source: 'r.projectId', target: 's.projectId' },
                        { table: 'scene_roles', as: 'sR', source: 'sR.sceneId', target: 's.id' },
                        { table: 'project_roles', as: 'pR', source: 'pR.id', target: 'sR.project_role_id' },
                        { table: 'project_role_users', as: 'pRU', source: 'pRU.project_role_id', target: 'pR.id' },
                        { table: 'casts', as: 'c', source: 'pRU.castId', target: 'c.id' },
                        { table: 'users', as: 'u', source: 'pRU.userId', target: 'u.id' },
                        { table: 'user_app_settings', as: 'uAS', source: 'uAS.userId', target: 'u.id' },
                        { table: 'locations', as: 'l', source: 'l.id', target: 'r.locationId' },
                    ],
                    where: [
                        { key: 'r.allChecked', operator: `=`, value: true },
                        { key: 'r.projectId', value: projectId },
                    ],
                    sort: 'r.startDt|ASC',
                };
            },
            groups: function(config = {}) {
                let { projectId } = config;

                return {
                    table: {
                        name: 'repetitions',
                        as: 'r',
                    },
                    fields: [
                        { field: 'r.identifier', as: 'Repetitie_volgnummer' },
                        { field: 'r.name', as: 'Repetitie_naam' },
                        { field: 'r.startDt', as: 'startDt' },
                        { field: 'DATE_FORMAT(r.startDt, "%d/%m/%Y")', as: 'Repetitie_datum' },
                        { field: 'DATE_FORMAT(r.startDt, "%H:%i")', as: 'Repetitie_start_uur' },
                        { field: 'DATE_FORMAT(r.endDt, "%H:%i")', as: 'Repetitie_eind_uur' },
                        { field: 'l.name', as: 'Repetitie_locatie' },
                        {
                            field: `CONCAT_WS('', l.streetName, ' ', l.houseNumber, ' ', l.cityName, ' ', l.zipCode)`,
                            as: 'Repetitie_locatie_adres',
                        },
                        { field: 's.identifier', as: 'Scene_volgnummer' },
                        { field: 's.name', as: 'Scene_naam' },
                        { field: 'g.identifier', as: 'Groep_of_rolnummer' },
                        { field: 'g.name', as: 'Groep_of_rolnaam' },
                        { field: 'c.name', as: 'Cast' },
                        { field: 'u.firstName', as: 'Voornaam' },
                        { field: 'u.lastName', as: 'Achternaam' },
                        { field: 'uAS.pin', as: 'Pin' },
                        { field: 'u.email', as: 'Emailadres' },
                        { field: 'DATE_FORMAT(u.date_of_birth, "%d/%m/%Y")', as: 'Geboortedatum' },
                        { field: 'YEAR(CURDATE()) - YEAR(u.date_of_birth)', as: 'Leeftijd' },
                        { field: `IF(u.sex is null, '', IF(u.sex = 1, 'Vrouw','Man'))`, as: 'Geslacht' },
                        { field: 'u.phoneNumber', as: 'Telefoonnummer' },
                        { field: 'u.cellPhone', as: 'Gsm' },
                    ],
                    joins: [
                        { table: 'scenes', as: 's', source: 'r.projectId', target: 's.projectId' },
                        { table: 'scene_groups', as: 'sG', source: 'sG.sceneId', target: 's.id' },
                        { table: 'groupes', as: 'g', source: 'g.id', target: 'sG.groupId' },
                        { table: 'group_users', as: 'gU', source: 'gU.groupId', target: 'g.id' },
                        { table: 'casts', as: 'c', source: 'gU.castId', target: 'c.id' },
                        { table: 'users', as: 'u', source: 'gU.userId', target: 'u.id' },
                        { table: 'user_app_settings', as: 'uAS', source: 'uAS.userId', target: 'u.id' },
                        { table: 'locations', as: 'l', source: 'l.id', target: 'r.locationId' },
                    ],
                    where: [
                        { key: 'r.allChecked', operator: `=`, value: true },
                        { key: 'r.projectId', value: projectId },
                    ],
                    sort: 'r.startDt|ASC',
                };
            },
        },
        scenes: {
            roles: function(config = {}) {
                let { projectId } = config;

                return {
                    table: {
                        name: 'repetition_users',
                        as: 'rU',
                    },
                    fields: [
                        { field: 'r.identifier', as: 'Repetitie_volgnummer' },
                        { field: 'r.name', as: 'Repetitie_naam' },
                        { field: 'r.startDt', as: 'startDt' },
                        { field: 'DATE_FORMAT(r.startDt, "%d/%m/%Y")', as: 'Repetitie_datum' },
                        { field: 'DATE_FORMAT(r.startDt, "%H:%i")', as: 'Repetitie_start_uur' },
                        { field: 'DATE_FORMAT(r.endDt, "%H:%i")', as: 'Repetitie_eind_uur' },
                        { field: 'l.name', as: 'Repetitie_locatie' },
                        {
                            field: `CONCAT_WS('', l.streetName, ' ', l.houseNumber, ' ', l.cityName, ' ', l.zipCode)`,
                            as: 'Repetitie_locatie_adres',
                        },
                        { field: 's.identifier', as: 'Scene_volgnummer' },
                        { field: 's.name', as: 'Scene_naam' },
                        { field: 'pR.identifier', as: 'Groep_of_rolnummer' },
                        { field: 'pR.name', as: 'Groep_of_rolnaam' },
                        { field: 'c.name', as: 'Cast' },
                        { field: 'u.firstName', as: 'Voornaam' },
                        { field: 'u.lastName', as: 'Achternaam' },
                        { field: 'uAS.pin', as: 'Pin' },
                        { field: 'u.email', as: 'Emailadres' },
                        { field: 'DATE_FORMAT(u.date_of_birth, "%d/%m/%Y")', as: 'Geboortedatum' },
                        { field: 'YEAR(CURDATE()) - YEAR(u.date_of_birth)', as: 'Leeftijd' },
                        { field: `IF(u.sex is null, '', IF(u.sex = 1, 'Vrouw','Man'))`, as: 'Geslacht' },
                        { field: 'u.phoneNumber', as: 'Telefoonnummer' },
                        { field: 'u.cellPhone', as: 'Gsm' },
                    ],
                    joins: [
                        { table: 'scenes', as: 's', source: 'rU.sceneId', target: 's.id' },
                        { table: 'scene_roles', as: 'sR', source: 'sR.sceneId', target: 's.id' },
                        { table: 'project_roles', as: 'pR', source: 'pR.id', target: 'sR.project_role_id' },
                        { table: 'project_role_users', as: 'pRU', source: 'pRU.project_role_id', target: 'pR.id' },
                        { table: 'casts', as: 'c', source: 'rU.castId', target: 'c.id' },
                        { table: 'repetitions', as: 'r', source: 'r.id', target: 'rU.repetitionId' },
                        { table: 'users', as: 'u', source: 'pRU.userId', target: 'u.id' },
                        { table: 'user_app_settings', as: 'uAS', source: 'uAS.userId', target: 'u.id' },
                        { table: 'locations', as: 'l', source: 'l.id', target: 'r.locationId' },
                    ],
                    where: [
                        { key: 'rU.groupId', operator: 'IS NULL', value: '' },
                        {
                            key: 'rU.roleId',
                            operator: 'IS NULL OR (rU.roleId IS NOT NULL AND rU.roleId = pRU.project_role_id)',
                            value: '',
                        },
                        { key: 'rU.castId', operator: 'IS NOT NULL', value: '' },
                        { key: 'pRU.castId', operator: `=rU.castId OR rU.castId IS NULL`, value: '' },
                        { key: 'r.projectId', value: projectId },
                    ],
                    sort: 'r.startDt|ASC',
                };
            },
            groups: function(config = {}) {
                let { projectId } = config;

                return {
                    table: {
                        name: 'repetition_users',
                        as: 'rU',
                    },
                    fields: [
                        { field: 'r.identifier', as: 'Repetitie_volgnummer' },
                        { field: 'r.name', as: 'Repetitie_naam' },
                        { field: 'r.startDt', as: 'startDt' },
                        { field: 'DATE_FORMAT(r.startDt, "%d/%m/%Y")', as: 'Repetitie_datum' },
                        { field: 'DATE_FORMAT(r.startDt, "%H:%i")', as: 'Repetitie_start_uur' },
                        { field: 'DATE_FORMAT(r.endDt, "%H:%i")', as: 'Repetitie_eind_uur' },
                        { field: 'l.name', as: 'Repetitie_locatie' },
                        {
                            field: `CONCAT_WS('', l.streetName, ' ', l.houseNumber, ' ', l.cityName, ' ', l.zipCode)`,
                            as: 'Repetitie_locatie_adres',
                        },
                        { field: 's.identifier', as: 'Scene_volgnummer' },
                        { field: 's.name', as: 'Scene_naam' },
                        { field: 'g.identifier', as: 'Groep_of_rolnummer' },
                        { field: 'g.name', as: 'Groep_of_rolnaam' },
                        { field: 'c.name', as: 'Cast' },
                        { field: 'u.firstName', as: 'Voornaam' },
                        { field: 'u.lastName', as: 'Achternaam' },
                        { field: 'uAS.pin', as: 'Pin' },
                        { field: 'u.email', as: 'Emailadres' },
                        { field: 'DATE_FORMAT(u.date_of_birth, "%d/%m/%Y")', as: 'Geboortedatum' },
                        { field: 'YEAR(CURDATE()) - YEAR(u.date_of_birth)', as: 'Leeftijd' },
                        { field: `IF(u.sex is null, '', IF(u.sex = 1, 'Vrouw','Man'))`, as: 'Geslacht' },
                        { field: 'u.phoneNumber', as: 'Telefoonnummer' },
                        { field: 'u.cellPhone', as: 'Gsm' },
                    ],
                    joins: [
                        { table: 'scenes', as: 's', source: 'rU.sceneId', target: 's.id' },
                        { table: 'scene_groups', as: 'sG', source: 'sG.sceneId', target: 's.id' },
                        { table: 'groupes', as: 'g', source: 'g.id', target: 'sG.groupId' },
                        { table: 'group_users', as: 'gU', source: 'gU.groupId', target: 'g.id' },
                        { table: 'casts', as: 'c', source: 'rU.castId', target: 'c.id' },
                        { table: 'repetitions', as: 'r', source: 'r.id', target: 'rU.repetitionId' },
                        { table: 'users', as: 'u', source: 'gU.userId', target: 'u.id' },
                        { table: 'user_app_settings', as: 'uAS', source: 'uAS.userId', target: 'u.id' },
                        { table: 'locations', as: 'l', source: 'l.id', target: 'r.locationId' },
                    ],
                    where: [
                        { key: 'rU.roleId', operator: 'IS NULL', value: '' },
                        {
                            key: 'rU.groupId',
                            operator: 'IS NULL OR (rU.groupId IS NOT NULL AND rU.groupId = gU.groupId)',
                            value: '',
                        },
                        { key: 'rU.castId', operator: 'IS NOT NULL', value: '' },
                        { key: 'gU.castId', operator: `=rU.castId OR rU.castId IS NULL`, value: '' },
                        { key: 'r.projectId', value: projectId },
                    ],
                    sort: 'r.startDt|ASC',
                };
            },
        },
        roles: function(config = {}) {
            let { projectId } = config;

            return {
                table: {
                    name: 'repetition_users',
                    as: 'rU',
                },
                fields: [
                    { field: 'r.identifier', as: 'Repetitie_volgnummer' },
                    { field: 'r.name', as: 'Repetitie_naam' },
                    { field: 'r.startDt', as: 'startDt' },
                    { field: 'DATE_FORMAT(r.startDt, "%d/%m/%Y")', as: 'Repetitie_datum' },
                    { field: 'DATE_FORMAT(r.startDt, "%H:%i")', as: 'Repetitie_start_uur' },
                    { field: 'DATE_FORMAT(r.endDt, "%H:%i")', as: 'Repetitie_eind_uur' },
                    { field: 'l.name', as: 'Repetitie_locatie' },
                    {
                        field: `CONCAT_WS('', l.streetName, ' ', l.houseNumber, ' ', l.cityName, ' ', l.zipCode)`,
                        as: 'Repetitie_locatie_adres',
                    },
                    { field: 'pR.identifier', as: 'Groep_of_rolnummer' },
                    { field: 'pR.name', as: 'Groep_of_rolnaam' },
                    { field: 'c.name', as: 'Cast' },
                    { field: 'u.firstName', as: 'Voornaam' },
                    { field: 'u.lastName', as: 'Achternaam' },
                    { field: 'uAS.pin', as: 'Pin' },
                    { field: 'u.email', as: 'Emailadres' },
                    { field: 'DATE_FORMAT(u.date_of_birth, "%d/%m/%Y")', as: 'Geboortedatum' },
                    { field: 'YEAR(CURDATE()) - YEAR(u.date_of_birth)', as: 'Leeftijd' },
                    { field: `IF(u.sex is null, '', IF(u.sex = 1, 'Vrouw','Man'))`, as: 'Geslacht' },
                    { field: 'u.phoneNumber', as: 'Telefoonnummer' },
                    { field: 'u.cellPhone', as: 'Gsm' },
                ],
                joins: [
                    { table: 'project_roles', as: 'pR', source: 'pR.id', target: 'rU.roleId' },
                    { table: 'project_role_users', as: 'pRU', source: 'pRU.project_role_id', target: 'pR.id' },
                    { table: 'casts', as: 'c', source: 'rU.castId', target: 'c.id' },
                    { table: 'repetitions', as: 'r', source: 'r.id', target: 'rU.repetitionId' },
                    { table: 'users', as: 'u', source: 'pRU.userId', target: 'u.id' },
                    { table: 'user_app_settings', as: 'uAS', source: 'uAS.userId', target: 'u.id' },
                    { table: 'locations', as: 'l', source: 'l.id', target: 'r.locationId' },
                ],
                where: [
                    { key: 'rU.groupId', operator: 'IS NULL', value: '' },
                    {
                        key: 'rU.roleId',
                        operator: 'IS NULL OR (rU.roleId IS NOT NULL AND rU.roleId = pRU.project_role_id)',
                        value: '',
                    },
                    { key: 'rU.castId', operator: 'IS NOT NULL', value: '' },
                    { key: 'pRU.castId', operator: `=rU.castId OR rU.castId IS NULL`, value: '' },
                    { key: 'r.projectId', value: projectId },
                ],
                sort: 'r.startDt|ASC',
            };
        },
        groups: function(config = {}) {
            let { projectId } = config;

            return {
                table: {
                    name: 'repetition_users',
                    as: 'rU',
                },
                fields: [
                    { field: 'r.identifier', as: 'Repetitie_volgnummer' },
                    { field: 'r.name', as: 'Repetitie_naam' },
                    { field: 'r.startDt', as: 'startDt' },
                    { field: 'DATE_FORMAT(r.startDt, "%d/%m/%Y")', as: 'Repetitie_datum' },
                    { field: 'DATE_FORMAT(r.startDt, "%H:%i")', as: 'Repetitie_start_uur' },
                    { field: 'DATE_FORMAT(r.endDt, "%H:%i")', as: 'Repetitie_eind_uur' },
                    { field: 'l.name', as: 'Repetitie_locatie' },
                    {
                        field: `CONCAT_WS('', l.streetName, ' ', l.houseNumber, ' ', l.cityName, ' ', l.zipCode)`,
                        as: 'Repetitie_locatie_adres',
                    },
                    { field: 'g.identifier', as: 'Groep_of_rolnummer' },
                    { field: 'g.name', as: 'Groep_of_rolnaam' },
                    { field: 'c.name', as: 'Cast' },
                    { field: 'u.firstName', as: 'Voornaam' },
                    { field: 'u.lastName', as: 'Achternaam' },
                    { field: 'uAS.pin', as: 'Pin' },
                    { field: 'u.email', as: 'Emailadres' },
                    { field: 'DATE_FORMAT(u.date_of_birth, "%d/%m/%Y")', as: 'Geboortedatum' },
                    { field: 'YEAR(CURDATE()) - YEAR(u.date_of_birth)', as: 'Leeftijd' },
                    { field: `IF(u.sex is null, '', IF(u.sex = 1, 'Vrouw','Man'))`, as: 'Geslacht' },
                    { field: 'u.phoneNumber', as: 'Telefoonnummer' },
                    { field: 'u.cellPhone', as: 'Gsm' },
                ],
                joins: [
                    { table: 'groupes', as: 'g', source: 'g.id', target: 'rU.groupId' },
                    { table: 'group_users', as: 'gU', source: 'gU.groupId', target: 'g.id' },
                    { table: 'casts', as: 'c', source: 'rU.castId', target: 'c.id' },
                    { table: 'repetitions', as: 'r', source: 'r.id', target: 'rU.repetitionId' },
                    { table: 'users', as: 'u', source: 'gU.userId', target: 'u.id' },
                    { table: 'user_app_settings', as: 'uAS', source: 'uAS.userId', target: 'u.id' },
                    { table: 'locations', as: 'l', source: 'l.id', target: 'r.locationId' },
                ],
                where: [
                    { key: 'rU.roleId', operator: 'IS NULL', value: '' },
                    {
                        key: 'rU.groupId',
                        operator: 'IS NULL OR (rU.groupId IS NOT NULL AND rU.groupId = gU.groupId)',
                        value: '',
                    },
                    { key: 'rU.castId', operator: 'IS NOT NULL', value: '' },
                    { key: 'gU.castId', operator: `=rU.castId OR rU.castId IS NULL`, value: '' },
                    { key: 'r.projectId', value: projectId },
                ],
                sort: 'r.startDt|ASC',
            };
        },
        users: function(config = {}) {
            let { projectId } = config;

            return {
                table: {
                    name: 'repetition_users',
                    as: 'rU',
                },
                fields: [
                    { field: 'r.identifier', as: 'Repetitie_volgnummer' },
                    { field: 'r.name', as: 'Repetitie_naam' },
                    { field: 'r.startDt', as: 'startDt' },
                    { field: 'DATE_FORMAT(r.startDt, "%d/%m/%Y")', as: 'Repetitie_datum' },
                    { field: 'DATE_FORMAT(r.startDt, "%H:%i")', as: 'Repetitie_start_uur' },
                    { field: 'DATE_FORMAT(r.endDt, "%H:%i")', as: 'Repetitie_eind_uur' },
                    { field: 'l.name', as: 'Repetitie_locatie' },
                    {
                        field: `CONCAT_WS('', l.streetName, ' ', l.houseNumber, ' ', l.cityName, ' ', l.zipCode)`,
                        as: 'Repetitie_locatie_adres',
                    },
                    { field: "''", as: 'Scene_volgnummer' },
                    { field: "''", as: 'Scene_naam' },
                    { field: "''", as: 'Groep_of_rolnummer' },
                    { field: "''", as: 'Groep_of_rolnaam' },
                    { field: "''", as: 'Cast' },
                    { field: 'u.firstName', as: 'Voornaam' },
                    { field: 'u.lastName', as: 'Achternaam' },
                    { field: 'uAS.pin', as: 'Pin' },
                    { field: 'u.email', as: 'Emailadres' },
                    { field: 'DATE_FORMAT(u.date_of_birth, "%d/%m/%Y")', as: 'Geboortedatum' },
                    { field: 'YEAR(CURDATE()) - YEAR(u.date_of_birth)', as: 'Leeftijd' },
                    { field: `IF(u.sex is null, '', IF(u.sex = 1, 'Vrouw','Man'))`, as: 'Geslacht' },
                    { field: 'u.phoneNumber', as: 'Telefoonnummer' },
                    { field: 'u.cellPhone', as: 'Gsm' },
                ],
                joins: [
                    { table: 'repetitions', as: 'r', source: 'r.id', target: 'rU.repetitionId' },
                    { table: 'users', as: 'u', source: 'rU.userId', target: 'u.id' },
                    { table: 'user_app_settings', as: 'uAS', source: 'uAS.userId', target: 'u.id' },
                    { table: 'locations', as: 'l', source: 'l.id', target: 'r.locationId' },
                ],
                where: [
                    { key: 'rU.userId', operator: 'IS NOT NULL', value: '' },
                    { key: 'r.projectId', value: projectId },
                ],
                sort: 'r.startDt|ASC',
            };
        },
    };
};
