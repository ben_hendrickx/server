/* istanbul ignore file */
module.exports = function() {
    return function(config) {
        const { offset, items, where = [] } = config;

        return {
            distinct: true,
            countField: 'distinct(m.id)',
            table: { name: 'messages', as: 'm' },
            fields: ['m.*'],
            joins: [{ table: 'message_groups', as: 'mG', source: 'm.id', target: 'mG.messageId' }],
            sort: 'm.id|ASC',
            offset,
            items,
            where,
            hasDelete: true,
        };
    };
};
