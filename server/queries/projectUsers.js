/* istanbul ignore file */
module.exports = function() {
    return function(config = {}) {
        let { sort = 's.identifier|ASC', items, offset, project, scene, role, group, cast, user, text } = config;

        const pieces = [];
        text !== undefined && pieces.push(...text.split(' '));

        return {
            table: {
                name: 'project_users',
                as: 'pU',
            },
            fields: [
                'pU.*',
                'uS.*',
                'u.firstName',
                'u.lastName',
                'u.email',
                'u.secondaryEmail',
                'u.phoneNumber',
                'u.cellPhone',
                { field: 'p.name', as: 'projectName' },
                { field: 'sS.name', as: 'shirtSize' },
                { field: 's.identifier', as: 'sceneIdentifier' },
                { field: 's.name', as: 'sceneName' },
                { field: 'pR.identifier', as: 'roleIdentifier' },
                { field: 'pR.name', as: 'roleName' },
                { field: 'g.identifier', as: 'groupIdentifier' },
                { field: 'g.name', as: 'groupName' },
                { field: 'c.name', as: 'castName' },
                { field: 'sS.name', as: 'shirtSize' },
                { field: 'pU.status', as: 'status' },
                { field: 'pUA.count', as: 'count' },
            ],
            joins: [
                {
                    table:
                        '(SELECT projectUserUuid, count(*) as count FROM project_user_articles GROUP BY projectUserUuid)',
                    as: 'pUA',
                    source: 'pU.uuid',
                    target: 'pUA.projectUserUuid',
                },
                { table: 'users', as: 'u', source: 'pU.userId', target: 'u.id' },
                { table: 'user_sizes', as: 'uS', source: 'u.id', target: 'uS.userId' },
                { table: 'shirt_sizes', as: 'sS', source: 'uS.shirtSize', target: 'sS.id' },
                { table: 'projects', as: 'p', source: 'pU.projectId', target: 'p.id' },
                { table: 'scenes', as: 's', source: 'pU.sceneId', target: 's.id' },
                { table: 'project_roles', as: 'pR', source: 'pU.roleId', target: 'pR.id' },
                { table: 'groupes', as: 'g', source: 'pU.groupId', target: 'g.id' },
                { table: 'casts', as: 'c', source: 'pU.castId', target: 'c.id' },
            ],
            sort,
            offset,
            items,
            where: [
                { key: 'p.projectClosed', operator: 'IS NOT NULL', value: '' },
                { key: 'p.projectClosed', operator: '> NOW()', value: '' },
                project !== undefined && { key: 'pU.projectId', value: project.toString().split(',') },
                scene !== undefined && { key: 'pU.sceneId', value: scene.toString().split(',') },
                role !== undefined && { key: 'pU.roleId', value: role.toString().split(',') },
                group !== undefined && { key: 'pU.groupId', value: group.toString().split(',') },
                cast !== undefined && { key: 'pU.castId', value: cast.toString().split(',') },
                user !== undefined && { key: 'pU.userId', value: user.toString().split(',') },
                ...pieces.map(piece => ({
                    key: [
                        'u.firstName',
                        'u.lastName',
                        'u.email',
                        'p.name',
                        's.identifier',
                        's.name',
                        'pR.identifier',
                        'pR.name',
                        'g.identifier',
                        'g.name',
                        'c.name',
                    ],
                    operator: 'LIKE',
                    value: `%${piece}%`,
                })),
            ],
        };
    };
};
