/* istanbul ignore file */
module.exports = function() {
    return function(config) {
        const {
            fields = [
                'a.*',
                {
                    field: 'r.description',
                    as: 'rackDescription',
                },
                'f.type',
                {
                    field: 'cat.name',
                    as: 'categoryName',
                },
                {
                    field: 'p.name',
                    as: 'patternName',
                },
                {
                    field: 'c.name',
                    as: 'colorName',
                },
                'acs.size',
            ],
            where = [],
        } = config;

        return {
            table: { name: 'articles', as: 'a' },
            fields,
            joins: [
                { table: 'categories', as: 'cat', source: 'a.categoryId', target: 'cat.id' },
                { table: 'article_clothing_sizes', as: 'acs', source: 'a.clothingSizeId', target: 'acs.id' },
                { table: 'fabrics', as: 'f', source: 'a.fabricId', target: 'f.id' },
                { table: 'patterns', as: 'p', source: 'a.patternId', target: 'p.id' },
                { table: 'colors', as: 'c', source: 'a.colorId', target: 'c.id' },
                { table: 'racks', as: 'r', source: 'a.rackId', target: 'r.id' },
            ],
            where,
            sort: 'a.id|ASC',
        };
    };
};
