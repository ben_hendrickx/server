FROM --platform=linux/amd64 node:14

WORKDIR /usr/src/app

COPY package.json ./

RUN npm install

COPY . .

EXPOSE 9998

ENV NODE_OPTIONS=--max_old_space_size=4096
ENV NODE_ENV=production

CMD [ "node", "index.js" ]
