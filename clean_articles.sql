DROP TABLE rental_items;
DROP TABLE rentals;
DROP TABLE project_user_articles;
DROP TABLE project_users;
DROP TABLE articles;
DROP TABLE article_clothing_sizes;
DROP TABLE set_items;
DROP TABLE fabrics;
DROP TABLE colors;
DROP TABLE patterns;
DROP TABLE category_prices;
DROP TABLE categories;
DROP TABLE racks;

CREATE TABLE IF NOT EXISTS racks(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `description` varchar(50) NOT NULL,
    deletedDt datetime DEFAULT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS categories(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS category_prices(
    categoryId INT UNSIGNED NOT NULL AUTO_INCREMENT,
    internalPrice float NOT NULL,
    externalPrice float NOT NULL,
    replacementPrice float NOT NULL,
    startDt datetime DEFAULT(now()),
    endDt datetime,
    FOREIGN KEY (categoryId) REFERENCES categories(id)
);

CREATE TABLE IF NOT EXISTS patterns(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    deletedDt datetime DEFAULT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS colors(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    deletedDt datetime DEFAULT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS fabrics(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    type VARCHAR(50) NOT NULL,
    deletedDt datetime DEFAULT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS article_clothing_sizes(
    id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    size VARCHAR(25) NOT NULL,
    deletedDt datetime DEFAULT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS articles(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    active BOOL NOT NULL DEFAULT true,
    washing BOOL NOT NULL DEFAULT false,
    stock SMALLINT UNSIGNED NOT NULL DEFAULT 1,
    rackId INT UNSIGNED,
    clothingSizeId SMALLINT UNSIGNED NOT NULL,
    categoryId INT UNSIGNED NOT NULL,
    patternId INT UNSIGNED,
    fabricId INT UNSIGNED,
    colorId INT UNSIGNED,
    `status` smallint unsigned DEFAULT NULL,
    tags text DEFAULT NULL,
    description text DEFAULT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (clothingSizeId) REFERENCES article_clothing_sizes(id),
    FOREIGN KEY (categoryId) REFERENCES categories(id),
    FOREIGN KEY (patternId) REFERENCES patterns(id),
    FOREIGN KEY (fabricId) REFERENCES fabrics(id),
    FOREIGN KEY (colorId) REFERENCES colors(id)
);

CREATE TABLE IF NOT EXISTS project_users(
  uuid varchar(50) NOT NULL,
  projectId smallint unsigned NOT NULL,
  sceneId smallint unsigned NOT NULL,
  roleId smallint unsigned DEFAULT NULL,
  groupId smallint unsigned DEFAULT NULL,
  castId smallint unsigned NOT NULL,
  userId smallint unsigned NOT NULL,
  updatedDt datetime DEFAULT (now()),
  `status` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (uuid),
  FOREIGN KEY (projectId) REFERENCES projects(id),
  FOREIGN KEY (sceneId) REFERENCES scenes(id),
  FOREIGN KEY (roleId) REFERENCES project_roles(id),
  FOREIGN KEY (groupId) REFERENCES groupes(id),
  FOREIGN KEY (castId) REFERENCES casts(id),
  FOREIGN KEY (userId) REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS project_user_articles(
  uuid varchar(50) NOT NULL,
  projectUserUuid varchar(50) NOT NULL,
  articleId int unsigned DEFAULT NULL,
  `status` smallint unsigned DEFAULT NULL,
  remarks text,
  PRIMARY KEY (uuid),
  FOREIGN KEY (projectUserUuid) REFERENCES project_users(uuid),
  FOREIGN KEY (articleId) REFERENCES articles(id)
);

CREATE TABLE IF NOT EXISTS rentals(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `type` smallint unsigned NOT NULL,
    fromDt datetime DEFAULT (now()),
    `name` varchar(50) NOT NULL,
    companyName varchar(255) DEFAULT NULL,
    streetName varchar(100) DEFAULT NULL,
    houseNumber varchar(10) DEFAULT NULL,
    bus varchar(10) DEFAULT NULL,
    cityName varchar(50) DEFAULT NULL,
    zipCode varchar(10) DEFAULT NULL,
    cellPhone varchar(20) DEFAULT NULL,
    email varchar(100) DEFAULT NULL,
    bankAccount varchar(50) DEFAULT NULL,
    uniqueNumber varchar(50) DEFAULT NULL,
    isFor varchar(50) DEFAULT NULL,
    toDt date NOT NULL,
    price float NOT NULL,
    replacementPrice float NOT NULL,
    periods smallint unsigned NOT NULL,
    creationDt datetime DEFAULT (now()),
    internal smallint unsigned NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS rental_items(
    rentalId INT UNSIGNED NOT NULL,
    artId INT UNSIGNED NOT NULL,
    amount int unsigned NOT NULL DEFAULT 1,
    remarks text,
    FOREIGN KEY (rentalId) REFERENCES rentals(id),
    FOREIGN KEY (artId) REFERENCES articles(id)
);
