require('dotenv').config();
const { NODE_ENV, SERVER_NAME = '' } = process.env;
const isProduction = NODE_ENV === 'production';
const isDevelop = NODE_ENV === 'development';
const isTest = !isProduction && !isDevelop;

function getHostName() {
    if (isDevelop) {
        return 'http://localhost';
    }

    if (isTest) {
        return SERVER_NAME === 'historalia' ? 'https://team.historalia.be' : 'https://fanvandimpna.ddns.net';
    }

    return SERVER_NAME === 'historalia' ? 'https://team.historalia.be' : 'https://fanvandimpna.be';
}

const hostname = getHostName();

module.exports = {
    logger: {
        enabled: isTest ? false : true,
        level: isProduction ? 'INFO' : 'TRACE',
        showLocation: true,
    },
    server: {
        hostname,
        hostnameShort: SERVER_NAME.includes('historalia') ? 'team.historalia.be' : 'fanvandimpna.be',
        port: 9998,
    },
    mail: {
        enable: isProduction ? true : false,
        userId: '685c4d38ed2fb317b536185b41060748',
        apiKey: 'ea99c1439131b9879f787c3939c71af5',
        client: SERVER_NAME.includes('historalia') ? 'Historalia' : 'Fan van Dimpna',
        colors: {
            primary: SERVER_NAME.includes('historalia') ? '#497db0' : '#fdc300',
            secondary: SERVER_NAME.includes('historalia') ? '#1f64b8' : '#fdaf00',
        },
        urls: {
            facebook: SERVER_NAME.includes('historalia')
                ? 'https://nl-nl.facebook.com/Historalia/'
                : 'https://www.facebook.com/toerismegeel/',
            instagram: SERVER_NAME.includes('historalia')
                ? 'https://www.instagram.com/historalia/'
                : 'https://instagram.com/toerismegeel',
        },
        name: SERVER_NAME.includes('historalia') ? 'Historalia' : 'Toerisme Geel - Fan van Dimpna',
        mail: SERVER_NAME.includes('historalia') ? 'no-reply@historalia.be' : 'toerisme@geel.be',
        noReply: SERVER_NAME.includes('historalia') ? 'no-reply@historalia.be' : 'info@fanvandimpna.be',
    },
    mysql: {
        credentials: {
            host: isDevelop ? 'localhost' : 'database',
            port: '3306',
            user: SERVER_NAME.includes('historalia') ? 'historalia' : 'fvd',
            password: SERVER_NAME.includes('historalia') ? 'HIST123' : 'FVD123',
            database: process.env.test
                ? 'fvd_test'
                : SERVER_NAME.includes('historalia')
                ? 'historalia'
                : 'fanvandimpna',
            insecureAuth: true,
        },
    },
    data: {
        type: 'MYSQL',
    },
};
