const sinon = require('sinon');

xdescribe('loggerSpec', function() {
    beforeEach('When i have a logger', () => {
        process.env.LOGGER_HIDE_LOCATION = true;
        this.logger = require('../logger')({
            stringUtil: require('../server/utils/string')(),
            dateUtil: require('../server/utils/date')({
                numberUtil: require('../server/utils/number')(),
            }),
        });
        this.clock = sinon.useFakeTimers({
            now: new Date(2019, 0, 1, 0, 0),
        });
    });

    describe('When I trace-log something', () => {
        beforeEach(() => {
            this.result = this.logger.trace('*');
        });

        it('result should contain everything', () => {
            expect(this.result).to.be.equal('[ 01/01/2019 00:00:00.000 ][ \u001b[37mtrace  \u001b[0m ]*');
        });
    });

    describe('When I log an object with a function', () => {
        beforeEach(() => {
            this.result = this.logger.trace({ func: () => {} });
        });

        it('result should contain everything', () => {
            expect(this.result).to.be.equal(
                '[ 01/01/2019 00:00:00.000 ][ \u001b[37mtrace  \u001b[0m ]\n{\n    "func": "[Function func]"\n}'
            );
        });
    });

    afterEach(() => {
        this.clock.restore();
        delete process.env.LOGGER_HIDE_LOCATION;
    });
});
