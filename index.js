const errorCodes = require('./errorCodes');

process.on('exit', function(code) {
    const baseMsg = `Exiting program with code: ${code}`;
    const msg = Object.prototype.hasOwnProperty.call(errorCodes, code) ? `${baseMsg} - ${errorCodes[code]}` : baseMsg;
    console.error(msg, code);
});

const config = require('./config');
const server = require('./server')(config.server);

//DEPENDENCY INJECTION
const dependencies = require('./dependencies');

server.start({
    ...dependencies.get(),
});
