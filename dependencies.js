let dependencies;

module.exports = {
    get: function() {
        if (dependencies) {
            return dependencies;
        }

        const awilix = require('awilix');
        const { Lifetime } = awilix;
        const container = awilix.createContainer();
        container.loadModules(['server/services/*.js'], {
            formatName: name => {
                const upperNamespace = 'Service';
                return name + upperNamespace;
            },
            resolverOptions: {
                lifetime: Lifetime.SINGLETON,
            },
        });

        container.loadModules(['server/queries/*.js'], {
            formatName: name => {
                const upperNamespace = 'Query';
                return name + upperNamespace;
            },
        });

        container.loadModules(['server/routes/**/*.js'], {
            formatName: (name, descriptor) => {
                const folderName =
                    descriptor.path
                        .split('routes')[1]
                        .split(require('path').sep)
                        .filter(v => v && !v.includes('.js'))[0] || '';
                const upperNamespace = 'Routes';

                if (folderName) {
                    if (name !== 'index') {
                        return `${folderName}${name[0].toUpperCase()}${name.slice(1)}${upperNamespace}`;
                    } else {
                        return `${folderName}${upperNamespace}`;
                    }
                }

                return `${name}${upperNamespace}`;
            },
            resolverOptions: {
                lifetime: Lifetime.SINGLETON,
            },
        });

        container.loadModules(['server/utils/*.js'], {
            formatName: name => {
                const upperNamespace = 'Util';
                return name + upperNamespace;
            },
        });

        container.loadModules(['server/errors.js'], {
            resolverOptions: {
                lifetime: Lifetime.SINGLETON,
            },
            cwd: __dirname,
        });

        container.loadModules(['logger.js'], {
            resolverOptions: {
                lifetime: Lifetime.SINGLETON,
            },
            cwd: __dirname,
        });

        container.loadModules(['server/connectors/*.js'], {
            resolverOptions: {
                lifetime: Lifetime.SINGLETON,
            },
            cwd: __dirname,
        });

        container.cradle.logger.debug(`Successfully loaded modules:`, container.cradle);

        dependencies = container.cradle;

        return dependencies;
    }
}
