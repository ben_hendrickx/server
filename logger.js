const path = require('path');

const { enabled, level: LEVEL, showLocation } = require('./config').logger;

module.exports = function logger({ stringUtil, dateUtil }) {
    const { colors = {}, logFn } = {};

    const levelColors = {
        SUCCESS: 32,
        ERROR: 31,
        FATAL: 31,
        WARN: 33,
        INFO: 36,
        DEBUG: 35,
        TRACE: 37,
        ...colors,
    };

    function logWhenAllowed(level) {
        return function() {
            return isAllowed(level) && log(level)(`\x1b[${levelColors[level].toString()}m`, ...arguments);
        };
    }

    function makeObjectLoggable(object) {
        if (object === undefined) {
            return '[undefined]';
        }

        if (object === null) {
            return '[null]';
        }

        if (object instanceof Error) {
            return object.message;
        }

        return Object.entries(object).reduce((acc, [key, value]) => {
            if (value instanceof Date) {
                acc[key] = value.toISOString();
                return acc;
            }

            if (typeof value === 'function') {
                acc[key] = `[Function ${value.name || 'anonymous'}]`;
                return acc;
            }

            if (value && typeof value === 'object') {
                acc[key] = makeObjectLoggable(value);
                return acc;
            }

            acc[key] = value;
            return acc;
        }, {});
    }

    function log(level) {
        return function() {
            try {
                throw new Error('');
            } catch (e) {
                const frame = e.stack.split('\n')[3];
                const [fileName, lineNumber, columnNumber] = frame.split(':');

                if (logFn && typeof logFn === 'function') {
                    logFn(...arguments, '\x1b[0m');
                    return;
                }

                const logArgs = [
                    `[ ${dateUtil.dateTimeString()} ][ ${arguments[0]}${stringUtil.fixedLength(
                        level.toLowerCase(),
                        7
                    )}${'\x1b[0m'} ]`,
                    ...[...arguments]
                        .filter((a, index) => index)
                        .map(a => (typeof a === 'object' ? `\n${JSON.stringify(makeObjectLoggable(a), null, 4)}` : a)),
                    showLocation && !process.env.LOGGER_HIDE_LOCATION
                        ? ` ${path.basename(fileName)}:${lineNumber}:${columnNumber}`
                        : '',
                ];

                try {
                    logToFile('error.log', logArgs.join(''));
                    // eslint-disable-next-line no-empty
                } catch (e) {}
                console.log(...logArgs);

                return logArgs.join('');
            }
        };
    }

    function logToFile(filename, string) {
        require('fs').appendFileSync(require('path').join(process.cwd(), '/logs/' + filename), string + '\n');
    }

    function isAllowed(level) {
        const levels = ['TRACE', 'DEBUG', 'INFO', 'SUCCESS', 'WARN', 'ERROR', 'FATAL'];

        return enabled && levels.indexOf(level.toUpperCase()) >= levels.indexOf(LEVEL.toUpperCase());
    }

    const logger = Object.keys(levelColors).reduce((acc, level) => {
        acc[level.toLowerCase()] = logWhenAllowed(level);
        return acc;
    }, {});

    logger.file = logToFile;

    return logger;
};
